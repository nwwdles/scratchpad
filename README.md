# Scratchpad

Personal monorepo for exercises, tutorials, examples, small projects, prototypes, notes, links, old code etc.

- Problems
  - https://leetcode.com/problems/
  - https://www.reddit.com/r/dailyprogrammer/
- Tutorials
  - https://learnto.computer/screencasts/bsd-cat
  - https://learnto.computer/screencasts/bsd-mkdir
- Ideas
  - https://github.com/danistefanovic/build-your-own-x
- Random links
  - [Domain Driven Design на практике](https://habr.com/ru/post/334126/)
