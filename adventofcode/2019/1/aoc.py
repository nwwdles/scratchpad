#!/usr/bin/env python3
#
# https://adventofcode.com/2019/day/1

INPUT_FILE = "input"


def get_fuel_req(mass):
    return (mass // 3) - 2


def get_total_fuel(mass):
    fuel_req = get_fuel_req(mass)
    fuel_fuel_req = get_fuel_req(fuel_req)
    while fuel_fuel_req > 0:
        fuel_req += fuel_fuel_req
        fuel_fuel_req = get_fuel_req(fuel_fuel_req)
    return fuel_req


def read_masses(file):
    modules_mass = []
    with open(file, "r") as f:
        modules_mass = [int(x) for x in f.readlines()]
    return modules_mass


def main():
    assert get_fuel_req(12) == 2
    assert get_fuel_req(14) == 2
    assert get_fuel_req(1969) == 654
    assert get_fuel_req(100756) == 33583

    assert get_total_fuel(14) == 2
    assert get_total_fuel(1969) == 966
    assert get_total_fuel(100756) == 50346

    modules_mass = read_masses(INPUT_FILE)

    total_fuel = 0
    for mass in modules_mass:
        total_fuel += get_fuel_req(mass)
    print("part 1:", total_fuel)

    total_fuel = 0
    for mass in modules_mass:
        total_fuel += get_total_fuel(mass)
    print("part 2:", total_fuel)


if __name__ == "__main__":
    main()
