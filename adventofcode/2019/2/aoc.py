#!/usr/bin/env python3
#
# https://adventofcode.com/2019/day/2

INPUT_FILE = "input"


def read_masses(file):
    modules_mass = []
    with open(file, "r") as f:
        modules_mass = [int(x) for x in f.readlines()]
    return modules_mass


def run_codes(codes, noun=None, verb=None):
    codes = codes.copy()
    if noun != None and verb != None:
        codes[1] = noun
        codes[2] = verb
    for x in range(0, len(codes), 4):
        opcode = codes[x]
        if opcode == 1:
            codes[codes[x + 3]] = codes[codes[x + 1]] + codes[codes[x + 2]]
        elif opcode == 2:
            codes[codes[x + 3]] = codes[codes[x + 1]] * codes[codes[x + 2]]
        elif opcode == 99:
            break
    return codes


def main():
    assert run_codes([1, 0, 0, 0, 99]) == [2, 0, 0, 0, 99]
    assert run_codes([2, 3, 0, 3, 99]) == [2, 3, 0, 6, 99]
    assert run_codes([2, 4, 4, 5, 99, 0]) == [2, 4, 4, 5, 99, 9801]
    assert run_codes([1, 1, 1, 4, 99, 5, 6, 0, 99]) == [30, 1, 1, 4, 2, 5, 6, 0, 99]

    input_codes = []
    with open(INPUT_FILE, "r") as f:
        input_codes = [int(x) for x in f.read().strip().split(",")]

    out = run_codes(input_codes, 2, 12)
    print("part 1 answer:", out[0])

    should_break = False
    for noun in range(1, 99):
        for verb in range(1, 99):
            out = run_codes(input_codes, noun, verb)
            if out[0] == 19690720:
                print("part 2 answer:", 100 * noun + verb)
                should_break = True
                break
        if should_break:
            break


if __name__ == "__main__":
    main()
