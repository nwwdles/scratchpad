#!/usr/bin/env python3
#
# https://adventofcode.com/2019/day/3

INPUT_FILE = "input"


def get_path(wire: list) -> list:
    current_x = 0
    current_y = 0
    points = []
    for step in wire:
        direction = step[0]
        dist = int(step[1:])
        if direction == "R":
            target_x = current_x + dist
            for x in range(current_x, target_x):
                points.append((x, current_y))
            current_x = target_x
        elif direction == "L":
            target_x = current_x - dist
            for x in range(current_x, target_x, -1):
                points.append((x, current_y))
            current_x = target_x
        elif direction == "U":
            target_y = current_y + dist
            for y in range(current_y, target_y):
                points.append((current_x, y))
            current_y = target_y
        elif direction == "D":
            target_y = current_y - dist
            for y in range(current_y, target_y, -1):
                points.append((current_x, y))
            current_y = target_y
        # print(step, "new pos:", current_x, current_y)
    return points


def get_manhattan_dist(point: list):
    return abs(point[0]) + abs(point[1])


def get_distance(wire_a: list, wire_b: list) -> int:
    # if it wasn't on grid, the way to solve it would be to make (x1,y1,x2,y2)
    # segments and check all pairs of segments for intersection, which can be
    # optimized by first filtering out segments that can't possibly intersect
    # by checking for non-overlapping bounding boxes
    path_a = get_path(wire_a)
    path_b = get_path(wire_b)
    intersections = set(path_a).intersection(set(path_b))
    intersections.remove((0, 0))
    distances = [x for x in map(get_manhattan_dist, intersections)]
    return min(distances)


def get_fewest_steps(wire_a: list, wire_b: list) -> int:
    path_a = get_path(wire_a)
    path_b = get_path(wire_b)
    intersections = set(path_a).intersection(set(path_b))
    intersections.remove((0, 0))

    step_sums = []
    for intersection in intersections:
        steps_a = path_a.index(intersection)
        steps_b = path_b.index(intersection)
        step_sums.append(steps_a + steps_b)

    return min(step_sums)


def main():

    assert (
        get_distance(
            ["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"],
            ["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"],
        )
        == 159
    )
    assert (
        get_distance(
            [
                "R98",
                "U47",
                "R26",
                "D63",
                "R33",
                "U87",
                "L62",
                "D20",
                "R33",
                "U53",
                "R51",
            ],
            ["U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"],
        )
        == 135
    )

    with open(INPUT_FILE, "r") as f:
        wire_a = f.readline().strip().split(",")
        wire_b = f.readline().strip().split(",")

    print("part 1 anwer:", get_distance(wire_a, wire_b))

    assert (
        get_fewest_steps(
            ["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"],
            ["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"],
        )
        == 610
    )
    assert (
        get_fewest_steps(
            [
                "R98",
                "U47",
                "R26",
                "D63",
                "R33",
                "U87",
                "L62",
                "D20",
                "R33",
                "U53",
                "R51",
            ],
            ["U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"],
        )
        == 410
    )
    print("part 2 anwer:", get_fewest_steps(wire_a, wire_b))


if __name__ == "__main__":
    main()
