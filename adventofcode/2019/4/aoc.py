#!/usr/bin/env python3
#
# https://adventofcode.com/2019/day/4


def meets_criteria(num: int) -> bool:
    s = str(num)
    has_double = False
    for x in range(1, len(s)):
        if s[x - 1] > s[x]:
            return False
        elif s[x - 1] == s[x]:
            has_double = True
    return has_double


def meets_new_criteria(num: int) -> bool:
    s = str(num)
    has_fitting_double = False
    double_count = 0
    for x in range(1, len(s)):
        if s[x - 1] > s[x]:
            return False
        elif s[x - 1] == s[x]:
            double_count += 1
        else:
            if double_count == 1:
                has_fitting_double = True
            double_count = 0

    if double_count == 1:  # well this is ugly
        has_fitting_double = True

    return has_fitting_double


def count_in_range(a: int, b: int, criteria) -> int:
    # the trivial way
    return sum(map(criteria, range(a, b)))


def main():
    assert meets_criteria(111111) == True
    assert meets_criteria(223450) == False
    assert meets_criteria(123789) == False
    print("part 1 answer:", count_in_range(231832, 767346, meets_criteria))

    assert meets_new_criteria(111111) == False
    assert meets_new_criteria(223450) == False
    assert meets_new_criteria(123789) == False

    assert meets_new_criteria(112233) == True
    assert meets_new_criteria(123444) == False
    assert meets_new_criteria(111122) == True
    print("part 2 answer:", count_in_range(231832, 767346, meets_new_criteria))


if __name__ == "__main__":
    main()
