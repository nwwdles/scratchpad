#!/usr/bin/env python3
#
# https://adventofcode.com/2019/day/6

INPUT_FILE = "input"


def count_indirect_orbits(orbits, body, level=0):
    ret = level
    for x in filter(lambda x: x[0] == body, orbits):
        ret += count_indirect_orbits(orbits, x[1], level + 1)
    return ret


def get_parents(orbits, body):
    parents = []
    for x in filter(lambda x: x[1] == body, orbits):
        parents.append(x[0])
        parents += get_parents(orbits, x[0])
    return parents


def get_common_ancestor(parents_a, parents_b):
    for a in parents_a:
        r = list(filter(lambda x: x == a, parents_b))
        if r:
            return r[0]
    return None


def get_transfers(orbits, src, dest):
    # a better way would be to stop on the first common body appearance
    parents_a = get_parents(orbits, src)
    parents_b = get_parents(orbits, dest)

    # a better way would be to use sets?
    common_ancestor = get_common_ancestor(parents_a, parents_b)
    return parents_a.index(common_ancestor) + parents_b.index(common_ancestor)


def tests():
    l = ["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"]
    l = [x.strip().split(")") for x in l]
    assert count_indirect_orbits(l, "COM") == 42

    l = [
        "COM)B",
        "B)C",
        "C)D",
        "D)E",
        "E)F",
        "B)G",
        "G)H",
        "D)I",
        "E)J",
        "J)K",
        "K)L",
        "K)YOU",
        "I)SAN",
    ]
    l = [x.strip().split(")") for x in l]
    assert get_transfers(l, "YOU", "SAN") == 4


def main():
    tests()
    with open(INPUT_FILE, "r") as f:
        orbits = [x.strip().split(")") for x in f.readlines()]
    print("part 1 answer", count_indirect_orbits(orbits, "COM", 0))
    print("part 2 answer", get_transfers(orbits, "YOU", "SAN"))


if __name__ == "__main__":
    main()
