#!/usr/bin/env python3
#
# https://adventofcode.com/2019/day/7
import logging
import itertools

INPUT_FILE = "input"

POSITIONAL_MODE = 0
IMMEDIATE_MODE = 1

SUM = 1
MULTIPLY = 2
INPUT = 3
OUTPUT = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6
LESS_THAN = 7
EQUALS = 8
HALT = 99


def get_param(codes, x, i):
    mode = (codes[x] // (10 ** (i + 2))) % 10
    pos = x + i + 1
    if mode == IMMEDIATE_MODE:
        val = codes[pos]
    elif mode == POSITIONAL_MODE:
        val = codes[codes[pos]]
    else:
        raise NotImplementedError
    # print("arg pos:", pos, "val:", val, "mode:", mode)
    return val


def run_codes(input_codes, noun=None, verb=None, input_val=None):
    if input_val is not None and type(input_val) != list:
        input_val = [input_val]
    input_index = 0

    codes = input_codes.copy()
    if noun != None and verb != None:
        codes[1] = noun
        codes[2] = verb
    x = 0
    output = None
    mode = POSITIONAL_MODE
    er_code = 1
    # print("\nStart\n")
    while True:
        instruction = str(codes[x])
        # opcode = int(instruction[-2:])
        opcode = codes[x] % 100
        # print(
        #     (5 - len(instruction)) * " " + instruction,
        #     "| opcode:",
        #     opcode,
        #     "pointer:",
        #     x,
        # )

        # print(
        #     "before:",
        #     # codes,
        #     x,
        # )

        if opcode == SUM:
            a = get_param(codes, x, 0)
            b = get_param(codes, x, 1)
            pos = codes[x + 3]
            codes[pos] = a + b
            # print(f"{a}+{b}")
            x += 4
        elif opcode == MULTIPLY:
            a = get_param(codes, x, 0)
            b = get_param(codes, x, 1)
            pos = codes[x + 3]
            codes[pos] = a * b
            # print(f"MUL {a}*{b} = {codes[pos]} @ {pos}")
            x += 4
        elif opcode == INPUT:
            pos = codes[x + 1]
            # print(
            #     f"STORE INPUT: {input_val[input_index]} @ {pos}, input#:{input_index}"
            # )
            codes[pos] = input_val[input_index]
            input_index = min(input_index + 1, len(input_val) - 1)
            x += 2
        elif opcode == OUTPUT:
            a = get_param(codes, x, 0)
            # print("OUTPUT:", a)
            output = a
            x += 2
        elif opcode == JUMP_IF_TRUE:
            a = get_param(codes, x, 0)
            b = get_param(codes, x, 1)
            if a != 0:
                # print(f"jumping {x}->{b}")
                x = b
            else:
                # print(f"not jumping {x}->{b}")
                x += 3
        elif opcode == JUMP_IF_FALSE:
            a = get_param(codes, x, 0)
            b = get_param(codes, x, 1)
            if a == 0:
                # print(f"jumping {x}->{b}")
                x = b
            else:
                # print(f"not jumping {x}->{b}")
                x += 3
        elif opcode == LESS_THAN:
            a = get_param(codes, x, 0)
            b = get_param(codes, x, 1)
            c = get_param(codes, x, 2)
            c = codes[x + 3]
            if a < b:
                # print(f"{a} < {b}")
                codes[c] = 1
            else:
                # print(f"{a} >= {b}")
                codes[c] = 0
            x += 4
        elif opcode == EQUALS:
            a = get_param(codes, x, 0)
            b = get_param(codes, x, 1)
            c = get_param(codes, x, 2)
            c = codes[x + 3]
            if a == b:
                # print(f"{a} == {b}")
                codes[c] = 1
            else:
                # print(f"{a} != {b}")
                codes[c] = 0
            x += 4
        elif opcode == HALT:
            er_code = 0
            break
        else:
            break

        # if c:
        # print(c, end=" ")
        # print(
        #     "after: ",
        #     # codes,
        #     x,
        #     "\n",
        # )

    if er_code != 0:
        s = "Unexpected exit"
        raise Exception(s)
    else:
        s = "Successful exit"

    # print(
    #     s,
    #     # input_codes, codes,
    #     "\noutput:",
    #     output,
    #     "\ninput:",
    #     input_val,
    # )
    return codes, output, er_code


def get_max_thruster_signal(input_codes: list, phase_sequence: list) -> int:
    out = 0
    for phase_setting in phase_sequence:
        codes, out, ercode = run_codes(input_codes, input_val=[phase_setting, out])
    return out


def get_max_thruster_signal_with_feedback(
    input_codes: list, phase_sequence: list
) -> int:
    out = 0
    for phase_setting in phase_sequence:
        codes, out, ercode = run_codes(input_codes, input_val=[phase_setting, out])
        print(out, ercode)
    return out


def tests():
    # day 2

    assert run_codes([1, 0, 0, 0, 99])[0] == [2, 0, 0, 0, 99]
    assert run_codes([1, 1, 1, 4, 99, 5, 6, 0, 99])[0] == [30, 1, 1, 4, 2, 5, 6, 0, 99]
    assert run_codes([2, 3, 0, 3, 99])[0] == [2, 3, 0, 6, 99]
    assert run_codes([2, 4, 4, 5, 99, 0])[0] == [2, 4, 4, 5, 99, 9801]

    # day 5 part 1

    assert run_codes([3, 0, 4, 0, 99], input_val=30)[1] == 30
    assert run_codes([3, 0, 4, 0, 99], input_val=30)[1] == 30
    assert run_codes([1002, 4, 3, 4, 33])[0] == [1002, 4, 3, 4, 99]
    assert run_codes([1101, 100, -1, 4, 0])[0] == [1101, 100, -1, 4, 99]

    # day 5 part 2
    assert run_codes([4, 2, 99])[1] == 99
    assert run_codes([104, 23, 99])[1] == 23

    assert run_codes([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], input_val=8)[1] == 1
    assert run_codes([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], input_val=9)[1] == 0
    assert run_codes([3, 3, 1108, -1, 8, 3, 4, 3, 99], input_val=8)[1] == 1
    assert run_codes([3, 3, 1108, -1, 8, 3, 4, 3, 99], input_val=7)[1] == 0
    assert run_codes([3, 3, 1107, -1, 8, 3, 4, 3, 99], input_val=7)[1] == 1
    assert run_codes([3, 3, 1107, -1, 8, 3, 4, 3, 99], input_val=8)[1] == 0

    tmp = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9]
    assert run_codes(tmp, input_val=1)[1] == 1

    tmp = [3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9]
    assert run_codes(tmp, input_val=0)[1] != 1

    tmp = [
        3,
        21,
        1008,
        21,
        8,
        20,
        1005,
        20,
        22,
        107,
        8,
        21,
        20,
        1006,
        20,
        31,
        1106,
        0,
        36,
        98,
        0,
        0,
        1002,
        21,
        125,
        20,
        4,
        20,
        1105,
        1,
        46,
        104,
        999,
        1105,
        1,
        46,
        1101,
        1000,
        1,
        20,
        4,
        20,
        1105,
        1,
        46,
        98,
        99,
    ]
    assert run_codes(tmp, input_val=7)[1] == 999
    assert run_codes(tmp, input_val=8)[1] == 1000
    assert run_codes(tmp, input_val=9)[1] == 1001


def tests_day7():
    # day 7 pt 1 tests

    codes = [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0]
    sequence = [4, 3, 2, 1, 0]
    assert get_max_thruster_signal(codes, sequence) == 43210

    codes = [
        3,
        23,
        3,
        24,
        1002,
        24,
        10,
        24,
        1002,
        23,
        -1,
        23,
        101,
        5,
        23,
        23,
        1,
        24,
        23,
        23,
        4,
        23,
        99,
        0,
        00,
    ]
    sequence = [0, 1, 2, 3, 4]
    assert get_max_thruster_signal(codes, sequence) == 54321

    codes = [
        3,
        31,
        3,
        32,
        1002,
        32,
        10,
        32,
        1001,
        31,
        -2,
        31,
        1007,
        31,
        0,
        33,
        1002,
        33,
        7,
        33,
        1,
        33,
        31,
        31,
        1,
        32,
        31,
        31,
        4,
        31,
        99,
        0,
        0,
        0,
    ]
    sequence = [1, 0, 4, 3, 2]
    assert get_max_thruster_signal(codes, sequence) == 65210


def main():
    # tests()
    tests_day7()
    with open(INPUT_FILE, "r") as f:
        input_codes = [int(x) for x in f.read().strip().split(",")]

    max_signal = 0
    for i in itertools.permutations([1, 2, 3, 4, 0]):
        out = get_max_thruster_signal(input_codes, i)
        if out > max_signal:
            max_signal = out
    print("Part 1 answer:", max_signal)

    for i in itertools.permutations([5, 6, 7, 8, 9]):
        out = get_max_thruster_signal_with_feedback(input_codes, i)
        if out > max_signal:
            max_signal = out


if __name__ == "__main__":
    main()
