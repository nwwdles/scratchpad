#include <iostream>

template <typename T> class BTree
{
    struct Node {
        Node* left;
        Node* right;
        T value;
        Node(T val) : value(val), left(nullptr), right(nullptr){};
        Node(T val, Node* l, Node* r) : value(val), left(l), right(r){};
    };

    Node* root;

    Node* search(T val, Node* leaf)
    {
        if (leaf != nullptr) {
            if (val == leaf->value) {
                return leaf;
            } else if (val < leaf->value) {
                return search(val, leaf->left);
            } else if (val > leaf->value) {
                return search(val, leaf->right);
            }
        }
        return nullptr;
    };
    void insert(T val, Node* leaf)
    {
        if (val < leaf->value) {
            if (leaf->left != nullptr)
                insert(val, leaf->left);
            else {
                leaf->left = new Node(val);
            }
        } else if (val > leaf->value) {
            if (leaf->right != nullptr)
                insert(val, leaf->right);
            else {
                leaf->right = new Node(val);
            }
        }
    };
    void destroy(Node* leaf)
    {
        if (leaf != nullptr) {
            destroy(leaf->left);
            destroy(leaf->right);
            delete leaf;
        }
    };

    void inorder(Node* leaf)
    {
        if (leaf != nullptr) {
            inorder(leaf->left);
            std::cout << leaf->value << " ";
            inorder(leaf->right);
        }
    }

  public:
    BTree() : root{nullptr} {};
    ~BTree() { clean(); };
    void clean() { destroy(root); };
    void insert(T val)
    {
        if (root == nullptr) {
            root = new Node(val);
        } else {
            insert(val, root);
        }
    };
    T search(T val)
    {
        if (search(val, root)) {
            return true;
        } else
            return false;
    };
    void inorder() { inorder(root); };
};