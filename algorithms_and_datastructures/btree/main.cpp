#include "btree.h"
#include <iostream>

int main(int argc, char const* argv[])
{
    BTree<int> mytree;
    mytree.insert(3);
    mytree.insert(76);
    mytree.insert(5);

    // std::cout << mytree.search(3);
    // std::cout << mytree.search(4);

    mytree.inorder();
    return 0;
}
