#include <iostream>

void printar(int * ar, int n);
void bubblesort(int * ar, int n);

int main(int argc, char const *argv[])
{
    const int n = 10;
    int ar[n] = {3, 2, 6, 7,4,33, 5,1, 6,7};
    printar(ar, n);
    bubblesort(ar, n);
    printar(ar, n);
    return 0;
}

void printar(int * ar, int n) {
    using namespace std;
    for(int i = 0; i < n; i++){
        // cout.width(2);
        cout << ar[i] << " ";
    }
    cout << endl;
}

void bubblesort(int * ar, int n) {
    bool swapped;
    int temp = 0;
    do {
        swapped = false;
        for(int i = 0; i < n - 1; i++) {
            if (ar[i] > ar[i + 1]) {
                temp = ar[i+1];
                ar[i + 1] = ar[i];
                ar[i] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}