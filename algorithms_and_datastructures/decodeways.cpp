// https://leetcode.com/problems/decode-ways/
#include <algorithm>
#include <iostream>
#include <map>
#include <string>

std::map<int, char> dic{
    {1, 'a'},  {2, 'b'},  {3, 'c'},  {4, 'd'},  {5, 'e'},  {6, 'f'},  {7, 'g'},
    {8, 'h'},  {9, 'i'},  {10, 'j'}, {11, 'k'}, {12, 'l'}, {13, 'm'}, {14, 'n'},
    {15, 'o'}, {16, 'p'}, {17, 'q'}, {18, 'r'}, {19, 's'}, {20, 't'}, {21, 'u'},
    {22, 'v'}, {23, 'w'}, {24, 'x'}, {25, 'y'}, {26, 'z'}};

int helper(const std::string data, int k)
{
    if (k == 0) {
        return 1;
    }
    int s = data.size() - k;
    if (data[s] == '0') {
        return 0;
    }
    int res = helper(data, k - 1);
    if (k >= 2 && (std::stoi(data.substr(s, 2)) <= 26)) {
        res += helper(data, k - 2);
    }
    return res;
}
int num_decodes(const std::string s) { return helper(s, s.size()); }

int main(int argc, char const* argv[])
{
    std::string s = "121312";
    std::cout << num_decodes(s) << '\n';
    return 0;
}
