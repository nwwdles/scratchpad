#include <iostream>

int fibonacci(int n)
{
    if (n == 0)
        return 0;
    int num0 = 0;
    int num1 = 1;
    int temp = 0;
    for (int i = 1; i < n; i++) {
        temp = num1;
        num1 = num0 + num1;
        num0 = temp;

        // with no temp variable:
        // num1 = num1 + num0;
        // num0 = num1 - num0;
    }
    return num1;
}

int fibonaccirec(int n)
{
    if (n == 0 || n == 1) {
        return n;
    } else {
        return fibonaccirec(n - 2) + fibonaccirec(n - 1);
    }
}
int main(int argc, char const* argv[])
{
    for (int i = 0; i < 35; i++) {
        std::cout << fibonacci(i) << '\n';
    }
    for (int i = 0; i < 35; i++) {
        std::cout << fibonaccirec(i) << '\n';
    }
    return 0;
}
