#include <iostream>

void fizzbuzz(int n)
{
    for (int i = 1; i < n + 1; i++) {
        std::cout << i << " ";
        if (i % 3 == 0)
            std::cout << "fizz";
        if (i % 5 == 0)
            std::cout << "buzz";
        std::cout << "\n";
    }
}

void fizzbuzzrec(int n)
{
    if (n > 1)
        fizzbuzzrec(n - 1);
    std::cout << n << " ";
    if (n % 3 == 0)
        std::cout << "fizz";
    if (n % 5 == 0)
        std::cout << "buzz";
    std::cout << "\n";
}
int main(int argc, char const* argv[])
{
    fizzbuzz(15);
    fizzbuzzrec(15);
}