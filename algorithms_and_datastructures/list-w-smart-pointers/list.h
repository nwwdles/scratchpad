// a linked list with smart pointers
#include <iostream>
#include <memory>
template <typename T>
class List
{
    struct Node {
        std::unique_ptr<Node> next;
        T value;
        Node(T val) : value{val}, next{nullptr} {};
    };

    std::unique_ptr<Node> root;
    int size;

  public:
    List() : size(0), root(nullptr){};
    ~List() { clean(); };
    void push(T val)
    {
        auto temp{std::make_unique<Node>(val)};
        if (root != nullptr) {
            temp->next = std::move(root);
        }
        root = std::move(temp);
        size++;
    };
    void clean()
    {
        while (root) {
            root = std::move(root->next);
        }
    }
    void pop()
    {
        if (root)
            root = std::move(root->next);
    }
    void display()
    {
        Node* ptr = root.get();
        while (ptr) {
            std::cout << ptr->value << "\n";
            ptr = ptr->next.get();
        }
    }
};