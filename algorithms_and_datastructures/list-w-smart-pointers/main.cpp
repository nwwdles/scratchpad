#include "list.h"
#include <iostream>
#include <string>

void ptrtest()
{
    auto listptr = std::make_unique<List<int>>();
    listptr->push(3);
    // listptr->display();
}
int main(int argc, char const* argv[])
{
    List<int> mylist;

    mylist.push(3);
    mylist.push(4);
    mylist.push(5);
    // mylist.display();
    mylist.pop();
    // mylist.display();

    // for (int j = 0; j < 100; j++) {
    for (int i = 0; i < 10000000; i++) {
        mylist.push(i);
        ptrtest();
    }
    for (int i = 0; i < 10000000; i++) {
        mylist.pop();
    }
    // }
    mylist.display();
    // std::cin.get();
    return 0;
}
