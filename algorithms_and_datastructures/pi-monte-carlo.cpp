#include <ctime>
#include <iostream>
#include <random>

int main(int argc, char const* argv[])
{
    srand(time(nullptr));

    double x = 0;
    double y = 0;
    double len = 0;
    long hits = 0;
    long count = 1000000000;
    for (long i = 0; i < count; i++) {
        x = double(rand()) / RAND_MAX;
        y = double(rand()) / RAND_MAX;
        len = (x * x + y * y);
        if (len <= 1) {
            hits++;
        }
    }
    std::cout.precision(20);
    std::cout << (long double)(4 * hits) / count << "\n";
    return 0;
}
