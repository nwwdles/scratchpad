#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <string>

int main(int argc, char const* argv[])
{
    using std::set;
    using std::string;
    const int N = 6;
    string s[] = {"hi", "ho", "he", "ha", "hu", "humu"};
    set<string> myset(s, s + N);
    set<string> anotherset{"hello", "how are you", "hi"};
    std::ostream_iterator<string, char> oi(std::cout, " ");
    std::copy(myset.begin(), myset.end(), oi);
    std::cout << "\n";
    std::copy(myset.rbegin(), myset.rend(), oi);
    std::cout << "\n";

    set<string> fullset;
    std::set_union(myset.begin(), myset.end(), anotherset.begin(),
                   anotherset.end(),
                   std::insert_iterator<set<string>>(fullset, fullset.begin()));
    std::copy(fullset.begin(), fullset.end(), oi);
    std::cout << "\n";

    return 0;
}
