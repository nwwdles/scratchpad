#include <iostream>
#include <string>
#include <vector>

template <typename T> struct TreeNode {
    T data;
    std::vector<TreeNode> children;

    std::vector<TreeNode> &operator[](int idx) { return children[idx]; };
    // TreeNode(std::initializer_list<TreeNode> list)
    // {
    //     data = list[0];
    //     for (auto var : list[1]) {
    //         children.push_back(var);
    //     }
    // };

    void printSelf() { printNode(*this, 0); }
    void printNode(TreeNode &n, int lvl)
    {
        std::cout << std::string(" ", lvl) << n.data << "\n";
        for (int i = 0; i < n.children.size(); ++i) {
            printNode(n.children[i], i + 1);
        }
    }
};

template <typename T> struct Tree {
    std::vector<TreeNode<T>> root;

    Tree(){};
    Tree(std::initializer_list<TreeNode<T>> list)
    {
        for (auto var : list) {
            root.push_back(var);
        }
    };

    // void push_back(T t) { root.push_back(t); }
    TreeNode<T> &operator[](int idx) { return root[idx]; };
    // int size() { return root.size(); }

    void printSelf()
    {
        for (int i = 0; i < root.size(); i++) {
            root[i].printSelf();
        }
    }
};
