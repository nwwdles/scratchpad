#include "tree.h"
#include <iostream>
#include <string>
#include <vector>

int main(int argc, char const *argv[])
{
    Tree<int> t;

    t.root.push_back({0, {}});
    t.root.push_back({1, {}});
    t.root.push_back({2, {}});
    t[2].children.push_back({44, {}});
    t.root.push_back({40, {}});
    t[3].children.push_back({44, {}});
    t[3].children.push_back({48, {}});

    t.printSelf();
    Tree<int> t2{{1, {}}, {2, {{3, {}}}}, {3, {}}};
    t2.printSelf();

    return 0;
}
