#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <vector>
// template <typename T> T sqrt(T a) { return pow(a, 0.5); }
double calc(double a, double b) { return 10.0 * ((a + b) / 2 + a * cos(b)); }
int main(int argc, char const* argv[])
{
    /* code */
    const int LIM = 5;
    double arr1[LIM] = {36, 39, 42, 45, 48};
    double arr2[LIM] = {37, 34, 42, 45, 48};
    std::vector<double> gr1(arr1, arr1 + LIM);
    std::vector<double> gr2(arr2, arr2 + LIM);
    std::vector<double> gr3(LIM);

    std::transform(gr1.begin(), gr1.end(), gr2.begin(), gr3.begin(), calc);

    std::ostream_iterator<double, char> out(std::cout, " ");
    std::copy(gr3.begin(), gr3.end(), out); //, sqrt);

    return 0;
}
