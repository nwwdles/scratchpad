# Anime Boxes Downloader

A script to download all pictures and videos from [Anime Boxes](http://www.animebox.es/) .abbj backup file.

- Single-threaded.
- Remembers which files were downloaded.
- Stores tags in a sidecar .xmp file.
  - Bundled XMP template keeps tags in the description field instead of the tags field (digikam becomes too laggy with thousands of tags).

```txt
usage: animeboxes_downloader.py [-h] [-l] [-i] [-r] filename

positional arguments:
  filename      your .abbj file

optional arguments:
  -h, --help    show this help message and exit
  -l, --log     log downloaded files
  -i, --ignore  ignore downloads log and redownload files
  -r, --reset   reset downloads log and redownload files
```
