#!/usr/bin/env python3
import html
import requests
import os
import time
import json
from pathlib import Path

import sqlite3
from argparse import ArgumentParser

RETRIES_MAX = 5
TIMEOUT = 10


def download_pic(picUrl, filename):
    # sleeptime = 1
    part_suffix = ".tmp"
    if not os.path.isfile(filename):
        retries = 0
        while retries < RETRIES_MAX:
            try:
                res = requests.get(
                    picUrl,
                    # stream=True,
                    timeout=TIMEOUT,
                )

                res.raise_for_status()
                with open(filename + part_suffix, "wb") as imageFile:
                    for chunk in res.iter_content(chunk_size=1024):
                        imageFile.write(chunk)
                os.rename(filename + part_suffix, filename)
                # time.sleep(sleeptime)
                return False
            except requests.exceptions.HTTPError:
                # print("skipping ", picUrl)
                # return True
                pass  # retry
        else:
            try:
                os.remove(filename + part_suffix)
            except OSError:
                pass
            return True


def expand_rating(rating):
    if rating == "e":
        rating = "explicit"
    elif rating == "q":
        rating = "questionable"
    elif rating == "s":
        rating = "safe"
    return rating


def write_xmp(filename, fav, template):
    tags = fav["tags"]
    rating = expand_rating(fav["rating"])
    escapedtags = html.escape(tags)
    with open(filename, "w") as f:
        f.write(template.format(rating=rating, tags=escapedtags))


def is_downloaded(c: sqlite3.Cursor, md5):
    t = (md5,)
    c.execute("SELECT md5 FROM downloads WHERE md5=?", t)
    results = c.fetchall()
    return len(results) >= 1


def log_download(c: sqlite3.Cursor, md5):
    if not is_downloaded(c, md5):
        c.execute(f"INSERT INTO downloads VALUES ('{md5}')")
        # todo: there's a better way to prevent duplicates
        # https://stackoverflow.com/questions/29312882/sqlite-preventing-duplicate-rows


def get_filename(fav: list):
    return fav["md5"] + "." + fav["file"]["ext"]


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("filename", metavar="filename", help="your .abbj file")
    parser.add_argument(
        "-l",
        "--log",
        dest="log_downloads",
        action="store_true",
        help="log downloaded files",
    )
    parser.add_argument(
        "-i",
        "--ignore",
        dest="ignore_log",
        action="store_true",
        help="ignore downloads log and redownload files",
    )
    parser.add_argument(
        "-r",
        "--reset",
        dest="reset_log",
        action="store_true",
        help="reset downloads log and redownload files",
    )
    args = parser.parse_args()
    favsname = args.filename

    # open DB, create table if new DB
    db = "finished.db"
    new_db = not os.path.isfile(db)
    conn = sqlite3.connect(db)
    c = conn.cursor()
    if new_db:
        c.execute("""CREATE TABLE downloads (md5,text)""")
        conn.commit()

    if args.reset_log:
        c.execute("DELETE FROM downloads")
        conn.commit()
        print("Download log was reset.")

    # read favs from file
    with open(favsname, "r") as f:
        ab_backup = json.load(f)
        favs = ab_backup["favorites"]

    # read template
    with open("xmp.txt", "r") as f:
        template = f.read()  # .replace('\n', '')

    # prepare download folder
    target_folder = "downloads"
    if not os.path.isdir(target_folder):
        os.mkdir(target_folder)

    # downloading
    total = len(favs)
    for i in range(total):
        fav = favs[i]
        url = fav["file"]["url"]
        md5 = fav["md5"]
        filename = os.path.join(target_folder, get_filename(fav))
        xmpfilename = filename + ".xmp"

        # download file if needed
        if not is_downloaded(c, md5) or args.ignore_log:
            if not download_pic(url, filename):
                if args.log_downloads:
                    log_download(c, md5)
                print(i + 1, "/", total, "\t", url)
                conn.commit()
            else:
                pass
        else:
            print(i + 1, "/", total, "\t", url, " already downloaded")

        # if file is present & no xmp, write xmp
        if os.path.isfile(filename) and not os.path.isfile(filename + ".xmp"):
            write_xmp(xmpfilename, fav, template)

    conn.commit()
    conn.close()

    print("Done.")
