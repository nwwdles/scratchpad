#include <mpd/client.h>
#include <mpd/entity.h>
#include <mpd/message.h>
#include <mpd/search.h>
#include <mpd/status.h>
#include <mpd/tag.h>

#include <cassert>
#include <iostream>

static int handle_error(struct mpd_connection *c)
{
    assert(mpd_connection_get_error(c) != MPD_ERROR_SUCCESS);
    std::cerr << mpd_connection_get_error_message(c) << '\n';
    mpd_connection_free(c);
    return EXIT_FAILURE;
}

int main(int argc, char const *argv[])
{
    auto conn = mpd_connection_new(NULL, 0, 30000);

    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
        return handle_error(conn);

    for (int i = 0; i < 3; i++) {
        std::cout << "version[" << i
                  << "]: " << mpd_connection_get_server_version(conn)[i]
                  << '\n';
    }

    return 0;
}
