// main.cpp

#include <mpd/client.h>
#include <mpd/entity.h>
#include <mpd/message.h>
#include <mpd/search.h>
#include <mpd/status.h>
#include <mpd/tag.h>

#include <cassert>
#include <iostream>

static int handle_error(struct mpd_connection *c)
{
    assert(mpd_connection_get_error(c) != MPD_ERROR_SUCCESS);
    std::cerr << mpd_connection_get_error_message(c) << '\n';
    mpd_connection_free(c);
    return EXIT_FAILURE;
}

int list_contents(mpd_connection *c, const char *query)
{
    if (!mpd_send_list_meta(c, query))
        return handle_error(c);

    struct mpd_entity *entity;
    while ((entity = mpd_recv_entity(c)) != NULL) {
        const struct mpd_song *song;
        const struct mpd_directory *dir;
        const struct mpd_playlist *pl;

        switch (mpd_entity_get_type(entity)) {
        case MPD_ENTITY_TYPE_UNKNOWN:
            break;

        case MPD_ENTITY_TYPE_SONG:
            song = mpd_entity_get_song(entity);
            std::cout << "uri: " << mpd_song_get_uri(song) << '\n';
            break;

        case MPD_ENTITY_TYPE_DIRECTORY:
            dir = mpd_entity_get_directory(entity);
            std::cout << "directory: " << mpd_directory_get_path(dir) << '\n';
            break;

        case MPD_ENTITY_TYPE_PLAYLIST:
            pl = mpd_entity_get_playlist(entity);
            std::cout << "playlist: " << mpd_playlist_get_path(pl) << '\n';
            break;
        }
        mpd_entity_free(entity);
    }

    if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS ||
        !mpd_response_finish(c)) {
        return handle_error(c);
    }

    return 0;
}

int main(int argc, char const *argv[])
{
    auto conn = mpd_connection_new(NULL, 0, 30000);

    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
        return handle_error(conn);

    int status = list_contents(conn, argv[1]);
    if (status) {
        // exit on any non-zero status
        return status;
    }

    return 0;
}
