// main.cpp

#include <mpd/client.h>
#include <mpd/entity.h>
#include <mpd/message.h>
#include <mpd/search.h>
#include <mpd/status.h>
#include <mpd/tag.h>

#include <cassert>
#include <iostream>
#include <string>

static int handle_error(struct mpd_connection *c)
{
    assert(mpd_connection_get_error(c) != MPD_ERROR_SUCCESS);
    std::cerr << mpd_connection_get_error_message(c) << '\n';
    mpd_connection_free(c);
    return EXIT_FAILURE;
}

int list_contents(mpd_connection *c, const char *query)
{
    if (!mpd_send_list_meta(c, query))
        return handle_error(c);

    struct mpd_entity *entity;
    while ((entity = mpd_recv_entity(c)) != NULL) {
        const struct mpd_song *song;
        const struct mpd_directory *dir;
        const struct mpd_playlist *pl;

        switch (mpd_entity_get_type(entity)) {
        case MPD_ENTITY_TYPE_UNKNOWN:
            break;

        case MPD_ENTITY_TYPE_SONG:
            song = mpd_entity_get_song(entity);
            std::cout << "uri: " << mpd_song_get_uri(song) << '\n';
            break;

        case MPD_ENTITY_TYPE_DIRECTORY:
            dir = mpd_entity_get_directory(entity);
            std::cout << "directory: " << mpd_directory_get_path(dir) << '\n';
            break;

        case MPD_ENTITY_TYPE_PLAYLIST:
            pl = mpd_entity_get_playlist(entity);
            std::cout << "playlist: " << mpd_playlist_get_path(pl) << '\n';
            break;
        }
        mpd_entity_free(entity);
    }

    if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS ||
        !mpd_response_finish(c)) {
        return handle_error(c);
    }

    return 0;
}

int get_state(mpd_connection *c, mpd_state &ret_state)
{
    auto status = mpd_run_status(c);

    if (status == NULL)
        return handle_error(c);

    ret_state = mpd_status_get_state(status);
    mpd_status_free(status);

    if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS)
        return handle_error(c);

    return 0;
}

int get_song_in_dir(mpd_connection *c, const char *query, std::string &ret_song)
{
    if (!mpd_send_list_meta(c, query))
        return handle_error(c);

    struct mpd_entity *entity;
    while ((entity = mpd_recv_entity(c)) != NULL) {
        const struct mpd_song *song;
        if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG) {
            song = mpd_entity_get_song(entity);
            std::cout << "uri: " << mpd_song_get_uri(song) << '\n';
            ret_song = mpd_song_get_uri(song);
            mpd_entity_free(entity);
            break;
        }
        mpd_entity_free(entity);
    }

    if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS ||
        !mpd_response_finish(c)) {
        return handle_error(c);
    }

    return 0;
}

int main(int argc, char const *argv[])
{
    auto conn = mpd_connection_new(NULL, 0, 30000);

    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
        return handle_error(conn);

    int status = list_contents(conn, argv[1]);
    if (status) {
        return status;
    }

    // get first song in a folder

    std::string song;
    status = get_song_in_dir(conn, argv[1], song);
    if (status) {
        return status;
    }
    std::cout << "Selected song: " << song << '\n';

    // play/pause
    mpd_state state;
    status = get_state(conn, state);
    if (status) {
        return status;
    }
    if (state == MPD_STATE_PLAY || state == MPD_STATE_PAUSE) {
        mpd_run_toggle_pause(conn);
    } else {
        mpd_run_clear(conn);
        mpd_run_add(conn, song.c_str());
        mpd_run_play(conn);
    }

    return 0;
}
