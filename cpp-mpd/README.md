# Let's make an MPD client

https://www.musicpd.org/libs/libmpdclient/
https://github.com/mkilgore/nclyr/blob/master/nclyr/players/mpd/mpd.c (gpl3)

It's not really a tutorial, it's more of a log of actions I took to make a
simple MPD client.

This is heavily based on the official
[libmpdclient/src/example.c](https://github.com/cmende/libmpdclient/blob/master/src/example.c).
I basically take the long example `main()` apart and present it in
smaller chunks.

I also don't know C, so I'll be using C++.
I also don't really know C++ so it'll be some ugly code with some original
C parts left in ;). Since the code is heavily based on the official
example and is still readable, I don't feel bad about it.

If anyone actually reads this, any critique (code or english) is welcome!
(fix my bugs pls)

## Table of Contents

- [Let's make an MPD client](#lets-make-an-mpd-client)
  - [Table of Contents](#table-of-contents)
  - [0. Preparation](#0-preparation)
  - [1. Hello, MPD!](#1-hello-mpd)
  - [2. Listing all files](#2-listing-all-files)
  - [3. Playing/pausing a song](#3-playingpausing-a-song)
  - [4. Make a client class](#4-make-a-client-class)
  - [?. Idle mode](#idle-mode)
  - [?. UI](#ui)

## 0. Preparation

To compile it, I'll be using a simple makefile.
Create a file named `Makefile` with the next contents:

```makefile
CXX=g++
CXXFLAGS=-lmpdclient

client: main.cpp
    $(CXX) -o client main.cpp $(CXXFLAGS)
```

It will just run `g++ -o client main.cpp -lmpdclient` when we run `make`.

## 1. Hello, MPD!

Let's just connect to the server and print its version.

> #TODO: remove unused headers here?

```cpp
// main.cpp

#include <mpd/client.h>
#include <mpd/entity.h>
#include <mpd/message.h>
#include <mpd/search.h>
#include <mpd/status.h>
#include <mpd/tag.h>

#include <cassert>
#include <iostream>

static int handle_error(struct mpd_connection *c)
{
    assert(mpd_connection_get_error(c) != MPD_ERROR_SUCCESS);
    std::cerr << mpd_connection_get_error_message(c) << '\n';
    mpd_connection_free(c);
    return EXIT_FAILURE;
}

int main(int argc, char const *argv[])
{
    auto conn = mpd_connection_new(NULL, 0, 30000);

    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
        return handle_error(conn);

    for (int i = 0; i < 3; i++) {
        std::cout << "version[" << i
                  << "]: " << mpd_connection_get_server_version(conn)[i]
                  << '\n';
    }

    return 0;
}
```

Let's run it. Three lines should be printed:

```sh
$ make && ./client
version[0]: 0
version[1]: 21
version[2]: 11
```

## 2. Listing all files

Let's list all library files.

```cpp
int list_contents(mpd_connection *c, const char *query)
{
    if (!mpd_send_list_meta(c, query))
        return handle_error(c);

    struct mpd_entity *entity;
    while ((entity = mpd_recv_entity(c)) != NULL) {
        const struct mpd_song *song;
        const struct mpd_directory *dir;
        const struct mpd_playlist *pl;

        switch (mpd_entity_get_type(entity)) {
        case MPD_ENTITY_TYPE_UNKNOWN:
            break;

        case MPD_ENTITY_TYPE_SONG:
            song = mpd_entity_get_song(entity);
            std::cout << "uri:" << mpd_song_get_uri(song) << '\n';
            break;

        case MPD_ENTITY_TYPE_DIRECTORY:
            dir = mpd_entity_get_directory(entity);
            std::cout << "directory:" << mpd_directory_get_path(dir) << '\n';
            break;

        case MPD_ENTITY_TYPE_PLAYLIST:
            pl = mpd_entity_get_playlist(entity);
            std::cout << "playlist:" << mpd_playlist_get_path(pl) << '\n';
            break;
        }
        mpd_entity_free(entity);
    }

    if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS ||
        !mpd_response_finish(c)) {
        return handle_error(c);
    }

    return 0;
}

int main(int argc, char const *argv[])
{
    auto conn = mpd_connection_new(NULL, 0, 30000);

    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
            return handle_error(conn);

    int status = list_contents(conn, argv[1]);
    if (status) {
        // exit on any non-zero status
        return status;
    }

    return 0;
}
```

After running `./client`, the contents of the library root are shown.
The query is passed as the first argument (`argv[1]`).

```txt
$ make && ./client KS
g++ -o client main.cpp -lmpdclient
uri:KS/1-01 Lullaby of Open Eyes.mp3
uri:KS/1-02 Cold Iron.mp3
...
```

To recursively list folder contents, change `mpd_send_list_meta`
to `mpd_send_list_all_meta`.
The documentation seems to be the most easily accessible by reading
the library headers (located in `/usr/include/mpd` in my case).
A text editor where you can click "Go to declaration" helps.

## 3. Playing/pausing a song

Let's play the first song in a folder. If a song is already playing,
we'll pause it.

These are some of the functions we'll use:

- `mpd_run_clear(conn)` to clear current queue.
- `mpd_run_add(conn, uri)` to add a song to the queue.
- `mpd_run_toggle_pause(conn)` to play/pause a song.
- `mpd_run_play(conn)` to start playing a song. It's required because
  `mpd_run_toggle_pause` won't work in `MPD_STATE_STOP` state, which happens
  after playlist is cleared.
- `mpd_send_list_meta(conn, uri)` to get the folder contents.

<!-- `mpd_send_list_files(connection, uri)` outputs the files in the folder. There are differences from `mpd_send_list_meta`:

- Non-song files are included as songs too.
- Sorting order differs from `mpd_send_list_meta`.
- Song uri's that are retrieved with `mpd_song_get_uri(song)` contain just filenames. -->

By the way, it's convenient to have a window with another MPD client (such as
ncmpcpp) open to see what's actually happening in a queue.

Also, if you have just changed a structure of your music folder and don't see
the changes, you can use `mpd_run_rescan(conn, "")` to rescan the library.

```cpp
int get_state(mpd_connection *c, mpd_state &ret_state)
{
    auto status = mpd_run_status(c);

    if (status == NULL)
        return handle_error(c);

    ret_state = mpd_status_get_state(status);
    mpd_status_free(status);

    if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS)
        return handle_error(c);

    return 0;
}

int get_song_in_dir(mpd_connection *c, const char *query, std::string &ret_song)
{
    if (!mpd_send_list_meta(c, query))
        return handle_error(c);

    struct mpd_entity *entity;
    while ((entity = mpd_recv_entity(c)) != NULL) {
        const struct mpd_song *song;
        if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG) {
            song = mpd_entity_get_song(entity);
            std::cout << "uri: " << mpd_song_get_uri(song) << '\n';
            ret_song = mpd_song_get_uri(song);
            mpd_entity_free(entity);
            break;
        }
        mpd_entity_free(entity);
    }

    if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS ||
        !mpd_response_finish(c)) {
        return handle_error(c);
    }

    return 0;
}

int main(int argc, char const *argv[])
{
    auto conn = mpd_connection_new(NULL, 0, 30000);

    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
        return handle_error(conn);

    int status = list_contents(conn, argv[1]);
    if (status) {
        return status;
    }

    // get first song in a folder

    std::string song;
    status = get_song_in_dir(conn, argv[1], song);
    if (status) {
        return status;
    }
    std::cout << "Selected song: " << song << '\n';

    // play/pause
    mpd_state state;
    status = get_state(conn, state);
    if (status) {
        return status;
    }
    if (state == MPD_STATE_PLAY || state == MPD_STATE_PAUSE) {
        mpd_run_toggle_pause(conn);
    } else {
        mpd_run_clear(conn);
        mpd_run_add(conn, song.c_str());
        mpd_run_play(conn);
    }

    return 0;
}
```

## 4. Make a client class

While it's not too late, let's (try to) organize some code.

Updated makefile:

```makefile
CXX=g++
CXXFLAGS=-lmpdclient

client: main.cpp
    $(CXX) -o client main.cpp $(CXXFLAGS)
```

Updated `main.cpp`:

New `mpd.h`:

<details>
  <summary>Implementation file</summary>
  <p>

```cpp
test
```

</p></details>

## ?. Idle mode

## ?. UI

Let's add an ncurses UI.
