#include "mpd.h"
#include "treeviewer.h"
#include <ncurses.h>

using mpd::MpdClient;

class Application
{
    mpd::MpdClient client;

  public:
    Application(){};
    int run()
    {
        auto status = client.connect();
        if (status) {
            return status;
        }
        // while (true) {
        //     client.idle();
        // }

        std::vector<std::string> songs;
        client.get_song_uris(songs, "");
        TreeViewer tree(songs);
        tree.run();
        // status = client.list_contents(argv[1]);
        // if (status) {
        //     return status;
        // }

        // status = client.current_song_info();
        // if (status) {
        //     return status;
        // }
        return 0;
    }
};

int main(int argc, char const *argv[])
{
    Application app;
    return app.run();
}
