#include <mpd/client.h>
#include <mpd/entity.h>
#include <mpd/message.h>
#include <mpd/search.h>
#include <mpd/status.h>
#include <mpd/tag.h>

#include <cassert>
#include <iostream>
#include <vector>

namespace mpd
{

static void print_tag(const struct mpd_song *song, enum mpd_tag_type type,
                      const char *label)
{
    unsigned i = 0;
    const char *value;

    while ((value = mpd_song_get_tag(song, type, i++)) != NULL)
        printf("%s: %s\n", label, value);
}

void print_entity(struct mpd_entity *entity)
{
    const struct mpd_song *song;
    const struct mpd_directory *dir;
    const struct mpd_playlist *pl;

    switch (mpd_entity_get_type(entity)) {
    case MPD_ENTITY_TYPE_UNKNOWN:
        break;

    case MPD_ENTITY_TYPE_SONG:
        song = mpd_entity_get_song(entity);
        std::cout << "uri:" << mpd_song_get_uri(song) << '\n';
        print_tag(song, MPD_TAG_ARTIST, "artist");
        print_tag(song, MPD_TAG_ALBUM, "album");
        print_tag(song, MPD_TAG_TITLE, "title");
        print_tag(song, MPD_TAG_TRACK, "track");
        break;

    case MPD_ENTITY_TYPE_DIRECTORY:
        dir = mpd_entity_get_directory(entity);
        std::cout << "directory:" << mpd_directory_get_path(dir) << '\n';
        break;

    case MPD_ENTITY_TYPE_PLAYLIST:
        pl = mpd_entity_get_playlist(entity);
        std::cout << "playlist:" << mpd_playlist_get_path(pl) << '\n';
        break;
    }
}

class MpdClient
{
    mpd_connection *conn;
    int handle_error()
    {
        assert(mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS);
        std::cerr << mpd_connection_get_error_message(conn) << '\n';
        mpd_connection_free(conn);
        return EXIT_FAILURE;
    }

  public:
    MpdClient(){};

    int connect(const char *host = NULL, unsigned int port = 0,
                unsigned int timeout_ms = 30000)
    {
        conn = mpd_connection_new(host, port, timeout_ms);

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
            return handle_error();

        return 0;
    };

    int list_contents(const char *query)
    {
        if (!mpd_send_list_all_meta(conn, query))
            return handle_error();

        struct mpd_entity *entity;
        while ((entity = mpd_recv_entity(conn)) != NULL) {
            print_entity(entity);
            mpd_entity_free(entity);
        }

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS ||
            !mpd_response_finish(conn)) {
            return handle_error();
        }

        return 0;
    }
    int get_song_uris(std::vector<std::string> &ret_song, const char *query)
    {
        if (!mpd_send_list_all_meta(conn, query))
            return handle_error();

        struct mpd_entity *entity;
        while ((entity = mpd_recv_entity(conn)) != NULL) {
            const struct mpd_song *song;
            if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG) {
                song = mpd_entity_get_song(entity);
                auto uri = mpd_song_get_uri(song);
                ret_song.push_back(uri);
            }
            mpd_entity_free(entity);
        }

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS ||
            !mpd_response_finish(conn)) {
            return handle_error();
        }

        return 0;
    }
    int get_state(mpd_connection *c, mpd_state &ret_state)
    {
        auto status = mpd_run_status(c);

        if (status == NULL)
            return handle_error();

        ret_state = mpd_status_get_state(status);
        mpd_status_free(status);

        if (mpd_connection_get_error(c) != MPD_ERROR_SUCCESS)
            return handle_error();

        return 0;
    }

    int print_current_status()
    {
        struct mpd_status *status;
        struct mpd_song *song;
        const struct mpd_audio_format *audio_format;

        mpd_command_list_begin(conn, true);
        mpd_send_status(conn);
        mpd_send_current_song(conn);
        mpd_command_list_end(conn);

        status = mpd_recv_status(conn);
        if (status == NULL)
            return handle_error();

        printf("volume: %i\n", mpd_status_get_volume(status));
        printf("repeat: %i\n", mpd_status_get_repeat(status));
        printf("queue version: %u\n", mpd_status_get_queue_version(status));
        printf("queue length: %i\n", mpd_status_get_queue_length(status));
        if (mpd_status_get_error(status) != NULL)
            printf("error: %s\n", mpd_status_get_error(status));

        if (mpd_status_get_state(status) == MPD_STATE_PLAY ||
            mpd_status_get_state(status) == MPD_STATE_PAUSE) {
            printf("song: %i\n", mpd_status_get_song_pos(status));
            printf("elaspedTime: %i\n", mpd_status_get_elapsed_time(status));
            printf("elasped_ms: %u\n", mpd_status_get_elapsed_ms(status));
            printf("totalTime: %i\n", mpd_status_get_total_time(status));
            printf("bitRate: %i\n", mpd_status_get_kbit_rate(status));
        }

        audio_format = mpd_status_get_audio_format(status);
        if (audio_format != NULL) {
            printf("sampleRate: %i\n", audio_format->sample_rate);
            printf("bits: %i\n", audio_format->bits);
            printf("channels: %i\n", audio_format->channels);
        }

        mpd_status_free(status);

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
            return handle_error();

        mpd_response_next(conn);

        while ((song = mpd_recv_song(conn)) != NULL) {
            printf("uri: %s\n", mpd_song_get_uri(song));
            print_tag(song, MPD_TAG_ARTIST, "artist");
            print_tag(song, MPD_TAG_ALBUM, "album");
            print_tag(song, MPD_TAG_TITLE, "title");
            print_tag(song, MPD_TAG_TRACK, "track");
            print_tag(song, MPD_TAG_NAME, "name");
            print_tag(song, MPD_TAG_DATE, "date");

            if (mpd_song_get_duration(song) > 0) {
                printf("time: %u\n", mpd_song_get_duration(song));
            }

            printf("pos: %u\n", mpd_song_get_pos(song));

            mpd_song_free(song);
        }

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS ||
            !mpd_response_finish(conn))
            return handle_error();

        return 0;
    }

    int subscribe(const char *channel)
    {
        /* subscribe to a channel and print all messages */

        if (!mpd_run_subscribe(conn, channel))
            return handle_error();

        while (mpd_run_idle_mask(conn, MPD_IDLE_MESSAGE) != 0) {
            if (!mpd_send_read_messages(conn))
                return handle_error();

            struct mpd_message *msg;
            while ((msg = mpd_recv_message(conn)) != NULL) {
                printf("%s\n", mpd_message_get_text(msg));
                mpd_message_free(msg);
            }

            if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS ||
                !mpd_response_finish(conn))
                return handle_error();
        }

        return handle_error();
    }

    int idle()
    {
        enum mpd_idle idle = mpd_run_idle(conn);
        if (idle == 0 && mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
            return handle_error();

        for (unsigned j = 0;; ++j) {
            enum mpd_idle i = static_cast<mpd_idle>(1 << j);
            const char *name = mpd_idle_name(i);

            if (name == NULL)
                break;

            if (idle & i)
                printf("%s\n", name);
        }

        return 0;
    }

    int get_channels(std::vector<std::string> &ret_playlists)
    {
        if (!mpd_send_channels(conn))
            return handle_error();

        struct mpd_pair *pair;
        while ((pair = mpd_recv_channel_pair(conn)) != NULL) {
            ret_playlists.push_back(pair->value);
            mpd_return_pair(conn, pair);
        }

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS ||
            !mpd_response_finish(conn))
            return handle_error();

        return 0;
    }
    int get_playlists(std::vector<std::string> &ret_playlists)
    {
        if (!mpd_send_list_playlists(conn))
            return handle_error();

        struct mpd_playlist *playlist;
        while ((playlist = mpd_recv_playlist(conn)) != NULL) {
            ret_playlists.push_back(mpd_playlist_get_path(playlist));
            mpd_playlist_free(playlist);
        }

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS ||
            !mpd_response_finish(conn))
            return handle_error();

        return 0;
    }
    int get_artists(std::vector<std::string> &ret_artists)
    {
        struct mpd_pair *pair;

        if (!mpd_search_db_tags(conn, MPD_TAG_ARTIST) ||
            !mpd_search_commit(conn))
            return handle_error();

        while ((pair = mpd_recv_pair_tag(conn, MPD_TAG_ARTIST)) != NULL) {
            ret_artists.push_back(pair->value);
            mpd_return_pair(conn, pair);
        }

        if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS ||
            !mpd_response_finish(conn))
            return handle_error();

        return 0;
    }
    int list_channels() // TODO: make a general 'list' function
    {
        std::vector<std::string> artists;
        int status = get_channels(artists);
        if (status)
            return status;
        for (auto artist : artists) {
            std::cout << artist << '\n';
        }
        return 0;
    }
    int list_playlists()
    {
        std::vector<std::string> artists;
        int status = get_artists(artists);
        if (status)
            return status;
        for (auto artist : artists) {
            std::cout << artist << '\n';
        }
        return 0;
    }
    int list_artists()
    {
        std::vector<std::string> artists;
        int status = get_artists(artists);
        if (status)
            return status;
        for (auto artist : artists) {
            std::cout << artist << '\n';
        }
        return 0;
    }

    int send_message(const char *channel, const char *message)
    {
        if (!mpd_run_send_message(conn, channel, message))
            return handle_error();
        return 0;
    }

    void print_version()
    {
        for (int i = 0; i < 3; i++) {
            printf("version[%i]: %i\n", i,
                   mpd_connection_get_server_version(conn)[i]);
        }
    }

    int play()
    {
        if (!mpd_run_play(conn))
            return handle_error();
        return 0;
    }
    int toggle_pause()
    {
        if (!mpd_run_toggle_pause(conn))
            return handle_error();
        return 0;
    }
    int clear_queue()
    {
        if (!mpd_run_clear(conn))
            return handle_error();
        return 0;
    }
    int add_to_queue(const char *uri)
    {
        if (!mpd_run_add(conn, uri))
            return handle_error();
        return 0;
    }
};

} // namespace mpd
