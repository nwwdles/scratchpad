#ifndef TREE_H_
#define TREE_H_

#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

/*
 * Base node just holds the children.
 * Inherit and define anything you (I) need.
 *
 * Example:
 * class TreeNode : public BasePtrTreeNode<TreeNode>
 * {
 * };
 */
template <typename T> class BasePtrTreeNode
{
    using ChildType = std::shared_ptr<T>;
    std::vector<ChildType> children{};

  public:
    // todo: learn which &&/& I should define
    ChildType &addChild(ChildType &&node)
    {
        children.push_back(node);
        return getLastChild();
    };
    ChildType &addChild(const ChildType &node)
    {
        children.push_back(node);
        return getLastChild();
    };
    template <typename... Args> ChildType &emplaceChild(Args &&... args)
    {
        children.emplace_back(std::make_shared<T>(std::forward<Args>(args)...));
        return getLastChild();
    }

    // todo: this should be an iterator
    int childIndex(const T &node) const
    {
        auto it =
            find_if(children.cbegin(), children.cend(),
                    [&node](const ChildType &c) { return c.get() == &node; });
        return it - children.cbegin();
    }

    const ChildType &getLastChild() const { return *(--getChildren().end()); }
    ChildType &getLastChild() { return *(--getChildren().end()); }

    ChildType &findChildIf(std::unary_function<T, bool> f)
    {
        return find_if(children.begin(), children.end(), f);
    }

    const std::vector<ChildType> &getChildren() const { return children; };
    std::vector<ChildType> &getChildren() { return children; };

    // populate a vector with children pointers
    void listChildren(std::vector<T *> &list)
    {
        list.push_back(static_cast<T *>(this));
        for (ChildType &child : children) {
            child->listChildren(list);
        }
    };

    // print information about the node
    virtual void printSelf(std::ostream &os, int depth = 0) const
    {
        os << std::string(depth, ' ') << "node\n";
    };
    // print information about the node and the children
    void print(std::ostream &os, int depth = 0) const
    {
        printSelf(os, depth);
        for (const auto &child : children) {
            child->print(os, depth + 1);
        }
    }

    ChildType &operator[](int idx) { return children[idx]; };
    const ChildType &operator[](int idx) const { return children[idx]; };
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const BasePtrTreeNode<T> &node)
{
    node.print(os);
    return os;
};

/*
 * This type adds some parent/child management functions
 */
template <typename T> class ExtendedTreeNode : public BasePtrTreeNode<T>
{
    using Node = T;
    Node *parent = nullptr;

  public:
    ExtendedTreeNode(Node *p = nullptr) : parent(p){};

    Node *getParent() const { return parent; }

    Node *getTreeRoot()
    {
        // is static_cast the right choice in this case since we know that the
        // object is of type T or do I need a dynamic_cast?
        auto p = static_cast<Node *>(this);
        while (auto newp = p->getParent()) {
            p = newp;
        }
        return p;
    }

    // get neighor of the same parent
    Node *getNeighbor(int indexOffset)
    {
        auto *t = static_cast<Node *>(this);
        auto p = getParent();
        if (p) {
            auto index = p->childIndex(*t) + indexOffset;
            if (index >= 0 && index < p->getChildren().size()) {
                return p->getChildren()[index].get();
            }
        }
        return t;
    };

    // get neighor of the same parent
    Node *getNeighborInTree(int indexOffset)
    {
        auto *t = static_cast<Node *>(this);
        auto p = getParent();
        if (p) {
            auto index = p->childIndex(*t) + indexOffset;
            if (index >= 0 && index < p->getChildren().size()) {
                return p->getChildren()[index].get();
            }
        }
        return t;
    };
};

/*
 * Tree should inherit from your (mine) custom TreeNode
 * I's just a pretty name to signify that it's a tree root and not just a node.
 * Also has an initializer.
 */
template <typename T> class Tree : public T
{
  public:
    Tree(){};
    Tree(const std::initializer_list<T> &list)
    {
        for (const auto &var : list) {
            addChild(var);
        }
    };
};

#endif // TREE_H_
