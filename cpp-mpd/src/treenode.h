#ifndef TREE_NODE_H
#define TREE_NODE_H

#include "tree.h"
#include <ncurses.h>

#include <iostream>
#include <string>
#include <vector>

// Class for ncurses label node
class TreeNode : public ExtendedTreeNode<TreeNode>
{
    using Node = TreeNode;

    WINDOW *pad = nullptr;

    std::string displayLabel;
    std::string label;
    std::string data;
    bool expanded = true;
    bool selected = false;
    int height = 1;
    int width = 1;
    int depth = 0;
    int x = 0;
    int y = 0;

  public:
    TreeNode() //:  label("root"), data("/")
    {
        update();
        // std::cout << "created empty: " << *this;
    };
    TreeNode(const std::string &l, const std::string &d, Node *p = nullptr)
        : label(l), data(d), ExtendedTreeNode(p)
    {
        update();
        // std::cout << "created: " << *this;
    };

    // updates y
    int enumerate(int num = 0);

    // updates label, depth and pad
    void updateSelf();
    // updates label, depth and pad recursively
    void update();

    // redraws the node text
    void drawSelf(WINDOW *pad) const;
    // redraws the node text recursively
    void draw(WINDOW *pad) const;

    int getDepth() const { return depth; }
    int getX() const { return x; }
    int getY() const { return y; }
    int getHeight() const { return height; }

    // find next collapsed parent
    // (it's not necessarily a neighbor of firstCollapsedParent)
    Node *nextVisibleNode();
    // find first collapsed (=deepest visible=closest to root) parent
    Node *firstCollapsedParent();
    // Check if any parent if collapsed (it means the this node is hidden)
    bool parentCollapsed();

    WINDOW *getPad() { return pad; };
    void setPad(WINDOW *p)
    {
        pad = p;
        for (auto &child : getChildren()) {
            child->setPad(p);
        }
    }

    const std::string &getDisplayLabel() const { return displayLabel; };
    void updateDisplayLabel();
    const std::string &getLabel() const { return label; };
    std::string &getLabel() { return label; };
    void setLabel(const std::string &l)
    {
        label = l;
        updateSelf();
    };

    // calculates total children height while respecting expanded status
    int getChildrenHeight();

    void toggleExpanded() { setExpanded(!expanded); }
    bool isExpanded() const { return expanded; }
    void setExpanded(bool b)
    {
        expanded = b;
        updateSelf();
        int h = getChildrenHeight() - getHeight();
        if (!b) {
            // erase children
            wmove(getPad(), y + getHeight(), x);
            for (size_t i = 0; i < h; i++) {
                wdeleteln(getPad());
            }
        } else {
            // add lines for children
            wmove(getPad(), y + getHeight(), x);
            for (size_t i = 0; i < h; i++) {
                winsertln(getPad());
            }
        }
        getTreeRoot()->enumerate();
        draw(getPad());
    }

    void toggleSelected() { setSelected(!selected); }
    bool isSelected() const { return selected; }
    void setSelected(bool b)
    {
        selected = b;
        update();
        drawSelf(getPad());
    }

    virtual void printSelf(std::ostream &os, int depth = 0) const override
    {
        os << std::string(depth, ' ')                    //
           << this                                       //
           << " label: " << label                        //
           << " data: " << data                          //
           << " childrencount: " << getChildren().size() //
           << "\n";
    };
};

#endif // TREE_NODE_H
