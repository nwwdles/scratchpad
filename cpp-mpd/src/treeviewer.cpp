#include "treeviewer.h"
#include <algorithm>
#include <iostream>

#include <cstring>
// TreeViewer::TreeViewer() { initialize(); }

TreeViewer::TreeViewer(const std::vector<std::string> data)
{
    initialize(data);
    // std::cout << tree;
}

TreeViewer::TreeViewer(const char *rootpath)
{
    // parse filetree and populate tree
    auto data = std::vector<std::string>{rootpath};
    initialize(data);
}

TreeViewer::~TreeViewer()
{
    echo();
    keypad(stdscr, false);
    curs_set(true);
    delwin(pad);
    endwin();
}

void TreeViewer::populateTree(const std::vector<std::string> data)
{
    // parse data and make a tree
    // for (int i = 0; i < data.size(); ++i) {
    //     auto& path = data[i];
    for (auto &path : data) {
        auto delimPos = 0;
        auto prevDelimPos = delimPos;
        Node *parent = &tree;
        do {
            delimPos = path.find(delimiter, prevDelimPos + 1);
            auto nodePath = path.substr(0, delimPos);
            auto label = path.substr(prevDelimPos, delimPos - prevDelimPos);
            prevDelimPos = delimPos;
            auto it = nodeMap.find(nodePath);
            if (it == nodeMap.end()) {
                // add new node
                parent = parent->emplaceChild(label, nodePath, parent).get();
                nodeMap.insert(std::make_pair(nodePath, parent));
            } else {
                // node exists, use it as a parent for the next node
                parent = it->second;
            }
        } while (delimPos > 0);
    }
}

void TreeViewer::initialize(const std::vector<std::string> data)
{
    populateTree(data);

    // basic init curses
    initscr();
    cbreak();
    noecho();
    curs_set(false);
    keypad(stdscr, true);
    scrollok(stdscr, true);

    // init colors
    if (!has_colors()) {
        mvprintw(0, 1, "Terminal doesn't support colors");

        if (can_change_color()) {
            init_color(COLOR_CYAN, 9, 99, 999);
        }
        start_color();
        init_pair(1, COLOR_MAGENTA, COLOR_CYAN);
        init_pair(2, COLOR_GREEN, COLOR_RED);
        // refresh();
        // getch();
    }
    clear();

    // init tree and the rest
    updateNodeList();
    totalRows = tree.enumerate();
    pad = newpad(totalRows + 1, 100);
    // tree.setLabel("root");
    tree.setPad(pad);
    update();
    redraw();
    selectIndex(0);
}

void TreeViewer::onSizeChanged()
{
    getmaxyx(stdscr, height, width);
    draw();
}

void TreeViewer::redraw() const
{
    wclear(pad);
    tree.draw(pad);
    draw();
}

void TreeViewer::draw() const
{
    clear();
    refresh();
    prefresh(pad, scrollY, 0, 0, 0, std::min(totalRows + 1, height) - 1,
             std::min(100, width));
}

void TreeViewer::update() { tree.update(); }

void TreeViewer::updateNodeList()
{
    nodeList.clear();
    tree.listChildren(nodeList);
}

int TreeViewer::getNodeIndex(Node *node)
{
    return std::find(nodeList.begin(), nodeList.end(), node) - nodeList.begin();
}

int TreeViewer::getSelectedIndex() { return selectedIndex; }
void TreeViewer::selectIndex(int i)
{
    if (i >= 0 && i < nodeList.size()) {
        selectNode(nodeList[i], i > selectedIndex);
    }
}
void TreeViewer::selectNext() { selectIndex(getSelectedIndex() + 1); }
void TreeViewer::selectPrev() { selectIndex(getSelectedIndex() - 1); }

void TreeViewer::selectNextNeighbor()
{
    selectNode(selectedNode->getNeighbor(1));
}
void TreeViewer::selectPrevNeighbor()
{
    selectNode(selectedNode->getNeighbor(-1));
}

void TreeViewer::selectParent()
{
    if (auto p = selectedNode->getParent()) {
        selectedIndex = selectNode(p);
    }
}

int TreeViewer::selectNode(Node *node, bool next)
{
    if (node == selectedNode) {
        return selectedIndex;
    }
    if (selectedNode) {
        selectedNode->setSelected(false);
    }

    if (node) {
        auto targetNode = node;
        if (targetNode->parentCollapsed()) {
            if (next) {
                targetNode = targetNode->nextVisibleNode();
            } else {
                targetNode = targetNode->firstCollapsedParent();
            }
        }
        if (targetNode) {
            selectedNode = targetNode;
            selectedNode->setSelected(true);
            selectedIndex = getNodeIndex(selectedNode);
        }
    }

    // draw(); // TODO: set dirty state instead
    syncScroll();
    return selectedIndex;
}

void TreeViewer::syncScroll()
{
    int scrollAhead = 3;
    int scrollIndex = selectedNode->getY();
    if (scrollIndex - scrollY > height - scrollAhead - 1) {
        scrollY = scrollIndex - (height - scrollAhead - 1);
    }
    if (scrollIndex - scrollY < scrollAhead) {
        scrollY = scrollIndex - scrollAhead;
    }
    scrollY = std::max(scrollY, 0);
    scrollY = std::min(scrollY, totalRows - height + 1);
}

void TreeViewer::expandNode(Node *node)
{
    node->toggleExpanded();
    totalRows = tree.enumerate();
    syncScroll();
    draw();
}

void TreeViewer::run()
{
    onSizeChanged();

    const int BUF_SIZE = 128;
    char buf[BUF_SIZE];
    int i = 0;

    while (true) {

        int input = getch();
        switch (input) {
        case 'r':
        case 'R':
            draw();
            break;
        case KEY_LEFT:
        case 'h':
        case 'H':
            selectParent();
            break;
        case 'L':
        case 'l':
        case ' ':
        case KEY_RIGHT:
            expandNode(selectedNode);
            break;
        case 'j':
        case 'J':
        case KEY_DOWN:
            selectNext();
            break;
        case 'k':
        case 'K':
        case KEY_UP:
            selectPrev();
            break;

        case 'd':
        case 'D':
            selectNextNeighbor();
            break;
        case 'u':
        case 'U':
            selectPrevNeighbor();
            break;
        case 'q':
        case 'Q':
            return;
        default:
            refresh();
            // todo: use std::cin
            buf[0] = input;
            getnstr(buf + 1, BUF_SIZE - 1);
            mvprintw(5, 0, "You entered: %s", buf);
            refresh();
            // std::cout << buf << "\n";
            // if (i < BUF_SIZE) {
            //     buf[++i] = input;
            // }
        }
        draw();
    } // while strcmp(buf, "STOP"));

    // while (true) {
    //     char c = getch();
    //     printw("%d\n", c);

    //     if (c == 'Q' || c == 'q') {
    //         break;
    //     }
    //     draw();
    //     wrefresh(win);
    // }
    // std::cout << "Hello world!\n";
}
