#ifndef TREE_VIEWER_H
#define TREE_VIEWER_H

#include "tree.h"
#include "treenode.h"
#include <map>
#include <ncurses.h>

class TreeViewer
{
    using Node = TreeNode;
    Tree<Node> tree{};

    // todo: i should probably implement iterators instead
    // this is a flat list of all nodes for ease of iterating
    std::vector<Node *> nodeList{};
    std::map<std::string, Node *> nodeMap{};

    WINDOW *pad;

    Node *selectedNode = nullptr;
    int selectedIndex = 0;
    int width = 1;
    int height = 1;
    int scrollY = 0;
    int totalRows = 0;
    char delimiter = '/';

    // initializes curses screen and curses things
    void populateTree(const std::vector<std::string> data);
    void initialize(const std::vector<std::string> data);

    // updates width/height and redraws the screen
    void onSizeChanged();

    // fully redraws pad and everything
    void redraw() const;
    // draw pad to screen and refresh the screen
    void draw() const;
    // updates nodes data
    void update();

    // updates list of nodes that's used for selection (for now)
    void updateNodeList();
    int getNodeIndex(Node *node);
    int getSelectedIndex();

    void syncScroll();

    void expandNode(Node *node);

    int selectNode(Node *node, bool nextparent = false);
    void selectIndex(int);
    void selectNext();
    void selectPrev();
    void selectNextNeighbor();
    void selectPrevNeighbor();
    void selectParent();

  public:
    // TreeViewer();
    TreeViewer(const std::vector<std::string> data);
    TreeViewer(const char *rootpath);
    virtual ~TreeViewer();

    void run();
};

#endif // TREE_VIEWER_H