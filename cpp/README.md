# C++

## Libs

- [BOOST](https://www.boost.org/)
- [{fmt}](https://github.com/fmtlib/fmt) - open-source formatting library for C++. It can be used as a safe and fast alternative to (s)printf and iostreams.
- [cxxopts](https://github.com/jarro2783/cxxopts) - lightweight C++ option parser library, supporting the standard GNU style syntax for options
- [Argh!](https://github.com/adishavit/argh)
- [Taywee/args](https://github.com/Taywee/args)

## Resources

- [Bjarne Stroustrup's C++ Style and Technique FAQ](http://www.stroustrup.com/bs_faq2.html)
- [Awesome C++](https://github.com/fffaraz/awesome-cpp)
- [The Definitive C++ Book Guide and List](https://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list)
- [The GNU C++ Library](https://gcc.gnu.org/onlinedocs/libstdc++/index.html)

Misc:

- [Most vexing parse](https://en.wikipedia.org/wiki/Most_vexing_parse)
