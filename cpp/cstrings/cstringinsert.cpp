#include <cstring>
#include <iostream>

int main(int argc, char const* argv[])
{
    char* s = "teststring";
    char c = 't';
    char* insert = "i";

    int len = strlen(s);
    int insertlen = strlen(insert);

    int count = 0;
    for (int i = 0; i < len; ++i) {
        if (s[i] == c)
            ++count;
    }

    int newlen = len + count * insertlen;
    char* newstring = new char[newlen];
    int offset = 0;
    for (int i = 0; i < len; ++i) {
        newstring[i + offset] = s[i];
        std::cout << i << " " << newstring << "\n";
        if (s[i] == c) {
            for (int j = 0; j < insertlen; ++j) {
                newstring[i + 1 + offset + j] = insert[j];
            }
            offset += insertlen;
        }
    }

    std::cout << newstring << "\n";
    return 0;
}
