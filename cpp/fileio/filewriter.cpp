// #include <cstring>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>
int main(int argc, char const* argv[])
{
    char fname[] = "test.txt";
    std::ifstream inf(fname);
    std::cout << "File contents:\n";
    char c;
    while (inf.get(c)) {
        std::cout << c;
    }
    inf.close();

    std::vector<std::string> strings;
    std::cout << "Write something to the file (Ctrl+D to stop):\n";
    std::string tempstr;
    while (std::cin) {
        getline(std::cin, tempstr);
        strings.push_back(tempstr);
    }

    std::ofstream outf(fname);
    std::ostream_iterator<std::string, char> oi(outf, "\n");
    std::copy(strings.begin(), strings.end(), oi);
    outf.close();
}
