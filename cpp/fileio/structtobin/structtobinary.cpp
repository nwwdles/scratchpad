#include <fstream>
#include <iostream>
#include <vector>

struct datastruct {
    int a = 30;
    char str[20] = "hey";
};

class test
{
  public:
    void (*func)(datastruct& d);
};

void printstruct(datastruct& d) { std::cout << d.a << " " << d.str << "\n"; }
void serialize(std::ofstream& ofile, std::vector<datastruct>& v)
{
    for (int i = 0; i < v.size(); i++) {
        ofile.write((char*)&v[i], sizeof(v[i]));
    }
}

void deserialize(std::ifstream& ifile, std::vector<datastruct>& v)
{
    while (ifile) {
        datastruct temp;
        ifile.read((char*)&temp, sizeof(temp));
        v.push_back(temp);
    }
}
int main(int argc, char const* argv[])
{

    test t;
    t.func = printstruct;

    std::vector<datastruct> vec;
    vec.push_back(datastruct{29, "yo"});
    vec.push_back(datastruct{31, "hi"});

    for (auto x : vec)
        printstruct(x);

    std::ofstream ofile("struct.dat");
    if (ofile.is_open()) {
        serialize(ofile, vec);
    }
    ofile.close();

    vec.clear();
    std::ifstream ifile("struct.dat");
    if (ifile.is_open()) {
        deserialize(ifile, vec);
    }
    ofile.close();

    for (auto x : vec)
        t.func(x);

    return 0;
}
