#include <iostream>
#include <thread>

void threadFunction(int &a)
{
     a++;
}
 
int main()
{
     int a = 1;
     std::thread thr(threadFunction, std::ref(a));
     thr.join();
     std::cout << a << std::endl << * &a << std::endl;
     return 0;
}
