#include <ncurses.h>

int main(int argc, const char **argv)
{
    initscr();
    noecho();

    // input modes
    // cbreak();
    // halfdelay(5);
    // nodelay(stdscr, true);
    // timeout(500);

    char c;
    while ((c = getch()) != 'x') {
        printw("%d\n", c);
    }

    // getch();
    endwin();
    return 0;
}