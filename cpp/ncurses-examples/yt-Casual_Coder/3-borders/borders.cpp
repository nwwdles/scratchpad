#include <ncurses.h>

int main(int argc, const char** argv)
{
    initscr();

    cbreak();
    // raw(); // disables cbreak
    noecho();

    int height = 10;
    int width = 20;
    int start_x = 10;
    int start_y = start_x;
    WINDOW* win = newwin(height, width, start_y, start_x);
    refresh();

    box(win, 0, 0);
    // wborder(win, 'l', 'r', 't', 'b', '1', '2', '3', '4');
    mvwprintw(win, 1, 1, "a box");
    wrefresh(win);

    getch();
    getch();

    endwin();
    return 0;
}