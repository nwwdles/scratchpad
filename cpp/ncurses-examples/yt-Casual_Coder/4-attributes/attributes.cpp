#include <ncurses.h>

void attributeDemo(int y, int x, int ATTR, const char* text)
{
    attron(ATTR);
    mvprintw(y, x, text);
    attroff(ATTR);
}
int main(int argc, const char** argv)
{
    initscr();
    cbreak();
    noecho();

    int line = 1;

    attributeDemo(line++, 1, A_NORMAL, "A_NORMAL");
    attributeDemo(line++, 1, A_STANDOUT, "A_STANDOUT");
    attributeDemo(line++, 1, A_REVERSE, "A_REVERSE");
    attributeDemo(line++, 1, A_BLINK, "A_BLINK");
    attributeDemo(line++, 1, A_DIM, "A_DIM");
    attributeDemo(line++, 1, A_BOLD, "A_BOLD");
    attributeDemo(line++, 1, A_PROTECT, "A_PROTECT");
    attributeDemo(line++, 1, A_INVIS, "A_INVIS");
    attributeDemo(line++, 1, A_ALTCHARSET, "A_ALTCHARSET");
    attributeDemo(line++, 1, A_CHARTEXT, "A_CHARTEXT");

    if (!has_colors) {
        mvprintw(line++, 1, "Terminal doesn't support colors");
    }
    if (can_change_color()) {
        init_color(COLOR_CYAN, 9, 99, 999);
    }
    start_color();
    init_pair(1, COLOR_MAGENTA, COLOR_CYAN);
    init_pair(2, COLOR_GREEN, COLOR_RED);
    attributeDemo(line++, 1, COLOR_PAIR(1), "COLOR_PAIR(1)");
    attributeDemo(line++, 1, COLOR_PAIR(2), "COLOR_PAIR(2)");

    refresh();
    getch();

    endwin();
    return 0;
}