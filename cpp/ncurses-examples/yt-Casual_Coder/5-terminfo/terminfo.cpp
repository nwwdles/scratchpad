#include <ncurses.h>

int main(int argc, const char** argv)
{
    initscr();
    cbreak();
    noecho();

    int y, x, begy, begx, maxy, maxx;

    getyx(stdscr, y, x);
    getbegyx(stdscr, begy, begx);
    getmaxyx(stdscr, maxy, maxx);
    printw("win:%dx%d beg:%dx%d max:%dx%d", y, x, begy, begx, maxy, maxx);

    WINDOW* win = newwin(5, 18, 5, 5);
    getyx(win, y, x);
    getbegyx(win, begy, begx);
    getmaxyx(win, maxy, maxx);
    wmove(win, 1, 1);
    wprintw(win, "xy:%dx%d beg:%dx%d max:%dx%d", y, x, begy, begx, maxy, maxx);
    box(win, 0, 0);
    refresh();
    wrefresh(win);

    getch();

    endwin();
    return 0;
}