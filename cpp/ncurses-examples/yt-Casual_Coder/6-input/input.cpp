#include <ncurses.h>

int main(int argc, const char** argv)
{
    initscr();
    cbreak();
    noecho();

    int xMax, yMax;
    getmaxyx(stdscr, yMax, xMax);

    WINDOW* inputwin = newwin(3, xMax - 12, yMax - 5, 5);
    box(inputwin, 0, 0);
    refresh();
    wrefresh(inputwin);

    keypad(inputwin, true);

    int c = wgetch(inputwin);
    if (c == KEY_UP) {
        mvwprintw(inputwin, 1, 1, "You pressed up");
        wrefresh(inputwin);
    }

    getch();

    endwin();
    return 0;
}