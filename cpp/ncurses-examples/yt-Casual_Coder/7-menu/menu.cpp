#include <ncurses.h>
#include <string>

int main(int argc, const char** argv)
{
    initscr();
    cbreak();
    noecho();

    int xMax, yMax;
    getmaxyx(stdscr, yMax, xMax);

    WINDOW* menuwin = newwin(6, xMax - 12, yMax - 8, 5);
    box(menuwin, 0, 0);
    refresh();
    wrefresh(menuwin);

    keypad(menuwin, true);

    std::string choices[]{"Sing", "Scream", "Roar"};

    int choice = 0;
    int highlight = 0;
    while (true) {
        for (int i = 0; i < 3; ++i) {
            if (i == highlight) {
                wattron(menuwin, A_REVERSE);
            }
            mvwprintw(menuwin, 1 + i, 1, choices[i].c_str());
            wattroff(menuwin, A_REVERSE);
        }
        choice = wgetch(menuwin);
        switch (choice) {
        case KEY_UP:
            highlight--;
            if (highlight < 0)
                highlight = 0;
            break;
        case KEY_DOWN:
            highlight++;
            if (highlight > 2)
                highlight = 2;
            break;
        }
        if (choice == 10)
            break;
    }

    printw("Your choice was %s", choices[highlight].c_str());
    getch();

    endwin();
    return 0;
}