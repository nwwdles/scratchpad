#include "player.h"

Player::Player(WINDOW *win, int start_y, int start_x, char c)
    : curwin(win), y(start_y), x(start_x), character(c)
{
    getmaxyx(curwin, yMax, xMax);
    keypad(curwin, true);
}

void Player::draw() { mvwaddch(curwin, y, x, character); }

int Player::getmv()
{
    int choice = wgetch(curwin);
    move(static_cast<Direction>(choice));
    return choice;
}

bool Player::isColliding(int x, int y)
{
    return (x <= 0 || x >= xMax - 1 || y <= 0 || y >= yMax - 1);
}

void Player::move(Direction dir)
{
    int newx = x;
    int newy = y;

    switch (dir) {
    case Direction::LEFT:
        newx -= 1;
        break;
    case Direction::RIGHT:
        newx += 1;
        break;
    case Direction::UP:
        newy -= 1;
        break;
    case Direction::DOWN:
        newy += 1;
        break;
    }

    if (!isColliding(newx, newy)) {
        wmove(curwin, y, x);
        attron(A_DIM);
        waddch(curwin, '.');
        attroff(A_DIM);
        x = newx;
        y = newy;
    }
}