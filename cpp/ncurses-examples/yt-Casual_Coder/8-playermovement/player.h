#ifndef PLAYER_H_
#define PLAYER_H_
#include <ncurses.h>

class Player
{
    int x;
    int y;
    int xMax;
    int yMax;
    char character;
    WINDOW *curwin;
    bool isColliding(int x, int y);

  public:
    Player(WINDOW *win, int y, int x, char c);
    enum Direction {
        UP = KEY_UP,
        DOWN = KEY_DOWN,
        LEFT = KEY_LEFT,
        RIGHT = KEY_RIGHT
    };
    void draw();
    int getmv();
    void move(Direction dir);
};

#endif // PLAYER_H_