#include "player.h"

#include <ncurses.h>
#include <string>

int main(int argc, const char **argv)
{
    initscr();
    cbreak();
    noecho();
    curs_set(0);

    int xMax, yMax;
    getmaxyx(stdscr, yMax, xMax);

    WINDOW *menuwin = newwin(yMax - 4, xMax - 4, 2, 2);
    refresh();

    keypad(menuwin, true);

    Player p(menuwin, 5, 5, '@');
    box(menuwin, 0, 0);

    while (true) {
        // wclear(menuwin);
        wrefresh(menuwin);
        p.draw();
        if (p.getmv() == 'x')
            break;
    }
    clear();
    printw("bye");
    getch();

    endwin();
    return 0;
}