#include <ncurses.h>

int main(int argc, const char **argv)
{
    initscr();
    cbreak();
    noecho();
    curs_set(0);

    int xMax, yMax;
    getmaxyx(stdscr, yMax, xMax);
    WINDOW *win = newwin(yMax - 4, xMax - 4, 2, 2);
    box(win, 0, 0);

    refresh();
    wrefresh(win);
    getch();
    endwin();
    return 0;
}