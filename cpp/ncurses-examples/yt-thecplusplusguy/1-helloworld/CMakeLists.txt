cmake_minimum_required(VERSION 3.0)

set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lncurses")

project(ncurses-helloworld)
add_executable(
    ncurses-helloworld.out
    ncurses-helloworld.cpp
)