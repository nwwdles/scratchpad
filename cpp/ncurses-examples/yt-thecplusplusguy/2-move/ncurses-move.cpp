/*
https://www.youtube.com/watch?v=fh_Pu4183C4
*/
#include <ncurses.h>

int main(int argc, const char** argv)
{
    initscr();

    move(5, 10); // move cursor 5 chars to bottom, 10 to right
    printw("Hello world!");
    mvprintw(8, 25, "Hi"); // move and print
    refresh();
    getch();

    endwin();
    return 0;
}