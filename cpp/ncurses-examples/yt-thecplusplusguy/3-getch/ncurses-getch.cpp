/*
https://www.youtube.com/watch?v=i_reXgq-0pM
*/

#include <ncurses.h>

int main(int argc, const char** argv)
{
    initscr();
    noecho();
    int c;
    printw("Press a key (Esc to quit)");
    while ((c = getch()) != 27) {
        move(0, 0);
        printw("Press a key (Esc to quit)");
        move(5, 0);
        clrtoeol(); // clear the line (otherwise we can get a remains of a
                    // previous line)
        printw("Keycode: %d, Character: %c", c, c);
        refresh();
    }
    endwin();
    return 0;
}