/*
https://www.youtube.com/watch?v=RGmXBazTx1Y
*/

#include <ncurses.h>

int main(int argc, const char** argv)
{
    initscr();
    noecho();
    int x, y;
    getmaxyx(stdscr, y, x);
    attron(A_REVERSE);
    mvprintw(y / 2, x / 2, "w=%d, h=%d", x, y);
    attroff(A_REVERSE);
    refresh();
    getch();
    endwin();
    return 0;
}