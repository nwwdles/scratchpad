/*
https://www.youtube.com/watch?v=RGmXBazTx1Y
*/

#include <ncurses.h>

int main(int argc, const char** argv)
{
    initscr();
    curs_set(0);

    // start_color();
    // init_pair(1, COLOR_RED, COLOR_BLACK);
    // attron(COLOR_PAIR(1));
    // printw("Something something");
    // attroff(COLOR_PAIR(1));
    // refresh();

    WINDOW* w = newwin(8, 30, 5, 30);
    box(w, 0, 0);
    mvwprintw(w, 4, 4, "Something");
    wrefresh(w);
    wgetch(w);

    delwin(w);
    endwin();
    return 0;
}