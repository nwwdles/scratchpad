/*
https://www.youtube.com/watch?v=4HgyStstIhw
*/

#include "snake.h"
#include <ncurses.h>

int main(int argc, const char** argv)
{
    Snake s;
    s.start();
    return 0;
}
