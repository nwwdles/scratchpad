#include "snake.h"
#include <chrono>
#include <thread>
void Snake::initCurses()
{
    initscr();
    nodelay(stdscr, true);
    keypad(stdscr, true);
    noecho();
    curs_set(0);
    getmaxyx(stdscr, maxheight, maxwidth);
}

void Snake::putFood()
{
    while (true) {
        food.x = rand() % (maxwidth - 2) + 1;
        food.y = rand() % (maxheight - 3) + 1;
        std::cout << food.x;
        bool badloc = false;
        for (int i = 0; i < snakeParts.size(); ++i) {
            if (snakeParts[i].x == food.x && snakeParts[i].y == food.y)
                badloc = true;
        }
        if (!badloc)
            break;
    }
    move(food.y, food.x);
    addch(foo);
    refresh();
}

Snake::~Snake()
{
    nodelay(stdscr, false);
    getch();
    endwin();
}

Point stepPoint(Point& p, Snake::Direction direction)
{
    using Direction = Snake::Direction;
    Point newpoint(p);
    switch (direction) {
    case Direction::LEFT:
        newpoint.x -= 1;
        break;
    case Direction::RIGHT:
        newpoint.x += 1;
        break;
    case Direction::DOWN:
        newpoint.y += 1;
        break;
    case Direction::UP:
        newpoint.y -= 1;
        break;
    }
    return newpoint;
}

void Snake::moveSnake()
{
    int input = getch();

    switch (input) {
    case KEY_LEFT:
        if (direction != Direction::RIGHT) {
            direction = Direction::LEFT;
        }
        break;
    case KEY_RIGHT:
        if (direction != Direction::LEFT) {
            direction = Direction::RIGHT;
        }
        break;
    case KEY_DOWN:
        if (direction != Direction::UP) {
            direction = Direction::DOWN;
        }
        break;
    case KEY_UP:
        if (direction != Direction::DOWN) {
            direction = Direction::UP;
        }
        break;
    }

    auto head = snakeParts.begin();
    Point next = stepPoint(*head, direction);

    if (!gotFood) {
        auto last = --snakeParts.end();
        move((*last).y, (*last).x);
        addch(' ');
        refresh();
        snakeParts.pop_back();
    }
    snakeParts.insert(head, next);
    move(snakeParts[0].y, snakeParts[0].x);
    addch(partchar);
    refresh();
}

void Snake::drawPoints()
{
    move(maxheight - 1, 0);
    printw("Points: %d", points);
}
bool Snake::isColliding()
{
    int x = snakeParts[0].x;
    int y = snakeParts[0].y;
    if (x == 0 || x == maxwidth - 1 || y == 0 || y == maxheight - 2) {
        std::cout << "border collision";
        return true;
    }
    for (int i = 3; i < snakeParts.size(); i++) {
        if (x == snakeParts[i].x && y == snakeParts[i].y) {
            std::cout << "part collision";
            return true;
        }
    }

    if (x == food.x && y == food.y) {
        gotFood = true;
        putFood();
        points += 10;
        if (points % 100 == 0) {
            del -= 10000;
        }
    } else {
        gotFood = false;
    }

    return false;
}
void Snake::start()
{
    while (true) {
        if (isColliding()) {
            move(maxheight / 2, maxwidth / 2 - 4);
            printw("game over");
            break;
        }
        moveSnake();
        std::this_thread::sleep_for(std::chrono::milliseconds(del / 1000));
    }
}

Snake::Snake()
    : points(0), food(0, 0), foo('o'), partchar('*'),
      direction(Direction::LEFT), gotFood(false), del(11000)
{
    // init ncurses
    initCurses();

    // seed random
    srand(time(nullptr));

    // init vars
    wallchar = static_cast<char>(219);
    for (int i = 0; i < 5; ++i) {
        snakeParts.push_back(Point(maxwidth / 2 + i, maxheight / 2));
    }
    del = 110000;

    // draw walls
    for (int i = 0; i < maxwidth - 1; ++i) {
        move(0, i);
        addch(wallchar);
        move(maxheight - 2, i);
        addch(wallchar);
    }
    for (int i = 0; i < maxheight - 2; ++i) {
        move(i, 0);
        addch(wallchar);
        move(i, maxwidth - 1);
        addch(wallchar);
    }

    // draw snake
    for (auto i = snakeParts.cbegin(); i != snakeParts.cend(); ++i) {
        move((*i).y, (*i).x);
        addch(partchar);
    }

    putFood();
    drawPoints();
}