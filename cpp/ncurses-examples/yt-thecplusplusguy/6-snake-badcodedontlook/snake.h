#ifndef SNAKE_H_
#define SNAKE_H_

#include <cstdlib>
#include <deque>
#include <iostream>
#include <ncurses.h>

struct Point {
    int x;
    int y;
    Point(int col = 0, int row = 0) : x(col), y(row){};
};

class Snake
{
  public:
    enum Direction { LEFT, RIGHT, UP, DOWN };

  private:
    Direction direction;

    int points;
    int del;
    int maxwidth;
    int maxheight;
    std::deque<Point> snakeParts;
    char partchar;
    char wallchar;
    char foo;

    Point food;
    bool gotFood;

    void drawPoints();
    void putFood();
    bool isColliding();
    void moveSnake();

    void initCurses();

  public:
    Snake();
    ~Snake();
    void start();
};

#endif // !SNAKE_H_