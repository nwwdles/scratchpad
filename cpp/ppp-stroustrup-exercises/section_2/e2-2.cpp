#include "std_lib_facilities.h"

int main()
{
    cout << "getUp();\n";
    cout << "turn(90);\n";
    cout << "walk(3);\n";
    cout << "turn(-90);\n";
    cout << "walk(2);\n";
    cout << "door mydoor=getDoorInFront();\n";
    cout << "mydoor.open();\n";
    cout << "walk(1);\n";
    cout << "mydoor.close();\n";
    cout << "onArrival(rooms.bathroom);\n";
    return 0;
}