#include "std_lib_facilities.h"

int main()
{
    int pennies;
    int nickels;
    int dimes;
    int quarters;
    int half_dollars;
    int dollar_coins;
    cout<<"How many pennies do you have?\n";
    cin>>pennies;
    cout<<"How many nickels do you have?\n";
    cin>>nickels;
    cout<<"How many dimes do you have?\n";
    cin>>dimes;
    cout<<"How many quarters do you have?\n";
    cin>>quarters;
    cout<<"How many half dollars do you have?\n";
    cin>>half_dollars;
    cout<<"How many one dollar coins do you have?\n";
    cin>>dollar_coins;
    double total = pennies + 5 * nickels + 10 * dimes + 25 * quarters + 50 * half_dollars + 100 * dollar_coins;

    if (pennies == 1){
        cout<<"You have "<<pennies<<" penny.\n";
    }
    else{
        cout<<"You have "<<pennies<<" pennies.\n";
    }
    if (nickels == 1){
        cout<<"You have "<<nickels<<" nickel.\n";
    }
    else{
        cout<<"You have "<<nickels<<" nickels.\n";
    }
    if (dimes == 1){
        cout<<"You have "<<dimes<<" dime.\n";
    }
    else{
        cout<<"You have "<<dimes<<" dimes.\n";
    }
    if (quarters == 1){
        cout<<"You have "<<quarters<<" quarter.\n";
    }
    else{
        cout<<"You have "<<quarters<<" quarters.\n";
    }
    if (half_dollars == 1){
        cout<<"You have "<<half_dollars<<" half dollar.\n";
    }
    else{
        cout<<"You have "<<half_dollars<<" half dollars.\n";
    }
    if (dollar_coins == 1){
        cout<<"You have "<<dollar_coins<<" one dollar coin.\n";
    }
    else{
        cout<<"You have "<<dollar_coins<<" one dollar coins.\n";
    }

    cout<<"The value of all your coins is "<< total <<" cents "
        <<"or $"<< total/100<<".\n";
}