#include "std_lib_facilities.h"

int main()
{
    double miles;
    cout << "Enter number of miles to convert to kilometers: \n";
    cin >> miles;
    cout << "Result: " << miles * 1.609 << " km.\n";
}