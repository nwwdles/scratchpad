#include "std_lib_facilities.h"

int main()
{
    int double = 0; // expected unqualified-id before «=» token
    int $cha = 0;
    int 4545y = 1; //expected unqualified-id before numeric constant
    int a b = 1; // expected initializer before «b»
    int a,b = 4;
}