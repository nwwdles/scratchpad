#include "std_lib_facilities.h"

int main()
{
    int a;
    int b;
    cout << "Input 2 integer values:\n";
    cin >> a >> b;
    cout << "a + b: " << a + b
        << "\na - b: " << a - b
        << "\na * b: " << a * b
        << "\na / b: " << a / b
        << "\na > b: " << (a > b)
        << "\na < b: " << (a < b)
        << "\na == b: " << (a == b)
        << "\na \% b: " << a % b
        << "\n";
}