#include "std_lib_facilities.h"

int main()
{
    double a;
    double b;
    cout << "Input 2 values:\n";
    cin >> a >> b;
    cout << "a + b: " << a + b
        << "\na - b: " << a - b
        << "\na * b: " << a * b
        << "\na / b: " << a / b
        << "\na > b: " << (a > b)
        << "\na < b: " << (a < b)
        << "\na == b: " << (a == b)
        << "\n";
}