#include "std_lib_facilities.h"

int main()
{
    int val1;
    int val2;
    int val3;
    cout << "Please enter three integers:\n";
    cin >> val1 >> val2 >> val3;
    
    // now this is ugly but it's what i've been taught at this point
    if ((val1 <= val2) & (val1 <= val3)){
        cout << val1 << ", ";
        if (val2 <= val3){
            cout << val2 << ", " << val3;
        }
        else {
            cout << val3 << ", " << val2;
        }
    }
    else if ((val2 <= val1) & (val2 <= val3)){
        cout << val2 << ", ";
        if (val1 <= val3){
            cout << val1 << ", " << val3;
        }
        else {
            cout << val3 << ", " << val1;
        }
    }
    else if ((val3 <= val1) & (val3 <= val2)){
        cout << val3 << ", ";
        if (val1 <= val2){
            cout << val1 << ", " << val2;
        }
        else {
            cout << val2 << ", " << val1;
        }
    }
    cout << "\n";
}