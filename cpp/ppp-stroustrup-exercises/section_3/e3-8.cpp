#include "std_lib_facilities.h"

int main()
{
    int number;
    cout<<"Please enter the number to determine if it's odd or even:\n";
    cin>>number;
    if (number % 2 == 0){
        cout<<"The number "<<number<<" is even.\n";
    }
    else{
        cout<<"The number "<<number<<" is odd.\n";
    }
}