#include "std_lib_facilities.h"

int main()
{
    string first_name;
    string friend_name;
    char friend_sex = 0;
    int age;

    // get input
    cout << "Enter the name of the person you want to write to:\n";
    cin >> first_name;

    cout << "Enter the name of your friend:\n";
    cin >> friend_name;

    cout << "Enter your friend's sex (m/f):\n";
    cin >> friend_sex;

    cout << "Please enter your recipient's age: \n";
    cin >> age;
    if (age <= 0 or age >= 110) {
        simple_error("you're kidding!");
    }

    // output
    cout << "Dear " << first_name << ",\n" 
        << "\tHow are you? I am fine. I miss you. ";

    cout << "Have you seen " << friend_name << " lately? ";

    if (friend_sex == 'm') {
        cout << "If you see " << friend_name << " please ask him to call me. ";
    } else if (friend_sex == 'f') {
        cout << "If you see " << friend_name << " please ask her to call me. ";
    }
    
    cout << "I hear you just had a birthday and you are " << age << " years old. ";
    if (age < 12) {
        cout << "Next year you will be " << age + 1 << ".\n";
    } else if (age == 17) {
        cout << "Next year you will be able to vote.\n";
    } else if (age > 70) {
        cout << "I hope you are enjoying retirement.\n";
    }
    cout << "Yours sincerely,\n\n" << "K. L.\n";
}