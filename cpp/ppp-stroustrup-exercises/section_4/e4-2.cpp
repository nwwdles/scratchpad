#include "std_lib_facilities.h"

double median(vector<double> v) {
    // implying that vector is sorted already
    int s = v.size();
    if (s % 2 == 0) {
        return (v[s/2] + v[s/2 - 1])/2;
    } else {
        return v[s/2];
    }
}

int main()
{
    vector<double> vals1 = {1, 2, 3, 4, 6}; // median is 3
    vector<double> vals2 = {1, 2, 3, 5, 6, 7}; // median between 3 and 5
    cout << median(vals1) << '\n' << median(vals2);
}