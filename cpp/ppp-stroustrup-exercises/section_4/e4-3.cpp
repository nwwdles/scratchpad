#include "std_lib_facilities.h"

int main()
{
    vector<double> vals;
    double temp;
    while(cin >> temp) {
        vals.push_back(temp);
    }
    double total = 0;
    double smallest = 0;
    double largest = 0;
    for (double x : vals) {
        total += x;
        if (x < smallest || smallest == 0) {
            smallest = x;
        }
        if (x > largest) {
            largest = x;
        }
    }
    double mean = total / vals.size();
    cout << "smallest distance: " << smallest
        << ". largest distance: " << largest
        << ". mean distance: " << mean
        << ". total distance: " << total << ". \n";
}