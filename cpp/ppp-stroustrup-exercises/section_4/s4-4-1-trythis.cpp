#include "std_lib_facilities.h"

int main()
{
    constexpr double rateDtoY = 106;
    constexpr double rateDtoE = 0.88;
    constexpr double rateDtoP = 0.69;
    constexpr double rateDtoK = 0.55;
    constexpr double rateDtoU = 20.69;

    double amount = 0;
    double dollaramt = 0;
    double rateIn = 1; 
    double rateOut = 1;
    char curr1 = 'x';
    char curr2 = 'x';
    string nameIn = "";
    string nameOut = "";
    
    cout << "Please input amount you want to convert, your currency and currency you want to convert to (e.g. 125 y d).\n"
          "Use y for yen, d for dollars, p for pounds, e for euros, u for yuan, k for kroners.\n";
    cin >> amount >> curr1 >> curr2;

    switch (curr1){
    case 'y':
        rateIn =  rateDtoY;
        nameIn = "yen";
        break;
    case 'd':
        rateIn = 1;
        nameIn = "dollars";
        break;
    case 'p':
        rateIn =  rateDtoP;
        nameIn = "pounds";
        break;
    case 'e':
        rateIn =  rateDtoE;
        nameIn = "euros";
        break;
    case 'k':
        rateIn = rateDtoK;
        nameIn = "kroners";
        break;
    case 'u':
        rateIn = rateDtoU;
        nameIn = "yuan";
        break;
    default:
        cout << "Sorry, I don't understand the first currency\n";
    }

    switch (curr2){
    case 'y':
        rateOut = rateDtoY;
        nameOut = "yen";
        break;
    case 'd':
        rateOut = 1;
        nameOut = "dollars";
        break;
    case 'p':
        rateOut = rateDtoP;
        nameOut = "pounds";
        break;
    case 'e':
        rateOut = rateDtoE;
        nameOut = "euros";
        break;
    case 'k':
        rateOut = rateDtoK;
        nameOut = "kroners";
        break;
    case 'u':
        rateOut = rateDtoU;
        nameOut = "yuan";
        break;
    default:
        cout << "Sorry, I don't understand the second currency\n";
    }

    dollaramt = amount / rateIn;
    double finalamt = dollaramt * rateOut;

    cout << amount << " " << nameIn << " is " << finalamt << " " << nameOut << "\n";
}