#include "std_lib_facilities.h"

int square(int x) {
    int sq = 0;
    for (int i = 0; i < x; i++) {
        sq += x;
    }
    return sq;
}

int main()
{
    cout << square(4) << "\n";
}