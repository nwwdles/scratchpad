#include "std_lib_facilities.h"

int main()
{
    vector<string> disliked = {"Broccoli", "Man"};
    
    vector<string> words;
    for(string temp; cin>>temp; )
        words.push_back(temp);
    cout << "Number of words: " << words.size() << '\n';

    sort(words);

    
    for (int i = 0; i < words.size(); ++i) {
        bool badword = false;
        for (int j = 0; j < disliked.size(); ++j) {
            if (words[i] == disliked[j])
                badword = true;
        }
        if (badword)
            cout << "BLEEP" << "\n";
        else
            cout << words[i] << "\n";
    }
}