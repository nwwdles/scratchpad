#include "std_lib_facilities.h"

int main()
{
    /*
        1-4
    */
    // double val = 0;
    // double b = 0;
    // while (cin >> val >> b) {
    //     if (val != b) {
    //         cout << "the smaller value is: " << min(val, b) << ". the larger value is: " << max(val,b) << "\n";
    //         if (abs(val-b) <= 1.0/100)
    //             cout << "the numbers are almost equal\n";
    //     }
    //     else 
    //         cout << "the numbers are equal\n";
    // }

    /*
        5-11
    */
    double val = 0;
    double smallest = 0;
    double largest = 0;
    string unit = "";
    vector<string> units = {"cm", "m", "in", "ft"};
    vector<double> rates = {1.0/100, 1, 2.54/100, 12 * 2.54/100};
    double sum = 0;
    int number = 0;
    vector<double> values;

    while (cin >> val >> unit) {
        bool validunit = false;
        int unitindex = 0;
        for (int i = 0; i < units.size(); ++i) {
            if (unit == units[i]) {
                validunit = true;
                unitindex = i;
            }
        }
        if (validunit) {
            double mval = rates[unitindex] * val;

            sum += mval;
            ++number;
            values.push_back(mval);

            if (smallest == 0 && largest == 0) { // first loop (or user didn't enter anything other than 0 anyway)
                largest = mval;
                smallest = mval;
            }
            if (mval > largest)
                largest = mval;
            if (mval < smallest)
                smallest = mval;

            cout << "number entered: " << val << " " << unit << " (" << mval<< " m)\n";
            cout << "smallest so far: " << smallest 
                << " m. largest so far: " << largest
                << " m. sum of values: " << sum
                << ". number of values: " << number
                << "\n";

            sort(values);
            for (double v : values) {
                cout << v << " ";
            }
            cout << '\n';
        }
    }
}