# prata-cpp-primer

My answers for chapter reviews and programming exercises from C++ Primer 6th Edition by Stephen Prata.

Note that this book generally [isn't recommended](https://accu.org/index.php?module=bookreviews&func=search&rid=1744).

For other choices refer to: 
- https://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list
- https://www.reddit.com/r/learnprogramming/wiki/faq_cpp


---

The book was recommended to me by some programmer and it was okay. (I wasn't an absolute total beginner though).