#include <iostream>
class BankAccount
{
  private:
    /* data */
    const char* depositorName;
    const char* accountNumber;
    long balance;

  public:
    void display() const;
    void deposit(long amount) { balance += amount; };
    void withdraw(long amount);
    BankAccount(const char* depName = "Unknown", const char* acNum = "0",
                long bal = 0.0);
    ~BankAccount();
};

// 7
BankAccount::BankAccount(const char* depName, const char* acNum, long bal)
{
    depositorName = depName;
    accountNumber = acNum;
    balance = bal;
}

BankAccount::~BankAccount() { std::cout << "Bank account destroyed\n"; }

void BankAccount::display() const
{
    using namespace std;
    cout << "Bank: " << depositorName << "\tAccount: " << accountNumber
         << "\tBalance: " << balance << "\n";
}

void BankAccount::withdraw(long amount)
{
    if (amount <= balance) {
        balance -= amount;
    } else {
        std::cout << "Can't withdraw amount > balance\n";
    }
}

int main(int argc, char const* argv[])
{
    BankAccount myAc{"Banky Bank", "1334", 12};
    myAc.display();
    myAc.withdraw(10);
    myAc.display();
    myAc.deposit(12);
    myAc.display();
    return 0;
}
