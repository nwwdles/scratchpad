#include <cstring>
#include <iostream>
#include <string>

class Person
{
  private:
    static const int LIMIT = 25;
    std::string lname; // Person’s last name
    char fname[LIMIT]; // Person’s first name
  public:
    Person()
    {
        lname = "";
        fname[0] = '\0';
    } // #1
    Person(const std::string& ln, const char* fn = "Heyyou");
    // #2
    // the following methods display lname and fname
    void Show() const;
    // firstname lastname format
    void FormalShow() const; // lastname, firstname format
};
Person::Person(const std::string& ln, const char* fn)
{
    lname = ln;
    strncpy(fname, fn, LIMIT);
}
void Person::Show() const { std::cout << fname << " " << lname; }
void Person::FormalShow() const { std::cout << lname << ", " << fname; }

int main(int argc, char const* argv[])
{
    //
    Person one;
    // use default constructor
    Person two("Smythecraft");
    // use #2 with one default argument
    Person three("Dimwiddy", "Sam");
    // use #2, no defaults
    one.Show();
    std::cout << std::endl;
    one.FormalShow();
    std::cout << std::endl;

    two.Show();
    std::cout << std::endl;
    two.FormalShow();
    std::cout << std::endl;

    three.Show();
    std::cout << std::endl;
    three.FormalShow();
    std::cout << std::endl;

    return 0;
}
