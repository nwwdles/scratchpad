#include "golf.h"
#include <cstring>
#include <iostream>

void Golf::showgolf() const
{
    std::cout << "Name:" << fullname << "\tHandicap:" << handicap << "\n";
}

Golf::Golf()
{
    int hc;
    char name[Len];
    std::cout << "Please enter name: ";

    std::cin.getline(name, Len);
    // if (name[0] == '\0') {
    //     return 0;
    // }

    std::cout << "Please enter handicap: ";
    std::cin >> hc;
    std::cin.get(); // get rid of newline

    // setgolf(g, name, hc);
    *this = Golf(name, hc);
}

Golf::Golf(const char* name, int hc)
{
    strncpy(fullname, name, Len);
    handicap = hc;
}