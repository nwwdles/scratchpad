#ifndef GOLF_H
#define GOLF_H

const int Len = 40;

class Golf
{
  private:
    char fullname[Len];
    int handicap;

  public:
    void sethandicap(int hc) { handicap = hc; }
    void showgolf() const;
    Golf();
    Golf(const char* name, int hc);
};

#endif