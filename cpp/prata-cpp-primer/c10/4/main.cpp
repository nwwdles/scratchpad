// static.cpp -- using a static local variable
#include "sales.h"
#include <iostream>

int main()
{
    double quartersales[4] = {13., 34., 54., 77.};
    SALES::Sales sales[2]{SALES::Sales(), SALES::Sales(quartersales, 4)};

    for (int i = 0; i < 2; i++) {
        sales[i].showSales();
    }

    return 0;
}