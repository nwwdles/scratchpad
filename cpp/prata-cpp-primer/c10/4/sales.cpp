#include "sales.h"
#include <iostream>
namespace SALES
{
// copies the lesser of 4 or n items from the array ar
// to the sales member of s and computes and stores the
// average, maximum, and minimum values of the entered items;
// remaining elements of sales, if any, set to 0
Sales::Sales(const double ar[], int n)
{
    n = (n > 4) ? 4 : n;
    double min_ = ar[0];
    double max_ = ar[0];
    double sum = ar[0];
    sales[0] = ar[0];
    for (int i = 1; i < n; i++) {
        sum += ar[i];
        sales[i] = ar[i];
        if (ar[i] > max) {
            max = ar[i];
        } else if (ar[i] < min) {
            min = ar[i];
        }
    }
    double avg_ = sum / n;

    min = min_;
    max = max_;
    average = avg_;
}
// gathers sales for 4 quarters interactively, stores them
// in the sales member of s and computes and stores the
// average, maximum, and minimum values
Sales::Sales()
{
    double ar[4] = {};
    int count = 0;
    std::cout << "Enter sales by quarter:\n";
    for (int i = 0; i < 4; i++) {
        std::cout << i + 1 << ". ";
        std::cin >> ar[i];
        if (!std::cin) {
            // bad input
            std::cin.clear();
            while (std::cin.get() != '\n') {
                continue;
            }
        } else {
            count++;
        }
    }
    *this = Sales(ar, count);
}
// display all information in structure s
void Sales::showSales() const
{

    std::cout << "Average: " << average << "\tMin: " << min << "\tMax: " << max
              << "\n";

    std::cout << "By quarter: ";
    for (int i = 0; i < QUARTERS; i++) {
        std::cout << sales[i] << " ";
    }
    std::cout << "\n";
}
} // namespace SALES
