#include "stack.h"
#include <iostream>

int main(int argc, char const* argv[])
{
    /* code */
    Stack mystack;
    customer cust;
    char op;

    while (true) {
        std::cout << "Enter an operation (A to add a customer, P to remove a "
                     "customer): ";
        std::cin >> op;
        switch (op) {
        case 'a':
        case 'A':
            std::cout << "Enter name:";
            std::cin.getline(cust.fullname, 35);
            std::cout << "Enter payment:";
            std::cin >> cust.payment;
            mystack.push(cust);
            break;
        case 'p':
        case 'P':
            if (mystack.pop(cust)) {
                std::cout << "Name: " << cust.fullname << "\n";
                std::cout << "Pay: " << cust.payment << "\n";
            } else {
                std::cout << "Error. Stack is empty.\n";
            }
            break;
        default:
            break;
        }
    }

    return 0;
}
