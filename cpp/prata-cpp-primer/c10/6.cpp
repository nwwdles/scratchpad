#include <iostream>

class Move
{
  private:
    double x;
    double y;

  public:
    Move(double a = 0, double b = 0);
    // sets x, y to a, b
    void showmove() const;
    // shows current x, y values
    Move add(const Move& m) const;
    // this function adds x of m to x of invoking object to get new x,
    // adds y of m to y of invoking object to get new y, creates a new
    // move object initialized to new x, y values and returns it
    void reset(double a = 0, double b = 0); // resets x,y to a, b
};

int main(int argc, char const* argv[])
{
    /* code */
    Move m{0, 1};
    m.showmove();
    Move m2;
    m2.showmove();

    m2 = m.add(Move(3, 4));
    m2.showmove();

    m2.reset();
    m2.showmove();
    return 0;
}

Move::Move(double a, double b)
{
    x = a;
    y = b;
}

void Move::reset(double a, double b)
{
    x = a;
    y = b;
}

Move Move::add(const Move& m) const { return Move(m.x + x, m.y + y); }

void Move::showmove() const { std::cout << "x: " << x << " y: " << y << "\n"; }
