#include <cstring>
#include <iostream>

class Plorg
{
  private:
    char name[19];
    int ci;

  public:
    Plorg(const char* n = "Plorga", int c = 50);
    void setCi(int c);
    void report() const;
};

Plorg::Plorg(const char* n, int c)
{
    strncpy(name, n, 19);
    ci = c;
}
void Plorg::setCi(int c) { ci = c; }
void Plorg::report() const
{
    std::cout << "Name: " << name << " CI: " << ci << "\n";
}

int main(int argc, char const* argv[])
{
    Plorg p;
    Plorg p1{"Plorgy", 34};
    p.report();
    p1.report();
    p.setCi(69);
    p.report();
    return 0;
}
