#include "list.h"

List::List() { len = 0; }
bool List::isFull() { return len == MAX; }
bool List::isEmpty() { return len == 0; }
bool List::addItem(Item i)
{
    if (!isFull()) {
        contents[len] = i;
        len++;
        return true;
    } else
        return false;
};
Item& List::getItem(int i) { return contents[i]; }
void List::visit(void (*pf)(Item&))
{
    for (int i = 0; i < len; i++) {
        pf(contents[i]);
    }
}