#ifndef LIST_H_
#define LIST_H_

typedef int Item;

class List
{
  private:
    enum { MAX = 10 };
    Item contents[MAX];
    Item len;

  public:
    List();
    bool isFull();
    bool isEmpty();
    bool addItem(Item i);
    Item& getItem(int i);
    void visit(void (*pf)(Item&));
};

#endif // !LIST_H_