#include "list.h"
#include <iostream>

void increment(int& i) { i++; }

int main(int argc, char const* argv[])
{
    List mylist;
    mylist.addItem(64343);
    mylist.addItem(7);
    mylist.addItem(75);
    std::cout << mylist.getItem(0) << "\n";
    std::cout << mylist.getItem(1) << "\n";

    mylist.visit(&increment);
    std::cout << mylist.getItem(0) << "\n";
    std::cout << mylist.getItem(1) << "\n";

    return 0;
}