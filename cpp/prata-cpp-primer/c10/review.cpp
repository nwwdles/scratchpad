// 1
// a class is a user-defined type?
// an oop concept

// 2
// by having private and public members

// 3
// an object is an instance of class

// 4
// each object of a class has its own copy of data members
// but they all use the same member functions

// 5
#include <iostream>
class BankAccount
{
  private:
    /* data */
    const char* depositorName;
    const char* accountNumber;
    long balance;

  public:
    void display() const;
    void deposit(long amount) { balance += amount; };
    void withdraw(long amount);
    BankAccount(const char* depName = "Unknown", const char* acNum = "0",
                long bal = 0.0);
    ~BankAccount();
};

// 7
BankAccount::BankAccount(const char* depName = "Unknown",
                         const char* acNum = "0", long bal = 0.0)
{
    depositorName = depName;
    accountNumber = acNum;
    balance = bal;
}

BankAccount::~BankAccount() {}

void BankAccount::display() const
{
    using namespace std;
    cout << "Name:" << depositorName << "\nAccount: " << accountNumber
         << "\nBalance" << balance << "\n";
}

void BankAccount::withdraw(long amount)
{
    if (amount <= balance) {
        balance -= amount;
    } else {
        std::cout << "Can't withdraw amount > balance\n";
    }
}

// 6
// constructor is called when creating an object
// destructor - when it's being destroyed either automatically or by using
// delete

// 8
// default constructor is a constructor either without arguments or with all
// default arguments it allows you to initialize class data members to some
// values

// 9
#include <string>

class Stock
{
  private:
    std::string company;
    int shares;
    double share_val;
    double total_val;
    void set_tot() { total_val = shares * share_val; }

  public:
    //  Stock();        // default constructor
    Stock(const std::string& co, long n = 0, double pr = 0.0);
    ~Stock(); // do-nothing destructor
    void buy(long num, double price);
    void sell(long num, double price);
    void update(double price);
    void show() const;
    const Stock& topval(const Stock& s) const;

    double getShareVal() const { return share_val; }
    double getTotalVal() const { return total_val; }
    int getShares() const { return shares; }
    const std::string& getCompany() const { return company; }
};

// 10
// this is a pointer to the class object
// *this is the class object