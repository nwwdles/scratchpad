// randwalk.cpp -- using the Vector class
// compile with the vect.cpp file
#include "vect.h"
// #include <climits> // __INT_MAX__
#include <cstdlib> // rand(), srand() prototypes
#include <ctime>   // time() prototype
#include <iostream>

int main()
{
    using namespace std;
    using VECTOR::Vector;
    srand(time(0)); // seed random-number generator
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps = 0;
    double target;
    double dstep;
    cout << "Enter target distance (q to quit): ";
    while (cin >> target) {
        cout << "Enter step length: ";
        if (!(cin >> dstep))
            break;
        int tests;
        cout << "Enter number of trials: ";
        cin >> tests;
        int maxsteps = 0;
        int minsteps = -1;
        double avgsteps = 0.0;
        long sumsteps = 0;
        for (int i = 0; i < tests; i++) {
            /* code */
            while (result.magval() < target) {
                direction = rand() % 360;
                step.reset(dstep, direction, Vector::POL);
                result = result + step;
                steps++;
            }

            sumsteps += steps;
            maxsteps = (steps > maxsteps) ? steps : maxsteps;
            minsteps = (steps < minsteps || minsteps == -1) ? steps : minsteps;

            // cout << "After " << steps
            //      << " steps, the subject "
            //         "has the following location:\n";
            // cout << result << endl;
            // result.polar_mode();
            // cout << " or\n" << result << endl;
            // cout << "Average outward distance per step = "
            //      << result.magval() / steps << endl;
            steps = 0;
            result.reset(0.0, 0.0);
        }
        avgsteps = sumsteps / tests;

        cout << "Results of " << tests << "trials: \n";
        cout << "Avg steps: " << avgsteps << "\n";
        cout << "Max steps: " << maxsteps << "\n";
        cout << "Min steps: " << minsteps << "\n";

        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    /* keep window open
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cin.get();
    */
    return 0;
}
