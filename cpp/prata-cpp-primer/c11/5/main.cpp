#include "stonewt.h"
#include <iostream>

int main(int argc, char const* argv[])
{
    Stonewt s = 340.1;
    std::cout << s;

    // test addition subtraction multiplication
    s = s + 1;
    std::cout << s;

    s = s * 1.1;
    std::cout << s;

    s = s - 1;
    std::cout << s;

    // test modes printing
    s.stn_mode();
    std::cout << s;

    s.int_mode();
    std::cout << s;

    s.flt_mode();
    std::cout << s;

    return 0;
}
