// stonewt.h -- definition for the Stonewt class
#ifndef STONEWT_H_
#define STONEWT_H_
#include <iostream>
class Stonewt
{
  private:
    enum { Lbs_per_stn = 14 };     // pounds per stone
    enum States { STN, INT, FLT }; // pounds per stone
    int stone;                     // whole stones
    double pds_left;               // fractional pounds
    double pounds;                 // entire weight in pounds
    States state = FLT;

  public:
    Stonewt(double lbs);          // constructor for double pounds
    Stonewt(int stn, double lbs); // constructor for stone, lbs
    Stonewt();                    // default constructor
    ~Stonewt();
    void show_lbs() const; // show weight in pounds format
    void show_stn() const; // show weight in stone format
    void stn_mode() { state = STN; }
    void int_mode() { state = INT; }
    void flt_mode() { state = FLT; }

    Stonewt operator+(const Stonewt& s) const
    {
        return Stonewt(s.pounds + pounds);
    };
    Stonewt operator-(const Stonewt& s) const
    {
        return Stonewt(pounds - s.pounds);
    };
    // Stonewt operator-() const;
    Stonewt operator*(double n) const { return Stonewt(pounds * n); };

    friend std::ostream& operator<<(std::ostream& os, const Stonewt& s);
};
#endif
