#include "stonewt.h"
#include <iostream>

int main(int argc, char const* argv[])
{
    Stonewt s = 11;
    s.show_lbs();
    Stonewt weighs[6]{6, 72, 43};
    double wt;
    for (int i = 3; i < 6; i++) {
        std::cout << "Please enter a value: ";
        std::cin >> wt;
        weighs[i] = wt;
    }

    Stonewt* min = weighs;
    Stonewt* max = weighs;
    int larger = weighs[0] > s;

    for (int i = 1; i < 6; i++) {
        if (weighs[i] >= s) {
            larger++;
        }
        if (weighs[i] < *min) {
            min = weighs + i;
        } else if (weighs[i] > *max) {
            max = weighs + i;
        }
    }

    std::cout << "Max: \n";
    max->show_lbs();
    std::cout << "Min: \n";
    min->show_lbs();
    std::cout << "Greater or equal to 11 stone: " << larger << " elements.\n";

    return 0;
}
