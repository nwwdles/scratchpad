#ifndef COMPL_H_
#define COMPL_H_
#include <iostream>

class Complex
{
  private:
    double re;
    double im;

  public:
    Complex(double r = 0, double i = 0);

    // math
    Complex operator+(Complex& c) const
    {
        return Complex(re + c.re, im + c.im);
    };
    Complex operator~() const { return Complex(re, -im); };
    Complex operator-() const { return Complex(-re, -im); };
    Complex operator-(Complex& c) const
    {
        return Complex(re - c.re, im - c.im);
    };
    friend Complex operator*(double a, Complex& c) { return c * a; };
    Complex operator*(double a) const { return Complex(re * a, im * a); };
    Complex operator*(Complex& c) const
    {
        return Complex(re * c.re - im * c.im, im * c.re + re * c.im);
    };

    // iostream
    friend std::ostream& operator<<(std::ostream& os, const Complex& c)
    {
        os << "(" << c.re << "," << c.im << "i)";
        return os;
    };
    friend std::istream& operator>>(std::istream& is, Complex& c)
    {
        std::cout << "Re: ";
        is >> c.re;
        if (is) {
            std::cout << "Im: ";
            is >> c.im;
        }
        return is;
    };
};

// Complex::~Complex() {}
#endif