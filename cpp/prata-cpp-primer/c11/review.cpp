// 1
// see review-1
// Stonewt operator*(double n){return Stonewt(n * pounds);};

// 2
// friend function is not a member function but can access private members of
// an object when used to overload operators, both objects are supplied as
// arguments, as opposed to having one of them invoking the method some
// operators can only be overloaded by member functions

// 3
// a nonmember function has to be a friend to access private members
// but it doesn't have to be a friend to access public members

// 4
// see review-1?
// #include <cmath>
// Stonewt operator*(double n){return Stonewt(stone * n + (pds_left * n)/14,
// fmod((pds_left * n),14));};

// 5
// sizeof . .* :: ?: typeid const_cast dynamic_cast reinterpret_cast static_cast

// 6
// they can only be overloaded by member functions

// 7
// see review-7
// Vector::operator double() {
//     return sqrt(x * x + y * y);
// }
