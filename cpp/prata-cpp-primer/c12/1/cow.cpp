#include "cow.h"
#include <cstring>
#include <iostream>
Cow::Cow() { hobby = nullptr; }
Cow::Cow(const char* nm, const char* ho, double wt)
{
    strncpy(name, nm, 20);
    hobby = new char[strlen(ho) + 1];
    strcpy(hobby, ho);
    weight = wt;
}
Cow::Cow(const Cow& c)
{
    strncpy(name, c.name, 20);
    hobby = new char[strlen(c.hobby) + 1];
    strcpy(hobby, c.hobby);
    weight = c.weight;
}

Cow::~Cow() { delete[] hobby; }
Cow& Cow::operator=(const Cow& c)
{
    if (&c == this)
        return *this;
    delete[] hobby;
    hobby = new char[strlen(c.hobby) + 1];
    strcpy(hobby, c.hobby);
    return *this;
}

void Cow::ShowCow() const
{
    using std::cout;
    cout << "name: " << name << " weight: " << weight << " hobby: " << hobby
         << '\n';
}