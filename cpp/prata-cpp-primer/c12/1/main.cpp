#include "cow.h"
#include <iostream>

int main(int argc, char const* argv[])
{
    Cow c = Cow("Mow", "Mowing", 366);
    c.ShowCow();
    Cow newcow = c;
    newcow.ShowCow();
    Cow anothercow = Cow();
    anothercow.ShowCow();
    return 0;
}
