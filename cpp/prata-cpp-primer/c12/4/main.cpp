#include "stack.h"
#include <iostream>

int main(int argc, char const* argv[])
{
    Stack mystack(10);
    mystack.push(1);
    mystack.push(13);
    mystack.push(45);
    mystack.push(76);
    unsigned long myint = 77;
    mystack.pop(myint);
    std::cout << myint << "\n";

    Stack newstack(mystack); // copy
    newstack.pop(myint);
    std::cout << myint << "\n";

    newstack = mystack; // assignment
    newstack.pop(myint);
    std::cout << myint << "\n";
    return 0;
}
