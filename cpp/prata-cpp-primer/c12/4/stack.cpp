#include "stack.h"
#include <iostream>
Stack::Stack(int n)
{
    size = n;
    pitems = new Item[size];
    top = 0;
}
Stack::Stack(const Stack& s)
{
    size = s.size;
    pitems = new Item[size];
    for (int i = 0; i < size; ++i) {
        pitems[i] = s.pitems[i];
    }
    top = s.top;
}
Stack::~Stack() { delete[] pitems; }
bool Stack::isempty() const { return (top == 0); }
bool Stack::isfull() const { return (top == size); }
bool Stack::push(const Item& item)
{
    if (isfull())
        return false;
    pitems[top] = item;
    // std::cout << "pushed " << pitems[top] << "\n";
    top++;
    return true;
}
bool Stack::pop(Item& item)
{
    if (isempty())
        return false;
    // std::cout << "popping " << pitems[top - 1] << "\n";
    item = pitems[top - 1];
    top--;
    return true;
}

Stack& Stack::operator=(const Stack& st)
{
    if (&st == this) {
        return *this;
    }
    delete[] pitems;
    size = st.size;
    pitems = new Item[size];
    for (int i = 0; i < size; ++i) {
        pitems[i] = st.pitems[i];
    }
    top = st.top;
    return *this;
}