/*
1a. it doesn't allocate any memory for the string, so calling delete in the
destructor may/will result in error. the other option is to set pointer to
nullptr

1b. it doesn't copy the string but it copies a pointer to it so further
operations on string may change the original data

1c. the problems may arise if there's not enough place for the string

2.
a. data may be left in memory after object deletion - don't forget to use delete
in the destructor

b. copy constructor is using memberwise copying, so it will
copy the pointer but not data - have to override the copy constructor

c.assignment operator is using memberwise copying, so you need to make it make a
deep copy too.

3.
constructor,
destructor,
copy constructor,
assignment operator.
NOTE: also a default address operator

the copy constructor and the assignment operator use memberwise assignment.

4. see review4.cpp

5a.
#1 default class constructor
#2 Golfer(const char * name, int g= 0);
#3 Golfer(const char * name, int g= 0);
#4 default
#5 copy constructor
#6 Golfer(const char * name, int g= 0)
#7 copy constructor NOTE: assignment operator
#8 Golfer(const char * name, int g= 0) NOTE: then assignment operator

5b it requires an assignment operator
*/