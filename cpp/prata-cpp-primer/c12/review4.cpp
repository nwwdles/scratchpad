/*
4.
a. operator<< calls itself instead of printing useful info like values of
members of nifty
b. single colon is used instead of ::
c. instead of using strcpy(s, personality), it assigns a pointer to personality,
forgetting the address of a variable made by using new resulting in memory leak.
d. no destructor is provided, which is needed since new is used in constructor
e. no methods are public, including constructor
f. using ostream instead of std::ostream without using declaration.
etc.
*/
#include <cstring>
#include <iostream>

using std::ostream;

class nifty
{
    // data
    char* personality;
    int talents;

    // make assignment and copy constructor private
    nifty& operator=(nifty& n);
    nifty(nifty& n);

  public:
    // methods
    nifty();
    nifty(const char* s);
    ~nifty();
    friend ostream& operator<<(ostream& os, const nifty& n);
};

nifty::nifty()
{
    personality = NULL;
    talents = 0;
}
nifty::~nifty() { delete[] personality; }
nifty::nifty(const char* s)
{
    personality = new char[strlen(s + 1)];
    strcpy(personality, s);
    talents = 0;
}
ostream& operator<<(ostream& os, const nifty& n)
{
    os << n.personality;
    return os;
}

int main(int argc, char const* argv[])
{
    nifty mynif("testy");
    std::cout << mynif << "\n";
    return 0;
}
