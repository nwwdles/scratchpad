#include <iostream>
using namespace std;
#include "classic.h"
#include <cstring>
Cd::Cd(char* art, char* lbl, int sel, double playt)
    : selections(sel), playtime(playt)
{
    strncpy(performers, art, 50);
    strncpy(label, lbl, 20);
};

void Cd::Report() const
{
    std::cout << "Performers: " << performers << "\n";
    std::cout << "Label: " << label << "\n";
    std::cout << "Selections: " << selections << "\n";
    std::cout << "Playtime: " << playtime << "\n";
}

Cd::Cd()
{
    selections = 0;
    playtime = 0;
}
Cd::~Cd() {}
Classic::~Classic() {}
Classic::Classic() {}

Classic::Classic(char* prim, char* art, char* lbl, int sel, double playt)
    : Cd::Cd(art, lbl, sel, playt)
{
    strncpy(primary, prim, 50);
}

void Classic::Report() const
{
    Cd::Report();
    std::cout << "Primary: " << primary << "\n";
}