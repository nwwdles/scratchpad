#include <iostream>
using namespace std;
#include "classic.h"
#include <cstring>
Cd::Cd(char* art, char* lbl, int sel, double playt)
    : selections(sel), playtime(playt)
{
    performers = new char[strlen(art)];
    label = new char[strlen(lbl)];
    strcpy(performers, art);
    strcpy(label, lbl);
};

Cd::Cd(const Cd& d)
{
    performers = new char[strlen(d.performers)];
    label = new char[strlen(d.label)];
    strcpy(performers, d.performers);
    strcpy(label, d.label);
};

Cd::Cd()
{
    performers = nullptr;
    label = nullptr;
    selections = 0;
    playtime = 0;
}

Cd::~Cd()
{
    delete[] performers;
    delete[] label;
}

Cd& Cd::operator=(const Cd& d)
{
    if (&d == this) {
        return *this;
    }
    delete[] performers;
    delete[] label;
    performers = new char[strlen(d.performers)];
    label = new char[strlen(d.label)];
    strcpy(performers, d.performers);
    strcpy(label, d.label);
    return *this;
}

void Cd::Report() const
{
    std::cout << "Performers: " << performers << "\n";
    std::cout << "Label: " << label << "\n";
    std::cout << "Selections: " << selections << "\n";
    std::cout << "Playtime: " << playtime << "\n";
}
// classic

Classic::Classic() { primary = nullptr; }

Classic::Classic(char* prim, char* art, char* lbl, int sel, double playt)
    : Cd::Cd(art, lbl, sel, playt)
{
    primary = new char[strlen(prim)];
    strcpy(primary, prim);
}

Classic::Classic(const Classic& d)
{
    primary = new char[strlen(d.primary)];
    strcpy(primary, d.primary);
}

Classic::~Classic() { delete[] primary; }

Classic& Classic::operator=(const Classic& d)
{
    if (&d == this) {
        return *this;
    }
    Cd::operator=(d);
    delete[] primary;
    primary = new char[strlen(d.primary)];
    strcpy(primary, d.primary);
    return *this;
}

void Classic::Report() const
{
    Cd::Report();
    std::cout << "Primary: " << primary << "\n";
}