#ifndef CLASSIC_H_
#define CLASSIC_H_
// base class
class Cd
{ // represents a CD disk
  private:
    char* performers;
    char* label;
    int selections;
    // number of selections
    double playtime; // playing time in minutes
  public:
    virtual ~Cd();
    Cd();
    Cd(char* s1, char* s2, int n, double x);
    Cd(const Cd& d);
    Cd& operator=(const Cd& d);

    virtual void Report() const; // reports all CD data
};

class Classic : public Cd
{
  private:
    char* primary;

  public:
    virtual ~Classic();
    Classic();
    Classic(char* prim, char* art, char* label, int sel, double playt);
    Classic(const Classic& d);
    Classic& operator=(const Classic& d);

    virtual void Report() const;
};
#endif // !CLASSIC_H_