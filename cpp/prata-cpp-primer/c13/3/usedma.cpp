// usedma.cpp -- inheritance, friends, and DMA
// compile with dma.cpp
#include "dma.h"
#include <iostream>

int main()
{
    using std::cout;
    using std::endl;

    baseDMA shirt("Portabelly", 8);
    lacksDMA balloon("red", "Blimpo", 4);
    hasDMA map("Mercator", "Buffalo Keys", 5);
    cout << "Displaying baseDMA object:\n";
    cout << shirt << endl;
    cout << "Displaying lacksDMA object:\n";
    cout << balloon << endl;
    cout << "Displaying hasDMA object:\n";
    cout << map << endl;
    lacksDMA balloon2(balloon);
    cout << "Result of lacksDMA copy:\n";
    cout << balloon2 << endl;
    hasDMA map2;
    map2 = map;
    cout << "Result of hasDMA assignment:\n";
    cout << map2 << endl;

    const int NUM = 3;

    abstractDMA* dmas[NUM];

    int rating;
    char label[50];
    int type;

    for (int i = 0; i < NUM; i++) {
        cout << "Enter DMA type. 1 for baseDMA, 2 for lackDMA, 3 for hasDMA: ";
        std::cin >> type;
        std::cin.get();
        cout << "Enter label: ";
        std::cin.getline(label, 50);
        cout << "Enter rating: ";
        std::cin >> rating;
        std::cin.get();
        if (type == 1) {
            dmas[i] = new baseDMA(label, type);
        } else if (type == 2) {
            cout << "Enter color: ";
            char color[40];
            std::cin.getline(color, 40);
            dmas[i] = new lacksDMA(color, label, type);
        } else {
            cout << "Enter style: ";
            char style[40];
            std::cin.getline(style, 40);
            dmas[i] = new hasDMA(style, label, type);
        }
    }

    for (int i = 0; i < NUM; i++) {
        dmas[i]->View();
        cout << "\n";
    }
    return 0;
}
