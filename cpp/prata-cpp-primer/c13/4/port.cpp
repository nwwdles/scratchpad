#include "port.h"
#include <cstring>
#include <iostream>

// port

Port::Port(const char* br, const char* st, int b)
{
    brand = new char[strlen(br)];
    strcpy(brand, br);
    strncpy(style, st, 20);
    bottles = b;
}

Port::Port(const Port& p)
{
    brand = new char[strlen(p.brand)];
    strcpy(brand, p.brand);
    strncpy(style, p.style, 20);
    bottles = p.bottles;
}

Port& Port::operator=(const Port& p)
{
    if (&p == this) {
        return *this;
    }
    delete[] brand;
    brand = new char[strlen(p.brand)];
    strcpy(brand, p.brand);
    strncpy(style, p.style, 20);
    bottles = p.bottles;
    return *this;
};

Port& Port::operator+=(int b) { bottles += b; };

Port& Port::operator-=(int b) { bottles -= b; };

void Port::Show() const
{
    std::cout << "Brand: " << brand << "\n";
    std::cout << "Kind: " << style << "\n";
    std::cout << "Bottles: " << bottles << "\n";
}

ostream& operator<<(ostream& os, const Port& p)
{
    os << p.brand << ", " << p.style << ", " << p.bottles;
}

// vintageport

VintagePort::VintagePort() : Port()
{
    nickname = nullptr;
    year = 0;
};
VintagePort::VintagePort(const char* br, int b, const char* nn, int y)
    : Port(br, "vintage", b)
{
    nickname = new char[strlen(nn)];
    strcpy(nickname, nn);
    year = y;
};

VintagePort::VintagePort(const VintagePort& vp) : Port(vp)
{
    nickname = new char[strlen(vp.nickname)];
    strcpy(nickname, vp.nickname);
    year = vp.year;
}
VintagePort& VintagePort::operator=(const VintagePort& vp)
{
    if (&vp == this) {
        return *this;
    }
    Port::operator=(vp);
    delete[] nickname;
    nickname = new char[strlen(vp.nickname)];
    strcpy(nickname, vp.nickname);
    year = vp.year;
    return *this;
};
void VintagePort::Show() const
{
    Port::Show();
    std::cout << "Nickname: " << nickname << "\n";
    std::cout << "Year: " << year << "\n";
};
ostream& operator<<(ostream& os, const VintagePort& vp)
{
    os << (Port&)vp << ", ";
    os << vp.nickname << ", " << vp.year;
};

int main(int argc, char const* argv[])
{
    VintagePort myvintage("This is a brand", 10, "Nickynamey", 1994);
    myvintage.Show();
    std::cout << myvintage;

    Port nonvintage("Brand", "nonvintage", 10);
    nonvintage.Show();
    return 0;
}
