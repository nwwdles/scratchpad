/*
1. Derived class inherits the data members and most methods of the class.

2. Derived class doesn't inherit constructors, destructor, assignment operators,
friend functions.

3. If return type were void, it wouldn't allow for statements like a = (b = c)
If return type were baseDMA, it would create a new object after assignment,
resulting in worse performance.

4. Base class constructor, then derived class constructor.
Derived class destructor, then base class destructor.

5. Yes, it requires it's own constructors.

6. Derived class method is called, unless scope resolution operator is
specifically used to call base class method.

7. Assignment operator for a derived class should be defined if derived class
uses dynamic memory allocation.

8. Yes, pointer to a base class can point to a derived class object.
Pointer to a derived class can point to a base class object only if with a type
cast but it's not safe.

9. Yes, you can assign an object of a derived class to a base class object
No, unless you define a conversion operator or an assignment operator.

10. Because upcasting is allowed. Object of a derived class 'is' an object of a
base class. So references to a base class can point to objects of a derived
class.

11. Because a derived class object contains a base class object in itself. It is
then copied into a new object.

12. Passing by reference is more performant, it doesn't create temporary
objects.

13. if head() is a nonvirtual method, ph->head() is interpreted based on a
pointer type. Since ph is a pointer to Corporation, Corporation::head() is
called. If head() is a virtual method, PublicCorporation::head() is called.

14.
- Constructor doesn't call base class constructor but it's fine, default one is
called anyway.
- all_sq_ft += kit_sq_ft does nothing,
- there's no way to change kit_sq_ft
- no virtual destructor is declared but it's fine
- area() in the derived class isn't set as virtual but it's probably fine
- no using std::cout, so cout << s; will fail.
- house isn't really a type of kitchen.

*/