#include "winec.h"
#include <iostream>
#include <valarray>
void Wine::Show()
{
    std::cout << "Wine:" << Label() << "\n";
    std::cout.width(4);
    std::cout << "Year";
    std::cout.width(10);
    std::cout << "Bottles"
              << "\n";
    for (int i = 0; i < numyears; i++) {
        std::cout.width(4);
        std::cout << bottles.first()[i];
        std::cout.width(10);
        std::cout << bottles.second()[i] << "\n";
    }
}

void Wine::GetBottles()
{
    for (int i = 0; i < numyears; i++) {
        std::cout << "Enter year: ";
        std::cin >> bottles.first()[i];
        std::cout << "Enter bottles for that year: ";
        std::cin >> bottles.second()[i];
    }
}
Wine::Wine(const char* l, int y, const int yr[], const int bot[])
{
    label = l;
    numyears = y;
    bottles.first() = std::valarray<int>(yr, y);
    bottles.second() = std::valarray<int>(bot, y);
    // for (int i = 0; i < y; i++) {
    // bottles.first[i] = yr[i];
    // bottles.second[i] = bot[i];
    // }
    // bottles.first = yr;
    // bottles.second = bot;
}
Wine::Wine(const char* l, int y)
{
    label = l;
    numyears = y;
    bottles.first() = std::valarray<int>(y);
    bottles.second() = std::valarray<int>(y);
}