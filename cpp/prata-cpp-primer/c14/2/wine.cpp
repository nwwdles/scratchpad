#include "winec.h"
#include <iostream>
#include <valarray>
void Wine::Show()
{
    std::cout << "Wine: " << Label() << "\n";
    std::cout.width(4);
    std::cout << "Year";
    std::cout.width(10);
    std::cout << "Bottles"
              << "\n";
    for (int i = 0; i < numyears; i++) {
        std::cout.width(4);
        std::cout << arpair::first()[i];
        std::cout.width(10);
        std::cout << arpair::second()[i] << "\n";
    }
}

void Wine::GetBottles()
{
    for (int i = 0; i < numyears; i++) {
        std::cout << "Enter year: ";
        std::cin >> arpair::first()[i];
        std::cout << "Enter bottles for that year: ";
        std::cin >> arpair::second()[i];
    }
}
Wine::Wine(const char* l, int y, const int yr[], const int bot[])
{
    (std::string&)* this = l;
    numyears = y;
    arpair::first() = std::valarray<int>(yr, y);
    arpair::second() = std::valarray<int>(bot, y);
}
Wine::Wine(const char* l, int y)
{
    (std::string&)* this = l;
    numyears = y;
    arpair::first() = std::valarray<int>(y);
    arpair::second() = std::valarray<int>(y);
}