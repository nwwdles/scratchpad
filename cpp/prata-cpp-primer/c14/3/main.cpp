// workmi.cpp -- multiple inheritance
// compile with workermi.cpp
#include "queuetp.h"
#include "workermi.h"
#include <cstring>
#include <iostream>

const int SIZE = 5;

int main()
{
    using std::cin;
    using std::cout;
    using std::endl;
    using std::strchr;

    Queue<Worker*> workers;

    int ct;
    Worker* w;
    for (ct = 0; ct < SIZE; ct++) {
        char choice;
        cout << "Enter the employee category:\n"
             << "w: waiter  s: singer  "
             << "t: singing waiter  q: quit\n";
        cin >> choice;
        while (strchr("wstq", choice) == NULL) {
            cout << "Please enter a w, s, t, or q: ";
            cin >> choice;
        }
        if (choice == 'q')
            break;
        switch (choice) {
        case 'w':
            w = new Waiter;
            break;
        case 's':
            w = new Singer;
            break;
        case 't':
            w = new SingingWaiter;
            break;
        }
        cin.get();
        w->Set();
        workers.enqueue(w);
    }

    cout << "\nHere is your staff:\n";
    int i;
    Worker* w;
    for (i = 0; i < ct; i++) {
        cout << endl;
        workers.dequeue(w);
        w->Show();
        delete w;
    }
    cout << "Bye.\n";
    // cin.get();
    // cin.get();
    return 0;
}
