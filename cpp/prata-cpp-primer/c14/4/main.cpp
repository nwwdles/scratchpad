// workmi.cpp -- multiple inheritance
// compile with workermi.cpp
#include "person.h"
#include "queuetp.h"
#include <cstring>
#include <iostream>

const int SIZE = 5;

int main()
{
    using std::cin;
    using std::cout;
    using std::endl;
    using std::strchr;

    Queue<Person*> people;

    int ct;
    Person* w;
    for (ct = 0; ct < SIZE; ct++) {
        char choice;
        cout << "Enter the employee category:\n"
             << "g: gunslinger  p: person  b: bad dude"
             << "o: pokerplayer  q: quit\n";
        cin >> choice;
        while (strchr("gpboq", choice) == NULL) {
            cout << "Please enter g, p, b, o, or q: ";
            cin >> choice;
        }
        if (choice == 'q')
            break;
        switch (choice) {
        case 'g':
            w = new Gunslinger;
            break;
        case 'p':
            w = new Person;
            break;
        case 'b':
            w = new BadDude;
            break;
        case 'o':
            w = new PokerPlayer;
            break;
        }
        cin.get();
        w->Get();

        people.enqueue(w);
    }

    cout << "\nHere is your people:\n";
    int i;
    for (i = 0; i < ct; i++) {
        cout << endl;
        people.dequeue(w);
        w->Show();
        delete w;
    }
    cout << "Bye.\n";
    // cin.get();
    // cin.get();
    return 0;
}
