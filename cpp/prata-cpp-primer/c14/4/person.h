#ifndef PERSON_H_
#define PERSON_H_

#include <cstdlib>
#include <iostream>
#include <string>
class Person
{
    std::string firstName;
    std::string lastName;

  public:
    virtual ~Person(){};
    Person() : firstName("No"), lastName("Nope"){};
    Person(std::string& fname, std::string& lname)
        : firstName(fname), lastName(lname){};
    virtual void Show() { std::cout << lastName << ", " << firstName << "\n"; }
    void GetName()
    {
        std::cout << "Enter first name: ";
        getline(std::cin, firstName);
        std::cout << "Enter last name: ";
        getline(std::cin, lastName);
        // while (std::cin.get() != '\n')
        // continue;
    }
    virtual void Get() { GetName(); }
};

class Gunslinger : public virtual Person
{
  private:
    double drawTime;
    int notches;

  public:
    virtual ~Gunslinger(){};
    Gunslinger() : drawTime(0), notches(0){};
    Gunslinger(std::string& fname, std::string& lname, double dt, int n)
        : Person(fname, lname), drawTime(dt), notches(n){};
    double Draw() { return drawTime; };
    virtual void Show()
    {
        Person::Show();
        std::cout << drawTime << ", " << notches << "\n";
    }
    virtual void GetDraw()
    {
        std::cout << "Enter draw time:";
        std::cin >> drawTime;
        std::cout << "Enter notches:";
        std::cin >> notches;
    }
    virtual void Get()
    {
        GetName();
        GetDraw();
    }
};

class PokerPlayer : public virtual Person
{
  private:
  public:
    virtual ~PokerPlayer(){};
    PokerPlayer(){};
    PokerPlayer(std::string& fname, std::string& lname)
        : Person(fname, lname){};
    int Draw() { return rand() % 52 + 1; }
};

class BadDude : public Gunslinger, public PokerPlayer
{
  public:
    BadDude(){};
    virtual ~BadDude(){};
    double Gdraw() { return Gunslinger::Draw(); }
    int Cdraw() { return PokerPlayer::Draw(); }
    void Show()
    {
        // PokerPlayer::Show();
        Gunslinger::Show();
    }
    virtual void Get() { Gunslinger::Get(); }
};

#endif // !PERSON_H_