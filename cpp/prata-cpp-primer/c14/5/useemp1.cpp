// pe14-5.cpp
// useemp1.cpp -- using the abstr_emp classes
#include <iostream>
using namespace std;
#include "emp.h"
int main(void)
{
    employee em("Trip", "Harris", "Thumper");
    cout << em << endl;
    em.ShowAll();

    manager ma("Amorphia", "Spindragon", "Nuancer", 5);
    cout << ma << endl;
    ma.ShowAll();

    fink fi("Matt", "Oggs", "Oiler", "Juno Barr");
    cout << fi << endl;
    fi.ShowAll();
    highfink hf(ma, "Curly Kew"); // recruitment?
    hf.ShowAll();
    cout << "Press a key for next phase:\n";
    cin.get();
    highfink hf2;
    hf2.SetAll();

    cout << "Using an abstr_emp * pointer:\n";
    abstr_emp* tri[4] = {&em, &fi, &hf, &hf2};
    for (int i = 0; i < 4; i++)
        tri[i]->ShowAll();
    return 0;
}

/*
Why is no assignment operator defined?
A default one is defined by compiler that uses member-wise assignment.

Why are ShowAll() and SetAll() virtual?
Because they're meant to be redefined in children classes to show more info.

? Why is abstr_emp a virtual base class?

Why does the highfink class have no data section?
Highfink doesn't require one, it's fine with data members from manager and fink.
Combining those is the main point of highfink.

Why is only one version of operator<<() needed?
Because it only uses portions of the same abstract base class which is present
in the derived classes.

What would happen if the end of the program were replaced with this code?
    abstr_emp tri[4] = {em, fi, hf, hf2};
    for (int i = 0; i < 4; i++)
        tri[i].ShowAll();

It would copy objects into an array and then print info for them. It would be
slower than the version with references.
*/