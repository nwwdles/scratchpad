#include <iostream>
using std::cout;
class Frabjous
{
  private:
    char fab[20];

  public:
    Frabjous(const char* s = "C++") : fab(s) {}
    virtual void tell() { cout << fab; }
};
class Gloam : private Frabjous
{
  private:
    int glip;

  public:
    Gloam(int g = 0, const char* s = "C++");
    Gloam(int g, const Frabjous& f);
    void tell();
};
void Gloam::tell()
{
    Frabjous::tell();
    cout << glip << "\n";
}
Gloam::Gloam(int g = 0, const char* s = "C++") : Frabjous(s), glip(g){};
Gloam::Gloam(int g, const Frabjous& f) : Frabjous(f), glip(g){};