/*
1.
public
private
public
private
public for person, private for automobile
*/

#include "arraytp.h"
#include "stacktp.h"
#include <string>

// 4
class Worker
{
};

class Stack<Worker*>
{
  private:
    enum { MAX = 10 };  // constant specific to class
    Worker* items[MAX]; // holds stack items
    int top;            // index for top stack item
  public:
    Stack();
    bool isempty();
    bool isfull();
    bool push(const Worker*& item); // add item to stack
    bool pop(Worker*& item);        // pop top into item
};

// 5
using std::string;
int main(int argc, char const* argv[])
{
    // array of string objects
    ArrayTP<string, 10> as;
    // stack of arrays of double
    Stack<ArrayTP<double, 10>> sdb;
    // array of stacks of pointers to worker objects
    ArrayTP<Stack<Worker*>, 10> aswp;
    return 0;
}

/*
When using multiple inheritance, one virtual base class object is shared across
all immediate parents that inherit from the base class. Each immediate parent
that doesn't declare it as virtual will have it's own object of the base class.
Declaring a class as virtual requires calling its' constructor manually in the
derived classes (if it isn't virtual, you only call the immediate parent
constructor), otherwise a default constructor is used.
*/