#include "tv.h"
#include <iostream>

int main(int argc, char const* argv[])
{
    Tv tv;
    Remote rem; // creating remote in a normal mode

    // switch on the tv
    tv.onoff();
    rem.print();
    tv.toggleremotemode(rem);
    rem.print();

    // switch off the tv
    tv.onoff();
    tv.toggleremotemode(rem);
    rem.print();

    return 0;
}
