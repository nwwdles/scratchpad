// exc_mean.h  -- exception classes for hmean(), gmean()
#include <iostream>
#include <stdexcept>
class bad_mean : public std::logic_error
{
  private:
    double v1;
    double v2;

  public:
    bad_mean(double a = 0, double b = 0, const char* s = "error uwu")
        : logic_error(s), v1(a), v2(b){};
    void mesg();
};

inline void bad_mean::mesg()
{
    std::cout << what();
    std::cout << "Values used:" << v1 << ", " << v2 << "\n";
}

class bad_hmean : public bad_mean
{
  private:
    // double v1;
    // double v2;

  public:
    bad_hmean(double a = 0, double b = 0)
        : bad_mean(a, b, "hmean() invalid arguments: a = -b\n"){};
    // void mesg();
};

// inline void bad_hmean::mesg()
// {
//     std::cout << "hmean(" << v1 << ", " << v2 << "): "
//               << "invalid arguments: a = -b\n";
// }

class bad_gmean : public bad_mean
{
  public:
    // double v1;
    // double v2;
    bad_gmean(double a = 0, double b = 0)
        : bad_mean(a, b, "gmean() arguments should be >= 0\n"){};
    // const char* mesg();
};

// inline const char* bad_gmean::mesg()
// {
//     return "gmean() arguments should be >= 0\n";
// }
