/*
1a. missing the world class

1b. need to decalre muff before using it in a function declaration (forward
declaration)

1c. use forward declaration for muff, then declare cuff, then muff

2. no, it's not possible since one of the method declarations is preceding the
other, and compiler doesn't have enough info about one of the classes

3. sauce class only has the public constructor so it's useless and it's info is
inaccessible

4. throw rises the execution point all the way up to the try block (if it's
present) involving stack unwinding and skipping the rest of the try block into
the catch block, return - jumps up one function at a time.

5. the most derived class first, the most basic class last. because base class
would match both itself and derived classes.

6. sample 1 matches both superb and magnificent classes, while sample 2 matches
just superb class.

7. static cast allows unsafe casting (downcasting), also numeric conversions
(double to int, etc)
*/