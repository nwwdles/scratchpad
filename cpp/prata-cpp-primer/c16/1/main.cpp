#include <algorithm>
#include <iostream>
#include <string>
bool isPali(std::string& s)
{
    // std::string temp = s;
    // std::reverse(temp.begin(), temp.end());
    // return (s == temp);

    for (int i = 0; i < s.size() / 2; i++) {
        // if (s[i] != s[s.size() - i - 1]) {
        if (*(s.begin() + i) != *(s.rbegin() + i)) {
            return false;
        }
    }
    return true;
}

int main(int argc, char const* argv[])
{
    /* code */
    std::string s;
    while (std::cin) {
        std::cout << "Please enter a string: ";
        getline(std::cin, s);
        // std::cin >> s;
        std::cout << (isPali(s) ? "It's a palindrome." : "Not a palindrome.")
                  << "\n";
    }
    return 0;
}
