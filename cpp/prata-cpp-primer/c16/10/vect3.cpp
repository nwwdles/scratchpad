// vect3.cpp -- using STL functions
#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

struct Review {
    std::string title;
    int rating;
    double price;
};

typedef std::shared_ptr<Review> revptr;

bool operator<(const revptr& r1, const revptr& r2);
bool worseThan(const revptr& r1, const revptr& r2);
bool FillReview(Review& rr);
void ShowReview(const revptr& rr);
int main()
{
    using namespace std;

    vector<shared_ptr<Review>> books;
    while (true) {
        shared_ptr<Review> temp{new Review};
        if (FillReview(*temp)) {
            books.push_back(temp);
        } else {
            break;
        }
    }
    if (books.size() > 0) {
        cout << "Thank you. You entered the following " << books.size()
             << " ratings:\n"
             << "Rating\tBook\n";
        for_each(books.begin(), books.end(), ShowReview);

        sort(books.begin(), books.end());
        cout << "Sorted by title:\nRating\tBook\n";
        for_each(books.begin(), books.end(), ShowReview);

        sort(books.begin(), books.end(), worseThan);
        cout << "Sorted by rating:\nRating\tBook\n";
        for_each(books.begin(), books.end(), ShowReview);

        random_shuffle(books.begin(), books.end());
        cout << "After shuffling:\nRating\tBook\n";
        for_each(books.begin(), books.end(), ShowReview);
    } else
        cout << "No entries. ";
    cout << "Bye.\n";
    // cin.get();
    return 0;
}

bool operator<(const revptr& r1, const revptr& r2)
{
    if (r1->title < r2->title)
        return true;
    else if (r1->title == r2->title && r1->rating < r2->rating)
        return true;
    else
        return false;
}

bool worseThan(const revptr& r1, const revptr& r2)
{
    if (r1->rating < r2->rating)
        return true;
    else
        return false;
}

bool FillReview(Review& rr)
{
    std::cout << "Enter book title (quit to quit): ";
    std::getline(std::cin, rr.title);
    if (rr.title == "quit")
        return false;
    std::cout << "Enter book rating: ";
    std::cin >> rr.rating;
    if (!std::cin)
        return false;
    // get rid of rest of input line
    while (std::cin.get() != '\n')
        continue;
    return true;
}

void ShowReview(const revptr& rr)
{
    std::cout << rr->rating << "\t" << rr->title << std::endl;
}
