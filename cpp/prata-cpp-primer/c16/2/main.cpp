#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>

std::string strippedString(const std::string& s)
{
    std::string out;
    for (int i = 0; i < s.size(); i++) {
        if (isalpha(s[i])) {
            out += tolower(s[i]);
        }
    }
    return out;
}

bool isPali(std::string& s)
{
    // std::string temp = s;
    // std::reverse(temp.begin(), temp.end());
    // return (s == temp);

    for (int i = 0; i < s.size() / 2; i++) {
        // if (s[i] != s[s.size() - i - 1]) {
        if (*(s.begin() + i) != *(s.rbegin() + i)) {
            return false;
        }
    }
    return true;
}

int main(int argc, char const* argv[])
{
    /* code */
    std::string s;
    while (std::cin) {
        std::cout << "Please enter a string: ";
        // std::cin >> s;
        getline(std::cin, s);
        std::string stripped = strippedString(s);
        std::cout << "Stripped string: " << stripped << "\n";
        std::cout << (isPali(stripped) ? "It's a palindrome."
                                       : "Not a palindrome.")
                  << "\n";
    }
    return 0;
}
