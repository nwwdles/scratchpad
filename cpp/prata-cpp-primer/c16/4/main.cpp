#include <algorithm>
#include <iostream>
#include <iterator>
int reduce(long ar[], int n);

int main(int argc, char const* argv[])
{
    std::ostream_iterator<long, char> oi(std::cout, " ");
    long ar[10] = {10, 30, 40, 55, 60, 18, 68, 30, 10, 40};

    copy(ar, ar + 10, oi);
    std::cout << "\n";
    int num = reduce(ar, 10);
    copy(ar, ar + num, oi);
    std::cout << "\n";

    return 0;
}

int reduce(long ar[], int n)
{
    std::sort(ar, ar + n);
    long* last = std::unique(ar, ar + n);
    return int(last - ar);
}