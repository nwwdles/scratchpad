#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
template <typename T> int reduce(T ar[], int n);

int main(int argc, char const* argv[])
{
    // long
    std::ostream_iterator<long, char> oi(std::cout, " ");
    long ar[10] = {10, 30, 40, 55, 60, 18, 68, 30, 10, 40};

    copy(ar, ar + 10, oi);
    std::cout << "\n";
    int num = reduce(ar, 10);
    copy(ar, ar + num, oi);
    std::cout << "\n";

    // string
    std::ostream_iterator<std::string, char> oiString(std::cout, " ");
    std::string arString[7] = {"ooh", "yea", "yay", "yeeh", "wow", "wow", "yo"};

    copy(arString, arString + 7, oiString);
    std::cout << "\n";
    num = reduce(arString, 7);
    copy(arString, arString + num, oiString);
    std::cout << "\n";

    return 0;
}

template <typename T> int reduce(T ar[], int n)
{
    std::sort(ar, ar + n);
    T* last = std::unique(ar, ar + n);
    return int(last - ar);
}