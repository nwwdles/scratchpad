#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>
std::vector<int> Lotto(int spots, int selected);

int main(int argc, char const* argv[])
{
    std::ostream_iterator<int, char> oi(std::cout, " ");
    std::vector<int> winners;
    winners = Lotto(51, 6);
    std::copy(winners.begin(), winners.end(), oi);
    std::cout << "\n";
    return 0;
}

std::vector<int> Lotto(int spots, int selected)
{
    std::vector<int> values(spots);
    for (int i = 0; i < spots; i++) {
        values[i] = i;
    }
    std::random_shuffle(values.begin(), values.end());
    std::vector<int> out(values.begin(), values.begin() + selected);
    return out;
}