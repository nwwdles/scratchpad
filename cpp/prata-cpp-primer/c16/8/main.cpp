#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <string>
#include <vector>

void populate(std::set<std::string>& v)
{
    std::string s;
    std::cout << "Enter a name (q to end): ";
    getline(std::cin, s);
    while (s != "q") {
        // v.push_back(s);
        v.insert(s);
        std::cout << "Enter a name (q to end): ";
        getline(std::cin, s);
    }
}

int main(int argc, char const* argv[])
{
    std::ostream_iterator<std::string, char> oi(std::cout, " ");
    std::set<std::string> matfriends;
    std::cout << "Mat friends:\n";
    populate(matfriends);
    // sort(matfriends.begin(), matfriends.end());
    std::copy(matfriends.begin(), matfriends.end(), oi);
    std::cout << "\n";

    std::set<std::string> patfriends;
    std::cout << "Pat friends:\n";
    populate(patfriends);
    // sort(patfriends.begin(), patfriends.end());
    std::copy(patfriends.begin(), patfriends.end(), oi);
    std::cout << "\n";

    std::set<std::string> matches;
    std::insert_iterator<std::set<std::string>> ii(matches, matches.begin());
    std::set_intersection(patfriends.begin(), patfriends.end(),
                          matfriends.begin(), matfriends.end(), ii);

    std::cout << "Common friends:\n";
    std::copy(matches.begin(), matches.end(), oi);
    std::cout << "\n";
    return 0;
}
