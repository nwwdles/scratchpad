#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <random>
#include <vector>

int main(int argc, char const* argv[])
{
    // this is a bit different from the exercise, i'm going to repeat tests NUM
    // times
    const int NUM = 10000;
    const int LEN = 5;

    std::ostream_iterator<int, char> oi(std::cout, " ");
    std::vector<int> vi0(LEN);
    for (int i = 0; i < LEN; i++) {
        vi0[i] = rand();
    }

    std::cout << "Original:\n";
    std::copy(vi0.begin(), vi0.end(), oi);

    clock_t start = clock();
    std::list<int> li;
    for (int i = 0; i < NUM; i++) {
        li = std::list<int>(vi0.begin(), vi0.end());
        li.sort();
    }
    clock_t end = clock();

    std::cout << "\nSorted:\n";
    std::copy(li.begin(), li.end(), oi);
    std::cout << "\nBuilt-in method: " << double(end - start) << "\n";

    start = clock();
    for (int i = 0; i < NUM; i++) {
        li = std::list<int>(vi0.begin(), vi0.end());
        std::vector<int> vi(li.begin(), li.end());
        std::sort(vi.begin(), vi.end());
        li = std::list<int>(vi.begin(), vi.end());
    }
    end = clock();

    std::cout << "\nSorted:\n";
    std::copy(li.begin(), li.end(), oi);
    std::cout << "\nVector sort: " << double(end - start) << "\n";

    return 0;
}
