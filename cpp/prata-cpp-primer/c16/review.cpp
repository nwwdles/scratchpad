// 1
#include <cstring>
#include <string>
class RQ1
{
  private:
    std::string st;
    // points to C-style string
  public:
    RQ1() { st = ""; }
    RQ1(const char* s) { st = s; }
    // RQ1(const RQ1& rq) { st = rq.st; }
    // ~RQ1(){};
    // RQ& operator=(const RQ& rq);
    // more stuff
};
// assignment operator and  copy constructor aren't needed anymore

// 2 resizability (no need to use `new` manually), built-in methods

// 3
void stringUpper(std::string& s)
{
    for (int i = 0; i < s.size(); i++) {
        s[i] = toupper(s[i]);
    }
}

// 4
#include <memory>
using std::auto_ptr;
using std::string;
auto_ptr<int> pia(new int[20]); // incorrect, auto_ptr isn't used with new[]
auto_ptr<string>(new string);   // incorrect, no name
int rigue = 7;
auto_ptr<int>
    pr(&rigue); // rigue is an automatic variable, no need for auto pointer
auto_ptr dbl(new double); // no typename?

// 5
// to reach a certain golf club, i would have to take all other clubs out first
// (in the worst case scenario) - no random access

// 6
// set stores sorted unique values, this isn't a use case where it's convenient.
// if scores are stored just as ints, the order of scores/holes is lost, also it
// won't work for the case where two holes have the same score

// 7
// because an iterator is a generalization of a pointer and in some containers
// pointers aren't sufficient, e.g. incrementing a pointer to a linked list
// element will not (likely) make it point to the next list element

// 8
//// because an iterator is a concept, and there isn't a way in which such
//// an iterator family would fit all containers
// this way all algorithms work with pointers too,

// 9
// resizable automatically, has built-in functions e.g. size(), push_back()

// 10
// sort() parts would become invalid, meaning you have to use list.sort() method
// instead also shuffle part requires random acess which list doesn't provide,
// but you can swap contents with a vector, shuffle it, and swap contents back

// 11
// it creates a TooBig functor wit cutoff value = 10, then invokes it with 15
// as an argument. the result is true, 15>10.
