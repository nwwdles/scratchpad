#include <iostream>

int main(int argc, char const* argv[])
{
    int count = 0;
    std::cout << "I will count letters up to the first $. Type: ";
    while (std::cin.peek() != '$') {
        std::cin.get();
        count++;
    }
    std::cout << "Letter count: " << count;
    return 0;
}