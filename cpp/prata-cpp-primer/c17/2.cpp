#include <fstream>
#include <iostream>

int main(int argc, char const* argv[])
{
    std::ofstream ofile("2.txt");
    std::cout << "Write a letter to a file. Ctrl+D to stop:";
    char c;
    while (std::cin.get(c)) {
        ofile.put(c);
    }
    ofile.close();
    return 0;
}