#include <fstream>
#include <iostream>
int main(int argc, char const* argv[])
{
    if (argc < 3) {
        std::cout << "Please input 2 file names.";
    }
    std::ifstream ifile(argv[1]);
    std::ofstream ofile(argv[2]);
    if (ifile.is_open() && ofile.is_open()) {
        char c;
        while (ifile) {
            ifile.get(c);
            ofile.put(c);
        }
    } else {
        std::cout << "Couldn't open a file.";
    }
    ifile.close();
    ofile.close();
    return 0;
}
