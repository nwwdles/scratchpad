#include <fstream>
#include <iostream>
#include <string>
int main(int argc, char const* argv[])
{
    if (argc < 3) {
        std::cout << "Please input 2 file names.";
    }
    std::ifstream in_a(argv[1]);
    std::ifstream in_b(argv[2]);
    std::ofstream ofile("4.txt");
    if (in_a.is_open() && in_b.is_open() && ofile.is_open()) {
        std::string tempa;
        std::string tempb;
        while (in_a || in_b) {
            if (in_a) {
                getline(in_a, tempa);
            } else {
                tempa = "";
            }
            if (in_b) {
                getline(in_b, tempb);
                tempb = " " + tempb;
            } else {
                tempb = "";
            }
            ofile << tempa << tempb << "\n";
        }
    } else {
        std::cout << "Couldn't open a file.";
    }
    in_a.close();
    in_b.close();
    ofile.close();
    return 0;
}
