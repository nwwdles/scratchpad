#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <set>
#include <string>

std::set<std::string> readfriends(const char* fname)
{
    std::set<std::string> friends;
    std::ifstream infile(fname);
    std::string temp;
    if (infile.is_open()) {
        while (infile) {
            getline(infile, temp);
            friends.insert(temp);
        }
    }
    infile.close();
    return friends;
}

int main(int argc, char const* argv[])
{
    // read
    std::set<std::string> matfriends = readfriends("mat.dat");
    std::set<std::string> patfriends = readfriends("pat.dat");

    // display
    std::ostream_iterator<std::string, char> cout_iter(std::cout, "\n");
    std::cout << "Mat's friends:\n";
    std::copy(matfriends.begin(), matfriends.end(), cout_iter);
    std::cout << "Pat's friends:\n";
    std::copy(patfriends.begin(), patfriends.end(), cout_iter);

    // merge
    std::set<std::string> matnpat;
    std::set_intersection(
        patfriends.begin(), patfriends.end(), matfriends.begin(),
        matfriends.end(),
        std::insert_iterator<std::set<std::string>>(matnpat, matnpat.begin()));

    // write
    std::ofstream outfile("matnpat.dat");
    std::ostream_iterator<std::string, char> oi(outfile, "\n");
    std::copy(matnpat.begin(), matnpat.end(), oi);
    return 0;
}
