#include "emp.h"
#include <iostream>

// abstract employee
abstr_emp::~abstr_emp() {}

abstr_emp::abstr_emp()
{
    fname = "Nobody";
    lname = "Nomnom";
    job = "Unemployed";
}
abstr_emp::abstr_emp(const std::string& fn, const std::string& ln,
                     const std::string& j)
{

    fname = fn;
    lname = ln;
    job = j;
}
void abstr_emp::ShowAll() const // labels and shows all data
{
    std::cout << "First name: " << fname << "\nLast name: " << lname
              << "\nJob: " << job << "\n";
}

void abstr_emp::SetAll() // prompts user for values
{
    std::cout << "Enter first name: ";
    std::cin >> fname;
    std::cout << "Enter last name: ";
    std::cin >> lname;
    std::cout << "Enter job: ";
    std::cin >> job;
}

std::ostream& operator<<(std::ostream& os,
                         const abstr_emp& e) // displays first and last name
{
    os << e.fname << " " << e.lname;
    return os;
}

// employee

employee::employee(){};
employee::~employee(){};
employee::employee(const std::string& fn, const std::string& ln,
                   const std::string& j)
    : abstr_emp(fn, ln, j){};
void employee::ShowAll() const { abstr_emp::ShowAll(); };
void employee::SetAll() { abstr_emp::SetAll(); };

// manager

manager::manager(){};
manager::~manager(){};
manager::manager(const std::string& fn, const std::string& ln,
                 const std::string& j, int ico)
    : abstr_emp(fn, ln, j), inchargeof(ico){};
manager::manager(const abstr_emp& e, int ico) : inchargeof(ico)
{
    (abstr_emp&)* this = e;
};
manager::manager(const manager& m)
{
    (abstr_emp&)* this = (abstr_emp&)m;
    inchargeof = m.inchargeof;
};
void manager::ShowAll() const
{
    abstr_emp::ShowAll();
    std::cout << "In charge of: " << inchargeof << "\n";
};
void manager::SetAll()
{
    abstr_emp::SetAll();
    std::cout << "Enter number of employees managed: ";
    std::cin >> inchargeof;
};

// fink

fink::fink(){};
fink::~fink(){};
fink::fink(const std::string& fn, const std::string& ln, const std::string& j,
           const std::string& rpo)
    : abstr_emp(fn, ln, j), reportsto(rpo){};
fink::fink(const abstr_emp& e, const std::string& rpo) : reportsto(rpo)
{
    (abstr_emp&)* this = e;
};
fink::fink(const fink& e)
{
    (abstr_emp&)* this = (abstr_emp&)e;
    reportsto = e.reportsto;
};
void fink::ShowAll() const
{
    abstr_emp::ShowAll();
    std::cout << "Reports to: " << reportsto << "\n";
};
void fink::SetAll()
{
    abstr_emp::SetAll();
    std::cout << "Enter to whom report to: ";
    std::cin >> reportsto;
};

// highfink

highfink::highfink(){};
highfink::~highfink(){};
highfink::highfink(const std::string& fn, const std::string& ln,
                   const std::string& j, const std::string& rpo, int ico)
    : fink(fn, ln, j, rpo)
{
    InChargeOf() = ico;
};
highfink::highfink(const abstr_emp& e, const std::string& rpo, int ico)
    : fink(e, rpo)
{
    InChargeOf() = ico;
};
highfink::highfink(const fink& f, int ico) : fink(f) { InChargeOf() = ico; };
highfink::highfink(const manager& m, const std::string& rpo) : manager(m)
{
    ReportsTo() = rpo;
};
highfink::highfink(const highfink& h)
{
    (manager&)* this = (manager&)h;
    ReportsTo() = h.ReportsTo();
};
void highfink::ShowAll() const
{
    manager::ShowAll();
    std::cout << "Reports to: " << ReportsTo() << "\n";
};
void highfink::SetAll()
{
    manager::SetAll();
    std::cout << "Enter to whom reports to: ";
    std::cin >> ReportsTo();
};

void abstr_emp::readall(std::istream& is) { is >> fname >> lname >> job; }
void manager::readall(std::istream& is)
{
    abstr_emp::readall(is);
    is >> inchargeof;
}
void fink::readall(std::istream& is)
{
    abstr_emp::readall(is);
    is >> reportsto;
}
void highfink::readall(std::istream& is)
{
    manager::readall(is);
    is >> ReportsTo();
}

void abstr_emp::writeall(std::ostream& os)
{
    os << fname << " " << lname << " " << job;
}
void employee::writeall(std::ostream& os)
{
    // os << "e ";
    abstr_emp::writeall(os);
}
void manager::writeall(std::ostream& os)
{
    // os << "m ";
    abstr_emp::writeall(os);
    os << " " << inchargeof;
}
void fink::writeall(std::ostream& os)
{
    // os << "f ";
    abstr_emp::writeall(os);
    os << " " << reportsto;
}
void highfink::writeall(std::ostream& os)
{
    // os << "h ";
    abstr_emp::writeall(os);
    os << " " << InChargeOf();
    os << " " << ReportsTo();
}