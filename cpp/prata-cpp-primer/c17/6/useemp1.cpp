#include "emp.h"
#include <fstream>
#include <iostream>
using namespace std;

const int MAX = 10;

int main(void)
{
    const char* fname = "employees.dat";
    ifstream infile(fname);

    abstr_emp* pc[MAX];
    abstr_emp* temp;
    int index = 0;
    char type;
    // read data
    if (infile.is_open()) {
        for (int i = 0; i < MAX; ++i) {
            infile >> type;
            if (!infile) {
                break;
            } else {
                switch (type) {
                case 'e':
                    temp = new employee;
                    break;
                case 'f':
                    temp = new fink;
                    break;
                case 'h':
                    temp = new highfink;
                    break;
                case 'm':
                    temp = new manager;
                    break;
                default:
                    cout << "Unrecognized type";
                    abort();
                }
                temp->readall(infile);
                temp->ShowAll();
                pc[i] = temp;
                index++;
            }
        }
    }
    infile.close();

    // input data
    while (index < MAX) {
        cout << "Which type of object to create: (f)ink, (e)mployee, "
                "(h)ighfink, (m)anager (Ctrl+D to stop)? ";
        cin >> type;
        if (!cin)
            break;
        switch (type) {
        case 'e':
            temp = new employee;
            break;
        case 'f':
            temp = new fink;
            break;
        case 'h':
            temp = new highfink;
            break;
        case 'm':
            temp = new manager;
            break;
        default:
            cout << "Please enter e, m, f or h: ";
            break;
        }
        pc[index] = temp;
        pc[index]->SetAll();
        ++index;
    }

    // write data
    ofstream fout(fname);
    for (int i = 0; i < index; i++) {
        if (dynamic_cast<highfink*>(pc[i])) {
            fout << "h ";
        } else if (dynamic_cast<manager*>(pc[i])) {
            fout << "m ";
        } else if (dynamic_cast<fink*>(pc[i])) {
            fout << "f ";
        } else if (dynamic_cast<employee*>(pc[i])) {
            fout << "e ";
        }

        pc[i]->writeall(fout);
        fout << "\n";
    }
    fout.close();

    return 0;
}