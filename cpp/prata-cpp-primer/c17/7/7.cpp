#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

void ShowStr(const std::string& s) { std::cout << s << "\n"; }
void GetStrs(std::ifstream& infile, std::vector<std::string>& vec)
{
    size_t len = 0;
    infile.read((char*)&len, sizeof(len));
    while (infile) {
        std::string s;
        char temp;
        for (size_t i = 0; i < len; ++i) {
            infile.read(&temp, sizeof(char));
            s.append(&temp);
        }
        vec.push_back(s);
        infile.read((char*)&len, sizeof(len));
    }
}
class Store
{
    std::ofstream& outfile;

  public:
    Store(std::ofstream& os) : outfile(os){};
    void operator()(const std::string& s)
    {
        std::size_t len = s.size();
        outfile.write((char*)&len, sizeof(std::size_t)); // store length
        outfile.write(s.data(), len);                    // store characters
    };
};

int main()
{
    using namespace std;
    vector<string> vostr;
    string temp;

    // acquire strings
    cout << "Enter strings (empty line to quit):\n";
    while (getline(cin, temp) && temp[0] != '\0')
        vostr.push_back(temp);
    cout << "Here is your input.\n";
    for_each(vostr.begin(), vostr.end(), ShowStr);

    // store in a file
    ofstream fout("strings.dat", ios_base::out | ios_base::binary);
    for_each(vostr.begin(), vostr.end(), Store(fout));
    fout.close();

    // recover file contents
    vector<string> vistr;
    ifstream fin("strings.dat", ios_base::in | ios_base::binary);
    if (!fin.is_open()) {
        cerr << "Could not open file for input.\n";
        exit(EXIT_FAILURE);
    }
    GetStrs(fin, vistr);
    cout << "\nHere are the strings read from the file:\n";
    for_each(vistr.begin(), vistr.end(), ShowStr);

    return 0;
}