/*
1. iostream header provides cin cout cerr clog and wchar_t equivalents

2. because 121 as a character representation (3 bytes) is different from 121 in
a number representation (can fit in 1 byte)

3. standard output is usually the stream that gets routed into a file or other
program (though you can route standard error too). standard error is also
unbuffered

4. templates?

5. concatenating output is possible when a function returns a ostream reference

8.
ct1 = 5; ct2 = 8

9. the one with a while loop can ignore more than 80 characters
*/

#include <iostream>

int main(int argc, char const* argv[])
{
    // 6
    int number;
    std::cout << "Please enter an integer: ";
    std::cin >> number;
    std::cout << "This number in\n";

    std::cout.width(15);
    std::cout << "hex";
    std::cout.width(15);
    std::cout << "dec";
    std::cout.width(15);
    std::cout << "oct\n";
    std::cout.setf(std::ios_base::showbase);
    std::cout.width(15);
    std::cout << std::hex << number;
    std::cout.width(15);
    std::cout << std::dec << number;
    std::cout.width(15);
    std::cout << std::oct << number;
    std::cout << "\n";

    // 7
    char name[20];
    double wages;
    double hours_worked;
    std::cout << "Enter your name: ";
    std::cin.getline(name, 20);
    std::cout << "Enter your hourly wages: ";
    std::cin >> wages;
    std::cout << "Enter number of hours worked: ";
    std::cin >> hours_worked;

    std::cout << "First format:\n";
    std::cout.width(20);
    std::cout << name << ": $";
    std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
    std::cout.precision(2);
    std::cout.width(8);
    std::cout << wages << ":";
    std::cout.unsetf(std::ios_base::fixed);
    std::cout.width(6);
    std::cout << hours_worked;

    std::cout << "\n";
    std::cout << "Second format:\n";
    std::cout.setf(std::ios_base::left, std::ios_base::adjustfield);
    std::cout.width(20);
    std::cout << name << ": $";
    std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
    std::cout.precision(2);
    std::cout.width(8);
    std::cout << wages << ":";
    std::cout.unsetf(std::ios_base::fixed);
    std::cout.width(6);
    std::cout << hours_worked;
    std::cout << "\n";
}
