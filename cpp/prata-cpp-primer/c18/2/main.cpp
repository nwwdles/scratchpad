#include <iostream>
#include <string>
class Cpmv
{
  public:
    struct Info {
        std::string qcode;
        std::string zcode;
    };

  private:
    Info* pi;

  public:
    Cpmv();
    Cpmv(std::string q, std::string z);
    Cpmv(const Cpmv& cp);
    Cpmv(Cpmv&& mv);
    ~Cpmv();
    Cpmv& operator=(const Cpmv& cp);
    Cpmv& operator=(Cpmv&& mv);
    Cpmv operator+(const Cpmv& obj) const;
    void Display() const;
};

Cpmv::Cpmv()
{
    std::cout << "Called default constructor\n";
    pi = new Info{"q", "z"};
}
Cpmv::Cpmv(std::string q, std::string z)
{
    std::cout << "Called constructor(q,z)\n";
    pi = new Info{q, z};
}
Cpmv::Cpmv(const Cpmv& cp)
{
    std::cout << "Called copy constructor\n";
    pi = new Info{cp.pi->qcode, cp.pi->zcode};
}
Cpmv::Cpmv(Cpmv&& mv)
{
    std::cout << "Called move constructor\n";
    pi = mv.pi;
    mv.pi = nullptr;
}
Cpmv::~Cpmv()
{
    std::cout << "Called destructor\n";
    delete pi;
}
Cpmv& Cpmv::operator=(const Cpmv& cp)
{
    std::cout << "Called copy assignment\n";
    if (&cp == this) {
        return *this;
    }
    delete pi;
    pi = new Info{cp.pi->qcode, cp.pi->zcode};
}
Cpmv& Cpmv::operator=(Cpmv&& mv)
{
    if (&mv == this) {
        return *this;
    }
    std::cout << "Called move assignment\n";
    delete pi;
    pi = mv.pi;
    mv.pi = nullptr;
}
Cpmv Cpmv::operator+(const Cpmv& obj) const
{
    std::cout << "Called addition\n";
    Cpmv out((pi->qcode + obj.pi->qcode), (pi->zcode + obj.pi->zcode));
    return out;
}
void Cpmv::Display() const
{
    if (pi != nullptr)
        std::cout << "q: " << pi->qcode << " z: " << pi->zcode << "\n";
    else
        std::cout << "empty\n";
}

int main(int argc, char const* argv[])
{
    Cpmv a("a", "b");           // constructor(q,z)
    Cpmv b;                     // default constructor
    b = a;                      // copy assignment
    Cpmv sum(std::move(b + a)); // move constructor (gcc optimizes this so i
                                // explicitly use std::move)
    b = std::move(a);           // move assignment
    a.Display();
    b.Display();
    sum.Display();
    return 0;
}
