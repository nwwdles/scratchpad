
#include <iostream>
template <typename T> long double sum_values(const T& t) { return t; }

template <typename T, typename... A>
long double sum_values(const T& t, const A&... a)
{
    return t + sum_values(a...);
}

int main(int argc, char const* argv[])
{
    std::cout << sum_values(30, 40, 90, 100.5) << "\n";
    return 0;
}
