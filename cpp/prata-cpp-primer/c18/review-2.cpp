#include <iostream>
using namespace std;
double up(double x) { return 2.0 * x; }
void r1(const double& rx) { cout << rx << endl; }
void r2(double& rx) { cout << rx << endl; }
void r3(double&& rx) { cout << rx << endl; }
int main()
{
    double w = 10.0;
    r1(w);     // valid
    r1(w + 1); // valid
    r1(up(w)); // valid
    r2(w);     // valid
    r2(w + 1); // invalid, reference to rvalue?
    r2(up(w)); // invalid, ref to temp rvalue?
    r3(w);     // invalid, w is lvalue, use std::move?
    r3(w + 1); // valid, rvalue
    r3(up(w)); // valid, rvalue
    return 0;
}