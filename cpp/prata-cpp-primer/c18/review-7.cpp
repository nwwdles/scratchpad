#include <array>
#include <iostream>
const int Size = 5;
template <typename T> void sum(std::array<double, Size> a, T& fp);
// class Adder
// {
//     double tot;

//   public:
//     Adder(double q = 0) : tot(q) {}
//     void operator()(double w) { tot += w; }
//     double tot_v() const { return tot; };
// };
int main()
{
    double total = 0.0;
    // Adder ad(total);
    auto ad = [&total](double w) { total += w; };
    std::array<double, Size> temp_c = {32.1, 34.3, 37.8, 35.2, 34.7};
    sum(temp_c, ad);
    // total = ad.tot_v();
    std::cout << "total: " << total << '\n';
    return 0;
}
template <typename T> void sum(std::array<double, Size> a, T& fp)
{
    for (auto pt = a.begin(); pt != a.end(); ++pt) {
        fp(*pt);
    }
}
