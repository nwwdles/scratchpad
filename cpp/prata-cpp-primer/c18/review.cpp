/*
4.
constructor
destructor
copy constructor
copy assignment operator
move constructor
move assignment operator

If they're not provided, a compiler provides them if they're used.

5.
It doesn't use dynamic memory allocation so default move constructor
works fine? Changing it to use new[] would require a user-provided move
constructor
*/