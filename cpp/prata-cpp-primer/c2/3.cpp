#include <iostream>

void printmice() 
{
    using namespace std;
    cout << "Three blind mice" << endl;
}

void printrun() 
{
    using namespace std;
    cout << "See how they run" << endl;
}

int main()
{
    printmice();
    printmice();
    printrun();
    printrun();
    return 0;
}