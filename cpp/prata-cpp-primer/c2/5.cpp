#include <iostream>

double tofahr (double cels)
{
    return 1.8 * cels + 32.0;
}

int main()
{
    using namespace std;

    double cels = 0;
    cout << "Enter Celsius value: ";
    cin >> cels;
    cout << cels << " degrees Celsius is " << tofahr(cels) << " degrees Fahrenheit.\n";

    return 0;
}