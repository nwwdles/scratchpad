#include <iostream>

double toastro (double ly)
{
    return 63240 * ly;
}

int main()
{
    using namespace std;

    double lightyears = 0;
    cout << "Enter the number of light years: ";
    cin >> lightyears;
    cout << lightyears << " light years = " << toastro(lightyears) << " astronomical units.\n";

    return 0;
}