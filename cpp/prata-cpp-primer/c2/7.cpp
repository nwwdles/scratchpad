#include <iostream>

void prtinttime (int h, int m)
{
    using namespace std;
    cout << "Time: " << h << ":" << m << endl;
}

int main()
{
    using namespace std;

    int m = 0;
    int h = 0;
    cout << "Enter the number of hours: ";
    cin >> h;
    cout << "Enter the number of minutes: ";
    cin >> m;
    prtinttime(h, m);

    return 0;
}