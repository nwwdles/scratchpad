#include <iostream>
#include <cmath>
int main()
{
    using namespace std;

    const int feettoinches = 12;
    const double inchestometers = 0.0254;
    const double poundstokgs = 2.2;
    int inches = 0;
    int feet = 0;
    int weight = 0;
    cout << "Input your height in feet and inches, separated by space (e.g. 6 0): ";
    cin >> feet >> inches;
    cout << "Input your weight in pounds: ";
    cin >> weight;
    double height = (feet * feettoinches + inches) * inchestometers;
    double weightkg = weight/poundstokgs;
    cout << "Height: " << height 
        << " m. Weight:" << weightkg 
        << " kg. BMI: " << weightkg / pow(height, 2) << endl;

    return 0;
}