#include <iostream>
#include <cmath>
int main()
{
    using namespace std;

    const int rate = 60;
    int degrees = 0;
    int minutes = 0;
    int seconds = 0;
    
    cout << "Enter a latitude in degrees minutes and seconds. \n";
    cout << "Enter degrees: ";
    cin >> degrees;
    cout << "Enter minutes: ";
    cin >> minutes;
    cout << "Enter seconds: ";
    cin >> seconds;

    double total = degrees + minutes / rate + seconds / pow(rate, 2);
    cout << degrees << ", " << minutes << ", " << seconds  
        << " = " << total << endl;

    return 0;
}