#include <iostream>
#include <cmath>
int main()
{
    using namespace std;

    const int minToS = 60;
    const int hrToMin = 60;
    const int dayToHr = 24;
    const int yrToDay = 365;
    long long seconds = 0;

    cout << "Enter a number of seconds. \n";
    cin >> seconds;

    int days = seconds / (dayToHr * hrToMin * minToS);
    int left = seconds % (dayToHr * hrToMin * minToS);

    int hrs = left / (hrToMin * minToS);
    left = left % (hrToMin * minToS);

    int mins = left / minToS;
    int secs = left % minToS;

    cout << seconds << "seconds = Days: " << days 
        << " Hrs: " << hrs
        << " Mins: " << mins
        << " Secs: " << secs
        << endl;

    return 0;
}