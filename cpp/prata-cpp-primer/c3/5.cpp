#include <iostream>
#include <cmath>
int main()
{
    using namespace std;

    const int minToS = 60;
    long long popTotal = 0;
    long long popUs = 0;
    cout << "Enter world's pop: ";
    cin >> popTotal;
    cout << "Enter US pop: ";
    cin >> popUs;
    cout << "US is " << (static_cast<double> (popUs) / popTotal) * 100 << "% of the world pop." << endl;

    return 0;
}