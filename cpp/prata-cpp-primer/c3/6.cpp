#include <iostream>
#include <cmath>
int main()
{
    using namespace std;

    double distance = 0;
    double consumption = 0;
    cout << "Enter kilometers driven: ";
    cin >> distance;
    cout << "Enter petrol consumed in liters: ";
    cin >> consumption;
    cout << "liters/100 kilometers: "<< consumption / (distance/100) << endl;

    return 0;
}