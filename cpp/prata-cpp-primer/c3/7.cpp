#include <iostream>
#include <cmath>
int main()
{
    using namespace std;

    double eurofigure = 0;
    const double gallontoliters = 3.875;
    const double kmtomiles = 0.6214;
    cout << "Enter consumtion figure (liters per 100 kilometers): ";
    cin >> eurofigure;
    cout << eurofigure << "l/100km = " << gallontoliters * 100 * kmtomiles / eurofigure << endl;

    return 0;
}