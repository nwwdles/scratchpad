#include <iostream>
#include <string>

char gradeconv(char grade) {
    // char newgrade = 'U';
    // char grades[] = {'A', 'B', 'C', 'D', 'F'};
    // for(int i = 0; i < sizeof(grades)/sizeof(grades[0]) - 1; ++i) {
    //     if (grades[i] == grade) {
    //         newgrade = grades[i + 1];
    //         break;
    //     }
    // }
    // return newgrade;
    return grade + 1;
}

int main()
{
    using namespace std;

    char firstname[20];
    char lastname[20];
    char grade;
    int age;

    cout << "What is your first name? ";
    cin.getline(firstname, 20);
    cout << "What is your last name? ";
    cin.getline(lastname, 20);
    cout << "What letter grade do you deserve? ";
    cin >> grade;
    cout << "What is your age? ";
    cin >> age;

    cout << "Name: " << lastname << ", " << firstname<< endl; 
    cout << "Grade: "<< gradeconv(grade) << endl;
    cout << "Age: " << age << endl;

    return 0;
}

// 1. Write a C++ program that requests and displays information as shown in the fol-
// lowing example of output:
// What is your first name? Betty Sue
// What is your last name? Yewe
// What letter grade do you deserve? B
// What is your age? 22
// Name: Yewe, Betty Sue
// Grade: C
// Age: 22
// Note that the program should be able to accept first names that comprise more
// than one word.Also note that the program adjusts the grade downward—that is, up
// one letter.Assume that the user requests an A, a B, or a C so that you don’t have to
// worry about the gap between a D and an F.