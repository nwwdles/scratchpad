#include <iostream>
#include <string>
#include <array>
#include <cstring>

int main()
{
    using namespace std;

    const int len = 3;
    array<double, len> times;
    double avg;

    for (int i = 0; i < len; ++i) {
        cout<< "Dash " << i+1 << ": ";
        cin >> times[i];
    }
    
    cout<< "40m dash times: " << endl;
    for (int i = 0; i < len; ++i) {
        cout << i+1 << ": " <<times[i] << endl;
        avg += times[i];
    }
    avg /= len;
    cout<< "average: " <<avg << endl;

    return 0;
}
