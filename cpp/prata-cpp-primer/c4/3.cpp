#include <iostream>
#include <string>
#include <cstring>

int main()
{
    using namespace std;

    char firstname[20];
    char lastname[20];

    cout << "What is your first name? ";
    cin.getline(firstname,20);
    cout << "What is your last name? ";
    cin.getline(lastname, 20);

    char fullname[40];
    strncpy(fullname, lastname, 40);
    strncat(fullname, ", ", 40);
    strncat(fullname, firstname, 40);
    cout << fullname << endl;

    return 0;
}
