#include <iostream>
#include <string>
#include <cstring>

int main()
{
    using namespace std;

    string firstname;
    string lastname;

    cout << "What is your first name? ";
    getline(cin,firstname);
    cout << "What is your last name? ";
    getline(cin, lastname);

    string fullname = lastname + ", " + firstname;
    cout << fullname << endl;

    return 0;
}
