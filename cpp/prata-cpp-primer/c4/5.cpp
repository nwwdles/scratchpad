#include <iostream>
#include <string>
#include <cstring>

int main()
{
    using namespace std;

    struct CandyBar {
        string name = "";
        double weight = 0;
        int calories = 0; 
    };

    CandyBar snack = {
        "Mocha Munch",
        2.3,
        350
    };

    cout << "snack name: " << snack.name << endl
        << "weight: " << snack.weight << endl
        << "calories: " << snack.calories << endl;
    
    return 0;
}
