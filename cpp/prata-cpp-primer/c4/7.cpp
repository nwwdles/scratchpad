#include <iostream>
#include <string>
#include <cstring>

struct pizza {
    std::string company = "";
    int diameter = 0;
    int weight = 0; 
};

int main()
{
    using namespace std;

    pizza mypizza;
    
    cout << "Pizza company: ";
    getline(cin, mypizza.company);
    cout << "diameter: ";
    cin >> mypizza.diameter;
    cout << "weight: ";
    cin >> mypizza.weight;
    cout << endl;

    cout << "Pizza company: " << mypizza.company <<endl
    << "Diameter: " << mypizza.diameter <<endl
    << "Weight: " << mypizza.weight <<endl;
    return 0;
}
