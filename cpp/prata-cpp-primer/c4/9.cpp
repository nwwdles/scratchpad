#include <iostream>
#include <string>
#include <cstring>

struct CandyBar {
        std::string name = "";
        double weight = 0;
        int calories = 0; 
    };

void printcandy(CandyBar snack) {
    using namespace std;
    cout << "snack name: " << snack.name << endl
        << "weight: " << snack.weight << endl
        << "calories: " << snack.calories << endl;
}
int main()
{
    using namespace std;

    CandyBar * (snacks) = new CandyBar[3];

    snacks[0] = {"Mocha Munch",     2.3,    350};
    snacks[1] = {"Noon Nut",        3.4,    450};
    snacks[2] = {"Orange Orange",   5.6,    550};
    
    for(int i = 0; i < 3; ++i){
        printcandy(snacks[i]);
        cout << endl;
    }
    return 0;
}
