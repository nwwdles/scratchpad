#include <iostream>
#include <string>
#include <array>
#include <vector>

// 8
struct fish {
    std::string kind;
    int weight;
    double length;
};

// 10
enum Response {Yes=1, No=0, Maybe=2};

int main()
{
    using namespace std;
    
    // 1
    char actors[30];
    short betsie[100];
    float chuck[13];
    array<long double, 64> dipsea; //2 here i use array template class

    // 3. was there some clever method? i forgot. in python there is
    int myints[5] = {1, 3, 5, 7, 9};
    int myints2[5];
    for (int i = 0; i < 5; ++i) {
        myints2[i] = i * 2 + 1;
        cout << i * 2 + 1 << "\t";
    }
    // 4
    int even = myints[4] + myints[0];
    
    // 5
    float ideas[3] = {1, 3, 66.6};
    cout << ideas[2] << endl;
    
    // 6
    char cb[] = "cheeseburger";

    //7
    string mystr = "Waldorf Salad";

    // 9
    fish myfish {
        "cutefish",
        10,
        12.4
    };

    // 11
    double ted = 0.1;
    double *tedp = &ted;
    cout << *tedp << endl;

    // 12
    cout << "treacle:" << endl;
    float treacle[10] = {};
    for(int i = 0; i < 10; ++i) {
        treacle[i] = rand() / 10.0;
        cout << treacle[i] << " ";
    }
    cout << endl;
    float * trp = treacle; 
    cout << *trp << " and " << *(trp + 9) << endl; 
    
    // 13
    cout << "input size of array: ";
    int size;
    cin >> size;
    int *ap = new int [size];
    vector<int> vecar(size);

    for(int i = 0; i < size; ++i) {
        vecar[i] = rand();
        *(ap + i) = rand();
        cout << vecar[i] << "\t";
        cout << *(ap+i) << "\n";
    }

    // 14 is valid. prints the address of where the string is stored
    // 15
    fish * mf = new fish {"hey guys"};
    cout << mf->kind << endl;

    // 16 *** stack smashing detected ***: <unknown> terminated
    // Аварийный останов (стек памяти сброшен на диск)
    // it would be able to write address outside of array bounds

    // 17
    const int strnum = 10;
    std::vector<string> strvec(strnum);
    std::array<string, strnum> strar;

    return 0;
}
