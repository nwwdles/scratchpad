#include <iostream>


int main()
{
    using namespace std;

    int x, y;
    cout << "Please enter two integers: ";
    cin >> x;
    cin >> y;
    cout << x << " " << y << endl;
    int sum = 0;
    for(int i = min(x, y); i <= max(x, y); i++){
        sum += i;
    }
    cout << sum << endl;
    return 0;
}
