#include <iostream>
#include <array>

int main()
{
    using namespace std;
    const int arSize = 100;
    array<long double, arSize> factorials;

    factorials[0] = 1;
    for (int i = 1; i < arSize; i++) {
        factorials[i] = i * factorials[i - 1];
        cout << i <<"! = " << factorials[i]<< endl;
    }
    return 0;
}
