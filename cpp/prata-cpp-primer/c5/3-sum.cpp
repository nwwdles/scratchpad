#include <iostream>

int main()
{
    using namespace std;
    int num;
    int sum = 0;
    cout << "Please enter numbers to sum. Enter 0 to finish." << endl;
    cin >> num;
    while(num != 0) {
        sum += num;
        cout << "Total sum:"<< sum << endl;
        cin >> num;
    }
    return 0;
}
