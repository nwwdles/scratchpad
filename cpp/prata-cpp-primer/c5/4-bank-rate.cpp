#include <iostream>

int main()
{
    using namespace std;
    float cleoBalance = 100;
    float cleoRate = 0.05; // compound

    float daphneBalance = 100;
    float daphneRate = .1; // simple
    float daphneInterest = daphneBalance * daphneRate;

    int years = 0;
    while(cleoBalance <= daphneBalance) {
        years++;
        cleoBalance *= (1 + cleoRate);
        daphneBalance += daphneInterest;
        cout << "Year " << years << ". Daphne's balance: " << daphneBalance << "; Cleo's balance: " << cleoBalance << endl;
    }
    cout << years << " years it took for Cleo's investment to exceed Daphne's." << endl;
    
    return 0;
}
