#include <iostream>
#include <array>
#include <string>
int main()
{
    using namespace std;
    const array<string, 12> monthNames = {
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    };
    const int months = 12;
    
    array<int, months> sales;
    for (int i = 0; i < months; i++) {
        cout << "Please enter book sales for " << monthNames[i] << ":" << endl;
        cin >> sales[i];
    }
    int sum = 0;
    for (int i = 0; i < months; i++) {
        sum += sales[i];
    }
    cout << "Total book sales in 12 months: "<< sum << endl;
    return 0;
}
