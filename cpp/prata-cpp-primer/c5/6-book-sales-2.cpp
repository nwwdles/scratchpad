#include <iostream>
#include <array>
#include <string>
int main()
{
    using namespace std;
    const array<string, 12> monthNames = {
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    };
    const int months = 12;
    const int years = 3;
    int sales[years][months];
    int totalsum = 0;
    for (int y = 0; y < years; y++){
        for (int i = 0; i < months; i++) {
            cout << "Please enter book sales for " << monthNames[i] <<", year " << y + 1 << ":" << endl;
            cin >> sales[y][i];
        }
    }
    for (int y = 0; y < years; y++){
        int sum = 0;
        for (int i = 0; i < months; i++) {
            sum += sales[y][i];
        }
        totalsum += sum;
        cout << "Total book sales in year " << y + 1 << ": " << sum << endl;
    }
    cout << "Total book sales in " << years << " years: "<< totalsum << endl;
    return 0;
}
