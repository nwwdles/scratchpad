#include <iostream>
#include <array>
#include <string>

struct car {
    int year;
    std::string make;
};

int main()
{
    using namespace std;
    int carsNumber = 0;
    cout << "How many cars do you wish to catalog? ";
    cin >> carsNumber;
    car * cars = new car [carsNumber];

    for (int i = 0; i < carsNumber; i++) {
        cin.get(); // get newline
        cout << "Please enter the make: ";
        getline(cin, cars[i].make);
        cout << "Please enter the year made: ";
        cin >> cars[i].year;
    }

    cout << "Here's your collection:" << endl;
    for (int i = 0; i < carsNumber; i++) {
        cout << cars[i].year << " " << cars[i].make << endl;
    }
    return 0;
}
