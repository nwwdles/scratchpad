#include <iostream>
#include <cstring>

int main()
{
    using namespace std;
    char word[20];
    int wordsNum = 0;
    cout << "Enter words (to stop, type the word done): " << endl;
    while(strcmp(word, "done") != 0) {
        wordsNum++;
        cin >> word;
    }
    cout << "You entered a total of " << wordsNum - 1 << " words." << endl; 
    return 0;
}