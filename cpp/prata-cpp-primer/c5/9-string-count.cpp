#include <iostream>
#include <string>

int main()
{
    using namespace std;
    string word;
    int wordsNum = 0;
    cout << "Enter words (to stop, type the word done): " << endl;
    while(word != "done") {
        wordsNum++;
        cin >> word;
    }
    cout << "You entered a total of " << wordsNum - 1 << " words." << endl; 
    return 0;
}