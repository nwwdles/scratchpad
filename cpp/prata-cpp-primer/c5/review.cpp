#include <iostream>

// 1:
// entry-condition loop: for, while
// exit-condition: do while
// entry-condition loop checks condition before executing the loop body

// 2:
// 01234
//

// 3:
// 0369
// 12

// 4:
// 6
// 8

// 5:
//  k = 8

// 6:
void powersoftwo(){
    for(int i = 1; i <= 64; i *= 2){
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

// 7:
// by using a block with brackets {}

// 9
// cin>>ch ignores whitespace

int main(int argc, char const *argv[])
{
    powersoftwo();
    
    // 8:
    // actually it's working so it's probably valid but pointless
    int x = (1, 024);
    std::cout << x << std::endl;
    // it assigns a value of 024=20 to x. 1 just goes to waste?

    int y;
    y = 1,024;
    std::cout << y << std::endl;
    // there are 2 expressions: y = 1 and 024; 024 is a meaningless expression.
    return 0;
}
