#include <iostream>

int main()
{
    using namespace std;

    int arSize = 10;

    double values[arSize];
    long double sum = 0;
    int count = 0;

    for (int i = 0; i < arSize; i++) {
        cin >> values[i];
        if (cin.fail()){
            break;
        } else {
            sum += values[i];
            count++;
        }
    }

    double average = sum / count;

    int largeNumbers = 0;
    for (int i = 0; i < count; i++) {
        if (values[i] > average) {
            largeNumbers++;
        }
    }

    cout << "Average: " << average << endl
        << "Larger than average: " << largeNumbers << " number(s)" << endl;
    return 0;
}

