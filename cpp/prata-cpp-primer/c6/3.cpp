#include <iostream>

void printmenu() {
    std::cout << "Please enter one of the following choices:\n"
        << "c) carnivore p) pianist\n"
        << "t) tree      g) game\n";
}

int main()
{
    using namespace std;
    printmenu();
    char ch;
    while (true) {
        cout << "Please enter a c, p, t, or g: ";
        cin.get(ch).get();
        switch (ch)
        {
            case 'c':
                cout << "A carnivore." << endl;
                break;
            case 'p':
                cout << "A pianist." << endl;
                break;
            case 't':
                cout << "A tree." << endl;
                break;
            case 'g':
                cout << "A game." << endl;
                break;
            default:
                break;
        }
    }
    return 0;
}

