#include <iostream>

const int strsize = 20;

struct bop {
    char fullname[strsize];
    char title[strsize];
    char bopname[strsize];
    int preference; // 0 - fullname 1 - title 2 - bopname
};

void listbop(bop list[], char ch){
    
}

int main()
{
    using namespace std;

    bop members[] = {
        {"Soba", "Noodles", "notPasta", 0},
        {"Sir Mister", "President", "misterSir", 1},
        {"Lady Madam", "Presidentess", "madamLady", 2},
    };
    int len = sizeof(members)/sizeof(members[0]);
    
    char ch;
    cout << "a. display by name    b. display by title" << endl
            << "c. display by bopname d. display by preference" << endl
            << "q. quit" << endl;
    
    cout << "Enter your choice: ";
    cin >> ch;
    while(ch != 'q'){
        switch (ch) {
            case 'a':
                for(int i = 0; i < len; i++){
                    cout << members[i].fullname << endl;
                }
            break;
            case 'b':
                for(int i = 0; i < len; i++){
                    cout << members[i].title << endl;
                }
            break;
            case 'c':
                for(int i = 0; i < len; i++){
                    cout << members[i].bopname << endl;
                }
            break;
            case 'd':
                for(int i = 0; i < len; i++){
                    if (members[i].preference == 0){ 
                        cout << members[i].fullname << endl;
                    } else if (members[i].preference == 1) {
                        cout << members[i].title << endl;
                    } else {
                        cout << members[i].bopname << endl;
                    }
                }
            break;
        }
        cout << "Next choice: ";
        cin >> ch;
    }
    return 0;
}