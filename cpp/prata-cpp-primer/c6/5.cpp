#include <iostream>

int main()
{
    using namespace std;

    double tvarps;
    double tax;

    
    while(true) {
        cout << "Please enter your income: ";
        cin >> tvarps;
        if (tvarps < 0 || !cin) {
            break;
        } else {
            if (tvarps > 35000) {
                tax = 10000 * 0.1 + 20000 * 0.15 + (tvarps - 35000) * 0.2;
            } else if (tvarps > 15000) {
                tax = 10000 * 0.1 + (tvarps - 15000) * 0.15;
            } else if (tvarps > 5000) {
                tax = (tvarps - 5000) * 0.1;
            } else {
                tax = 0;
            }
            cout << "Your tax: " << tax << endl;
        }
    }
    return 0;
}