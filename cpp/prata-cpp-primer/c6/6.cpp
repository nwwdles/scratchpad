#include <iostream>
#include <string>

int main()
{
    struct contributor {
        std::string name;
        double contribution;
    };

    using namespace std;

    int count;
    cout << "Please enter amount of contributors: ";
    cin >> count;

    contributor * contributions = new contributor[count];

    for (int i = 0; i < count; ++i) {
        cout << "Please enter a name and a contribution: ";
        cin >> contributions[i].name;
        cin >> contributions[i].contribution;
    }

    bool hasPatrons = false;
    cout << "Grand patrons:" << endl;
    for (int i = 0; i < count; ++i) {
        if (contributions[i].contribution >= 10000) {
            cout << contributions[i].name << ", " << contributions[i]. contribution<<endl;
            hasPatrons = true;
        }
    }
    if (!hasPatrons) {
        cout << "None" << endl;
    }

    hasPatrons = false;
    cout << "Patrons:" << endl;
    for (int i = 0; i < count; ++i) {
        if (contributions[i].contribution < 10000) {
            cout << contributions[i].name << ", " << contributions[i]. contribution<<endl;
            hasPatrons = true;
        }
    }
    if (!hasPatrons) {
        cout << "None" << endl;
    }

    return 0;
}