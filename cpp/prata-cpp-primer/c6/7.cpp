#include <iostream>
#include <string>
#include <cctype>
int main()
{
    using namespace std;

    string word;
    int consonants = 0;
    int vowels = 0;
    int others = 0;
    cout << "Enter words (q to quit)" << endl;
    while(cin >> word && word != "q") {
        char first = word[0];
        if (isalpha(first)) {
            switch (first)
            {
                case 'a':
                case 'i':
                case 'u':
                case 'e':
                case 'o':
                    vowels++;
                    break;
            
                default:
                    consonants++;
                    break;
            }
        } else {
            others++;
        }
    }
    cout << vowels << " words beginning with vowels" << endl;
    cout << consonants << " words beginning with consonants" << endl;
    cout << others << " others" << endl;
    return 0;
}