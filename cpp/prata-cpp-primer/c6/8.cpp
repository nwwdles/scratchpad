#include <iostream>
#include <fstream>

int main()
{
    using namespace std;

    ifstream infile;
    infile.open("8.cpp"); 
    
    int chcount = 0;
    char ch;
    while(!infile.fail()){
        infile.get(ch);
        chcount++;
    }
    cout << "Total characters: " << chcount - 1 << endl; // EOF got counted too so i remove it
    return 0;
}