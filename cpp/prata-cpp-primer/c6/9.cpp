#include <iostream>
#include <string>
#include <fstream>

int main()
{
    struct contributor {
        std::string name;
        double contribution;
    };

    using namespace std;

    ifstream infile;
    infile.open("data.txt");
    if (infile.fail()) {
        cout << "Failed to open data.txt";
        exit;
    }

    int count;
    infile >> count;
    infile.get(); // get newline
    contributor * contributions = new contributor[count];

    for (int i = 0; i < count; ++i) {
        // cout << "Please enter a name and a contribution: ";
        getline(infile, contributions[i].name);
        infile >> contributions[i].contribution;
        infile.get(); // get newline
    }

    bool hasPatrons = false;
    cout << "Grand patrons:" << endl;
    for (int i = 0; i < count; ++i) {
        if (contributions[i].contribution >= 10000) {
            cout << contributions[i].name << ", " << contributions[i]. contribution<<endl;
            hasPatrons = true;
        }
    }
    if (!hasPatrons) {
        cout << "None" << endl;
    }

    hasPatrons = false;
    cout << "Patrons:" << endl;
    for (int i = 0; i < count; ++i) {
        if (contributions[i].contribution < 10000) {
            cout << contributions[i].name << ", " << contributions[i]. contribution<<endl;
            hasPatrons = true;
        }
    }
    if (!hasPatrons) {
        cout << "None" << endl;
    }

    return 0;
}