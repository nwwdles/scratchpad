// 1
// the second form doesn't compare a character to a newline if character is found to be a space already. the advantage is performance

// 2
// it outputs character codes instead of characters, ch + 1 is an integer

// 3
// HHii!!
//
// SSeenndd  ct1 = 9, ct2 = 0
//
// WRONG, yeah notice = instead of ==

// 4
// weight >= 115 && weight < 125
// ch == 'q' || ch == 'Q'
// x % 2 == 0 && x != 26
// x % 2 == 0 && x % 26 != 0
// (donation >= 1000 && dontaion <= 2000) || guest == 1
// (ch >= 'A' && dontaion <= 'Z') || (ch >= 'a' && ch <= 'z') // isalpha(ch)

// 5
// no, !!x can only be 1 or 0. it's true only for bool

// 6 
// (x > 0)? x : -x

// 7
// switch(ch) {
//     case 'A': 
//         a_grade++;
//         break;
//     case 'B': 
//         b_grade++;
//         break;
//     case 'C': 
//         c_grade++;
//         break;
//     case 'D': 
//         d_grade++;
//         break;
//     default:
//         f_grade++;
// }

#include <iostream>

int main()
{
    using namespace std;

    // 9
    int line = 0;
    char ch;
    while (cin.get(ch) && ch != 'Q')
    {
        if (ch == '\n')
            line++;
    }
    
    return 0;
}

