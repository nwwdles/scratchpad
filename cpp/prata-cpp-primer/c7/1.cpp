#include <iostream>

double harmonicmean(double, double);
int main(int argc, char const *argv[])
{
    using namespace std;
    double num1;
    double num2;
    do {
        cout << "Enter a pair of numbers: ";
        cin >> num1;
        cin >> num2;
        double hmean = harmonicmean(num1, num2);
        cout << "Harmonic mean: " << hmean << endl;
    } while(num1 != 0 && num2 != 0);
    return 0;
}

double harmonicmean(double a, double b) {
    return (2. * a * b / (a + b));
}