#include <iostream>

double calculate( double, double, double (*)(double, double));

double add(double x, double y);
double subtract(double x, double y);
double multiply(double x, double y);

int main(int argc, char const *argv[])
{
    using std::cout;
    using std::endl;
    using std::cin;
    cout << calculate(2.5,10.4, add) << endl;
    
    double (*funcs[3])(double, double) = {add, subtract, multiply};

    double a;
    double b;
    while(cin) {
        cout << "Enter two numbers: ";
        cin >> a >> b;
        // cout<< "Adding:" << funcs[0](a, b) << endl;
        // cout<< "Subtracting:" << funcs[1](a, b) << endl;
        // cout<< "Multiplying:" << funcs[2](a, b) << endl;
        for(int i = 0; i < 3; i++)
        {
            cout << funcs[i](a,b) << " ";
        }
        cout << endl;
        
    }
    return 0;
}


double calculate( double a, double b, double (*fun)(double, double)) {
    return fun(a, b);
}

double add(double x, double y){
    return x + y;
}
double subtract(double x, double y){
    return x - y;
}
double multiply(double x, double y){
    return x * y;
}