#include <iostream>

void displayarray(const int * ar, int size);
double calcavg(const int * ar, int size);
int processinput(int * ar, int size);

int main(int argc, char const *argv[])
{
    using namespace std;
    int size = 10;
    int scores[size];

    int count = processinput(scores, size);

    displayarray(scores, count);

    double avg = calcavg(scores, count);
    cout << "Average score: " << avg << endl;

    return 0;
}

int processinput(int * ar, int size) {
    using namespace std;
    int count = 0;
    cout << "Enter up to 10 golf scores. Enter any letter to stop." << endl;
    for(int i = 0; i < size; i++){
        cout << i + 1 << ". ";
        cin >> ar[i];
        if (cin) {
            count++;
        } else {
            break;
        }
    }
    return count;
}

double calcavg(const int * ar, int size) {
    int sum = 0;
    for(int i = 0; i < size; i++) {
        sum += ar[i];
    }
    return (sum / size);
}
void displayarray(const int * ar, int size) {
    std::cout << "Scores:";
    for(int i = 0; i < size; i++) {
        std::cout << ar[i] << " ";
    }
    std::cout << std::endl;
}