#include <iostream>

struct box {
    char maker[40];
    float height;
    float width;
    float length;
    float volume;
};

void displaybox(const box *);
void calcvolume(box *);

int main(int argc, char const *argv[])
{
    box mybox = {"BestBoxes", 1.f, 2.f, 5.f, 0.f};
    calcvolume(&mybox);
    displaybox(&mybox);

    return 0;
}

void displaybox(const box * b){
    using namespace std;
    cout <<"Maker: " << b->maker << endl;
    cout <<"Height: " << b->height << endl;
    cout <<"Width: " << b->width << endl;
    cout <<"Length: " << b->length << endl;
    cout <<"Volume: " << b->volume << endl;

}
void calcvolume(box * b){
    b->volume = b->height * b->length * b->width;
}