#include <iostream>

long double factorial(unsigned n);

int main()
{
    using namespace std;

    unsigned number;
    while (true) {
        cout << "Please enter a number: ";
        cin >> number;
        if (cin) 
            cout << number << "! = " << factorial(number) << endl;
        else 
            break;
    }

    cout << "bye\n";
    return 0;
}

long double factorial(unsigned n)
{
    if (n == 0) 
        return 1;
    else 
        return n * factorial(n-1);
}