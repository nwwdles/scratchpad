#include <iostream>

unsigned Fill_array(double *, unsigned);
void Show_array(const double *, unsigned);
void Reverse_array(double *, unsigned);

int main()
{
    using namespace std;

    unsigned size = 10;
    double ar[size];

    unsigned count = Fill_array(ar, size);
    cout << "Entered array:" << endl;
    Show_array(ar, count);
    
    Reverse_array(ar, count);
    cout << "Reversed array:" << endl;
    Show_array(ar, count);

    Reverse_array(ar+1, count-2);
    cout << "Reversed array:" << endl;
    Show_array(ar, count);

    cout << "bye\n";
    return 0;
}

unsigned Fill_array(double * ar, unsigned size){
    using namespace std;
    cout << "Please enter up to " << size << " values. Enter non-numerical value to stop."<<endl;
    unsigned count = 0;
    for(unsigned i = 0; i < size; i++){
        cin >> ar[i];
        if (!cin) {
            break;
        } else {
            count++;
        }
    }
    return count;
}
void Show_array(const double * ar, unsigned size) {
    using namespace std;
    for(unsigned i = 0; i < size; i++){
        cout << ar[i] << " ";
    }
    cout << endl;
}
void Reverse_array(double * ar, unsigned size) {
    double temp;
    for(unsigned i = 0; i < size/2; i++){
        temp = ar[i];
        ar[i] = ar[size-i-1];
        ar[size-i-1] = temp;
    }
}