//arrobj.cpp -- functions with array objects
#include <iostream>
#include <string>
const int Seasons = 4;
const char * Snames[] =
    {"Spring", "Summer", "Fall", "Winter"};

struct exp {
    double expenses[Seasons];
};

void fill(exp *);
void show(const exp *);
int main()
{
    exp myexp = {{}};
    // double expenses[Seasons] = {};
    fill(&myexp);
    show(&myexp);
    // std::cin.get();
    // std::cin.get();
    return 0;
}

void fill(exp * e)
{
    double * pa = e->expenses;
    for (int i = 0; i < Seasons; i++)
    {
        std::cout << "Enter " << Snames[i] << " expenses: ";
        std::cin >> pa[i];
    }
}

void show(const exp * e)
{
    const double * da = e->expenses;
    double total = 0.0;
    std::cout << "\nEXPENSES\n";
    for (int i = 0; i < Seasons; i++)
    {
        std::cout << Snames[i] << ": $" << da[i] << '\n';
        total += da[i];
    }
    std::cout << "Total: $" << total << '\n';
}
