// #include <string>
#include <iostream>
// 1
// prototype
// declare
// call

// 2
// 

void igor();
float tofu(int);
double mpg(double, double);
long summation(long *, int);
double doctor(const char *);
struct boss {};
void ofcourse(boss);
struct map {};
char * plot(map *);

// 3
void printvalues(const int * ar, int size);
void setvalues(int *, int, int);
// 4
void setvaluesrange(int *, int *, int);

// 5
double getmax(const double *, int);

// 6 
// because they're passed by value so modifying them 
// would not modify the state outside of function scope

// 7 
// array of char
// quoted string constant
// pointer to char set to string address

// 8
int replace(char * str, char c1, char c2);

// 9
// "taco"[2] is a third character in the string (c)
// *"pizza" is a first character in the string (p)

// 10
// function(glitz); to pass it by value
// function(&glitz); to pass it by address

// 11
int judge(int (*p)(const char *));

// 12
struct applicant {
    char name[30];
    int credit_ratings[3];
};
void displayapplicant(applicant);
void displayapplicant_p(const applicant *);

// 13
void f1(applicant * a);
const char * f2(const applicant * a1, const applicant * a2);

int main()
{
    int arsize = 4;
    int ar[4] = {1, 2, 3, 4};

    setvalues(ar, arsize, 5);
    // printvalues(ar, arsize);

    setvaluesrange(ar, &(ar[4]), 6);
    // printvalues(ar, arsize);

    double dar[4] = {-1., -3., 13., -1123.};
    std::cout<<getmax(dar, 4)<<std::endl;

    char str[] = "m-my string";
    std::cout << "Original string: " << str<< std::endl;
    std::cout << "Replacements: " << replace(str, 'm', 'n') << std::endl;
    std::cout << "Modified string: " << str<< std::endl;

    // 9
    std::cout << *"pizza" << std::endl; // == 'p'
    std::cout << "taco"[2] << std::endl; // == 'c

    // 12
    applicant myapplicant = {"Johnny", {1,2,3}};
    displayapplicant(myapplicant);
    displayapplicant_p(&myapplicant);

    // 13 
    void (*p1) (applicant *) = f1;
    f1(&myapplicant);
    p1(&myapplicant);

    const char * (*p2) (const applicant *, const applicant *) = f2;

    void (*(ap[5])) (applicant *) = {f1,f1,f1,f1,f1};
    ap[1](&myapplicant);

    typedef const char * (*p_fun) (const applicant *, const applicant *);
    p_fun (*pa)[10];
    return 0;
}



void printvalues(const int * ar, int size) {
    for(int i = 0; i < size; i++) {
        std::cout<<ar[i]<< std::endl;
    }
}
void setvalues(int * ar, int size, int value){
    for(int i = 0; i < size; i++) {
        ar[i] = value;
    }
}

void setvaluesrange(int * ar, int * finish, int value){
    while(ar < finish) {
        *ar = value;
        ar++;
    }
}

double getmax(const double * ar, int size) {
    double max = ar[0]; // has to be either a first element, or -DBL_MAX
    for(int i = 1; i < size; i++){
       if (ar[i] > max) {
           max = ar[i];
       }
    }
    return max;
}

// 8
// here i wasn't using pointers in a smart way
int replace(char * str, char c1, char c2) {
    int i = 0;
    int replacecount = 0;
    while(str[i] != '\0') {
        if (str[i] == c1) {
            str[i] = c2;
            replacecount++;
        }
        i++;
    }
    return replacecount;
}

// 12
void displayapplicant(applicant a) {
    using namespace std;
    cout << "Name:" << a.name << endl;
    cout << "Credit ratings: ";
    for(int i = 0; i < 3; i++) {
        cout << a.credit_ratings[i] << ", ";
    }
    cout << endl;
}
void displayapplicant_p(const applicant * a) {
    using namespace std;
    cout << "Name:" << a->name << endl;
    cout << "Credit ratings: ";
    for(int i = 0; i < 3; i++) {
        cout << a->credit_ratings[i] << ", ";
    }
    cout << endl;
}

// 13
void f1(applicant * a){
    std::cout<<a->name<<std::endl;
};
const char * f2(const applicant * a1, const applicant * a2){
    std::cout<<a1->name << " and " << a2->name<<std::endl;
    return nullptr;
};