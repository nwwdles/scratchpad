#include <iostream>

int calls = 0; // this would be a fine static variable inside printstr

void printstr(const char *, int times = 0);

int main(int argc, char const *argv[])
{
    printstr("hello", 13); //  print once
    printstr("hello", 13); //  print two times more
    printstr("hello", 0); //  print once more
    return 0;
}

void printstr(const char * str, int times){
    using namespace std;
    calls++;
    int count = (times != 0)? calls : 1;
    for(int i = 0; i < count; i++){
        cout << str << endl;
    }
}