#include <iostream>

struct CandyBar {
    char * brand;
    double weight;
    int calories;
};

void setCandyBar(CandyBar & b, char * brand = "Millenium Munch", double weight = 2.85, int calories = 350);
void displayCandyBar(const CandyBar & b);

int main(int argc, char const *argv[])
{
    CandyBar mybar;
    setCandyBar(mybar);
    displayCandyBar(mybar);

    CandyBar anotherbar;
    setCandyBar(anotherbar, "Generic Candy Bar", 1.19, 330);
    displayCandyBar(anotherbar);
    return 0;
}

void setCandyBar(CandyBar & b, char * brand, double weight, int calories){
    b.brand = brand;
    b.weight = weight;
    b.calories = calories;
}
void displayCandyBar(const CandyBar & b){
    using namespace std;
    cout << "brand: " <<b.brand <<endl;
    cout << "weight: " <<b.weight <<endl;
    cout << "calories: " <<b.calories <<endl;
}