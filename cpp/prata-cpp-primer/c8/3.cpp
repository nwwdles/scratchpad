#include <iostream>
#include <cctype>
#include <string>

std::string & stringUpper(std::string&);

int main(int argc, char const *argv[])
{
    using namespace std;
    string s;
    while(true){
        cout<<"Enter a string (q to quit): ";
        getline(cin, s);
        if (s == "q")
            break;
        cout << stringUpper(s) << endl;
    }
    cout << "Bye." << endl;
}

std::string & stringUpper(std::string& s){
    for(int i = 0; i < s.size(); i++) {
        s[i] = toupper(s[i]);
    }
    return s;
}