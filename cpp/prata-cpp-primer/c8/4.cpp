#include <iostream>
#include <cstring>
using namespace std;

struct stringy {
    char * str;
    int ct;
};

void set(stringy &, char *);
void show(const char *, int t = 1);
void show(const stringy &, int t = 1);

int main()
{
    stringy beany;
    char testing[] = "Reality isn't what it used to be.";
    set(beany, testing); // first argument is a reference,
        // allocates space to hold copy of testing,
        // sets str member of beany to point to the
        // new block, copies testing to new block,
        // and sets ct member of beany
    show(beany);
    // prints member string once
    show(beany, 2);
    // prints member string twice
    testing[0] = 'D';
    testing[1] = 'u';
    show(testing);
    // prints testing string once
    show(testing, 3);
    // prints testing string thrice
    show("Done!");
    return 0;
}


void set(stringy & s, char * cs) {
    s.ct = strlen(cs);
    s.str = new char [s.ct];
    strcpy(s.str, cs);
    // for(int i = 0; i < s.ct; i++)
    // {
    //     s.str[i] = cs[i];
    // }
    
}

void show(const char * s, int times){
    for(int i = 0; i < times; i++){
        cout << s;
    }
    cout << endl;
}

void show(const stringy & s, int times){
    for(int i = 0; i < times; i++){
        cout << s.str;
    }
    cout << endl;
}
