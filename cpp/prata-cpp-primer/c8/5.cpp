#include <iostream>
#include <cstring>

template <typename T>
T max5(T* a) {
    T max = a[0];
    for(int i = 0; i < 5; i++){
        if (a[i] > max) {
            max = a[i];
        }
    }
    return max;
}


int main(int argc, char const *argv[])
{
    using namespace std;
    double doubles[5] = {3., 4., 123., 44., 66.};
    cout << max5(doubles) << endl;
    int ints[5] = {3, 4, 5,6,7};
    cout << max5(ints) << endl;
}