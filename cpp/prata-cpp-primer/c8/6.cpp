#include <iostream>
#include <cstring>

template <typename T>
T maxn(T* a, int n) {
    T max = a[0];
    for(int i = 0; i < n; i++){
        if (a[i] > max) {
            max = a[i];
        }
    }
    return max;
}

template <> 
char * maxn(char ** a, int n) {
    char * longest = a[0];
    int maxlen = strlen(longest);
    int len = maxlen;
    for(int i = 1; i < n; i++){
        len = strlen(a[i]);
        if (len > maxlen) {
            maxlen = len;
            longest = a[i];
        }
    }
    return longest;
}

int main(int argc, char const *argv[])
{
    using namespace std;
    double doubles[4] = {3., 4., 123., 44.};
    cout << maxn(doubles, 4) << endl;
    int ints[6] = {3, 4, 5,6,7, 1};
    cout << maxn(ints, 6) << endl;

    char a[] = "hi";
    char b[] = "hello there";
    char c[] = "435";
    char d[] = "43635";
    char e[] = "43885s5";
    char* strs[5] = {a,b,c,d,e};
    cout << maxn(strs, 5) << endl;

}