#include <string>
#include <iostream>
#include <array>
// 1
// short functions that are executed fast

// 2
//a
void song(const char * name, int times = 1);
//b none
//c yes, in the same way

// 3
void iquote(int); 
void iquote(double);
void iquote(std::string);

// 4
struct box
{
    char maker[40];
    float height;
    float width;
    float length;
    float volume;
};

void displaybox(const box &);
void setboxvolume(box &);

// 5 see review-5.cpp

// 6
// a
double mass(double density, double volume = 1.);
// b
void repeat(int times, const char * str);
void repeat(const char * str);
// c
int average(int a, int b);
double average(double a, double b);
//d
// char * mangle(char * str); won't work
// both have the same signatures, is it even doable?

// 7
template <typename T>
T larger(T a, T b) {
    return a>b?a:b;
};
// 8
template <> box larger(box a, box b){
    return a.volume > b.volume? a : b;
}

// 9
// v1 - float
// v2 - float &
// v3 - float &
// v4 - int
// v5 - double

int main(int argc, char const *argv[])
{
    // 3
    iquote(3);
    iquote(4.5);
    iquote("hello");
    // 4
    box mybox {"BoxCraft", 1, 2, 3, 0};
    displaybox(mybox);
    setboxvolume(mybox);
    displaybox(mybox);

    // 7
    std::cout << larger(3, 60) << std::endl;
    std::cout << larger(343.f, 60.5f) << std::endl;

    // 8     
    box biggerbox {"BoxMaster", 2, 3, 4, 0};
    setboxvolume(biggerbox);
    displaybox(larger(mybox, biggerbox));
    
    return 0;
}



// 3
void iquote(int a){
    std::cout << '"' << a <<'"'<<std::endl;
}
void iquote(double a){
    std::cout << '"' << a <<'"'<<std::endl;
}
void iquote(std::string a){
    std::cout << '"' << a <<'"'<<std::endl;
}
// 4
void displaybox(const box & b){
    using namespace std;
    cout <<  "maker:\t" << b.maker << endl;
    cout <<  "height:\t" << b.height << endl;
    cout <<  "width:\t" << b.width << endl;
    cout <<  "length:\t" << b.length << endl;
    cout <<  "volume:\t" << b.volume << endl;
}
void setboxvolume(box & b){
    b.volume = b.height * b.length * b.width;
}