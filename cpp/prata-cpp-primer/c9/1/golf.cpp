#include "golf.h"
#include <cstring>
#include <iostream>

void setgolf(golf& g, const char* name, int hc)
{
    strncpy(g.fullname, name, Len);
    g.handicap = hc;
}

int setgolf(golf& g)
{
    int hc;
    char name[Len];
    std::cout << "Please enter name: ";

    std::cin.getline(name, Len);
    if (name[0] == '\0') {
        return 0;
    }

    std::cout << "Please enter handicap: ";
    std::cin >> hc;
    std::cin.get(); // get rid of newline

    setgolf(g, name, hc);
    return 1;
}

void handicap(golf& g, int hc) { g.handicap = hc; }

void showgolf(const golf& g)
{
    std::cout << "Name:" << g.fullname;
    std::cout << "\tHandicap:" << g.handicap << "\n";
}