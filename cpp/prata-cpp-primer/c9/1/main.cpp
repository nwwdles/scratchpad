#include "golf.h"
#include <iostream>

int main(int argc, char const* argv[])
{

    const int arsize = 10;
    int count = 0;
    golf golfs[arsize];

    for (int i = 0; i < arsize; i++) {
        if (!setgolf(golfs[i])) {
            break;
        } else {
            count++;
        }
    }
    for (int i = 0; i < count; i++) {
        showgolf(golfs[i]);
    }

    std::cout << "Setting handicaps to 0\n";
    for (int i = 0; i < count; i++) {
        handicap(golfs[i], 0);
    }
    for (int i = 0; i < count; i++) {
        showgolf(golfs[i]);
    }

    return 0;
}