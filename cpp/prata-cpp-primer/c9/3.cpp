// static.cpp -- using a static local variable
#include <cstring>
#include <iostream>
#include <new>
#include <string>

struct chaff {
    char dross[20];
    int slag;
};

void setChaff(chaff& c, const char* d, int s);
void showChaff(const chaff& c);

int main()
{
    // while (true) {
    int bufsize = 64;
    char* buf = new char[bufsize];
    chaff* chaffs = new (buf) chaff[2];

    setChaff(chaffs[0], "yo", 32);
    setChaff(chaffs[1], "yohallo", 63);

    for (int i = 0; i < 2; i++) {
        showChaff(chaffs[i]);
    }

    std::cout << "Buffer contents:\n";
    for (int i = 0; i < bufsize; i++) {
        std::cout << buf + i;
    }
    std::cout << "\n";

    // delete[] chaffs; // gives double free
    delete[] buf; // i can just delete the buffer, right?
    // }

    return 0;
}

void setChaff(chaff& c, const char* d, int s)
{
    strcpy(c.dross, d);
    c.slag = s;
}

void showChaff(const chaff& c)
{
    std::cout << "Dross: " << c.dross << "\t Slag: " << c.slag << "\n";
}