// static.cpp -- using a static local variable
#include "sales.h"
#include <iostream>

int main()
{
    SALES::Sales sales[2];
    SALES::setSales(sales[0]);
    double quartersales[4] = {13., 34., 54., 77.};
    SALES::setSales(sales[1], quartersales, 4);

    for (int i = 0; i < 2; i++) {
        SALES::showSales(sales[i]);
    }

    return 0;
}