#include "sales.h"
#include <iostream>
namespace SALES
{
// copies the lesser of 4 or n items from the array ar
// to the sales member of s and computes and stores the
// average, maximum, and minimum values of the entered items;
// remaining elements of sales, if any, set to 0
void setSales(Sales& s, const double ar[], int n)
{
    n = (n > 4) ? 4 : n;
    double min = ar[0];
    double max = ar[0];
    double sum = ar[0];
    s.sales[0] = ar[0];
    for (int i = 1; i < n; i++) {
        sum += ar[i];
        s.sales[i] = ar[i];
        if (ar[i] > max) {
            max = ar[i];
        } else if (ar[i] < min) {
            min = ar[i];
        }
    }
    double avg = sum / n;

    s.min = min;
    s.max = max;
    s.average = avg;
}
// gathers sales for 4 quarters interactively, stores them
// in the sales member of s and computes and stores the
// average, maximum, and minimum values
void setSales(Sales& s)
{
    double ar[4] = {};
    int count = 0;
    std::cout << "Enter sales by quarter:\n";
    for (int i = 0; i < 4; i++) {
        std::cout << i + 1 << ". ";
        std::cin >> ar[i];
        if (!std::cin) {
            // bad input
            std::cin.clear();
            while (std::cin.get() != '\n') {
                continue;
            }
        } else {
            count++;
        }
    }
    setSales(s, ar, count);
}
// display all information in structure s
void showSales(const Sales& s)
{
    std::cout << "Average: " << s.average << "\tMin: " << s.min
              << "\tMax: " << s.max << "\n";

    std::cout << "By quarter: ";
    for (int i = 0; i < QUARTERS; i++) {
        std::cout << s.sales[i] << " ";
    }
    std::cout << "\n";
}
} // namespace SALES
