// 1:
// a. automatic storage
// b. static with external linkage
// c. static with internal linkage // or in unnamed namespace
// d. static inside a function // local static

// 2:
// using directive imports everything from a namespace
// it doesn't produce errors if something inside the namespace
// has the same name as already declared variable

// 3:
// #include <iostream>
// int main() {
//     double x;
//     std::cout << "Enter value: ";
//     while (!(std::cin >> x)) {
//         std::cout << "Bad input. Please enter a number: ";
//         std::cin.clear();
//         while (std::cin.get() != '\n')
//             continue;
//     }
//     std::cout << "Value = " << x << std::endl;
//     return 0;
// }

// 4:
// #include <iostream>
// int main() {
//     using std::cout;
//     using std::cin;
//     using std::endl;
//     double x;
//     cout << "Enter value: ";
//     while (!(cin >> x)) {
//         cout << "Bad input. Please enter a number: ";
//         cin.clear();
//         while (cin.get() != '\n')
//             continue;
//     }
//     cout << "Value = " << x << endl;
//     return 0;
// }

// 5:
// declare two functions with internal linkage by using static keyword
// or in unnamed namespace

// 6:
// 10
// 4
// 0
// Other: 10, 1
// another(): 10, -4

// 7
// 1
// 4, 1, 2
// 2
// 2
// 4, 1, 2
// 2