#include <climits>
#include <iostream>
using namespace std;

template <class T1, class T2> auto sum(T1 a, T2 b) -> decltype(a + b)
{
    return a + b;
}

int main()
{
    unsigned char ac = 127, bc = 255;
    cout << sum(ac, bc) << endl;

    short as = 7, bs = 3;
    cout << sum(as, bs) << endl;

    unsigned int ai = UINT_MAX, bi = 1;
    cout << sum(ai, bi) << endl;

    cout << sum(ai, 0) << endl;
}