// https://www.reddit.com/r/dailyprogrammer/comments/aphavc/20190211_challenge_375_easy_print_a_new_number_by/
// A number is input in computer then a new no should get printed by adding one
// to each of its digit. If you encounter a 9, insert a 10 (don't carry over,
// just shift things around).

// For example, 998 becomes 10109.

#include <iostream>

int digincrement_bad(int num)
{
    int out = 0;
    int shift = 1;
    for (int i = 1; num / i > 0; i *= 10) {
        int digit = (num / i) - 10 * (num / (i * 10));
        out += (digit + 1) * (i * shift);
        if (digit + 1 >= 10) {
            shift *= 10;
        }
        std::cout << digit << " " << out << "\n";
    }
    return out;
}

int digincrement(int num)
{
    int out = 0;
    int shift = 1;
    for (; num > 0; num /= 10) {
        int digit = num % 10 + 1;
        out += digit * shift;
        shift *= 10;
        if (digit >= 10) {
            shift *= 10;
        }
        std::cout << digit - 1 << " " << out << "\n";
    }
    return out;
}

int main(int argc, char const* argv[])
{
    digincrement(530189);
    digincrement(999);
    return 0;
}
