#include <algorithm>
#include <iostream>
#include <vector>

void printv(std::vector<int>& v)
{
    for (auto var : v) {
        std::cout << var;
    }
    std::cout << "\n";
}
bool havelhakimi(std::vector<int>& v)
{
    printv(v);

    while (true) {
        auto last = std::remove_if(v.begin(), v.end(),
                                   [](int val) { return val <= 0; });
        v.erase(last, v.end());
        if (v.size() <= 0) {
            return true;
        }

        std::sort(v.rbegin(), v.rend());
        int n = v[0];
        v.erase(v.begin());
        if (n > v.size()) {
            return false;
        }

        std::transform(v.begin(), v.begin() + n, v.begin(),
                       [](int v) { return v -= 1; });

        printv(v);
    }
}
int main(int argc, char const* argv[])
{
    std::vector<int> v{1, 1};
    std::cout << havelhakimi(v) << "\n";
    return 0;
}
