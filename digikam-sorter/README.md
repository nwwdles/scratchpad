# digiKam sorter

Automatically sorts images into folders based on image tags.

```
usage: digikamsorter.py [-h] [-f FILE] [-m]

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  specify config file (relations.json by default)
  -m, --move            actually move files
```

## Example config (relations.json):

<!-- - Only `&&` operation is supported for tags (it's literally hardcoded and `&&` is actually just a delimiter because I'm lazy) -->
  <!-- - No functionality is lost, you just need to write multiple lines instead of one. -->

- Tags support logical operators ( `~~` NOT, `||` OR, `&&` AND, brackets `{{` `}}`). I chose these ones because they're not often used in tags.
- Line order matters. Lower lines are prioritized (`"tag1"` should go before `"tag1 && tag2"`)
- It doesn't verify tags, so it's (probably) possible for you DROP TABLE etc if you really try to. Most likely it would give you an error that tag named 'DROP TABLE' isn't found though.

```json
{
  "database": "/home/user/.digikam/digikam4.db",
  "album": "/home/user/Pictures/",
  "relations": {
    "meme": "random/memes",
    "meme && ~~funny": "random/unfunny memes",
    "wallpaper": "wallpapers",
    "wallpaper && breeze": "wallpapers/breeze",
    "wallpaper && solarized": "wallpapers/solarized"
  }
}
```

## TODO:

- [x] support logical operators

  - [stackoverflow: SQL include AND exclude query](https://stackoverflow.com/questions/21413523/sql-include-and-exclude-query)
  - [stackoverflow: How to exclude records with certain values in sql](https://stackoverflow.com/questions/20144027/how-to-exclude-records-with-certain-values-in-sql)
  - sql would look something like this (maybe it's not the most optimal way but it works and a statement is simple to assemble)

  ```sql
  SELECT DISTINCT imageid FROM ImageTags WHERE (
    (
      tagid=57
      AND imageid IN (SELECT imageid FROM ImageTags WHERE tagid=8)
    )
    AND NOT imageid IN (SELECT imageid FROM ImageTags WHERE tagid=33)
  )
  ```

- [ ] use `!` instead of `~~` and `()` instead of `{{}}`
  - tags containing these characters will have to be enclosed in `''`
  - or support using backslashes to escape the characters when used in tags
