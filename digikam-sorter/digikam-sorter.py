#!/usr/bin/env python3
from argparse import ArgumentParser
import sqlite3
import os.path
import hashlib
import json
from pathlib import Path


def md5_checksum(filePath):
    with open(filePath, "rb") as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()


def get_image_id(c, path):
    # table: Images
    # columns: name fileSize uniqueHash
    filename = os.path.basename(path)
    filesize = os.path.getsize(path)
    t = (filename,)
    c.execute("SELECT id,fileSize,uniqueHash FROM Images WHERE name=?", t)
    results = c.fetchall()
    image_id = -1

    if len(results) > 1:
        # len > 1 when we have multiple files with the same name
        size = -1
        # try to compare by size
        compareBySize = True
        for entry in results:
            newsize = entry[1]
            if newsize == size:
                compareBySize = False
                break
            else:
                size = newsize
                if size == filesize:
                    image_id = entry[0]
        if not compareBySize:
            # compare by md5 since fast methods have failed us
            md5 = md5_checksum(path)
            for entry in results:
                if entry[2] == md5:
                    image_id = entry[0]
                    break
    elif len(results) == 1:
        image_id = results[0][0]

    return image_id


def get_image_tag_ids(c, imageid):
    t = (imageid,)
    c.execute("SELECT tagid FROM ImageTags WHERE imageid=?", t)
    results = c.fetchall()
    return results


def get_tag_name(c: sqlite3.Cursor, tagId: int):
    t = (tagId,)
    c.execute("SELECT name FROM Tags WHERE id=?", t)
    results = c.fetchall()
    if len(results) < 1:
        raise Exception("Tag not found! Tag ID: " + tagId)
    return results[0][0]


def get_tag_id(c: sqlite3.Cursor, tagName: str):
    t = (tagName,)
    c.execute("SELECT id FROM Tags WHERE name=?", t)
    results = c.fetchall()
    if len(results) < 1:
        raise Exception("Tag not found! Tag name: " + tagName)
    return results[0][0]


def get_move_paths(c: sqlite3.Cursor, lib: str, subfolder: str, idfoldermap: dict):
    # decide where to move files and store paths in map
    movepaths = {}
    walkpath = os.path.join(lib, subfolder)
    for root, dirs, files in os.walk(walkpath):
        # ignore folders
        if "oc" in dirs:
            dirs.remove("oc")
        if "Screenshots" in dirs:
            dirs.remove("Screenshots")
        if "_sets-small" in dirs:
            dirs.remove("_sets-small")
        if "sets" in dirs:
            dirs.remove("sets")
        if ".dtrash" in dirs:
            dirs.remove(".dtrash")

        for name in files:
            oldpath = os.path.join(root, name)
            imageid = get_image_id(c, oldpath)

            if imageid in idfoldermap:
                folder = idfoldermap[imageid]
                newpath = os.path.join(lib, folder, name)
                if newpath != oldpath:
                    # print(imageid)
                    print("old path:", oldpath)
                    print("new path:", newpath)
                    movepaths[oldpath] = newpath
    return movepaths


def move_files(movepaths: dict):
    for oldpath in movepaths:
        newpath = movepaths[oldpath]
        try:
            if not os.path.isfile(newpath):
                os.rename(oldpath, newpath)
            else:
                print("duplicate name", oldpath, "->", newpath)
        except FileNotFoundError as e:
            folder = os.path.dirname(newpath)
            os.makedirs(os.path.join(lib, folder))
            os.rename(oldpath, newpath)


def tags_to_ids_list(c: sqlite3.Cursor, tags: str):
    tagslist = tags.split("&&")
    tagslist = map(str.strip, tagslist)
    # remove empty tags and substitute with ids
    tagslist = [get_tag_id(c, x) for x in tagslist if x]
    return tagslist


def tags_to_query_ext(c: sqlite3.Cursor, tags: str):

    # get rid of operators so that only tags delimited nicely are left
    delim = "&&"
    tagstring = (
        tags.replace("||", delim)
        .replace("{{", delim)
        .replace("}}", delim)
        .replace("~~", delim)
    )
    tagnames = tagstring.split(delim)
    tagnames = list(map(str.strip, tagnames))  # strip whistespace from tags
    tagnames = [x for x in tagnames if x]  # and remove empty tags

    tagids = [get_tag_id(c, x) for x in tagnames]  # make a list of tag ids

    query = ""

    if len(tagids) == 1:
        # with one tag query is simple
        tagid = tagids[0]
        query = 'SELECT imageid FROM ImageTags WHERE tagid="' + str(tagid) + '"'
    elif tagids:
        # with multiple elements query is more complex
        for i in range(len(tagnames)):
            condition = "tagid=" + str(tagids[i])
            if i > 0:
                condition = (
                    "imageid IN (SELECT imageid FROM ImageTags WHERE " + condition + ")"
                )
            tags = tags.replace(tagnames[i], condition)
        query = (
            tags.replace("{{", "(")
            .replace("}}", ")")
            .replace("&&", " AND ")
            .replace("~~", " NOT ")
            .replace("||", " OR ")
        )
        query = "SELECT DISTINCT imageid FROM ImageTags WHERE (" + query + ")"

    return query


def tags_to_query_simple(tagids):
    if not tagids:
        return ""

    if len(tagids) == 1:
        tagid = tagids[0]
        query = 'SELECT imageid FROM ImageTags WHERE tagid="' + str(tagid) + '"'
    else:
        start = "SELECT A.imageid FROM ImageTags A \n"
        mid = "ON "
        query = ""
        i = 0
        for tagid in tagids:
            current_table = chr(i + 97)
            if i >= 1:
                prev_table = chr(i + 96)
                start += "INNER JOIN ImageTags " + current_table + " \n"
                mid += prev_table + ".imageid = " + current_table + ".imageid and "
                query += " and "
            query += current_table + '.tagid="' + str(tagid) + '"'
            i += 1
        query = start + mid + "(" + query + ")"

    return query


def map_ids_to_folders(c, relations):
    idfoldermap = {}
    for tags in relations:
        folder = relations[tags]

        # wip
        q = tags_to_query_ext(c, tags)
        # continue

        # tagids = tags_to_ids_list(c, tags)
        # q = tags_to_query_simple(tagids)

        c.execute(q)
        results = c.fetchall()
        for res in results:
            idfoldermap[res[0]] = folder
    return idfoldermap


def create_default_json(filename: str):
    homedir = str(Path.home())
    data = {
        "database": os.path.join(homedir, ".digikam/digikam4.db"),
        "album": os.path.join(homedir, "Pictures/"),
        "relations": {"tag": "subfolder"},
    }
    with open(filename, "w") as outfile:
        json.dump(data, outfile, indent=4)


if __name__ == "__main__":
    # command line arguments
    parser = ArgumentParser()
    parser.add_argument(
        "-f",
        "--file",
        dest="filename",
        help="specify config file (relations.json by default)",
        metavar="FILE",
        default="relations.json",
    )
    parser.add_argument(
        "-m",
        "--move",
        dest="move",
        action="store_false",
        help="actually move files",
        default=False,
    )
    # parser.add_argument("-q", "--quiet",
    #                     action="store_false", dest="verbose", default=True,
    #                     help="don't print status messages to stdout")

    args = parser.parse_args()

    # open file
    filename = args.filename
    try:
        with open(filename, "r") as f:
            data = json.loads(f.read())

        db = data["database"]
        lib = data["album"]
        relations = data["relations"]

        conn = sqlite3.connect(db)
        c = conn.cursor()

        idfoldermap = map_ids_to_folders(c, relations)  # {id:folder}
        movepaths = get_move_paths(c, lib, "", idfoldermap)  # {oldpath:newpath}

        if args.move:
            move_files(movepaths)

        conn.close()
    except FileNotFoundError as e:
        if filename == parser.get_default("filename"):
            print("File not found. Creating " + filename)
            create_default_json(filename)
        else:
            print("File not found.")
