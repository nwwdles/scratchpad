#!/usr/bin/env python3
#
# fix jpeg pixel art
# it won't work with non-scaled aliased images (mspaints),
# it's not general-purpose 'jpeg articaft remover'.
#
# Reference:
# https://pillow.readthedocs.io/en/latest/reference/Image.html

from PIL import Image, ImageDraw, ImageChops, ImageFilter, ImageStat
import argparse


def color_sum(col_a, col_b):
    return (col_a[0] + col_b[0], col_a[1] + col_b[1], col_a[2] + col_b[2])


def color_div(col, factor):
    return (int(col[0] / factor), int(col[1] / factor), int(col[2] / factor))


def color_multiply(color, factor):
    return (int(color[0] * factor), int(color[1] * factor), int(color[2] * factor))


def get_avg_color(pix, x1, x2, y1, y2):
    color = (0, 0, 0)
    scoresum = 0
    w = x2 - x1
    h = y2 - y1
    totalpixcount = w * h
    for x in range(w):
        dist_x = 1 - abs(x / (w / 2) - 1)
        weightx = dist_x
        for y in range(h):
            dist_y = 1 - abs(y / (h / 2) - 1)
            weighty = dist_y
            pixcolor = pix[x1 + x, y1 + y]
            score = 1  # weightx * weighty
            # pixcolor = color_multiply(pixcolor, score)
            color = color_sum(color, pixcolor)
            scoresum += score
    color = color_div(color, max(scoresum, 1))
    final_color = color
    return final_color


def avg(l):
    sum = 0
    for i in l:
        sum += i
    return sum / len(l)


def median(l):
    s = sorted(l)
    return s[len(s) // 2]


def downsample(im, scale):
    size = im.size
    pix = im.load()
    # TODO: option get a palette from the original image
    pixelw = scale
    pixelh = scale
    pixelcenter = (pixelw // 2, pixelh // 2)
    wpixelcutout = (1, 1)  # meaningful pixels around pixelcenter
    hpixelcutout = (1, 1)

    left = pixelcenter[0] - wpixelcutout[0]
    right = pixelcenter[0] + wpixelcutout[1]
    top = pixelcenter[1] - hpixelcutout[0]
    bottom = pixelcenter[1] + hpixelcutout[1]
    original_w = size[0] // pixelw
    original_h = size[1] // pixelh

    # downsample
    out = Image.new("RGBA", (original_w, original_h), (255, 255, 255, 0))
    draw = ImageDraw.Draw(out)
    for ox in range(original_w):
        original_x = ox * pixelw
        for oy in range(original_h):
            original_y = oy * pixelh
            final_color = get_avg_color(
                pix,
                (original_x + left),
                min(original_x + right, size[0]),
                (original_y + top),
                min(original_y + bottom, size[1]),
            )
            draw.point((ox, oy), final_color)
    return out


def clusterize(l):
    prev_x = 0
    w = 1
    for x in range(w, len(l) - w):
        for i in range(-w // 2, w // 2 + 1):
            if l[x] > l[x + i]:
                l[x] = l[x] + l[x + i]
                l[x + i] = 0


def get_grid_size(sums):
    clusterize(sums)

    # get 5% biggest valleys
    valleys = sorted(enumerate(sums), key=lambda kv: kv[1], reverse=True)
    valleys = valleys[: len(valleys) // 20]

    # get distances
    valleys = sorted(valleys)
    valleydists = []
    prev_x = 0
    for x, val in valleys:
        dist = x - prev_x
        valleydists.append(dist)
        prev_x = x

    # remove/clusterize outliers
    med = median(valleydists)
    for d in reversed(range(len(valleydists) - 1)):
        if valleydists[d] < med // 4:
            valleydists[d + 1] += valleydists[d]
            valleydists.pop(d)
        elif valleydists[d] > med * 1.5:
            valleydists.pop(d)

    return median(valleydists)


def guess_scale(im):
    imageWithEdges = im.filter(ImageFilter.FIND_EDGES)
    pix = imageWithEdges.load()
    xsums = [0] * imageWithEdges.size[0]
    ysums = [0] * imageWithEdges.size[1]

    for x in range(imageWithEdges.size[0]):
        line = imageWithEdges.crop((x, 0, x + 1, imageWithEdges.size[1]))
        stat = ImageStat.Stat(line)
        xsums[x] += stat.sum[0] + stat.sum[1] + stat.sum[2]
    for y in range(imageWithEdges.size[1]):
        line = imageWithEdges.crop((0, y, imageWithEdges.size[0], y + 1))
        stat = ImageStat.Stat(line)
        ysums[y] += stat.sum[0] + stat.sum[1] + stat.sum[2]

    scale = get_grid_size(xsums + ysums)
    print("Guessed scale:", scale)
    return scale


def main():
    parser = argparse.ArgumentParser(description="Fix jpeg pixel art.")
    parser.add_argument("image", nargs="+", help="image file")
    parser.add_argument("-s", "--scale", help="original image scale (don't autodetect)")
    args = parser.parse_args()

    if not args.image:
        exit(1)

    for img_fname in args.image:
        im = Image.open(img_fname)
        if not args.scale:
            scale = guess_scale(im)
        else:
            scale = args.scale

        out = downsample(im, scale)
        out = out.resize(im.size, resample=Image.NEAREST)
        out.save(img_fname + "_out.png", "PNG")


if __name__ == "__main__":
    main()
