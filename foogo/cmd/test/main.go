package main

import (
	"bytes"
	"fmt"
	"strings"
	"text/tabwriter"
)

func main() {
	s := ""
	buf := bytes.NewBufferString(s)

	w := new(tabwriter.Writer)
	w.Init(buf, 0, 8, 0, '\t', 0)
	fmt.Fprintln(w, "a\tb\tc\td\t.")
	fmt.Fprintln(w, "123\t12345\t1234567\t123456789\t.")
	fmt.Fprintln(w)
	w.Flush()

	table := strings.Split(buf.String(), "\n")
	for _, v := range table {
		fmt.Println(v)
	}
}
