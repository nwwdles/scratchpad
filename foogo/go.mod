module gitlab.com/cupnoodles14/scratchpad/go/foogo

go 1.14

require (
	github.com/fhs/gompd v2.0.0+incompatible
	github.com/fhs/gompd/v2 v2.1.1
	github.com/gdamore/tcell v1.3.0
	github.com/rivo/tview v0.0.0-20200219210816-cd38d7432498
)
