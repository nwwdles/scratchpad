package app

import (
	"fmt"
	"log"

	"github.com/fhs/gompd/mpd"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

type App struct {
	c          *mpd.Client
	tvApp      *tview.Application
	tvRoot     *tview.Pages
	songsTable *tview.Table
}

func makeSongRow(a mpd.Attrs, keys ...string) []string {
	row := []string{}
	for _, k := range keys {
		row = append(row, a[k])
	}
	return row
}

func (app *App) displaySongs(songs []mpd.Attrs) {
	for k, v := range songs {
		color := tcell.ColorWhite
		row := makeSongRow(v, "Track", "Title", "Album", "Artist", "duration", "file")
		for i, part := range row {
			app.songsTable.SetCell(k, i, tview.NewTableCell(part).
				SetTextColor(color).SetReference(v["file"]))
		}
	}
}

func New() *App {
	app := &App{}

	c, err := mpd.Dial("tcp", "localhost:6602")
	if err != nil {
		err = fmt.Errorf(": %w", err)
		log.Printf("%v", err)
	}
	app.c = c
	app.tvApp = tview.NewApplication()

	app.tvRoot = tview.NewPages()

	app.songsTable = tview.NewTable()
	app.songsTable.Select(0, 0).SetFixed(1, 1).SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEscape {
			app.tvApp.Stop()
		}
		if key == tcell.KeyEnter {
			app.songsTable.SetSelectable(true, true)
		}
	}).SetSelectedFunc(func(row int, column int) {
		if _, c := app.songsTable.GetSelectable(); c {
			cell := app.songsTable.GetCell(row, column)
			cell.SetTextColor(tcell.ColorRed)
			f := cell.GetReference().(string)
			err := app.c.Add(f)
			if err != nil {
				err = fmt.Errorf(": %w", err)
				log.Printf("%v", err)
			}

			app.songsTable.SetSelectable(true, false)
		} else {
			app.songsTable.SetSelectable(true, true)
		}
	}).SetSelectable(true, false)

	app.tvRoot.AddPage(fmt.Sprintf("page-%d", 0), app.songsTable, true, true)

	return app
}

func (app *App) Run() {
	songs, err := app.c.ListAllInfo("/")
	if err != nil {
		err = fmt.Errorf(": %w", err)
		log.Printf("%v", err)
	}

	app.displaySongs(songs)

	if err := app.tvApp.SetRoot(app.tvRoot, true).Run(); err != nil {
		panic(err)
	}
}

func (app *App) Close() {

}
