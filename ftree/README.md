# ftree

## class structure

- Tree, TreeNode
- Application - main app class
- Page - a collection of widgets
- (Widget) - an ncurses drawable
  - Window
    - Popup
  - Action panel
  - Status bar
  - Seekbar
  - View - a special widget that's drawn to a pad first
    - ScrollArea - implements scroll position and syncScroll
      - TreeView - tree with nodes.
      - ListView - list with columns. a line can be selected
      - TableView - table with cells. a cell/column/row can be selected
