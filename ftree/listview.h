#ifndef LISTVIEW_H
#define LISTVIEW_H

#include "widget.h"
#include <algorithm>
#include <string>
#include <vector>

class ListView : public ScrollView
{
    enum class Align { left, center, right };
    std::vector<int> columns = {1, 0};

    std::vector<Align> columnAlignments = {Align::left, Align::right};
    Align separatorAlignment = Align::center;

    int columnSpacing = 1;
    int selectedIndex = 0;
    std::wstring columnSeparator = L"|";
    std::vector<std::vector<std::wstring>> items;

  public:
    ListView(){};
    ListView(const std::vector<std::vector<std::wstring>> &data)
    {
        setData(data);
    };
    void onSizeChanged(int old_w, int old_h) override
    {
        syncScroll();
        redraw_content();
    };

    void setData(const std::vector<std::vector<std::wstring>> &data)
    {
        items = data;
        setPadSize(100, items.size());
        setContentSize(padWidth, padHeight);
        // paddingTop = 1;

        selectIndex(0);
        redraw_content();
    };

    std::vector<std::wstring> &push_back(std::vector<std::wstring> &item)
    {
        items.push_back(item);
        return *(items.end()--);
    }

    void update() override{};
    int handleInput(int input) override
    {
        switch (input) {
        case KEY_DOWN:
            selectIndex(selectedIndex + 1);
            break;
        case KEY_UP:
            selectIndex(selectedIndex - 1);
            break;
        case 'r':
        case 'R':
            redraw_content();
            break;
        default:
            return 1;
        }
        return 0;
    };

    void redraw_content() const override
    {
        for (size_t i = 0; i < items.size(); i++) {
            drawItem(i);
        }
    };
    void drawItem(int n) const
    {
        wmove(pad, n, 0);
        // wclrtoeol(pad);
        auto text = getIndexText(n);
        if (n == selectedIndex) {
            auto attr = A_STANDOUT;
            wattron(pad, attr);
            waddnwstr(pad, text.c_str(), padWidth);
            wattroff(pad, attr);
        } else {
            waddnwstr(pad, text.c_str(), padWidth);
        }
    }
    void drawItemSplit(int n) const
    {
        int xoffset = 0;
        auto &item = items[n];
        for (auto column : columns) {
            int colW = getColumnWidth(column);
            if (column < item.size()) {
                wmove(pad, n, xoffset);
                auto text = items[n][column];
                if (n == selectedIndex) {
                    auto attr = A_STANDOUT;
                    wattron(pad, attr);
                    // wprintw(pad, "%s\n", items[n][0].c_str());
                    waddnwstr(pad, text.c_str(), colW);
                    wattroff(pad, attr);
                } else {
                    waddnwstr(pad, text.c_str(), colW);
                }
            }
            xoffset += colW + columnSpacing;
        }
    }
    const std::wstring getIndexText(int n) const
    {
        std::wstring out;
        auto &item = items[n];
        for (size_t i = 0; i < columns.size(); i++) {
            auto &column = columns[i];
            auto colW = getColumnWidth(column);
            if (i == columns.size() - 1) {
                out += alignPadString(item[column], colW,
                                      columnAlignments[column]);
            } else {
                out += alignPadString(item[column], colW,
                                      columnAlignments[column]) +
                       alignPadString(columnSeparator, columnSpacing,
                                      separatorAlignment);
            }
        }
        return out;
    }

    void selectIndex(int n)
    {
        int prev = selectedIndex;
        selectedIndex = std::min(n, static_cast<int>(items.size()) - 1);
        selectedIndex = std::max(selectedIndex, 0);
        drawItem(prev);
        drawItem(selectedIndex);
    }

    int getColumnWidth(int n) const
    {
        return (std::min(width, padWidth) -
                (columnSeparator.size() * columns.size() - 1)) /
               columns.size();
    }

    // pads/cuts and aligns string to n characters
    static std::wstring alignPadString(const std::wstring &str, int n,
                                       Align alignment = Align::center)
    {
        auto out = str;
        auto padChar = ' ';
        int size = n - out.size();
        if (size > 0) {
            switch (alignment) {
            case Align::right:
                out.insert(0, size, padChar);
                break;
            case Align::left:
                out.append(size, padChar);
                break;
            default:
                out.insert(0, size / 2, padChar);
                out.append(size / 2 + size % 2, padChar);
            }
        }
        return out.substr(0, n);
    }
};

#endif // LISTVIEW_H
