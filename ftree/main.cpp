//
// ftree (will probably be) a simple tool to display trees in terminal
// It should:
// - accept a list of paths as arguments
//    - or one path that's going to be the root
// - output a clicked path to stdout
// - react to stdin by looking up the passed path
//
// remember expansion info

#include "listview.h"
#include "treeview.h"
#include <iostream>
#include <ncurses.h>
#include <random>

class Application
{
    int width;
    int height;
    TreeView tree_view;
    ListView list_view;

    void init_curses()
    {
        initscr();
        cbreak();
        noecho();
        curs_set(false);
        keypad(stdscr, true);
        scrollok(stdscr, true);

        if (!has_colors()) {
            mvprintw(0, 1, "Terminal doesn't support colors");
        } else {
            if (can_change_color()) {
                init_color(COLOR_CYAN, 9, 99, 999);
            }
            // start_color();
            // init_pair(1, COLOR_MAGENTA, COLOR_CYAN);
            // init_pair(2, COLOR_GREEN, COLOR_RED);
        }
        clear();
    }

    void stop_curses()
    {
        echo();
        keypad(stdscr, false);
        curs_set(true);
        endwin();
    }

    void on_size_changed()
    {
        getmaxyx(stdscr, height, width);
        tree_view.setRect(0, 0, width / 2, height);
        list_view.setRect(width / 2, 0, width / 2, height);
        clear();
        draw();
    }

    void draw()
    {
        refresh();
        tree_view.refresh();
        list_view.refresh();
    };

  public:
    Application(std::vector<std::string> data)
    {
        init_curses();
        tree_view.setData(data);
        list_view.setData(                              //
            {{L"line1", L"col2zzzzzzzzzzzzzzzzzzzzzz"}, //
             {L"и "
              L"юникод😦😧😨😩🤯😬😰😱🥵🥶😳🤪"
              L"😵"
              L"😡"
              L"😠"
              L"🤬"
              L"😷"
              L"🤒"
              L"🤕"
              L"🤢"
              L"🤮"
              L"🤧",
              L"col2"}} //
        );
    };
    Application(const char *data)
    {
        init_curses();
        // TODO: create data suitable for the TreeView...
        // tree_view.initialize(data);
    };

    ~Application() { stop_curses(); }

    int exec()
    {
        on_size_changed();

        const int BUF_SIZE = 128;
        char buf[BUF_SIZE];
        int i = 0;
        // std::string input;

        auto focused_view = &tree_view;

        draw();
        while (true) {
            int input = getch();
            int status = 0;
            switch (input) {
            case KEY_RESIZE:
                on_size_changed();
                draw();
                // clear();
                // mvprintw(0, 0, "new size: %dx%d", width, height);
                // refresh();
                break;
            case 'Q':
            case 'q':
                return 0;
            default:
                status = focused_view->handleInput(input);
                status = list_view.handleInput(input);
                if (status) {
                    // todo: use std::cin
                    // std::cin >> input;
                    // buf[0] = input;
                    // getnstr(buf + 1, BUF_SIZE - 1);
                    // mvprintw(5, 0, "You entered: %s", buf);
                }
                // std::cout << buf << "\n";
                // if (i < BUF_SIZE) {
                //     buf[++i] = input;
                // }
                draw();
            }
        }
        // while strcmp(buf, "STOP"));

        // while (true) {
        //     char c = getch();
        //     printw("%d\n", c);

        //     if (c == 'Q' || c == 'q') {
        //         break;
        //     }
        //     draw();
        //     wrefresh(win);
        // }
        // std::cout << "Hello world!\n";
        return 0;
    }
};

std::vector<std::string> dummyData()
{
    std::vector<std::string> test{
        {"/first/secondlevel1"},
        {"/first/secondlevel2"},
        {"/first/secondlevel1/thirdlevel1"},
        {"/second/anotherlevel"},
        {"/third"},
        // clang-format off
        {"/юникод 😇🤠🤡🥳🥴🥺🤥🤫🤭🧐🤓😈👿👹👺💀👻👽"},
        // clang-format on
        {"/superlooooooooooooooooooooooooooooooooooooooooooooooooooo"
         "oooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
         "oooooooooooooooooooooooooong"}
        //
    };

    srand(time(NULL));
    for (int i = 0; i < 8; ++i) {
        int c = rand() % 4 + 2;
        std::string part;
        for (int j = 0; j < c; ++j) {
            part += "/" + std::string(1 + rand() % 1, char(66 + rand() % 5));
        }
        test.push_back(part);
    }

    return test;
}

int main(int argc, char const *argv[])
{
    setlocale(LC_ALL, ""); // compile with -lncursesw

    StringTree tree{"test"};
    // auto node = tree.addChild(StringNode(std::string("test")));

    auto node = tree.emplaceChild("test");
    // Application app(dummyData());
    // return app.exec();

    // if (argc <= 1) {
    //     // std::cout << "Please provide a path or data.\n"
    // } else if (argc == 2) {
    //     std::cout << "Path: " << argv[1];
    //     Application app(argv[1]);
    //     app.exec();
    // } else {
    //     // data
    // }
    // return 0;
}
