/*
 * Usage:
 *
 *  1. Inherit from a tree node and add data members:
 *      class MyDataNode : public ExtendedTreeNode<MyDataNode> {...};
 *
 *  2. Use your tree node class as a template parameter for the Tree:
 *      Tree<MyDataNode> tree;
 */

#ifndef TREE_H_
#define TREE_H_

#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

/*
 * BasePtrTreeNode holds a vector of smart pointers to its children.
 * To make it hold data, inherit it and define data members.
 *
 * Inherit from ExtendedTreeNode if you need access to parents/siblings.
 *
 * Example:
 * class MyDataNode : public BasePtrTreeNode<MyDataNode> {...};
 */
template <typename T> class BasePtrTreeNode
{
    using ChildType = std::unique_ptr<T>;
    std::vector<ChildType> children{};

  public:
    /*
     * Adding children
     */
    // todo: learn which &&/& I should define
    ChildType &addChild(ChildType &&node)
    {
        children.push_back(node);
        return getLastChild();
    };
    ChildType &addChild(const ChildType &node)
    {
        children.push_back(node);
        return getLastChild();
    };

    // Construct child in place
    template <typename... Args> ChildType &emplaceChild(Args &&... args)
    {
        children.emplace_back(std::make_unique<T>(std::forward<Args>(args)...));
        return getLastChild();
    }

    /*
     * Managing children
     */

    // Delete all children.
    void clear() { children.clear(); }

    /*
     * Getting children
     */
    // todo: this should be an iterator
    ChildType &findChildIf(std::unary_function<T, bool> f)
    {
        return find_if(children.begin(), children.end(), f);
    }

    int childIndex(const T &node) const
    {
        auto it =
            find_if(children.cbegin(), children.cend(),
                    [&node](const ChildType &c) { return c.get() == &node; });
        return it - children.cbegin();
    }

    const ChildType &getLastChild() const { return *(--getChildren().end()); }
    ChildType &getLastChild() { return *(--getChildren().end()); }

    const std::vector<ChildType> &getChildren() const { return children; };
    std::vector<ChildType> &getChildren() { return children; };

    // populate a vector with non-smart pointers to children
    void listChildren(std::vector<T *> &list)
    {
        list.push_back(static_cast<T *>(this));
        for (ChildType &child : children) {
            child->listChildren(list);
        }
    };

    /*
     * printing functions
     */
    // print information about the node (redefine to print datamembers)
    virtual void printSelf(std::ostream &os, int depth = 0) const
    {
        os << std::string(depth, ' ') << "node\n";
    };

    // print information about the node and the children
    void print(std::ostream &os, int depth = 0) const
    {
        printSelf(os, depth);
        for (const auto &child : children) {
            child->print(os, depth + 1);
        }
    }

    /*
     * operators
     */
    ChildType &operator[](int idx) { return children[idx]; };
    const ChildType &operator[](int idx) const { return children[idx]; };
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const BasePtrTreeNode<T> &node)
{
    node.print(os); // it will call the base class's print function though
    return os;
};

/*
 * This class adds some parent/sibling getter functions to BasePtrTreeNode.
 * No data is held, inherit from this class and define data members.
 *
 * Example:
 * class MyDataNode : public ExtendedTreeNode<MyDataNode> {...};
 */
template <typename T> class ExtendedTreeNode : public BasePtrTreeNode<T>
{
    using Node = T;
    Node *parent = nullptr;

  public:
    ExtendedTreeNode(Node *p = nullptr) : parent(p){};

    Node *getParent() const { return parent; }

    // Returns the tree root //TODO: or nullptr if the node is the tree root?
    Node *getTreeRoot()
    {
        // TODO: is static_cast the right choice in this case since we know that
        // the object is of type T or do I need a dynamic_cast?
        // auto p = static_cast<Node *>(this);
        auto p = static_cast<Node *>(this);
        while (auto newp = p->getParent()) {
            p = newp;
        }
        return p;
    }

    // Get other node with the same parent.
    Node *getSibling(int indexOffset)
    {
        auto *t = static_cast<Node *>(this);
        auto p = getParent();
        if (p) {
            auto index = p->childIndex(*t) + indexOffset;
            if (index >= 0 && index < p->getChildren().size()) {
                return p->getChildren()[index].get();
            }
        }
        return t;
    };
};

/*
 * A more traditional data node with parent/sibling methods
 */

/*
 * Tree should use your custom TreeNode (the one with data members) as the
 * template parameter.
 * It's just a pretty name to signify that it's a tree root and not just a node.
 *
 * Also it has a constructor with an initializer list.
 *
 * Constructor can be moved to Node class but it would require explicitly
 * redeclaring it.
 * https://stackoverflow.com/questions/347358/inheriting-constructors
 */
template <typename T> class Tree : public T
{
  public:
    Tree(){};
    Tree(const std::initializer_list<T> &list)
    {
        for (const auto &var : list) {
            addChild(var);
        }
    };
};

template <typename T>
class ExtDataNode : public ExtendedTreeNode<ExtDataNode<T>>
{
  public:
    T value;
    ExtDataNode(const T &val) : value(val){};
    ExtDataNode(T &&val) : value(val){};
    ExtDataNode(){};
};
/*
 * A more traditional tree that can be used without defining a custom node class
 */
template <typename T> class DataTree : public ExtDataNode<T>
{
  public:
    // using Tree<ExtDataNode<T>>::Tree;
    DataTree(){};
    DataTree(const std::initializer_list<T> &list)
    {
        for (const auto &var : list) {
            this->emplaceChild(var);
        }
    }
};

class StringNode : public ExtendedTreeNode<StringNode>
{
  public:
    std::string value;
    StringNode(const std::string &val) : value(val){};
    StringNode(std::string &&val) : value(val){};
    StringNode(){};
};
class StringTree : public StringNode
{
  public:
    // using Tree<StringNode>::Tree;
    StringTree(){};
    StringTree(const std::initializer_list<std::string> &list)
    {
        for (const auto &var : list) {
            this->emplaceChild(var);
        }
    };
};

#endif // TREE_H_
