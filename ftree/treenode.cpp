#include "treenode.h"

void TreeNode::update()
{
    updateSelf();
    if (isExpanded()) {
        for (auto &child : getChildren()) {
            child->update();
        }
    }
}

void TreeNode::updateSelf()
{
    if (!pad) {
        pad = getTreeRoot()->getPad();
    }
    if (getParent()) {
        depth = getParent()->getDepth() + 1;
    } else {
        depth = 0;
    }
    updateDisplayLabel();
}

void TreeNode::updateDisplayLabel()
{
    std::string lbl = label;
    if (getChildren().size() > 0) {
        if (isExpanded()) {
            lbl = "-" + lbl;
        } else {
            lbl = "+" + lbl;
        }
    }

    auto depthoffset = std::string(depth * 2, ' ');
    lbl = depthoffset + lbl;
    if (isSelected()) {
        lbl = ">" + lbl;
    } else {
        lbl = " " + lbl;
    }
    displayLabel = lbl;
}

void TreeNode::draw(WINDOW *p) const
{
    drawSelf(pad);
    if (isExpanded()) {
        for (const auto &child : getChildren()) {
            child->draw(p);
        }
    }
}

void TreeNode::drawSelf(WINDOW *pad) const
{
    wmove(pad, y, x);
    auto label = getDisplayLabel();
    if (isSelected()) {
        auto attr = A_STANDOUT;
        wattron(pad, attr);
        // wprintw(pad, "%s\n", label.c_str());
        waddnstr(pad, label.c_str(), width);
        wattroff(pad, attr);
    } else {
        // wprintw(pad, "%s\n", label.c_str());
        waddnstr(pad, label.c_str(), width);
    }
}

int TreeNode::enumerate(int num)
{
    // TODO: this is confusing and should be replaced with something sane
    y = num;
    if (isExpanded()) {
        for (auto &child : getChildren()) {
            num = child->enumerate(num + height);
        }
    }
    return num;
}

TreeNode *TreeNode::nextVisibleNode()
{
    auto deepest = firstCollapsedParent();
    auto targetNode = deepest;
    while (auto p = targetNode->getParent()) {
        auto neighbor = targetNode->getSibling(1);
        if (neighbor != targetNode) {
            return neighbor;
        }
        targetNode = p;
    }
    return deepest;
}

TreeNode *TreeNode::firstCollapsedParent()
{
    TreeNode *p = this;
    TreeNode *lastExpanded = p;
    do {
        if (!p->isExpanded()) {
            lastExpanded = p;
        }
    } while ((p = p->getParent()));
    return lastExpanded;
}

bool TreeNode::parentCollapsed()
{
    TreeNode *p = this;
    while ((p = p->getParent())) {
        if (!p->isExpanded()) {
            return true;
        }
    }
    return false;
}

int TreeNode::getChildrenHeight()
{
    int h = getHeight();
    for (auto &child : getChildren()) {
        if (child->isExpanded())
            h += child->getChildrenHeight();
        else
            h += child->getHeight();
    }
    return h;
};
