#ifndef TREE_NODE_H
#define TREE_NODE_H

#include "tree.h"
#include "widget.h"
#include <ncurses.h>

#include <iostream>
#include <string>
#include <vector>

class Label : public Widget
{
  protected:
    std::string label;

  public:
    Label(const std::string &l) { label = l; }
    Label(){};

    const std::string &getLabel() const { return label; };
    std::string &getLabel() { return label; };

    void setLabel(const std::string &l) { label = l; };
};

// Class for an ncurses label node
// TODO: i should probably separate model and presentation
class TreeNode : public Label, public ExtendedTreeNode<TreeNode>
{
    using Node = TreeNode;
    WINDOW *pad = nullptr;
    std::string displayLabel;

    std::string data;
    bool expanded = true;
    bool selected = false;

    int depth = 0;

  public:
    TreeNode() { update(); };
    TreeNode(const std::string &l, const std::string &d, Node *p = nullptr)
        : Label(l), data(d), ExtendedTreeNode(p)
    {
        update();
    };

    // updates y
    int enumerate(int num = 0);

    // updates label, depth and pad
    void updateSelf();
    // updates label, depth and pad recursively
    void update();

    // redraws the node text
    void drawSelf(WINDOW *pad) const;
    // redraws the node text recursively
    void draw(WINDOW *pad) const;

    int getDepth() const { return depth; }

    // find next collapsed parent
    // (it's not necessarily a neighbor of firstCollapsedParent)
    Node *nextVisibleNode();
    // find first collapsed (=deepest visible=closest to root) parent
    Node *firstCollapsedParent();
    // Check if any parent if collapsed (it means the this node is hidden)
    bool parentCollapsed();

    WINDOW *getPad() { return pad; };
    void setPad(WINDOW *p)
    {
        pad = p;
        for (auto &child : getChildren()) {
            child->setPad(p);
        }
    }

    void updateDisplayLabel();
    const std::string &getDisplayLabel() const { return displayLabel; };
    std::string &getDisplayLabel() { return displayLabel; };
    void setLabel(const std::string &l)
    {
        label = l;
        updateSelf();
    };

    // Calculate total children height while respecting expanded status.
    int getChildrenHeight();

    void toggleExpanded() { setExpanded(!expanded); }
    bool isExpanded() const { return expanded; }
    void setExpanded(bool expand)
    {
        expanded = expand;
        updateSelf();
        int h = getChildrenHeight() - getHeight();
        if (!expanded) {
            // erase children
            wmove(getPad(), y + getHeight(), x);
            for (size_t i = 0; i < h; i++) {
                wdeleteln(getPad());
            }
        } else {
            // add lines for children
            wmove(getPad(), y + getHeight(), x);
            for (size_t i = 0; i < h; i++) {
                winsertln(getPad());
            }
        }

        getTreeRoot()->enumerate();
        draw(getPad());
    }

    void toggleSelected() { setSelected(!selected); }
    bool isSelected() const { return selected; }
    void setSelected(bool b)
    {
        selected = b;
        update();
        drawSelf(getPad());
    }

    virtual void printSelf(std::ostream &os, int depth = 0) const override
    {
        os << std::string(depth, ' ')                    //
           << this                                       //
           << " label: " << label                        //
           << " data: " << data                          //
           << " childrencount: " << getChildren().size() //
           << "\n";
    };
};

#endif // TREE_NODE_H
