#include "treeview.h"
#include <algorithm>
#include <cstring>
#include <iostream>

void TreeView::setData(const std::vector<std::string> &data)
{
    tree.clear();
    populateTree(data);
    updateNodeList();

    setPadSize(50, tree.enumerate() + 1);
    setContentSize(padWidth, padHeight);

    // tree.setLabel("root");

    tree.setPad(pad);

    update();
    selectIndex(0);
    redraw_content();
}

void TreeView::populateTree(const std::vector<std::string> data)
{
    // parse data and make a tree
    // data is assumed to be a collection of path-like strings
    for (auto &path : data) {
        int delimPos = 0;
        auto prevDelimPos = delimPos;
        Node *parent = &tree;
        parent->setSize(50, 1);
        do {
            delimPos = path.find(delimiter, prevDelimPos + 1);
            auto nodePath = path.substr(0, delimPos);
            auto label =
                path.substr(prevDelimPos + 1, delimPos - prevDelimPos - 1);
            prevDelimPos = delimPos;
            auto it = nodeMap.find(nodePath);
            if (it == nodeMap.end()) {
                // add new node
                parent = parent->emplaceChild(label, nodePath, parent).get();
                parent->setSize(50, 1);
                // store node in map for quick lookup
                nodeMap.insert(std::make_pair(nodePath, parent));
            } else {
                // node exists, use it as a parent for the next node
                parent = it->second;
            }
        } while (delimPos > 0);
    }
    // std::cout << tree;
}

int TreeView::handleInput(int input)
{
    switch (input) {
    case 'r':
    case 'R':
        refresh();
        break;
    case KEY_LEFT:
    case 'h':
    case 'H':
        selectParent();
        break;
    case 'L':
    case 'l':
    case ' ':
    case KEY_RIGHT:
        expandNode(selectedNode);
        break;
    case 'j':
    case 'J':
    case KEY_DOWN:
        selectNext();
        break;
    case 'k':
    case 'K':
    case KEY_UP:
        selectPrev();
        break;
    case 'd':
    case 'D':
        selectNextSibling();
        break;
    case 'u':
    case 'U':
        selectPrevSibling();
        break;
    default:
        return 1;
    }
    return 0;
}

int TreeView::selectNode(Node *node, bool next)
{
    if (node == selectedNode) {
        return selectedIndex;
    }
    if (selectedNode) {
        selectedNode->setSelected(false);
    }

    if (node) {
        auto targetNode = node;
        if (targetNode->parentCollapsed()) {
            if (next) {
                targetNode = targetNode->nextVisibleNode();
            } else {
                targetNode = targetNode->firstCollapsedParent();
            }
        }
        if (targetNode) {
            selectedNode = targetNode;
            selectedNode->setSelected(true);
            selectedIndex = getNodeIndex(selectedNode);
        }
    }

    // refresh(); // TODO: set dirty state
    setScrollPos(0, selectedNode->getY());
    return selectedIndex;
}
