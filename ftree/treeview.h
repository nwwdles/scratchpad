#ifndef TREE_VIEWER_H
#define TREE_VIEWER_H

#include "tree.h"
#include "treenode.h"
#include "widget.h"
#include <map>
#include <ncurses.h>

class TreeView : public ScrollView
{
  private:
    using Node = TreeNode;
    Tree<Node> tree;

    // todo: i should probably implement iterators instead
    // this is a flat list of all nodes for ease of iterating
    std::vector<Node *> nodeList{};
    std::map<std::string, Node *> nodeMap{};

    Node *selectedNode = nullptr;
    int selectedIndex = 0;
    char delimiter = '/';

    // Creates content nodes.
    void populateTree(const std::vector<std::string> data);

    // Updates list of nodes that's used for selection (for now).
    void updateNodeList()
    {
        nodeList.clear();
        tree.listChildren(nodeList);
    };

    int getNodeIndex(Node *node)
    {
        return std::find(nodeList.begin(), nodeList.end(), node) -
               nodeList.begin();
    };

    void expandNode(Node *node)
    {
        node->toggleExpanded();
        setContentSize(padWidth, tree.enumerate() + 1);
        // refresh(); // TODO: set dirty state
    }

    int selectNode(Node *node, bool nextparent = false);
    void selectIndex(int i)
    {
        if (i >= 0 && i < nodeList.size()) {
            selectNode(nodeList[i], i > selectedIndex);
        }
    };
    void selectParent()
    {
        if (auto p = selectedNode->getParent()) {
            selectedIndex = selectNode(p);
        }
    };

    int getSelectedIndex() { return selectedIndex; }
    void selectNext() { selectIndex(getSelectedIndex() + 1); }
    void selectPrev() { selectIndex(getSelectedIndex() - 1); }
    void selectNextSibling() { selectNode(selectedNode->getSibling(1)); }
    void selectPrevSibling() { selectNode(selectedNode->getSibling(-1)); }

  public:
    TreeView(){};
    TreeView(const std::vector<std::string> &data) { setData(data); };
    virtual ~TreeView() { delwin(pad); };
    void setData(const std::vector<std::string> &data);
    int handleInput(int input) override;

    // Fully redraw whole tree. Shouldn't be needed except once on
    // initialization.
    void redraw_content() const override
    {
        wclear(pad);
        tree.draw(pad);
    };
    // Udate nodes' data.
    void update() override { tree.update(); };
};

#endif // TREE_VIEWER_H
