#include "treeviewer.h"
#include <algorithm>
#include <cstring>
#include <iostream>

void TreeViewer::setData(const std::vector<std::string> &data)
{
    tree.clear();
    populateTree(data);
    // init tree and the rest
    updateNodeList();

    padWidth = 100;
    padHeight = tree.enumerate() + 1;
    totalVisibleRows = padHeight;
    if (pad)
        wresize(pad, padHeight, padWidth);
    else
        pad = newpad(padHeight, padWidth);

    // tree.setLabel("root");
    tree.setPad(pad);
    update();
    selectIndex(0);
    redraw_content();
}

void TreeViewer::populateTree(const std::vector<std::string> data)
{
    // parse data and make a tree
    // data is assumed to be a collection of path-like strings
    for (auto &path : data) {
        int delimPos = 0;
        auto prevDelimPos = delimPos;
        Node *parent = &tree;
        do {
            delimPos = path.find(delimiter, prevDelimPos + 1);
            auto nodePath = path.substr(0, delimPos);
            auto label = path.substr(prevDelimPos, delimPos - prevDelimPos);
            prevDelimPos = delimPos;
            auto it = nodeMap.find(nodePath);
            if (it == nodeMap.end()) {
                // add new node
                parent = parent->emplaceChild(label, nodePath, parent).get();
                nodeMap.insert(std::make_pair(nodePath, parent));
            } else {
                // node exists, use it as a parent for the next node
                parent = it->second;
            }
        } while (delimPos > 0);
    }
    // std::cout << tree;
}

void TreeViewer::onSizeChanged(int old_w, int old_h) { syncScroll(); }

void TreeViewer::redraw_content() const
{
    wclear(pad);
    tree.draw(pad);
}

void TreeViewer::refresh() const
{
    auto w_on_scr = std::min(padWidth, width - 1);
    auto h_on_scr = std::min(padHeight, height - 1);
    prefresh(pad,               //
             scrollY, scrollX,  // position in pad
             y, x,              // position on stdscr
             h_on_scr, w_on_scr // size on stdscr
    );
}

void TreeViewer::syncScroll()
{
    // TODO: if height is smaller than scrollAhead, it doesn't show selection
    int scrollIndex = selectedNode->getY();
    if (scrollIndex - scrollY > height - scrollAhead - 1) {
        scrollY = scrollIndex - (height - scrollAhead - 1);
    }
    if (scrollIndex - scrollY < scrollAhead) {
        scrollY = scrollIndex - scrollAhead;
    }
    scrollY = std::max(scrollY, 0);
    scrollY = std::min(scrollY, totalVisibleRows - height);
}

void TreeViewer::update() { tree.update(); }

int TreeViewer::handleInput(int input)
{
    switch (input) {
    case 'r':
    case 'R':
        refresh();
        break;
    case KEY_LEFT:
    case 'h':
    case 'H':
        selectParent();
        break;
    case 'L':
    case 'l':
    case ' ':
    case KEY_RIGHT:
        expandNode(selectedNode);
        break;
    case 'j':
    case 'J':
    case KEY_DOWN:
        selectNext();
        break;
    case 'k':
    case 'K':
    case KEY_UP:
        selectPrev();
        break;
    case 'd':
    case 'D':
        selectNextSibling();
        break;
    case 'u':
    case 'U':
        selectPrevSibling();
        break;
    case 'q':
    case 'Q':
        requestQuit();
    default:
        return 1;
    }
    return 0;
}

void TreeViewer::updateNodeList()
{
    nodeList.clear();
    tree.listChildren(nodeList);
}

int TreeViewer::getNodeIndex(Node *node)
{
    return std::find(nodeList.begin(), nodeList.end(), node) - nodeList.begin();
}

void TreeViewer::expandNode(Node *node)
{
    node->toggleExpanded();
    totalVisibleRows = tree.enumerate() + 1;
    syncScroll();
    // refresh(); // TODO: set dirty state
}

int TreeViewer::getSelectedIndex() { return selectedIndex; }
void TreeViewer::selectNext() { selectIndex(getSelectedIndex() + 1); }
void TreeViewer::selectPrev() { selectIndex(getSelectedIndex() - 1); }
void TreeViewer::selectIndex(int i)
{
    if (i >= 0 && i < nodeList.size()) {
        selectNode(nodeList[i], i > selectedIndex);
    }
}
void TreeViewer::selectNextSibling()
{
    selectNode(selectedNode->getSibling(1));
}
void TreeViewer::selectPrevSibling()
{
    selectNode(selectedNode->getSibling(-1));
}
void TreeViewer::selectParent()
{
    if (auto p = selectedNode->getParent()) {
        selectedIndex = selectNode(p);
    }
}
int TreeViewer::selectNode(Node *node, bool next)
{
    if (node == selectedNode) {
        return selectedIndex;
    }
    if (selectedNode) {
        selectedNode->setSelected(false);
    }

    if (node) {
        auto targetNode = node;
        if (targetNode->parentCollapsed()) {
            if (next) {
                targetNode = targetNode->nextVisibleNode();
            } else {
                targetNode = targetNode->firstCollapsedParent();
            }
        }
        if (targetNode) {
            selectedNode = targetNode;
            selectedNode->setSelected(true);
            selectedIndex = getNodeIndex(selectedNode);
        }
    }

    // refresh(); // TODO: set dirty state instead
    syncScroll();
    return selectedIndex;
}
