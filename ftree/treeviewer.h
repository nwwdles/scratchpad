#ifndef TREE_VIEWER_H
#define TREE_VIEWER_H

#include "tree.h"
#include "treenode.h"
#include <map>
#include <ncurses.h>

struct ModelNode {
    bool expanded = true;
    std::string data;
};

class TreeModel : public Tree<ModelNode>
{
};

/*
 * View is like a window/widget but it's based on an ncurses pad.
 *
 * // TODO: in future it probably should inherit from a widget
 */
class View
{
    // React to size change.
    virtual void onSizeChanged(int old_w, int old_h) = 0;

  protected:
    WINDOW *pad = nullptr;
    int x = 0;
    int y = 0;
    int width = 1;
    int height = 1;
    int scrollY = 0;
    int scrollX = 0;
    int padWidth = 0;
    int padHeight = 0;
    static void requestQuit(){};

  public:
    void setPos(int new_x, int new_y)
    {
        x = new_x;
        y = new_y;
    }
    void setSize(int w, int h)
    {
        int old_w = width;
        int old_h = height;
        width = w;
        height = h;
        onSizeChanged(old_w, old_h);
    }
    void setRect(int x, int y, int w, int h)
    {
        setPos(x, y);
        setSize(w, h);
    }

    // Redraw all content to pad.
    virtual void redraw_content() const = 0;
    // Draw pad to screen.
    virtual void refresh() const = 0;
    // Update underlying data.
    virtual void update() = 0;
    // React to input
    virtual int handleInput(int input) = 0;
};

class TreeViewer : public View
{
  private:
    using Node = TreeNode;
    Tree<Node> tree;
    TreeModel model;

    int scrollAhead = 3;
    int totalVisibleRows = 0;

    // todo: i should probably implement iterators instead
    // this is a flat list of all nodes for ease of iterating
    std::vector<Node *> nodeList{};
    std::map<std::string, Node *> nodeMap{};

    Node *selectedNode = nullptr;
    int selectedIndex = 0;
    char delimiter = '/';

    // Creates content nodes.
    void populateTree(const std::vector<std::string> data);

    // Updates size and redraws the screen.
    void onSizeChanged(int old_w, int old_h) override;

    // Updates list of nodes that's used for selection (for now).
    void updateNodeList();
    int getNodeIndex(Node *node);
    int getSelectedIndex();

    void syncScroll();

    void expandNode(Node *node);

    int selectNode(Node *node, bool nextparent = false);
    void selectIndex(int);
    void selectNext();
    void selectPrev();
    void selectNextSibling();
    void selectPrevSibling();
    void selectParent();

  public:
    TreeViewer(){};
    TreeViewer(const std::vector<std::string> &data) { setData(data); };
    virtual ~TreeViewer() { delwin(pad); };
    void setData(const std::vector<std::string> &data);
    int handleInput(int input) override;

    // Fully redraw whole tree. Shouldn't be needed except once on
    // initialization.
    void redraw_content() const override;
    // Draw pad to stdscr (prefresh).
    void refresh() const override;
    // Udate nodes' data.
    void update() override;
};

#endif // TREE_VIEWER_H
