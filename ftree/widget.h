#ifndef VIEW_H
#define VIEW_H

#include <algorithm>
#include <ncurses.h>

class Widget
{
    virtual void onSizeChanged(int old_w, int old_h){};

  protected:
    int x = 0;
    int y = 0;
    int height = 1;
    int width = 1;

  public:
    int getX() const { return x; }
    int getY() const { return y; }
    int getHeight() const { return height; }
    int getWidth() const { return width; }
    void setPos(int new_x, int new_y)
    {
        x = new_x;
        y = new_y;
    }
    void setSize(int w, int h)
    {
        int old_w = width;
        int old_h = height;
        width = w;
        height = h;
        onSizeChanged(old_w, old_h);
    }
    void setRect(int x, int y, int w, int h)
    {
        setPos(x, y);
        setSize(w, h);
    }
    virtual int handleInput(int input) { return 0; };
};

class View : public Widget
{
    void drawPad() const
    {
        auto w_on_scr =
            std::min(padWidth, width - 1 - paddingLeft - paddingRight);
        auto h_on_scr =
            std::min(padHeight, height - 1 - paddingTop - paddingBottom);
        prefresh(pad,                             //
                 padY, padX,                      // position in pad
                 y + paddingTop, x + paddingLeft, // position on stdscr
                 y + h_on_scr,
                 x + w_on_scr // size on stdscr
        );
    }

  protected:
    WINDOW *pad = nullptr;

    int padY = 0;
    int padX = 0;
    int padWidth = 1;
    int padHeight = 1;
    int paddingTop = 0;
    int paddingBottom = 0;
    int paddingRight = 0;
    int paddingLeft = 0;

  public:
    View() { pad = newpad(padHeight, padWidth); }
    virtual ~View() { deletePad(); }
    void setPadSize(int w, int h)
    {
        padWidth = w;
        padHeight = h;
        if (pad)
            wresize(pad, padHeight, padWidth);
        else
            pad = newpad(padHeight, padWidth);
    }
    void deletePad()
    {
        if (pad)
            delwin(pad);
        pad = nullptr;
    }

    // Update underlying data/contents.
    virtual void update() = 0;

    // Redraw all content to pad.
    virtual void redraw_content() const = 0;

    // Draw pad to screen.
    virtual void refresh() const { drawPad(); };
};

class ScrollView : public View
{
    // position of interest which should be kept in view (e.g. cursor)
    int scrollX = 0;
    int scrollY = 0;

    // how much columns/lines to keep visible
    int scrollAheadX = 3;
    int scrollAheadY = 3;

    // content can be smaller than total pad size so we need to know it
    int contentW = 0;
    int contentH = 0;

    void onSizeChanged(int old_w, int old_h) override { syncScroll(); };

    static int calcPadOffset(int scroll, int padPos, int size, int contentSize,
                             int scrollAhead)
    {
        // TODO: if size is smaller than scrollAhead, it's incorrect
        if (scroll - padPos > size - scrollAhead - 1) {
            padPos = scroll - (size - scrollAhead - 1);
        }
        if (scroll - padPos < scrollAhead) {
            padPos = scroll - scrollAhead;
        }
        padPos = std::max(padPos, 0);
        padPos = std::min(padPos, contentSize - size);
        return padPos;
    }

  protected:
    void setScrollPos(int x, int y)
    {
        scrollX = x;
        scrollY = y;
        syncScroll();
    }
    void setContentSize(int w, int h)
    {
        contentW = w;
        contentH = h;
        syncScroll();
    }
    // Change pad offset so that position (scrollX, scrollY) is visible.
    void syncScroll()
    {
        padX = calcPadOffset(scrollX, padX, width, contentW, scrollAheadX);
        padY = calcPadOffset(scrollY, padY, height, contentH, scrollAheadY);
        // TODO: set dirtyState if offset has changed
    }
};

#endif // VIEW_H
