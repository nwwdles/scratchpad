# Go

## Resources

- [Official site](https://golang.org/)
  - [The Go Programming Language Specification - The Go Programming Language](https://golang.org/ref/spec)
  - [Effective Go - The Go Programming Language](https://golang.org/doc/effective_go.html)
  - [The Go Programming Language Blog](https://blog.golang.org/)
  - [How to Write Go Code - The Go Programming Language](https://golang.org/doc/code.html)
- [Go wiki](https://github.com/golang/go/wiki)
  - [Slice tricks](https://github.com/golang/go/wiki/SliceTricks)
- [Go Resources](https://www.golang-book.com/)
- [StackOverflow: Go](https://stackoverflow.com/questions/tagged/go)
- [Go101](https://github.com/go101/go101) (also check out its' GitHub wiki)
- [Awesome Go Books](https://github.com/dariubs/GoBooks)
- [Awesome Go](https://github.com/avelino/awesome-go)
  - [one more](https://github.com/uhub/awesome-go)
- [Dave Cheney :: The acme of foolishness](https://dave.cheney.net/)
- [Go Design Patterns](https://www.godesignpatterns.com/)
- [r/golang](https://www.reddit.com/r/golang/)
- [Learn Go in Y minutes](https://learnxinyminutes.com/docs/go/)
- [Xah Golang Tutorial](http://xahlee.info/golang/golang_index.html)
- [Awesome Go Interview Questions and Answers](https://goquiz.github.io/)
- [50 Shades of Go: Traps, Gotchas, and Common Mistakes for New Golang Devs](http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/)
- [Go has both make and new functions, what gives ? :: Dave Cheney](https://dave.cheney.net/2014/08/17/go-has-both-make-and-new-functions-what-gives)
- [golangspec – Medium](https://medium.com/golangspec)
- Youtube Resources
  - [JustForFunc: Programming in Go](https://www.youtube.com/channel/UC_BzFbxG2za3bp5NRRRXJSw/videos)
- Microservices
  - [Microservices in Go - SEEK blog - Medium](https://medium.com/seek-blog/microservices-in-go-2fc1570f6800)
  - [Peter Bourgon · Go kit: Go in the modern enterprise](https://peter.bourgon.org/go-kit/)
  - [Micro-services Using go-kit: Service Discovery - ru-rocker](https://www.ru-rocker.com/2017/04/17/micro-services-using-go-kit-service-discovery/)
  - [How to write a microservice in Go with Go kit - DEV Community 👩‍💻👨‍💻](https://dev.to/napolux/how-to-write-a-microservice-in-go-with-go-kit-a66)
- Domain-Driven Design
  - [Domain-Driven Design in Go](https://medium.com/learnfazz/domain-driven-design-in-go-253155543bb1)
  - Examples
    - [GoDDD](https://github.com/marcusolsson/goddd) - an attempt to port the DDD Sample App to idiomatic Go
    - [go-ddd-sample](https://github.com/takashabe/go-ddd-sample)

## Packages

- [Tcell](https://github.com/gdamore/tcell) is a Go package that provides a cell based view for text terminals, like xterm. It was inspired by termbox, but includes many additional improvements.
