package main

import (
	"fmt"
	"golang-book/chapter11/math"
)

func main() {
	xs := []float64{1, 2, 3, 4}
	avg := math.Average(xs)
	fmt.Println(avg)
	fmt.Println(math.Min(xs))
	fmt.Println(math.Max(xs))
}

/*
1. packages let's us reuse and organize code

2. identifiers starting with capital letters are visible to other packages

3. import f "fmt"
aliases lets us resolve name conflicts

4. see in file

5. By prepending comments to them. see in file
*/
