package main

import (
	"container/list"
	"crypto/sha1"
	"errors"
	"flag"
	"fmt"
	"hash/crc32"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"time"
)

type Person struct {
	Name string
	Age  int
}

type ByName []Person

func (this ByName) Len() int {
	return len(this)
}
func (this ByName) Less(i, j int) bool {
	return this[i].Name < this[j].Name
}
func (this ByName) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}

func sortPeople() {
	kids := []Person{
		{"Jill", 9},
		{"Jack", 10},
	}
	sort.Sort(ByName(kids))
	fmt.Println(kids)
}

func hashTest() {
	h := crc32.NewIEEE()
	h.Write([]byte("test"))
	v := h.Sum32()
	fmt.Println(v)
}

func getHash(filename string) (uint32, error) {
	bs, err := ioutil.ReadFile((filename))
	if err != nil {
		return 0, err
	}
	h := crc32.NewIEEE()
	h.Write(bs)
	return h.Sum32(), nil
}
func stringsExample() {
	fmt.Println(
		// true
		strings.Contains("test", "es"),

		// 2
		strings.Count("test", "t"),

		// true
		strings.HasPrefix("test", "te"),

		// true
		strings.HasSuffix("test", "st"),

		// 1
		strings.Index("test", "e"),

		// "a-b"
		strings.Join([]string{"a", "b"}, "-"),

		// == "aaaaa"
		strings.Repeat("a", 5),

		// "bbaa"
		strings.Replace("aaaa", "a", "b", 2),

		// []string{"a","b","c","d","e"}
		strings.Split("a-b-c-d-e", "-"),

		// "test"
		strings.ToLower("TEST"),

		// "TEST"
		strings.ToUpper("test"),
	)
}
func fileExample() {
	file, err := os.Create("test.txt")
	if err != nil {
		// handle the error here
		return
	}
	defer file.Close()

	file.WriteString("test")

	file, err = os.Open("test.txt")
	if err != nil {
		// handle the error here
		return
	}
	defer file.Close()

	// get the file size
	stat, err := file.Stat()
	if err != nil {
		return
	}

	// read the file
	bs := make([]byte, stat.Size())
	_, err = file.Read(bs)
	if err != nil {
		return
	}

	str := string(bs)
	fmt.Println(str)

	bs, err = ioutil.ReadFile("test.txt")
	if err != nil {
		return
	}
	str = string(bs)
	fmt.Println(str)
}
func dirExample() {
	dir, err := os.Open("..")
	if err != nil {
		return
	}
	defer dir.Close()
	fileInfos, err := dir.Readdir(-1)
	if err != nil {
		return
	}
	for _, fi := range fileInfos {
		fmt.Println(fi.Name())
	}

	filepath.Walk("..", func(path string, info os.FileInfo, err error) error {
		fmt.Println(path)
		return nil
	})
}
func linkedlistExample() {
	var x list.List
	x.PushBack(1)
	x.PushBack(2)
	x.PushBack(3)

	for e := x.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value.(int))
	}
}
func filehashExample() {
	h1, err := getHash("main.go")
	if err != nil {
		return
	}
	h2, err := getHash("../chapter11/main.go")
	if err != nil {
		return
	}
	h3, err := getHash("main.go")
	if err != nil {
		return
	}

	fmt.Println(h1, h2, h1 == h2)
	fmt.Println(h1, h3, h1 == h3)
}
func shaExample() {
	h := sha1.New()
	h.Write([]byte("test"))
	bs := h.Sum([]byte{})
	fmt.Println(bs)
}

func argsExample() {
	maxp := flag.Int("max", 6, "the max value")
	flag.Parse()
	// rand.Seed(time.Now().UnixNano())
	// Generate a number between 0 and max
	fmt.Println(rand.Intn(*maxp))
}

func mutexExample() {
	m := new(sync.Mutex)
	for i := 0; i < 10; i++ {
		go func(i int) {
			m.Lock()
			fmt.Println(i, "start")
			time.Sleep(time.Second)
			fmt.Println(i, "end")
			m.Unlock()
		}(i)
	}
}

func main() {
	stringsExample()
	fileExample()
	dirExample()
	err := errors.New("my error message")
	fmt.Println(err)
	linkedlistExample()
	sortPeople()
	hashTest()
	filehashExample()
	shaExample()
	argsExample()

	mutexExample()
	var input string
	fmt.Scanln(&input)
}
