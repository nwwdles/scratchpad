package main

import (
	"fmt"
	"net"
	"net/rpc"
	"time"
)

type Server struct {
	count int64
}

func (this *Server) Increment(i int64, reply *int64) error {
	this.count++
	*reply = (this.count)
	return nil
}

func (this *Server) Negate(i int64, reply *int64) error {
	*reply = -i
	return nil
}

func server() {
	rpc.Register(new(Server))
	ln, err := net.Listen("tcp", ":9999")
	if err != nil {
		fmt.Println(err)
		return
	}
	for {
		c, err := ln.Accept()
		if err != nil {
			continue
		}
		go rpc.ServeConn(c)
	}
}

func client() {
	for {
		c, err := rpc.Dial("tcp", "127.0.0.1:9999")
		if err != nil {
			fmt.Println(err)
			time.Sleep(time.Second * 1)
			continue
		}
		var result int64
		err = c.Call("Server.Negate", int64(999), &result)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Server.Negate(999) =", result)
		}

		err = c.Call("Server.Increment", 0, &result)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Server.Increment =", result)
		}
		// break
		time.Sleep(time.Second * 1)
	}
}

func main() {
	go server()
	go client()

	var input string
	fmt.Scanln(&input)
}
