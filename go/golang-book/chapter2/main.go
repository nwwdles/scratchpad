package main

import (
	"fmt"
	"os"
)

// this is a comment

func main() {
	fmt.Println("Hello, my name is", os.Getenv("USER"))
	os.Exit(0)
}
