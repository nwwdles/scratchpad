package main

import "fmt"

// this is a comment

func main() {
	// fmt.Println("1 + 1 =", 1+1)

	// fmt.Println(len("Hello World"))
	// fmt.Println("Hello World"[1])
	// fmt.Println("Hello " + "World")

	// fmt.Println(true && true)
	// fmt.Println(true && false)
	// fmt.Println(true || true)
	// fmt.Println(true || false)
	// fmt.Println(!true)

	fmt.Println(32132 * 42452)
}

/*
# types

1. in binary form
2. 2^8-1 = 255
3. see above
4. string is a sequence of characters; len(str)
5.
	(true && false) || (false && true) || !(false && false) =
	(false) || (false) || !(false) =
	!(false) =
	true
*/
