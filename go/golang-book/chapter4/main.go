package main

import "fmt"

// this is a comment

func main() {
	// var x string = "Hello World"
	// fmt.Println(x)

	// x := "hello world"
	// fmt.Println(x)

	// fmt.Print("Enter a number: ")
	// var input float64
	// fmt.Scanf("%f", &input)
	// output := input * 2
	// fmt.Println(output)

	// fahrenheit to celsius:
	// fmt.Print("Enter degrees F: ")
	// var fahr float64
	// fmt.Scanf("%f", &fahr)
	// cels := (fahr - 32) * 5 / 9
	// fmt.Println("In Celsius, it's: ", cels)

	// feet to meters:
	fmt.Print("Enter distance in feet: ")
	var feet float64
	fmt.Scanf("%f", &feet)
	meters := feet * 0.3048
	fmt.Println("In meters, it's: ", meters)
}

/*
1.
	var name type = value;
	name := value;
2. 6
3. scope is an area of visibility of a variable
	in go, curly brackets limit scope
	variables are visible in nested scopes
4. const values can't be changed after compilation
5. above
6. above
*/
