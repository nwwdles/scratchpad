package main

import "fmt"

func main() {
	// i := 1
	// for i <= 10 {
	// 	switch i {
	// 	case 0:
	// 		fmt.Println(i, "Zero")
	// 	case 1:
	// 		fmt.Println(i, "One")
	// 	case 5:
	// 		fmt.Println(i, "Five")
	// 	default:
	// 		if i%2 == 0 {
	// 			fmt.Println(i, "is even")
	// 		} else {
	// 			fmt.Println(i, "is odd")
	// 		}
	// 	}
	// 	i++
	// }

	// 2
	// i := 1
	// for i <= 100 {
	// 	if i%3 == 0 {
	// 		fmt.Println(i)
	// 	}
	// 	i++
	// }

	// 3
	i := 1
	for i <= 100 {
		fmt.Print(i)
		if i%3 == 0 {
			fmt.Print(" Fizz")
			if i%5 == 0 {
				fmt.Print("Buzz")
			}
		} else if i%5 == 0 {
			fmt.Print(" Buzz")
		}
		fmt.Print("\n")
		i++
	}
}

/*
1. Small
2-3. above
*/
