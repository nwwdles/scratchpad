package main

import "fmt"

func main() {
	// var x [5]int
	// x[4] = 100
	// fmt.Println(x)

	// var x [5]float64
	// x[0] = 98
	// x[1] = 93
	// x[2] = 77
	// x[3] = 82
	// x[4] = 83

	x := [5]float64{98, 93, 77, 82, 83}

	var total float64 = 0
	for i := 0; i < len(x); i++ {
		total += x[i]
	}

	fmt.Println(total / float64(len(x)))

	total = 0
	for _, value := range x {
		total += value
	}
	fmt.Println(total / float64(len(x)))

	y := x[0:2]
	fmt.Println("y:", y)
	slice := append(y, 3, 2)
	fmt.Println("slice:", slice)

	copied := make([]float64, 2)
	copy(copied, slice)
	fmt.Println("copied:", copied)

	m := make(map[string]int)
	m["the key"] = 10
	fmt.Println(m)

	name, ok := m["a key"]
	fmt.Println(name, ok)
	nestedMap := map[string]map[string]string{
		"a key": map[string]string{
			"name":  "Hydrogen",
			"state": "gas",
		},
	}
	if el, ok := nestedMap["a key"]; ok {
		fmt.Println(el["name"], el["state"])
	}
	test := [6]string{"a", "b", "c", "d", "e", "f"}
	fmt.Println(test[2:5])

	// problem 4

	problem := []int{
		48, 96, 86, 68,
		57, 82, 63, 70,
		37, 34, 83, 27,
		19, 97, 9, 17,
	}
	min := problem[0]
	for _, value := range problem[1:] {
		if value < min {
			min = value
		}
	}
	fmt.Println("Min value:", min)
}

/*
1. x := array[3]
2. slice is 3, underlying array is 9
3. ["c","d","e"]
4. above
*/
