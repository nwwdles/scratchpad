package main

import "fmt"

func makeEvenGenerator() func() uint {
	i := uint(0)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

func makeOddGenerator() func() uint {
	i := uint(1)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

func average(xs []float64) float64 {
	// panic("Not implemented")
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

func factorial(x uint) uint {
	if x == 0 {
		return 1
	}
	return x * factorial(x-1)
}

func first() {
	fmt.Println("1st")
}
func second() {
	fmt.Println("2nd")
}
func deferTest() {
	defer second()
	first()
}

func half(n int) (int, bool) {
	res := n / 2
	return res, res == 0
}

func max(nums ...int) (max int) {
	max = nums[0]
	for _, value := range nums[1:] {
		if value > max {
			max = value
		}
	}
	return
}

func fibonacci(n int) int {
	if n <= 0 {
		return 1
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

func main() {
	xs := []float64{98, 93, 77, 82, 83}
	fmt.Println(average(xs))

	x := 0
	increment := func() int {
		x++
		return x
	}
	fmt.Println(increment())
	fmt.Println(increment())

	nextEven := makeEvenGenerator()
	fmt.Println(nextEven()) // 0
	fmt.Println(nextEven()) // 2
	fmt.Println(nextEven()) // 4

	fmt.Println(factorial(4))

	deferTest()

	// 2
	fmt.Println(half(1))
	fmt.Println(half(2))

	// 3
	fmt.Println(max(1, 3, 2))

	//4
	nextOdd := makeOddGenerator()
	fmt.Println(nextOdd()) // 1
	fmt.Println(nextOdd()) // 3
	fmt.Println(nextOdd()) // 5

	// 5
	fmt.Println(fibonacci(8))
}

/*
1. sum(nums ...int) int
2-5. above
6. defer makes a function be executed upon the exit from the function, which is convenient if you have multiple return statements or panic statements
panic throws runtime error
recover stops error propagation. recover should be put into a deferred function
*/
