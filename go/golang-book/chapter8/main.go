package main

import "fmt"

func swap(a *int, b *int) {
	temp := *a
	*a = *b
	*b = temp
}

func zero(xPtr *int) {
	*xPtr = 0
}
func main() {
	x := 5
	zero(&x)
	fmt.Println(x) // x is 0

	y := 3
	z := 44
	swap(&y, &z)
	fmt.Println(y, z)
}

/*
1. &a returns address of a
2. *a=4
3. ptr := new(int)
4. 1.5 * 1.5 = 2.25
5. above
*/
