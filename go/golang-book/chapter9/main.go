package main

import (
	"fmt"
	"math"
)

type Shape interface {
	area() float64
	perimeter() float64
}

type Circle struct {
	x, y, r float64
}

func (c *Circle) area() float64 {
	return circleArea(c)
}
func (c *Circle) perimeter() float64 {
	return 2 * math.Pi * c.r
}

type Rect struct {
	x1, y1, x2, y2 float64
}

func (r *Rect) area() float64 {
	return rectangleArea(r)
}

func (r *Rect) perimeter() float64 {
	return 2 * ((r.x2 - r.x1) + (r.y2 - r.y1))
}

func totalArea(shapes ...Shape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.area()
	}
	return area
}
func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a*a + b*b)
}

func rectangleArea(r *Rect) float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}

func circleArea(c *Circle) float64 {
	return math.Pi * c.r * c.r
}

type MultiShape struct {
	shapes []Shape
}

func (m *MultiShape) area() float64 {
	return totalArea(m.shapes...)
}

func (m *MultiShape) perimeter() float64 {
	panic("Not implemented")
}

func main() {
	r := Rect{0, 0, 10, 10}
	c := Circle{0, 0, 5}
	fmt.Println(rectangleArea(&r))
	fmt.Println(r.area())
	fmt.Println(circleArea(&c))
	fmt.Println(c.area())
	fmt.Println(totalArea(&c, &r))

	var m MultiShape
	m.shapes = []Shape{&r, &c, &r}
	fmt.Println(m.area())
	fmt.Println(c.perimeter())
	fmt.Println(r.perimeter())
}

/*
1. method is a function that has a receiver object
2. to model is-a relationship and because it lets you call field's methods without specifying the fields name
3. above
*/
