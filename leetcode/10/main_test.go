package main

import "testing"

func Test_isMatch(t *testing.T) {
	type args struct {
		s string
		p string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"", args{"", ""}, true},
		{"", args{"aa", "a"}, false},
		{"", args{"b", "a"}, false},

		{"", args{"a", "."}, true},
		{"", args{"a", ".*"}, true},
		{"", args{"ba", "a*"}, false},
		{"", args{"ba", "ba*"}, true},
		{"", args{"ba", "b*a"}, true},
		{"", args{"ba", "b*."}, true},
		{"", args{"ba", ".*a"}, true},
		{"", args{"ba", ".*"}, true},
		{"", args{"ba", ".a"}, true},
		{"", args{"ba", "b."}, true},
		{"", args{"ba", "b."}, true},

		{"", args{"a b c d b c b", "a.*b.*b.*b"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isMatch(tt.args.s, tt.args.p); got != tt.want {
				t.Errorf("isMatch(%v) = %v, want %v", tt.args, got, tt.want)
			}
		})
	}
}
