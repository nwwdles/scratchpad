package main

import (
	"fmt"
)

func main() {
	fmt.Println("hey")
}

func lengthOfLongestSubstring(s string) int {
	maxcount := 0
	m := map[byte]int{}
outer:
	for pivot := 0; pivot < len(s); pivot++ {
		for i := range s[pivot:] {
			if index, ok := m[s[pivot+i]]; ok {
				if len(s)-pivot < maxcount {
					break outer
				}
				m = map[byte]int{}
				pivot = index
				break
			}
			m[s[pivot+i]] = pivot + i
			if len(m) > maxcount {
				maxcount = len(m)
			}
		}
	}
	return maxcount
}

// func lengthOfLongestSubstring(s string) int {
// 	maxcount := 0
// 	m := map[byte]struct{}{}
// outer:
// 	for pivot := range s {
// 		for i := range s[pivot:] {
// 			if _, ok := m[s[pivot+i]]; ok {
// 				m = map[byte]struct{}{}
// 				if len(s)-pivot < maxcount {
// 					break outer
// 				}
// 				break
// 			}
// 			m[s[pivot+i]] = struct{}{}
// 			if len(m) > maxcount {
// 				maxcount = len(m)
// 			}
// 		}
// 	}
// 	return maxcount
// }
