package main

import "testing"

func Test_lengthOfLongestSubstring(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"", args{s: "pwwkew"}, 3},
		{"", args{s: "abcabcbb"}, 3},
		{"", args{s: "bbbbb"}, 1},
		{"", args{s: "bbbbbzzzz"}, 2},
		{"", args{s: "bbbbbzazzz"}, 3},
		{"", args{s: "bbbabbzazzgz"}, 3},
		{"", args{s: "zzgzxv"}, 4},
		{"", args{s: "zzgbxv"}, 5},
		{"", args{s: "zgbxv"}, 5},
		{"", args{s: " "}, 1},
		{"", args{s: " 1 "}, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := lengthOfLongestSubstring(tt.args.s); got != tt.want {
				t.Errorf("lengthOfLongestSubstring() = %v, want %v", got, tt.want)
			}
		})
	}
}
