package main

import (
	"fmt"
)

func main() {
	fmt.Println("hey")
}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	for len(nums1)+len(nums2) >= 3 {
		if len(nums1) == 0 {
			nums2 = nums2[1 : len(nums2)-1]
		} else if len(nums2) == 0 {
			nums1 = nums1[1 : len(nums1)-1]
		} else {
			a, b := &nums1, &nums2
			if nums1[0] > nums2[0] {
				a, b = &nums2, &nums1
			}

			*a = (*a)[1:]
			if len(*a) == 0 || (*b)[len(*b)-1] >= (*a)[len(*a)-1] {
				*b = (*b)[:len(*b)-1]
			} else {
				*a = (*a)[:len(*a)-1]
			}
		}
	}

	res := append(nums1, nums2...)
	if len(res) > 1 {
		return float64(res[0]+res[1]) / 2
	}

	return float64(res[0])
}
