package main

import (
	"fmt"
)

func main() {
	fmt.Println("hey")
}

func isPalindrome(s string) bool {
	for i := range s[len(s)/2:] {
		if s[i] != s[len(s)-1-i] {
			return false
		}
	}
	return true
}

// testOdd attempts to find the longest palindrome with center in `center`
// and at least of len `startLen`
func testOdd(s string, center, startWidth int) (width int, sub string) {
	halfwidth := startWidth / 2

	for center-halfwidth >= 0 && center+1+halfwidth <= len(s) {
		candidateSub := s[center-halfwidth : center+1+halfwidth]
		if !isPalindrome(candidateSub) {
			break
		}
		sub = candidateSub
		halfwidth++
	}

	return len(sub), sub
}

func testEven(s string, center, startWidth int) (width int, sub string) {
	halfwidth := startWidth / 2
	for center-halfwidth+1 >= 0 && center+1+halfwidth <= len(s) {
		candidateSub := s[center-halfwidth+1 : center+1+halfwidth]
		if !isPalindrome(candidateSub) {
			break
		}
		sub = candidateSub
		halfwidth++
	}

	return len(sub), sub
}

func longestPalindrome(s string) string {
	sub := ""
	longest := 0

	for i := 0; i <= len(s)/2; i++ {
		if (len(s)-i*2)+2 < longest {
			break
		}
		center1 := len(s)/2 - i
		center2 := len(s)/2 + 1 + i
		fmt.Println(center1, center2, len(s))
		l, st := testOdd(s, center1, longest)
		if l > longest {
			longest = l
			sub = st
		}
		l, st = testEven(s, center1, longest)
		if l > longest {
			longest = l
			sub = st
		}

		l, st = testOdd(s, center2, longest)
		if l > longest {
			longest = l
			sub = st
		}
		l, st = testEven(s, center2, longest)
		if l > longest {
			longest = l
			sub = st
		}

	}

	return sub
}
