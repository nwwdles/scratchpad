package main

import (
	"testing"
)

func Test_longestPalindrome(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"", args{s: "3"}, "3"},
		{"", args{s: "232"}, "232"},
		{"", args{s: "12321"}, "12321"},
		{"", args{s: "7123217"}, "7123217"},
		{"", args{s: "zabba"}, "abba"},
		{"", args{s: "aaaa"}, "aaaa"},

		{"", args{s: "babad"}, "aba"},
		{"", args{s: "cbbbd"}, "bbb"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := longestPalindrome(tt.args.s); got != tt.want {
				t.Errorf("longestPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isPalindrome(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"", args{s: "aa"}, true},
		{"", args{s: "aaa"}, true},
		{"", args{s: "baab"}, true},
		{"", args{s: "bazab"}, true},
		{"", args{s: "bazaab"}, false},
		{"", args{s: "a"}, true},
		{"", args{s: ""}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isPalindrome(tt.args.s); got != tt.want {
				t.Errorf("isPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_testOdd(t *testing.T) {
	type args struct {
		s              string
		center         int
		startHalfwidth int
	}
	tests := []struct {
		name    string
		args    args
		wantN   int
		wantSub string
	}{
		{"", args{s: "1", center: 0, startHalfwidth: 0}, 1, "1"},
		{"", args{s: "212", center: 1, startHalfwidth: 0}, 3, "212"},
		{"", args{s: "2125365", center: 1, startHalfwidth: 0}, 3, "212"},
		{"", args{s: "12125365", center: 2, startHalfwidth: 1}, 3, "212"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotN, gotSub := testOdd(tt.args.s, tt.args.center, tt.args.startHalfwidth)
			if gotN != tt.wantN {
				t.Errorf("testOdd() gotN = %v, want %v", gotN, tt.wantN)
			}
			if gotSub != tt.wantSub {
				t.Errorf("testOdd() gotSub = %v, want %v", gotSub, tt.wantSub)
			}
		})
	}
}

func Test_testEven(t *testing.T) {
	type args struct {
		s              string
		center         int
		startHalfwidth int
	}
	tests := []struct {
		name          string
		args          args
		wantHalfwidth int
		wantSub       string
	}{
		{"", args{s: "11", center: 0, startHalfwidth: 0}, 2, "11"},
		{"", args{s: "311", center: 1, startHalfwidth: 0}, 2, "11"},
		{"", args{s: "113", center: 0, startHalfwidth: 0}, 2, "11"},
		{"", args{s: "3113", center: 1, startHalfwidth: 0}, 4, "3113"},
		{"", args{s: "314413", center: 2, startHalfwidth: 0}, 6, "314413"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotHalfwidth, gotSub := testEven(tt.args.s, tt.args.center, tt.args.startHalfwidth)
			if gotHalfwidth != tt.wantHalfwidth {
				t.Errorf("testEven() gotHalfwidth = %v, want %v", gotHalfwidth, tt.wantHalfwidth)
			}
			if gotSub != tt.wantSub {
				t.Errorf("testEven() gotSub = %v, want %v", gotSub, tt.wantSub)
			}
		})
	}
}
