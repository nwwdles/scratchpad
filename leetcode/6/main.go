package main

import (
	"fmt"
)

func main() {
	fmt.Println("hey")
}

func convert(s string, numRows int) string {
	result := ""

	y := 0
	x := 0

	a := make([][]rune, numRows)

	for i := range a {
		a[i] = make([]rune, len(s)+1)
	}

	for i, v := range s {
		period := i
		counter := 1
		if numRows > 1 {
			counter = i % (2 * (numRows - 1))
			period = i / (2 * (numRows - 1))
		}
		if counter >= numRows {
			x++
			y--
			if y < 0 {
				y = 0
			}
		} else {
			x = period * (numRows - 1)
			y = counter
		}
		a[y][x] = v
	}
	result = ""
	for _, b := range a {
		for _, y := range b {
			if y != rune(0) {
				result += string(y)
			}
		}

	}
	return result
}
