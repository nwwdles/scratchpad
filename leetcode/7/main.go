package main

import (
	"fmt"
)

func main() {
	fmt.Println("hey")
}

func reverse(x int) int {
	a := x
	z := int32(2147483647)
	fmt.Println(z + 1)

	result := 0
	for a != 0 {
		c := a % 10
		a = a / 10
		result = result*10 + c
		if int(int32(result)) != result {
			return 0
		}
		fmt.Println(a, c, result)
	}
	return result
}
