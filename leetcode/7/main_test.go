package main

import (
	"testing"
)

func Test_reverse(t *testing.T) {
	type args struct {
		x int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"", args{x: 123}, 321},
		{"", args{x: -123}, -321},
		{"", args{x: 120}, 21},
		{"", args{x: 1534236469}, 0},
		{"", args{x: 4294967296900000999}, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := reverse(tt.args.x); got != tt.want {
				t.Errorf("reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}
