package main

import (
	"fmt"
)

func main() {
	fmt.Println("hey")
}

func isPalindrome(x int) bool {
	if x < 0 || x%10 == 0 {
		return false
	}
	if x/10 == 0 {
		return true
	}

	ac := 0
	for y := x; y > 0; y /= 10 {
		ac = ac*10 + y%10
		if ac == y {
			return true
		}
	}

	return ac == x
}
