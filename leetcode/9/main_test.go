package main

import "testing"

func Test_isPalindrome(t *testing.T) {
	type args struct {
		x int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"", args{x: -101}, false},
		{"", args{x: 101}, true},
		{"", args{x: 111}, true},
		{"", args{x: 3210123}, true},
		{"", args{x: 10}, false},
		{"", args{x: 11}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isPalindrome(tt.args.x); got != tt.want {
				t.Errorf("isPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}
