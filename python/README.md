# My Python Scripts

(that aren't important enough to have their own repos)

- [`animeboxes-downloader`](http://www.animebox.es/)
  - Downloads favorites from an .abbj backup file.
  - Stores tags in .xmp sidecars that can be imported into digikam.
- [`digikam-sorter`](https://www.digikam.org/)
  - Automatically sorts images into folders based on tags.
  - Supports logical operations on tags for more complex rules.
- `tg-archiver`
  - Downloads messages and media from Telegram chats/channels.
- `fjpa` - fix upscaled jpeg pixel art.
  - it's trivial and only works on upscaled pixel art, it's not a jpeg artifact remover

## Ideas

- [x] GitLab repos backup script ([API](https://docs.gitlab.com/ee/api/))
- [ ] ~~Habitica CLI ([API](https://habitica.com/apidoc/))~~
  - ~~I only want to make todos with checklists.~~
  - ~~Todos could be synced with some local kanban app.~~
  - Bash seems fine for what I want.

## References

- https://pypi.org/classifiers/
- [Documentation](https://docs.python.org/3/index.html)
  - [The Python Tutorial](https://docs.python.org/3/tutorial/index.html)
- [Python Tips](http://book.pythontips.com/en/latest/index.html)
- [The Hitchhiker’s Guide to Python](https://docs.python-guide.org/)
