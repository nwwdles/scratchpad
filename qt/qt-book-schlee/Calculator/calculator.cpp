#include "calculator.h"
#include <QDebug>
#include <QGridLayout>
Calculator::Calculator(QWidget* parent)
    : QWidget(parent), lcd(new QLCDNumber(12)), awaitingNewNumber(false)
{
    lcd->setSegmentStyle(QLCDNumber::Flat);
    lcd->setMinimumSize(150, 50);

    QChar buttonLabels[4][4] = {{'7', '8', '9', '/'},
                                {'4', '5', '6', '*'},
                                {'1', '2', '3', '-'},
                                {'0', '.', '=', '+'}};

    QGridLayout* grid = new QGridLayout;
    grid->addWidget(lcd, 0, 0, 1, 4);
    grid->addWidget(createButton("CE"), 1, 3);

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            grid->addWidget(createButton(buttonLabels[i][j]), i + 2, j);
        }
    }
    setLayout(grid);
}
void Calculator::slotButtonClicked()
{
    QString str = qobject_cast<QPushButton*>(sender())->text();

    // this is pretty convoluted but i think this is how calculator supposed to
    // work as opposed to the example which is buggy
    // the right way would be to use a tree instead of a stack
    qDebug() << displayString << str;
    if (str == "CE") { // clear operation
        operationStack.clear();
        displayString = "";
        lcd->display("0");
    } else if (str.contains(QRegExp("[0-9\\.]"))) {
        // add digit or decimal point
        if (awaitingNewNumber) {
            awaitingNewNumber =
                false; // btw false means that a new number was entered
            displayString = "";
        }
        if (str == "." && displayString.contains(str))
            return;
        displayString += str;
        lcd->display(displayString);
    } else if (operationStack.size() >= 2) { // operation is pressed
        if (!awaitingNewNumber) {
            // new number was entered, process it
            operationStack.push(QString().setNum(lcd->value()));
            calculate();
            operationStack.clear();
            if (str != "=") {
                operationStack.push(QString().setNum(lcd->value()));
                operationStack.push(str);
            }
            // display the result until digit button is pressed
            awaitingNewNumber = true;
        } else if (str != "=") {
            // no new number entered,
            // an operation pressed twice, set to newest operation
            operationStack.pop();
            operationStack.push(str);
        }
    } else {
        // an operation was pressed
        if (str != "=") { // don't react to '=' with just one number
            operationStack.push(QString().setNum(lcd->value()));
            operationStack.push(str);
        }
        awaitingNewNumber = true;
    }
}

QPushButton* Calculator::createButton(const QString& buttonText)
{
    QPushButton* pb = new QPushButton(buttonText);
    pb->setMinimumSize(40, 40);
    connect(pb, SIGNAL(clicked()), SLOT(slotButtonClicked()));
    return pb;
}

void Calculator::calculate()
{
    qreal rhs = operationStack.pop().toDouble();
    QString operation = operationStack.pop();
    qreal lhs = operationStack.pop().toDouble(); // toFloat();
    qreal result = 0;

    if (operation == "+")
        result = lhs + rhs;
    else if (operation == "-")
        result = lhs - rhs;
    else if (operation == "*")
        result = lhs * rhs;
    else if (operation == "/")
        result = lhs / rhs;

    lcd->display(result);
}
