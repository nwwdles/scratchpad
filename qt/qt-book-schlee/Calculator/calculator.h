#pragma once

#include <QLCDNumber>
#include <QPushButton>
#include <QStack>
#include <QWidget>

class Calculator : public QWidget
{
    Q_OBJECT

  private:
    QLCDNumber* lcd;
    QStack<QString> operationStack;
    QString displayString;
    bool awaitingNewNumber;

  public:
    explicit Calculator(QWidget* parent = nullptr);
    QPushButton* createButton(const QString& buttonText);
    void calculate();

  signals:

  public slots:
    void slotButtonClicked();
};
