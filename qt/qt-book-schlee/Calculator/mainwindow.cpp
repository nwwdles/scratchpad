#include "mainwindow.h"
#include "calculator.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    calculator = new Calculator();
    ui->vertical->addWidget(calculator);
}

MainWindow::~MainWindow() { delete ui; }
