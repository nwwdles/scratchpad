#include "analogclock.h"
#include <QLabel>
#include <QPainter>
#include <QTime>
#include <QTimer>
AnalogClock::AnalogClock(QWidget* parent) : QWidget(parent)
{
    QTimer* timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);
    setWindowTitle(tr("Analog Clock"));
    resize(200, 200);

    label = new QLabel(this);
    label->setAlignment(Qt::AlignCenter);
    label->resize(100, label->height());
}

void AnalogClock::paintEvent(QPaintEvent* event)
{
    static const QPoint hourHand[3] = {QPoint(7, 8), QPoint(-7, 8),
                                       QPoint(0, -40)};
    static const QPoint minuteHand[3] = {QPoint(7, 8), QPoint(-7, 8),
                                         QPoint(0, -70)};
    static const QPoint secondHand[3] = {QPoint(2, 8), QPoint(-2, 8),
                                         QPoint(0, -80)};
    QColor hourColor(127, 0, 127);
    QColor minuteColor(0, 127, 127, 191);
    QColor secondColor(0, 127, 0, 191);

    int side = qMin(width(), height());

    QTime time = QTime::currentTime();

    // label
    label->move((width() - label->width()) / 2, height() / 2 + side / 4);
    label->setText(time.toString());

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(width() / 2, height() / 2);
    painter.scale(side / 200.0, side / 200.0);

    // paint hour hand
    painter.setPen(Qt::NoPen);
    painter.setBrush(hourColor);

    painter.save();
    painter.rotate(30.0 * ((time.hour() + time.minute() / 60.0)));
    painter.drawConvexPolygon(hourHand, 3);
    painter.restore();

    // paint hour checks
    painter.setPen(hourColor);

    for (int i = 0; i < 12; ++i) {
        painter.drawLine(88, 0, 96, 0);
        painter.rotate(30.0);
    }

    // paint minute hand
    painter.setPen(Qt::NoPen);
    painter.setBrush(minuteColor);

    painter.save();
    painter.rotate(6.0 * (time.minute() + time.second() / 60.0));
    painter.drawConvexPolygon(minuteHand, 3);
    painter.restore();

    // minute checks
    painter.setPen(minuteColor);

    for (int j = 0; j < 60; ++j) {
        if ((j % 5) != 0)
            painter.drawLine(92, 0, 96, 0);
        painter.rotate(6.0);
    }

    // paint second hand
    painter.setPen(Qt::NoPen);
    painter.setBrush(minuteColor);

    painter.save();
    painter.rotate(6.0 * (time.second()));
    painter.drawConvexPolygon(secondHand, 3);
    painter.restore();
}
