#pragma once

#include <QLabel>
#include <QPaintEvent>
#include <QWidget>
class AnalogClock : public QWidget
{
    Q_OBJECT
  public:
    explicit AnalogClock(QWidget* parent = nullptr);

  public slots:

  signals:

  protected:
    void paintEvent(QPaintEvent* event) override;

  private:
    QLabel* label;
};
