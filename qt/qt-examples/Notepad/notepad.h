#ifndef NOTEPAD_H
#define NOTEPAD_H

#include <QMainWindow>
#include <QTextDocument>

namespace Ui
{
class Notepad;
}

class Notepad : public QMainWindow
{
    Q_OBJECT

  public:
    explicit Notepad(QWidget* parent = nullptr);
    ~Notepad();

  private slots:
    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSave_as_triggered();

    void on_actionPrint_triggered();

    void on_actionExit_triggered();

    void on_actionCopy_triggered();

    void on_actionCut_triggered();

    void on_actionPaste_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

    void on_actionFont_triggered();

    void on_findButton_clicked();

    void on_textEdit_textChanged();

  private:
    void undoHighlight(QTextDocument* document);
    bool highlighted;
    Ui::Notepad* ui;
    QString currentFile;
};

#endif // NOTEPAD_H
