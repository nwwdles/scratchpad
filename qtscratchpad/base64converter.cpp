#include "base64converter.h"
#include "ui_base64converter.h"
#include <QTextDocument>

Base64Converter::Base64Converter(QWidget *parent)
    : QWidget(parent), ui(new Ui::Base64Converter)
{
    ui->setupUi(this);
}

Base64Converter::~Base64Converter() { delete ui; }

void Base64Converter::on_pushButtonText_clicked()
{
    // convert text to base64
    QTextDocument *text = ui->textEditText->document();
    QTextDocument *base = ui->textEditBase->document();
    QByteArray a = text->toPlainText().toUtf8();
    base->setPlainText(a.toBase64());
}

void Base64Converter::on_pushButtonBase_clicked()
{
    // convert base64 to text
    QTextDocument *text = ui->textEditText->document();
    QTextDocument *base = ui->textEditBase->document();
    QByteArray a = base->toPlainText().toUtf8();
    text->setPlainText(a.fromBase64(a));
}
