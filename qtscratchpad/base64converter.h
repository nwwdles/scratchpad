#ifndef BASE64CONVERTER_H
#define BASE64CONVERTER_H

#include <QWidget>

namespace Ui
{
class Base64Converter;
}

class Base64Converter : public QWidget
{
    Q_OBJECT

  public:
    explicit Base64Converter(QWidget *parent = nullptr);
    ~Base64Converter();

  private slots:
    void on_pushButtonText_clicked();

    void on_pushButtonBase_clicked();

  private:
    Ui::Base64Converter *ui;
};

#endif // BASE64CONVERTER_H
