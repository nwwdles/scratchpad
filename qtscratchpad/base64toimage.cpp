#include "base64toimage.h"
#include <QFileDialog>
#include <QMimeDatabase>
#include <QMimeType>
#include <QPushButton>
#include <QVBoxLayout>

Base64ToImage::Base64ToImage(QWidget *parent) : QWidget(parent)
{
    auto *layout = new QVBoxLayout(this);
    convertButton = new QPushButton("Convert...");
    inputEdit = new QPlainTextEdit();
    layout->addWidget(inputEdit);
    layout->addWidget(convertButton);
    connect(convertButton, &QAbstractButton::clicked, //
            this, &Base64ToImage::convertButtonClicked);
    connect(inputEdit, &QPlainTextEdit::cursorPositionChanged, inputEdit,
            &QPlainTextEdit::selectAll);
}

void Base64ToImage::convertButtonClicked()
{
    auto data = inputEdit->document()->toPlainText();
    auto b = data.toUtf8();
    b = b.fromBase64(b);
    QImage img;
    img.loadFromData(b);

    QMimeType type = QMimeDatabase().mimeTypeForData(b);
    auto format = type.preferredSuffix().toStdString();
    format = "Image file (*." + format + ");;All files (*.*)";
    QString fileName = QFileDialog::getSaveFileName(
        this, tr("Save File"), QDir::currentPath() + "/out.pdf",
        tr(format.c_str()));
    if (fileName.size() == 0)
        return; // if filename is empty it starts to print pics on a default
    img.save(fileName);
}
