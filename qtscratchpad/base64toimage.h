#ifndef BASE64TOIMAGE_H
#define BASE64TOIMAGE_H

#include <QByteArray>
#include <QImage>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QWidget>

class Base64ToImage : public QWidget
{
    Q_OBJECT

  public:
    explicit Base64ToImage(QWidget *parent = nullptr);
    QPushButton *convertButton;
    QPlainTextEdit *inputEdit;

  signals:
  public slots:
    void convertButtonClicked();

  private:
    QImage convertImage(QByteArray &input);
};

#endif // BASE64TOIMAGE_H
