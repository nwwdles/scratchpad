#include "imagetobase64.h"
#include "ui_imagetobase64.h"
#include <QBuffer>
#include <QDebug>
#include <QFileDialog>
#include <QMimeDatabase>
#include <QMimeType>

ImageToBase64::ImageToBase64(QWidget *parent)
    : QWidget(parent), ui(new Ui::ImageToBase64)
{
    ui->setupUi(this);
    connect(ui->fileEdit, &QLineEdit::returnPressed, this,
            &ImageToBase64::on_convertButton_clicked);
}

ImageToBase64::~ImageToBase64() { delete ui; }

void ImageToBase64::on_fileOpenButton_clicked()
{
    auto filename = QFileDialog::getOpenFileName(
        this, tr("Open image files"), QDir::currentPath(),
        tr("Image files (*.png *.jpg *.bmp);;All files (*.*)"));
    if (filename.size() == 0)
        return;
    ui->fileEdit->setText(filename);
};

void ImageToBase64::on_convertButton_clicked()
{
    auto filename = ui->fileEdit->text();
    auto arr = convertImage(filename);

    ui->outputText->document()->setPlainText(arr.toBase64());
};

QByteArray ImageToBase64::convertImage(QString &filename)
{
    auto img = QImage(filename);
    QByteArray arr;
    QBuffer buffer(&arr);
    buffer.open(QIODevice::WriteOnly);
    QMimeType type =
        QMimeDatabase().mimeTypeForFile(filename, QMimeDatabase::MatchContent);
    img.save(&buffer,
             const_cast<char *>(type.preferredSuffix().toStdString().c_str()));
    return arr;
};
