#ifndef IMAGETOBASE64_H
#define IMAGETOBASE64_H

#include <QByteArray>
#include <QString>
#include <QWidget>

namespace Ui
{
class ImageToBase64;
}

class ImageToBase64 : public QWidget
{
    Q_OBJECT

  public:
    explicit ImageToBase64(QWidget *parent = nullptr);
    ~ImageToBase64();

  private slots:
    void on_fileOpenButton_clicked();
    void on_convertButton_clicked();

  private:
    QByteArray convertImage(QString &filename);
    Ui::ImageToBase64 *ui;
};

#endif // IMAGETOBASE64_H
