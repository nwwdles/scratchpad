#include "mainwindow.h"
#include "picstopdf.h"
#include <QApplication>
#include <QSplitter>
#include <QTextEdit>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    a.setApplicationDisplayName("Scratchpad");
    return a.exec();
}
