#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "base64converter.h"
#include "base64toimage.h"
#include "imagetobase64.h"
#include "picstopdf.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    contentsWidget = ui->contentsWidget;
    pagesWidget = ui->pagesWidget;

    setWindowTitle(tr("Showcase"));

    addPage(new Base64Converter(), "Base64 converter");
    addPage(new PicsToPdf(), "Pictures to PDF");
    addPage(new ImageToBase64(), "Picture to Base64");
    addPage(new Base64ToImage(), "Base64 to picture");

    contentsWidget->setCurrentRow(0);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::addPage(QWidget *wgt, const char *name)
{
    pagesWidget->addWidget(wgt);
    QListWidgetItem *item = new QListWidgetItem(contentsWidget);
    item->setText(tr(name));
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
}

void MainWindow::on_contentsWidget_currentItemChanged(QListWidgetItem *current,
                                                      QListWidgetItem *previous)
{
    if (!current)
        current = previous;
    pagesWidget->setCurrentIndex(ui->contentsWidget->row(current));
}
