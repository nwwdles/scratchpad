#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QListWidget>
#include <QListWidgetItem>
#include <QMainWindow>
#include <QStackedWidget>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

  private slots:

    void on_contentsWidget_currentItemChanged(QListWidgetItem *current,
                                              QListWidgetItem *previous);

  private:
    void addPage(QWidget *wgt, const char *name);

    Ui::MainWindow *ui;

    QListWidget *contentsWidget;
    QStackedWidget *pagesWidget;
};

#endif // MAINWINDOW_H
