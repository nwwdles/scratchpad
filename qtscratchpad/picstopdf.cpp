#include "picstopdf.h"

#include <QDebug>
#include <QDesktopServices>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QListWidget>
#include <QMenu>
#include <QMessageBox>
#include <QPainter>
#include <QPrinter>
#include <QPushButton>
#include <QVBoxLayout>

PicsToPdf::PicsToPdf(QWidget *parent) : QWidget(parent)
{
    // create wgts
    fileListWidget = new QListWidget(this);
    progressBar = new QProgressBar(this);
    auto addButton = new QPushButton(tr("Add files..."));
    auto convertButton = new QPushButton(tr("Convert to PDF..."));
    auto removeButton = new QPushButton(tr("Remove selected item(s)"));
    auto clearButton = new QPushButton(tr("Clear"));

    // connect button clicks
    connect(addButton, &QAbstractButton::clicked, this,
            &PicsToPdf::on_addButton_clicked);
    connect(convertButton, &QAbstractButton::clicked, this,
            &PicsToPdf::on_convertButton_clicked);
    connect(removeButton, &QAbstractButton::clicked, this,
            &PicsToPdf::removeItems);
    connect(clearButton, &QAbstractButton::clicked, this,
            &PicsToPdf::clearItems);

    //    connect(&fileListWidget, SIGNAL(indexesMoved(const QModelIndexList&)),
    //    this,
    //            SLOT(onReordered(const QModelIndexList&)));
    connect(fileListWidget->model(), &QAbstractItemModel::rowsMoved, this,
            &PicsToPdf::onReordered);
    connect(fileListWidget, &QListWidget::itemDoubleClicked, this,
            &PicsToPdf::onItemDoubleClicked);

    // setup filelist
    fileListWidget->setIconSize(QSize(64, 64));
    fileListWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    fileListWidget->setDragEnabled(true);
    fileListWidget->setDragDropMode(QAbstractItemView::InternalMove);
    fileListWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(fileListWidget, SIGNAL(customContextMenuRequested(QPoint)), this,
            SLOT(showContextMenu(const QPoint &)));

    // setup layout
    auto hBoxLayout = new QHBoxLayout();
    hBoxLayout->addWidget(removeButton);
    hBoxLayout->addWidget(clearButton);
    auto vBoxLayout = new QVBoxLayout();
    vBoxLayout->addWidget(addButton);
    vBoxLayout->addLayout(hBoxLayout);
    vBoxLayout->addWidget(fileListWidget);
    vBoxLayout->addWidget(convertButton);
    vBoxLayout->addWidget(progressBar);
    setLayout(vBoxLayout);

    // setup wgt names (i don't use them though)
    fileListWidget->setObjectName("fileListWidget");
    addButton->setObjectName("addButton");
    convertButton->setObjectName("convertButton");
}

void PicsToPdf::on_convertButton_clicked()
{
    // no files in file list
    if (fileList.empty()) {
        QMessageBox msgBox;
        msgBox.setText("Please select at least 1 picture.");
        msgBox.exec();
        return;
    }

    // get filename
    QString fileName = QFileDialog::getSaveFileName(
        this, tr("Save File"), QDir::currentPath() + "/out.pdf",
        tr("PDF documents (*.pdf);;All files (*.*)"));
    if (fileName.size() == 0)
        return; // if filename is empty it starts to print pics on a default
                // printer. we do not want this

    // setup printer
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fileName);
    printer.setFullPage(true);
    printer.setPageMargins(QMarginsF(0, 0, 0, 0));
    printer.setResolution(96);

    // setup first page size (should be done before using painter.begin())
    QImage image(fileList[0]);
    printer.setPageSize(
        QPageSize(QSizeF(qreal(image.width()) / 96, qreal(image.height()) / 96),
                  QPageSize::Inch));

    // draw images
    QPainter painter(&printer);
    painter.drawImage(0, 0, image); // draw 1st image
    progressBar->setRange(0, fileList.size());
    for (int i = 1; i < fileList.size(); ++i) { // starting with 2nd image
        progressBar->setValue(i);
        QImage image(fileList[i]);
        printer.setPageSize(QPageSize(
            QSizeF(qreal(image.width()) / 96, qreal(image.height()) / 96),
            QPageSize::Inch));
        printer.newPage(); // should be called after setting wanted page size
        painter.drawImage(0, 0, image);
    }
    progressBar->setValue(fileList.size());
}

void PicsToPdf::on_addButton_clicked()
{

    // get filenames
    QStringList filenames = QFileDialog::getOpenFileNames(
        this, tr("Open image files"), QDir::currentPath(),
        tr("Image files (*.png *.jpg *.bmp);;All files (*.*)"));
    if (filenames.empty())
        return;

    // remember old and new filenames
    int prevItemCount = fileList.size();
    auto oldItems = fileList;
    fileList += filenames;
    fileListWidget->addItems(filenames);

    // add icons for new files
    progressBar->setRange(prevItemCount, fileList.size());
    for (int i = prevItemCount; i < fileList.size(); ++i) {
        progressBar->setValue(i);
        QIcon icon;
        // check if we already made an icon for this file
        if (oldItems.contains(fileList[i])) {
            // use an already created icon
            int prev = oldItems.indexOf(fileList[i]);
            icon = fileListWidget->item(prev)->icon();
        } else {
            // QIcon(fname) cam use a lot of memory so we first load img to
            // a pixmap and resize it, then make a QIcon
            auto pix = QPixmap(fileList[i]);
            pix = pix.scaledToWidth(64);
            icon = QIcon(pix);
        }
        fileListWidget->item(i)->setIcon(icon);
    }
    progressBar->setValue(fileList.size());
}

void PicsToPdf::showContextMenu(const QPoint &pos) const
{
    QPoint globalPos = fileListWidget->mapToGlobal(pos);

    // Create menu and insert some actions
    QMenu myMenu;
    myMenu.addAction("Remove", this, SLOT(removeItems()));
    myMenu.addAction("Clear", this, SLOT(clearItems()));

    // Show context menu at handling position
    myMenu.exec(globalPos);
}

void PicsToPdf::removeItems()
{
    for (auto item : fileListWidget->selectedItems()) {
        // select items one by one, get its row and remove it
        fileListWidget->setCurrentItem(item);
        int row = fileListWidget->currentRow();
        item = fileListWidget->takeItem(row);
        Q_ASSERT(fileList[row] == item->text());
        fileList.removeAt(row); // remove corresponding entry from fileList
        delete item;
    }
}

void PicsToPdf::clearItems()
{
    fileList.clear();
    fileListWidget->clear();
}

void PicsToPdf::onItemDoubleClicked(QListWidgetItem *item) const
{
    //    fileListWidget.setCurrentItem(item);
    int row = fileListWidget->row(item);
    //    QDesktopServices::openUrl(QUrl("file:///" + fileList[row]));
    auto url = QUrl::fromLocalFile(fileList[row]);
    qDebug() << url;
    QDesktopServices::openUrl(url);
}

void PicsToPdf::onReordered(const QModelIndex &parent, int start, int end,
                            const QModelIndex &destination, int row)
{
    // this is probably slow and honestly isn't needed because we can get rid of
    // fileList variable completely and rely on fileListWidget to store the data
    // instead
    if (start < row) {
        fileList.insert(row, fileList[start]);
        fileList.removeAt(start);
    } else {
        fileList.insert(row, fileList[start]);
        fileList.removeAt(start + 1);
    }
}
