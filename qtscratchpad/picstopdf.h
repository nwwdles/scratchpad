#pragma once

#include <QListWidget>
#include <QProgressBar>
#include <QWidget>

class PicsToPdf : public QWidget
{
    Q_OBJECT

  private:
    QStringList fileList;
    QListWidget *fileListWidget;
    QProgressBar *progressBar;

  public:
    explicit PicsToPdf(QWidget *parent = nullptr);

  signals:

  public slots:
    void on_addButton_clicked();
    void on_convertButton_clicked();
    void showContextMenu(const QPoint &) const;
    void removeItems();
    void clearItems();
    void onItemDoubleClicked(QListWidgetItem *item) const;
    void onReordered(const QModelIndex &parent, int start, int end,
                     const QModelIndex &destination, int row);
};
