# tg-archiver

Archiver for telegram.

## References

- [Telegram app configuration (get api id, hash)](https://my.telegram.org/apps)
- [Telethon docs](https://docs.telethon.dev/en/latest/index.html)
  - [Using proxy](https://docs.telethon.dev/en/latest/basic/signing-in.html?highlight=proxy#signing-in-behind-a-proxy)
