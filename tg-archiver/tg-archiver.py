#!/usr/bin/env python3

import argparse
import sqlite3
import json
import io
import os
import time
from datetime import datetime
from itertools import chain

import socks  # pip install PySocks
import telethon.network.connection.tcpfull  # pip install telethon
from telethon.sync import TelegramClient, events, connection


#
# consts
#


# session params
DEFAULT_SESSION_NAME = "TEST_SESSION"

# paths/filenames
DEFAULT_LOG_DB = "tg_dumped.db"
DEFAULT_DUMP_FOLDER = "tg-dump"
DEFAULT_JSON_PREFIX = "dump"
DEFAULT_MEDIA_SUBFOLDER = "media"

# environment variables from which id and hash are read
# if not provided as args
API_ID_ENV = "TG_API_ID"
API_HASH_ENV = "TG_API_HASH"


#
# utils
#


def fix_ext(ext):
    """Fix jpgs having .jpe extensions."""

    # .jpe weirdness is most likely because .jpe is first in
    #   mimetypes.guess_all_extensions("image/jpeg")

    return {".jpe": ".jpg"}.get(ext, ext)


def dump_to_json(dump_folder, messages: dict):
    fname = os.path.join(dump_folder, "dump" + str(datetime.now()) + ".json")

    # simple open() results in unicode looking like \u0437
    with io.open(fname, "w", encoding="utf8") as f:
        json.dump(
            messages,
            f,
            ensure_ascii=False
            # skipkeys=True
        )

    return fname


def get_entity_title(entity):
    # todo: make it less ugly
    out = ""
    if hasattr(entity, "title") and entity.title:
        out = entity.title
    elif hasattr(entity, "username") and entity.username:
        out += entity.username
    elif hasattr(entity, "first_name") and entity.first_name:
        out += entity.first_name
        if entity.last_name:  #
            out += " " + entity.last_name
    return out


class DownloadLogger:
    """Logs chat/message IDs, checks if messages were logged."""

    # SQLite can hande multiple connections.
    # And currently it's not important to ensure that one database
    # is accessed by just one logger.

    def __init__(self, filename=DEFAULT_LOG_DB):
        self.conn = sqlite3.connect(filename)
        self.c = self.conn.cursor()

        # check if database is new (empty) and initialise it
        self.c.execute("SELECT name FROM sqlite_master WHERE type='table';")
        if not self.c.fetchall():
            self.c.execute("CREATE TABLE dumped_posts (channel_id,post_id)")
            self.c.execute(
                "CREATE unique index uid_dumped_posts on dumped_posts (channel_id,post_id);"
            )
            self.commit()

    def commit(self):
        """Write logged IDs to database."""

        self.conn.commit()

    def was_downloaded(self, message, print_message=False):
        res = self.c.execute(
            "SELECT * FROM dumped_posts WHERE channel_id=? AND post_id=?",
            (message.chat_id, message.id),
        )
        if res.fetchone():
            # self.print_message_old(message)
            return True
        self.print_message_new(message)
        return False

    def mark_downloaded(self, message):
        """Log chat & message ID (but don't write it to database)."""

        self.c.execute(
            "insert OR IGNORE into dumped_posts values (?,?)",
            (message.chat_id, message.id),
        )

    # TODO: these two methods can as well output 'Downloaded' and 'Skipped'
    # and be moved into the Runner
    @classmethod
    def print_message_new(cls, message):
        print("New:", get_entity_title(message.sender), "\tid:", message.id)

    @classmethod
    def print_message_old(cls, message):
        print("Old:", get_entity_title(message.sender), "\tid:", message.id)


class MessageDownloader:
    """Downloads message attachments, text and relevant fields.

    Stores attachment filepath in the message data."""

    def __init__(
        self,
        media_folder,
        download_media=True,
        download_text=True,
        download_raw_text=True,
    ):
        self.download_media = download_media
        self.download_text = download_text
        self.download_raw_text = download_raw_text

        self.set_media_folder(media_folder)

    def set_media_folder(self, media_folder):
        self.media_folder = media_folder
        os.makedirs(self.media_folder, exist_ok=True)

    async def get_message_data(self, message) -> dict:
        data = self.get_main_data(message)

        # media contents
        data = {**await self.get_media(message), **data}
        return data

    async def get_media(self, message) -> dict:
        data = {}
        media_path = None
        if self.download_media and message.file:
            fname = message.file.id + fix_ext(message.file.ext)
            media_path = os.path.join(self.media_folder, fname)
            if not os.path.exists(media_path):
                media_path = await message.download_media(file=media_path)
            data["file_id"] = message.file.id
            data["file_path"] = media_path
            if message.document:
                data["attribute_file_name"] = message.document.attributes[1].file_name
            if message.file.name:
                data["file_name"] = message.file.name
        return data

    def get_main_data(self, message) -> dict:
        """Extracts text data and metadata (sender ID, etc) from the message."""

        data = {
            "id": message.id,
            "sender_id": message.sender_id,
            "sender": {"username": message.sender.username},
            "chat_id": message.chat_id,
            "date": str(message.date),
        }

        # if message.sender.title:
        #     data["sender"]["title"] = message.sender.title

        # text contents
        if self.download_text:
            # todo: find out the difference between text and message
            if message.text:
                data["text"] = message.text
            if message.message:
                data["message"] = message.message
        if self.download_raw_text:
            if message.raw_text:
                data["raw_text"] = message.raw_text

        return data


class Runner:
    """Main downloader class. Manages which chats to download.
    """

    # TODO: try not to make a monstrosity out of it

    def __init__(
        self,
        client,
        channels=[],
        dump_folder=DEFAULT_DUMP_FOLDER,
        include_contacts=True,
        log_database=None,
    ):
        self.include_contacts = include_contacts
        self.client = client
        self.channels = channels

        self.dump_folder = dump_folder

        media_folder = os.path.join(self.dump_folder, DEFAULT_MEDIA_SUBFOLDER)
        self.downloader = MessageDownloader(media_folder)

        self.download_logger = (
            DownloadLogger(log_database) if log_database else DownloadLogger()
        )

    def run(self):
        with self.client:
            self.client.loop.run_until_complete(self.download_messages())

    async def download_messages(self):
        # TODO: consistently use channels/dialogs/chats. choose one.

        # download
        downloaded_messages = {}
        async for channel in self.iter_channels():
            channel_messages = await self.download_channel_messages(channel)
            if channel_messages:
                downloaded_messages[channel] = channel_messages

        # dump messages json
        if downloaded_messages:
            fname = dump_to_json(self.dump_folder, downloaded_messages)
            self.download_logger.commit()
            print("New messages were saved to", fname)
        else:
            print("Nothing to write.")
            # TODO: use logging

    async def download_channel_messages(self, channel):
        ATTEMPTS = 10
        for attempt in range(ATTEMPTS):
            try:
                dump = []
                title = get_entity_title(await self.client.get_entity(channel))
                print("Checking", title)
                async for message in self.client.iter_messages(channel, limit=23):
                    # TODO: add date/count limits to argparse
                    if not self.download_logger.was_downloaded(message, True):
                        data = await self.downloader.get_message_data(message)
                        if data:
                            dump.append(data)
                            self.download_logger.mark_downloaded(message)
                return dump
            except telethon.errors.rpcerrorlist.ChannelInvalidError as e:
                print(
                    f"Some channel fetch error that randomly appears sometimes. Retrying {attempt}/{ATTEMPTS}."
                )
                time.sleep(1)

    async def iter_channels(self):
        # include dialogs from contacts
        if self.include_contacts:
            channels = self.channels + await self.get_user_dialogs()
        else:
            channels = self.channels
        channels = list(set(channels))  # deduplicate
        # TODO: decide if it's better to use a set everywhere
        # TODO: check if there's a way to deduplicate a chain of generator and a list
        # without dumping generator to a list first

        # list channel names for the user
        # TODO: generator is not a place for such notifications.
        names = await self.get_channels_names(channels)
        print("Downloading messages for:", ", ".join(names))

        for channel in channels:
            yield channel

    async def get_channels_names(self, channels):
        a = await self.convert_names_to_entities(channels)
        return [get_entity_title(x) for x in a]

    async def convert_names_to_entities(self, names):
        """Gets User/Channel/etc entities by their names/IDs."""
        out = []
        async for n in self.iter_entities(names):
            out.append(n)
        return out

    async def iter_entities(self, names):
        for n in names:
            yield await self.client.get_entity(n)

    async def get_user_dialogs(self) -> list:
        user_dialogs = []
        async for dialog in self.client.iter_dialogs():
            user_dialogs.append(dialog.entity.id)
        return user_dialogs


def parse_arguments():
    parser = argparse.ArgumentParser(description="Dump Telegram chat messages.")

    # todo: should be one arg?
    parser.add_argument(
        "--api-id",
        help=f"api id. If not provided, read from {API_ID_ENV} variable",
        metavar="ID",
        default=os.getenv(API_ID_ENV),
    )
    parser.add_argument(
        "--api-hash",
        help=f"api hash. If not provided, read from {API_HASH_ENV} variable",
        metavar="HASH",
        default=os.getenv(API_HASH_ENV),
    )

    parser.add_argument(
        "-s",
        "--session",
        help=f"session name. Different name requires relogin. Defaults to '{DEFAULT_SESSION_NAME}'",
        type=str,
        default=DEFAULT_SESSION_NAME,
        metavar="NAME",
    )
    parser.add_argument(
        "-i", "--include-dialogs", action="store_true", help="include user dialogs"
    )
    parser.add_argument(
        "-c",
        "--chats",
        help="chats/groups/channels to dump",
        nargs="+",
        metavar="C",
        default=[],
    )
    parser.add_argument(
        "-m",
        "--monitor",
        action="store_true",
        help="wait for new messages after dumping, don't close",
    )

    # output args
    parser.add_argument("-d", "--dry-run", action="store_true", help="don't clone/pull")

    parser.add_argument(
        "-o",
        "--output-folder",
        help=f"folder to dump media and messages to. defaults to '{DEFAULT_DUMP_FOLDER}'",
        default=DEFAULT_DUMP_FOLDER,
        metavar="PATH",
    )

    # TODO: specifying a filename should overwrite filename completely, not just a prefix
    parser.add_argument(
        "-f",
        "--filename",
        help="json filename",
        metavar="NAME",
        default=DEFAULT_JSON_PREFIX,
    )

    # proxy args
    # todo: use --proxy HOST PORT
    parser.add_argument("--host", metavar=("HOST"), help="proxy host")
    parser.add_argument("--port", metavar=("PORT"), help="proxy port")
    parser.add_argument(
        "--secret",
        metavar=("SECRET"),
        help="proxy secret. If set, proxy is interpreted as mtproto. If not set - socks5",
    )

    args = parser.parse_args()

    return args


def main():
    args = parse_arguments()

    #
    # process args
    #

    session_name = args.session
    api_id = args.api_id
    api_hash = args.api_hash

    channels = args.chats
    include_dialogs = args.include_dialogs

    dryrun = args.dry_run
    dump_folder = args.output_folder
    dump_filename = args.filename

    host = args.host
    port = args.port
    secret = args.secret

    # proxy setup
    if host:
        if secret:
            proxy = (host, port, secret)
            proxy_con = connection.ConnectionTcpMTProxyRandomizedIntermediate
        else:
            proxy = (socks.SOCKS5, host or "127.0.0.1", port or 9150)
            proxy_con = telethon.network.connection.tcpfull.ConnectionTcpFull
    else:
        proxy = None
        proxy_con = telethon.network.connection.tcpfull.ConnectionTcpFull

    #
    # run
    #

    client = TelegramClient(
        session_name, api_id, api_hash, connection=proxy_con, proxy=proxy
    )
    runner = Runner(client, channels=channels, include_contacts=include_dialogs)
    runner.run()


if __name__ == "__main__":
    main()

