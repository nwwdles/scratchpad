# -*- coding: cp1251 -*-
from PyQt4.QtGui import QApplication, QWidget, QFileDialog
from PyQt4 import uic


def fileOpen():
    global fileLines
    
    w.textBrowser.append("Opening file")
    path = QFileDialog.getOpenFileName(
        w,
        u"�������� ����",
        '.',
        u"��� �����(*);;TXT-����(*.txt);;JSON-����(*.json)")
    
    if path != "":   
        F = open(path, "r")
        fileLines = []
        w.textBrowser.append("Reading File")
        
        for a in F:
            fileLines.append(a)
            w.textBrowser_Input.append(a.strip("\n"))
        F.close()
        w.textBrowser.append("File read")
        w.pushButton_save.setEnabled(True)
        w.pushButton_work.setEnabled(True)
        #work()
    else:
        w.textBrowser.append("File not selected")
        w.pushButton_save.setEnabled(False)
        w.pushButton_work.setEnabled(False)
    

def fileSave():
    global fileLines
    global newFileLines
    path = QFileDialog.getSaveFileName(
        w,
        u"Save As",
        '.',
        u"JSON-����(*.json);;TXT-����(*.txt);;��� �����(*)")

    F = open(path, "w")
    for a in newFileLines:
        F.write(a)
    F.close()
    w.textBrowser.append("File saved")


def work():
    global fileLines
    global newFileLines
    newFileLines = []
    for i in range(len(fileLines)):
        newFileLines.append(fileLines[i])
    framesEntered = False
    frameTagsEntered = False
    
    additionalpart = "_"
    additionalpart = str(w.lineEdit.text())
    w.textBrowser_Output.clear()
    
    #finding sections
    for i in range(len(fileLines)):

        if fileLines[i].strip().startswith("{ \"frames\":"):
            #w.textBrowser_Output.append(fileLines[i].strip("\n"))
            framesEntered = True
            framesStart = i
            w.textBrowser.append("frames start line: " + str(framesStart))
            
        if framesEntered == True:
            if fileLines[i].strip().startswith("]"):
                #w.textBrowser_Output.append(fileLines[i].strip("\n"))
                framesEntered = False
                framesEnd = i
                w.textBrowser.append("frames end line: " + str(framesEnd))
        
        if fileLines[i].strip().startswith("\"frameTags\":"):
            #w.textBrowser_Output.append(fileLines[i].strip("\n"))
            frameTagsEntered = True
            frameTagsStart = i
            w.textBrowser.append("frameTags start line: " + str(frameTagsStart))
            
        if frameTagsEntered == True:
            if fileLines[i].strip().startswith("]"):
                #w.textBrowser_Output.append(fileLines[i].strip("\n"))
                frameTagsEntered = False
                frameTagsEnd = i
                w.textBrowser.append("frameTags end line: " + str(frameTagsEnd))
                
        ### Image address fix ###
        if fileLines[i].strip().startswith("\"image\":"):
            #w.textBrowser_Output.append(fileLines[i].strip("\n"))
            imageLine = i
            w.textBrowser.append("image line: " + str(imageLine))
            #find left and right sides
            leftSlashPos = fileLines[i].rfind("\\", 0, len(fileLines[i]))
            rightPos = fileLines[i].find("\": \"", 0, len(fileLines[i])) + len("\": \"")
            #join them
            if leftSlashPos > 0:
                newFileLines[i] = fileLines[i][0: rightPos] + fileLines[i][leftSlashPos+1: len(fileLines[i])]
                #w.textBrowser_Output.append(fileLines[i].strip("\n"))

    ## find amount of frames
    framesAmt = 0
    for i in range(framesStart + 1, framesEnd):
        if fileLines[i].strip().startswith("\"filename"):
            framesAmt += 1
    print(framesAmt)
    
    #Make array out of frameTags
    frameTagsArray = []
    TagsArray = []
    k = 0
    
    for i in range(frameTagsStart + 1, frameTagsEnd):
        name = fileLines[i].split()[2].strip(',."')
        startNum = int(fileLines[i].split()[4].strip(',.\"'))
        endNum = int(fileLines[i].split()[6].strip(',."'))
        z = 0
    
        for a in range(startNum, endNum+1):
            frameTagsArray.append(name + "%(k)02d" % {'k': z})
            z += 1
        
        if i < frameTagsEnd-1:
            for a in range(endNum+1, int(fileLines[i+1].split()[4].strip(',.\"'))):
                frameTagsArray.append("nah%(k)02d" % {'k': k})
                k+=1

        TagsArray.append(name)
        
    #make sure that all frames after last tag are named in array
    for a in range(len(frameTagsArray), framesAmt):
        frameTagsArray.append("nah%(k)02d" % {'k': k})
        k+=1
        
    #w.textBrowser.append(frameTagsArray)
    #w.textBrowser.append(len(frameTagsArray))
    #w.textBrowser.append()
    #w.textBrowser.append(TagsArray)
    
    #duplicate tags check
    if len(TagsArray) != len(list(set(TagsArray))):
        print("\n### WARNING! Duplicate tags! ###")
        #w.textBrowser_Output.append("\n### WARNING! Duplicate tags! ###")
        w.textBrowser.append("\n### WARNING! Duplicate tags! ###")
        w.pushButton_save.setEnabled(False)
    else:
        w.pushButton_save.setEnabled(True)

    z = 0
    
    for i in range(framesStart + 1, framesEnd):
        if fileLines[i].strip().startswith("\"filename"):
            rightlim = fileLines[i].rfind(".ase\"")
            leftlim = fileLines[i].rfind(" ")
            newFileLines[i] = fileLines[i][0:leftlim]+additionalpart+str(frameTagsArray[z])+fileLines[i][rightlim+len(".ase\"") - 1:len(fileLines)]
            z+=1
            #else:
                #fileLines[i][fileLines[i].find(additionalpart)] = 
            #w.textBrowser_Output.append(fileLines[i])
    #w.textBrowser.append(framesAmt)

    for i in range(len(fileLines)):
        w.textBrowser_Output.append(newFileLines[i].strip("\n"))

    
app = QApplication([])
w = uic.loadUi("mainwindow.ui")

w.pushButton_open.clicked.connect(fileOpen)
w.pushButton_save.clicked.connect(fileSave)
w.pushButton_work.clicked.connect(work)
w.pushButton_save.setEnabled(False)
w.pushButton_work.setEnabled(False)
w.textBrowser.append("Please open file")
w.actionExit.triggered.connect(w.close)

w.show()
exit(app.exec_())
