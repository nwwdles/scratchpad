#!/usr/bin/env python3
import sys
import json


# class Food():
#     def __init__(self, name=None, kcal=0, fat=0, protein=0, carb=0):
#         self.name = name
#         self.protein = protein
#         self.kcal = kcal
#         self.carb = carb
#         self.fat = fat


def processmeal(meal, data):
    totalprot = 0
    totalfat = 0
    totalcarb = 0
    totalkcal = 0
    totalmass = 0
    for ingname in meal["ingredients"]:
        mass = meal["ingredients"][ingname]
        food_data = data[ingname]
        totalprot += food_data["prot"] / 100 * mass
        totalfat += food_data["fat"] / 100 * mass
        totalcarb += food_data["carb"] / 100 * mass
        totalkcal += food_data["kcal"] / 100 * mass
        totalmass += mass
    meal["kcal"] = 100 * totalkcal / totalmass
    meal["fat"] = 100 * totalfat / totalmass
    meal["carb"] = 100 * totalcarb / totalmass
    meal["prot"] = 100 * totalprot / totalmass


def processdata(data):
    # calc kcal for foods if missing
    for foodname in data:
        f = data[foodname]
        if "ingredients" in f:
            processmeal(f, data)
            print(f)
        else:
            if f["kcal"] < 0:
                f["kcal"] = (f["prot"] + f["carb"]) * 4 + f["fat"] * 9


FOODS_FNAME = "foods.json"

if __name__ == "__main__":
    try:
        with open(FOODS_FNAME) as f:
            data = json.load(f)
            # print(data)
            processdata(data)
            with open(FOODS_FNAME, "w") as outfile:
                json.dump(data, outfile, indent=4)

    except FileNotFoundError as e:
        print("Creating new files")
        data = {
            "example": {"kcal": 0, "carb": 0, "fat": 0, "prot": 0, "defaultmass": 0},
            "examplemeal": {"ingredients": {"example": 100}},
        }

        with open(FOODS_FNAME, "w") as outfile:
            json.dump(data, outfile, indent=4)
#     while(True):
#         a = input("""(A)dd food
# (L)ist today
# New (f)ood
# New (m)eal
# New (r)ecipe
# (Q)uit\n""")
#         if a == "Q" or a == "q":
#             break
#     print("Bye")

# a = sys.stdin.readline()
# if a == "a":
#     print("you've typed 'a'")
# print(a)
