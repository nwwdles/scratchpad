#!/usr/bin/env python3

import math


def circle_area(r):
    return r ** 2 * math.pi


def ball_volume(r):
    return (4 / 3) * math.pi * (r ** 3)


def main():
    d = 10
    r = d / 2
    circle_s = circle_area(r)
    square_s = d ** 2
    k = circle_s / square_s
    print(k, k ** 2)

    k2 = ball_volume(r) / (d ** 3)
    print(k2)
    pass


if __name__ == "__main__":
    main()
