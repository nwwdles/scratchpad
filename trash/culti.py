#!/usr/bin/env python3


def calc(hassoil, haswater, hasnutri):
    time = 3600
    basegrowtime = 180

    basefail = 0.2
    basecrit = 0.01
    basesuper = 0.01

    soilcrit = 0.01 * int(hassoil)
    watercrit = 0.01 * int(haswater)
    nutrifail = 0.01 * int(hasnutri)

    crit_chance = basecrit + soilcrit
    super_chance = basesuper + watercrit
    fail_chance = basefail - nutrifail

    soiltime = 1 / 30 * int(hassoil)
    watertime = 1 / 30 * int(haswater)
    nutritime = 1 / 30 * int(hasnutri)

    growtime = basegrowtime * (1 - soiltime - watertime - nutritime)
    baseplants = (4 * time / basegrowtime) * (1 + basecrit + basesuper) * (1 - basefail)
    plant = (4 * time / growtime) * (1 + crit_chance + super_chance) * (1 - fail_chance)

    plantsellprice = 8
    soilprice = 0.3 * int(hassoil)
    waterprice = 0.3 * int(haswater)
    nutriprice = 0.3 * int(hasnutri)

    cost = soilprice * plant + waterprice * plant + nutriprice * plant
    print("hassoil:", hassoil, "haswater:", haswater, "hasnutri:", hasnutri, sep="\t")
    # print("plants grown:", plant, "base plants", baseplants)
    # print("cost:", cost)
    # print("minplantcost", cost/plant)
    print("profit:", (plantsellprice * plant - cost) - plantsellprice * baseplants)
    print("profit%:", (plantsellprice * plant - cost) / (plantsellprice * baseplants))


calc(True, True, True)

