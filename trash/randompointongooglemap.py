#!/usr/bin/env python3
from random import uniform
from subprocess import call

latitude = uniform(-90, 90)
longitude = uniform(-180, 180)
call(
    [
        "firefox",
        "https://www.google.com/maps/@" + str(latitude) + "," + str(longitude) + ",14z",
    ]
)

