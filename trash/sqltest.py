#!/usr/bin/env python3

import sqlite3


def expr_to_query(table_name, fields, expfield, expr: str):
    q = f"SELECT DISTINCT ({fields}) FROM {table_name} WHERE {expfield}=1"
    return q
    # # get rid of operators so that only tags delimited nicely are left
    # delim = "&&"
    # tagstring = (
    #     tags.replace("||", delim)
    #     .replace("{{", delim)
    #     .replace("}}", delim)
    #     .replace("~~", delim)
    # )
    # tagnames = tagstring.split(delim)
    # tagnames = list(map(str.strip, tagnames))  # strip whistespace from tags
    # tagnames = [x for x in tagnames if x]  # and remove empty tags

    # tagids = [get_tag_id(c, x) for x in tagnames]  # make a list of tag ids

    # query = ""

    # if len(tagids) == 1:
    #     # with one tag query is simple
    #     tagid = tagids[0]
    #     query = 'SELECT imageid FROM ImageTags WHERE tagid="' + str(tagid) + '"'
    # elif tagids:
    #     # with multiple elements query is more complex
    #     for i in range(len(tagnames)):
    #         condition = "tagid=" + str(tagids[i])
    #         if i > 0:
    #             condition = (
    #                 "imageid IN (SELECT imageid FROM ImageTags WHERE " + condition + ")"
    #             )
    #         tags = tags.replace(tagnames[i], condition)
    #     query = (
    #         tags.replace("{{", "(")
    #         .replace("}}", ")")
    #         .replace("&&", " AND ")
    #         .replace("~~", " NOT ")
    #         .replace("||", " OR ")
    #     )
    #     query = "SELECT DISTINCT imageid FROM ImageTags WHERE (" + query + ")"

    # return query


def run_query(c, query):
    c.execute(query)
    return c.fetchall()


def main():
    conn = sqlite3.connect(":memory:")
    c = conn.cursor()
    table_name = "tags"
    fields = "img"
    exprfield = "tag"
    data = [
        ("1" "a"),
        ("2" "a"),
        ("3" "a"),
        ("1" "b"),
        ("2" "b"),
        ("1" "c"),
        ("3" "c"),
        ("2" "d"),
        ("3" "d"),
        ("1" "e"),
        ("2" "f"),
        ("3" "g"),
        ("4" "h"),
    ]
    c.execute(f"CREATE TABLE {table_name} ({exprfield} text, img text)")
    c.executemany(f"INSERT INTO {table_name} VALUES (?,?)", data)
    conn.commit()

    # expr = "1"
    # trueres = run_query(c, expr_to_query(table_name, fields, exprfield, expr))
    # print(trueres)
    # assert trueres == [("a",), ("b",), ("c",), ("e",)]

    trueres = run_query(c, f"SELECT DISTINCT img from {table_name} WHERE tag=2")
    print(trueres)


if __name__ == "__main__":
    main()
