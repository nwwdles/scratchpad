#!/usr/bin/env python3
import os
import csv


def ideaexport():
    with open("ideas.csv", "r") as f:
        spamreader = csv.DictReader(f, delimiter="\t", quotechar='"')
        for row in spamreader:
            title = row["Title"].replace("\n", " ").replace("\t", " ")
            text = row["Description"]

            firstline = title

            fulltext = firstline + "\n\n" + text.replace("<br/>", "\n")
            print(fulltext)
            with open("temp/" + firstline + ".md", "w") as out:
                out.write(fulltext)


def anqexport():
    with open("tellico-new.csv", "r") as f:
        spamreader = csv.DictReader(f, delimiter="\t", quotechar='"')
        for row in spamreader:
            title = row["Title"]
            date = row["Date"]
            text = row["Text"]
            m = row["M"]
            f = row["F"]
            u = row["U"]
            sent = row["Sent"]
            firstline = date + " " + title
            if sent:
                firstline += " sent"
            if int(m) > 0:
                firstline += " m" + str(m)
            if int(f) > 0:
                firstline += " f" + str(f)
            if int(u) > 0:
                firstline += " u" + str(u)

            fulltext = firstline + "\n\n" + text.replace("<br/>", "\n")
            print(fulltext)
            with open("temp/" + firstline + ".md", "w") as out:
                out.write(fulltext)
            # print(', '.join(row))


anqexport()
