#!/usr/bin/env python3
#
# some script i sent somebody on 2ch. it doesn't do anything useful

import argparse
import io
import os
import datetime
import socks  # pip install PySocks
import telethon.network.connection.tcpfull  # pip install telethon
from telethon.sync import TelegramClient, events, connection


# session params
DEFAULT_SESSION_NAME = "TEST_SESSION"

# environment variables from which id and hash are read
# if not provided as args
API_ID_ENV = "TG_API_ID"
API_HASH_ENV = "TG_API_HASH"


class Runner:
    def __init__(self, *args, **kwags):
        self.client = TelegramClient(*args, **kwags)

    def run(self):
        with self.client:
            self.client.loop.run_until_complete(self.send_messages())

    async def send_messages(self):
        await self.client.send_message("me", "konnichiwa!")


def parse_arguments():
    parser = argparse.ArgumentParser(description="Dump Telegram chat messages.")

    parser.add_argument(
        "--api-id",
        help=f"api id. If not provided, is read from {API_ID_ENV} variable",
        metavar="ID",
        default=os.getenv(API_ID_ENV),
    )
    parser.add_argument(
        "--api-hash",
        help=f"api hash. If not provided, is read from {API_HASH_ENV} variable",
        metavar="HASH",
        default=os.getenv(API_HASH_ENV),
    )

    parser.add_argument(
        "-s",
        "--session",
        help=f"session name. Different name requires relogin. Defaults to '{DEFAULT_SESSION_NAME}'",
        type=str,
        default=DEFAULT_SESSION_NAME,
        metavar="NAME",
    )

    parser.add_argument("--host", metavar=("HOST"), help="proxy host")
    parser.add_argument("--port", metavar=("PORT"), help="proxy port")
    parser.add_argument(
        "--secret",
        metavar=("SECRET"),
        help="proxy secret. If set, proxy is interpreted as mtproto. If not set - socks5",
    )

    args = parser.parse_args()

    return args


def main():
    args = parse_arguments()
    args.port = int(args.port)

    if args.host:
        if args.secret:
            proxy = (args.host, args.port, args.secret)
            proxy_con = connection.ConnectionTcpMTProxyRandomizedIntermediate
        else:
            proxy = (socks.SOCKS5, args.host, args.port)
            proxy_con = telethon.network.connection.tcpfull.ConnectionTcpFull
    else:
        proxy = None
        proxy_con = telethon.network.connection.tcpfull.ConnectionTcpFull

    print("started with proxy:", proxy, proxy_con)
    runner = Runner(
        args.session, args.api_id, args.api_hash, connection=proxy_con, proxy=proxy
    )
    runner.run()


if __name__ == "__main__":
    main()

