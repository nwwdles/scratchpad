#! python3
# downloadXkcd.py - Downloads every Trello img.

import requests, os, bs4, re, time

starturl = 'https://blog.trello.com/page/'              # starting url
url = starturl
os.makedirs('trello', exist_ok=True)   # store comics in ./xkcd
i = 9

pageLinks = []
picLinks = []
while not url.endswith('38'):
    # Download the page.
    url = starturl + str(i)
    i += 1
    print('Downloading page %s...' % url)
    res = requests.get(url)
    res.raise_for_status()

    soup = bs4.BeautifulSoup(res.text, "html.parser")
    # i don't fucking know what im doing
    linkmatches = re.findall('"((http|ftp)s?://blog.trello.com/(?!(hubf|rss|hs-f|pag|search-result))[0-9A-Za-z\-]+(?!/)(?!\?)).*?"', str(soup))

    for link in linkmatches:
        pageLinks.append(link[0])
    time.sleep(0.2)

print(pageLinks)
pageLinks = list(set(pageLinks))


for page in pageLinks:
    print('Downloading page %s...' % page)
    try:
        res = requests.get(page)
        res.raise_for_status()
        soup = bs4.BeautifulSoup(res.text, "html.parser")
        # print(str(soup))
        # find pics
        # picmatches = re.findall('"(((http|ftp)s?|):?//(blog.trello.com/((.+?(?![\s|"]))\.(jpg|png|svg).?(?=["?]))))',

        picmatches = re.findall('"(((http|ftp)s?|):?//((blog.trello.com/([^\s.]*?)\.(png))(\?)?(.+?))")',
                                str(soup))

        print (picmatches)
        for link in picmatches:
            picLinks.append("https://"+link[4])

        # print(pageLinks)
        # time.sleep(0.2)
    except requests.exceptions.HTTPError:
        print("error")
        continue

picLinks = list(set(picLinks))
print(picLinks)

for picUrl in picLinks:
    try:
        res = requests.get(picUrl)
        res.raise_for_status()
        imageFile = open(os.path.join('trello', os.path.basename(picUrl)), 'wb')
        for chunk in res.iter_content(100000):
            imageFile.write(chunk)
        imageFile.close()
        time.sleep(0.1)
        print(picUrl)
    except requests.exceptions.HTTPError:
        print("skipping")
        continue

print('Done.')