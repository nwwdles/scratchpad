# TUI (ncurses/urwid/etc) things

Minor projects and example code from tutorials.

## Materials

- [ncurses man page](https://www.manpagez.com/man/3/ncurses/)
- [urwid](http://urwid.org) ([github](https://github.com/urwid/urwid))

### Tutorials and examples

- ncurses
  - (C) [**NCURSES Programming HOWTO**](http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/index.html)
  - (C++) [Roguelike game in C++](https://solarianprogrammer.com/2012/07/12/roguelike-game-cpp-11-part-0/)
  - (C++) [NCURSES Terminal Text Editor](https://cheukyin699.github.io/tutorial/c++/2015/02/01/ncurses-editor-tutorial-01.html)
  - http://www.cs.ukzn.ac.za/~hughm/os/notes/ncurses.html
- urwid
  - [official urwid tutorial](http://urwid.org/tutorial/index.html)

#### Youtube

- ncurses
  - (C++) [CasualCoder playlist](https://www.youtube.com/playlist?list=PL2U2TQ__OrQ8jTf0_noNKtHMuYlyxQl4v)
  - (C++) [Rachel Singh's dungeon crawler](https://www.youtube.com/playlist?list=PL9Kj-MdBMaPCBjSg8DTLAZPpCU26dTqCT)
  - (C++) [thecplusplusguy playlist](https://www.youtube.com/playlist?&list=PL2C01CC54638DD952)
    - snake game structure is terrible though
  - (C) [Giga Raptor playlist](https://www.youtube.com/playlist?list=PLGqirp9MxbFr2T94um1M1cIKnbVM2w_G7)

#### Project Repos

- ncurses
  - (C++) [dafrito/NCurses-Examples](https://github.com/dafrito/NCurses-Examples/) - Binary clock and more. (Different programs are not separate files but commits.)
  - [xorg62/tty-clock](https://github.com/xorg62/tty-clock)
  - [fundamelon/terminal-game-tutorial](https://github.com/fundamelon/terminal-game-tutorial)
  - [brenns10/tetris](https://github.com/brenns10/tetris)
  - [jvns/snake](https://github.com/jvns/snake)
  - [fabiomontefuscolo/snake-ncurse](https://github.com/fabiomontefuscolo/snake-ncurse)
  - (C) [abishekvashok/cmatrix](https://github.com/abishekvashok/cmatrix)
- urwid
  - [Applications and libraries using urwid](https://github.com/urwid/urwid/wiki/Application-list)

### Libraries

- [CPPurses](https://github.com/a-n-t-h-o-n-y/CPPurses) - UI Library (widgets, layouts, events...)

## Ideas

- a simple program to cut videos
  - image previews
    - [ueberzug](https://github.com/seebye/ueberzug)
    - [w3m](https://gist.github.com/iacchus/ec5dafa6feee64848940a6e94a302462)
      - `echo -e '0;1;0;0;200;160;;;;;ant.jpg\n4;\n3;' | /usr/lib/w3m/w3mimgdisplay`
- Todo list
- Mind map software
- Disk usage analyzer
  - (Rust) [dua-cli](https://github.com/Byron/dua-cli)
- UI library (widgets, layouts, events...)
- Calories tracker
- Meal planner
- SQLite database browser
  - (Python) [SQLcrush](https://github.com/coffeeandscripts/sqlcrush)
- Game
  - Sudoku / Tic-Tac-Toe
  - Roguelike / Space RPG
  - Visual novel
  - Cookie clicker
  - Stock trading sim
- Music tracker / synthesizer / etc
  - (C++) [(Youtube) Making a synthesizer](https://www.youtube.com/watch?v=tgamhuQnOkM)
- Music player (like cmus/ncmpcpp)
  - mpd client
    - [libraries](https://www.musicpd.org/libs/)
      - (Python) [python-mpd2](https://github.com/Mic92/python-mpd2/)
    - example programs
      - (C) [mpc](https://github.com/MusicPlayerDaemon/mpc)
      - (C++) [ncmpcpp](https://github.com/arybczak/ncmpcpp)
      - (C) [ncmpc](https://github.com/MusicPlayerDaemon/ncmpc)
- more:
  - [gotbletu's TUI programs youtube playlist](https://www.youtube.com/playlist?list=PL15B45A9D5A0000A5)

## Notes
