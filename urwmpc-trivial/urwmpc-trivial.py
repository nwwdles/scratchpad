#!/usr/bin/env python3

# IMPORTS
from mpd import MPDClient, CommandError, ConnectionError
from random import choice
from socket import error as SocketError
from sys import exit

import urwid

# SETTINGS
##
HOST = "localhost"
PORT = "6600"
PASSWORD = False
###


def exit_on_q(key):
    if key in ("q", "Q"):
        raise urwid.ExitMainLoop()


def menu(title, choices, client):
    body = [urwid.Text(title), urwid.Divider()]
    for c in choices:
        button = urwid.Button(c)
        urwid.connect_signal(button, "click", item_chosen, user_args=[c, client])
        body.append(urwid.AttrMap(button, None, focus_map="reversed"))
    return urwid.ListBox(urwid.SimpleFocusListWalker(body))


def item_chosen(choice, client, button):
    # what the hell is wrong with the argument order
    try:
        current = client.currentsong()
        if current and (current["file"] == choice):
            if client.status()["state"] != "play":
                client.play()
            else:
                client.pause()
        else:
            client.clear()
            client.add(choice)
            client.play()
    except ConnectionError as e:
        print(e)
        setupConnection(client)


def setupConnection(client):
    try:
        client.connect(host=HOST, port=PORT)
    except SocketError:
        exit(1)

    if PASSWORD:
        try:
            client.password(PASSWORD)
        except CommandError:
            exit(1)


if __name__ == "__main__":
    client = MPDClient(use_unicode=True)
    setupConnection(client)
    files = client.list("file")

    main = urwid.Padding(menu(u"Pythons", files, client), left=2, right=2)
    top = urwid.Overlay(
        main,
        urwid.SolidFill(u"\N{MEDIUM SHADE}"),
        align="center",
        width=("relative", 60),
        valign="middle",
        height=("relative", 60),
        min_width=20,
        min_height=9,
    )

    urwid.MainLoop(
        top, palette=[("reversed", "standout", "")], unhandled_input=exit_on_q
    ).run()
