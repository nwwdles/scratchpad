#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <cstdio>

#include <iostream>

#include <chrono>
#include <thread>

#include <array>
#include <iostream>
#include <memory>
#include <stdexcept>

std::string exec(const char *cmd)
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

Bool xerror = False;

Window get_focus_window(Display *d)
{
    Window w;
    int revert_to;
    printf("getting input focus window ... ");
    XGetInputFocus(d, &w, &revert_to); // see man
    if (xerror) {
        printf("fail\n");
        // abort();
    } else if (w == None) {
        printf("no focus window\n");
        // exit();
    } else {
        printf("success (window: %d)\n", (int)w);
    }

    return w;
}
std::string get_window_name(Display *d, Window w)
{
    XTextProperty prop;
    Status s;
    std::string name;
    // printf("window name:\n");

    s = XGetWMName(d, w, &prop); // see man

    if (!xerror && s) {
        int count = 0, result;
        char **list = nullptr;
        result = XmbTextPropertyToTextList(d, &prop, &list, &count); // see man
        if (result == Success) {
            if (list != nullptr) {
                // printf("\t%s\n", list[0]);
                name = list[0];
            }
        } else {
            // printf("ERROR: XmbTextPropertyToTextList\n");
        }
    } else {
        // printf("ERROR: XGetWMName\n");
    }
    return name;
}

void nag() { exec("notify-send \"Get back to work\""); }

void onFocusOut(Display *dpy, Window &w, const char *name)
{
    Window newW = get_focus_window(dpy);

    // if a window with FocusChangeMask was destroyed and a new focused window
    // is invalid, we will stop receiving events and the program won't work
    // anymore. so we look for a valid focused window until we find one.
    while (newW <= 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        newW = get_focus_window(dpy);
    }

    // window has changed
    if (newW != w) {
        std::string prevname = get_window_name(dpy, w);
        std::string curname = get_window_name(dpy, newW);
        std::cout << "changed " << prevname << "->" << curname << "\n";
        if (curname.find(name) == curname.npos) {
            if (prevname.find(name) != prevname.npos) {
                // switching from a match to a non-match
                std::cout << "get back to work!\n";
                nag();
            } else {
                // switching between non-matches
                nag();
            }
        } else {
            // switching to a match
            std::cout << "good boy!\n";
        }

        if (w > 1) {
            XSelectInput(dpy, w, NoEventMask);
        }
        XSelectInput(dpy, newW, FocusChangeMask);
        w = newW;
    }
}

int handle_error(Display *display, XErrorEvent *error)
{
    printf("ERROR: X11 error\n");
    // xerror = True;
    return 1;
}

int main(int argc, char const *argv[])
{
    if (argc < 2) {
        throw std::invalid_argument("no program name provided!");
    }
    char const *name = argv[1];
    std::cout << "Watching " << name << "\n";
    Display *dpy = XOpenDisplay(0);
    Window root = DefaultRootWindow(dpy);
    XSetErrorHandler(handle_error);

    Window w;
    w = get_focus_window(dpy);
    XSelectInput(dpy, w, FocusChangeMask);

    XEvent ev;
    while (true) {
        XNextEvent(dpy, &ev);
        switch (ev.type) {
        case FocusOut:
            onFocusOut(dpy, w, name);
            break;
        }
    }

    XCloseDisplay(dpy);
    return 0;
}
