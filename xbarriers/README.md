# X Corner Pointer Barrier

A program that adds pointer barriers to corners (aka sticky corners) in multimonitor setups. The feature that they hate on Windows.

![This is how it looks](https://i.stack.imgur.com/RxDz4.png)

## References

- https://www.uninformativ.de/git/xpointerbarrier/file/README.html
- https://who-t.blogspot.com/2012/12/whats-new-in-xi-23-pointer-barrier.html
- https://manpages.debian.org/testing/libx11-protocol-other-perl/X11::Protocol::Ext::XFIXES.3pm.en.html
