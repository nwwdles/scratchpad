
#include <X11/Xlib.h>
#include <X11/extensions/Xfixes.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    Display *dpy = XOpenDisplay(NULL);
    int fixes_opcode, fixes_event_base, fixes_error_base;
    PointerBarrier barrier;

    if (!XQueryExtension(dpy, "XFIXES", &fixes_opcode, &fixes_event_base,
                         &fixes_error_base))
        return EXIT_FAILURE;

    barrier =
        XFixesCreatePointerBarrier(dpy, DefaultRootWindow(dpy), 0, 200, 300,
                                   200, 0,   /* block in all directions */
                                   0, NULL); /* no per-device barriers */
    XFixesCreatePointerBarrier(dpy, DefaultRootWindow(dpy), 800, 200, 1920, 200,
                               0,        /* block in all directions */
                               0, NULL); /* no per-device barriers */
    while (true) {
        XEvent ev;
        XNextEvent(dpy, &ev);
    }
    return 0;
}
