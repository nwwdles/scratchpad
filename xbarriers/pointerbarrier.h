#ifndef POINTERBARRIER_H
#define POINTERBARRIER_H
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xrandr.h>
#include <iostream>

// expand barrier lengths so that pointer wouldn't leak in corners
const int LEAKWORKAROUND = 0;

struct Point {
    int x = 0;
    int y = 0;
    Point() {}
    Point(int x_, int y_) : x(x_), y(y_) {}
};

struct Line {
    Point a;
    Point b;
    Line(){};
    Line(Point a_, Point b_) : a(a_), b(b_) {}
    Line(int x, int y, int x2, int y2) : a(Point(x, y)), b(Point(x2, y2)) {}
};

class Barrier
{
    PointerBarrier barrier = 0;
    Display *display;

  public:
    Barrier(Display *dpy, Window root, Point a, Point b, int direction = 0,
            int num_devices = 0, int *devices = NULL)
        : display(dpy)
    {
        barrier = XFixesCreatePointerBarrier(dpy, root, a.x, a.y, b.x, b.y,
                                             direction, num_devices, devices);
        XSync(dpy, True);
    };
    Barrier(Display *dpy, Window root, int x1, int y1, int x2, int y2,
            int direction, int num_devices = 0, int *devices = NULL)
        : display(dpy)
    {
        barrier = XFixesCreatePointerBarrier(dpy, root, x1, y1, x2, y2,
                                             direction, num_devices, devices);
        XSync(dpy, True);
    };
    Barrier() {}
    ~Barrier()
    {
        if (barrier) {
            XFixesDestroyPointerBarrier(display, barrier);
        }
    }
};

class MonitorInfo
{
  public:
    int nmon;
    XRRMonitorInfo *moninf;
    MonitorInfo(Display *dpy, Window root, bool getActive = True)
    {
        moninf = XRRGetMonitors(dpy, root, getActive, &nmon);
    }
    ~MonitorInfo() { XRRFreeMonitors(moninf); }
};

int get_monitor_contact_line(XRRMonitorInfo &a, XRRMonitorInfo &b, Line &out)
{
    // two monitors can have one side in common. we find the contact line
    bool may_contact_vertically =
        (a.x <= b.x + b.width) && (b.x <= a.x + a.width);
    bool may_contact_horizontally =
        (a.y <= b.y + b.height) && (b.y <= a.y + a.height);
    int top = std::max(a.y, b.y) - LEAKWORKAROUND;
    int bottom = std::min(a.y + a.height, b.y + b.height) + LEAKWORKAROUND;
    int left = std::max(a.x, b.x) - LEAKWORKAROUND;
    int right = std::min(a.x + a.width, b.x + b.width) + LEAKWORKAROUND;
    if (a.x == b.x + b.width && may_contact_horizontally) {
        int x_contact = a.x;
        out = Line(x_contact, top, x_contact, bottom);
    } else if (a.y == b.y + b.height && may_contact_vertically) {
        int y_contact = a.y;
        out = Line(left, y_contact, right, y_contact);
    } else if (a.x + a.width == b.x && may_contact_horizontally) {
        int x_contact = a.x + a.width;
        out = Line(x_contact, top, x_contact, bottom);
    } else if (a.y + a.height == b.y && may_contact_vertically) {
        int y_contact = a.y + a.height;
        out = Line(left, y_contact, right, y_contact);
    } else {
        return 1;
    }
    return 0;
}

#endif // POINTERBARRIER_H
