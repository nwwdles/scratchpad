// The MIT License (MIT)
//
// Copyright (c) 2019 cupnoodles
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "pointerbarrier.h"
#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>
#include <cmath>
#include <iostream>
#include <vector>

std::vector<Barrier> createBarriers(Display *dpy, Window root, int barw,
                                    int barh)
{
    auto info = MonitorInfo(dpy, root);
    auto &moninf = info.moninf;
    auto &nmon = info.nmon;
    if (nmon <= 0 || moninf == nullptr) {
        throw std::runtime_error("No xranrd screens.\n");
    }

    std::vector<Barrier> barriers;

    for (int i = 0; i < nmon; i++) {
        for (int j = i + 1; j < nmon; j++) {
            Line contact;
            auto mon_a = moninf[i];
            auto mon_b = moninf[j];
            if (get_monitor_contact_line(mon_a, mon_b, contact) == 0) {
                if (contact.a.x == contact.b.x) {
                    barriers.emplace_back(
                        dpy, root, contact.a,
                        Point(contact.a.x, contact.a.y + barh));
                    barriers.emplace_back(
                        dpy, root, Point(contact.b.x, contact.b.y - barh),
                        contact.b);
                } else if (contact.a.y == contact.b.y) {
                    barriers.emplace_back(
                        dpy, root, contact.a,
                        Point(contact.a.x + barw, contact.a.y));
                    barriers.emplace_back(
                        dpy, root, Point(contact.b.x - barw, contact.b.y),
                        contact.b);
                }
            }
        }
    }
    return barriers;
}

int main(int argc, char const *argv[])
{
    int barw = 50;
    int barh = 50;

    if (argc == 3) {
        barw = atoi(argv[1]);
        barh = atoi(argv[2]);
    } else if (argc > 1) {
        throw std::invalid_argument("Wrong amount of arguments.");
    }

    Display *dpy = XOpenDisplay(NULL);
    int screen = DefaultScreen(dpy);
    Window root = RootWindow(dpy, screen);

    std::vector<Barrier> barriers = createBarriers(dpy, root, barw, barh);

    while (true) {
        XEvent ev;
        XNextEvent(dpy, &ev);
    }

    return 0;
}
