// The MIT License (MIT)
//
// Copyright (c) 2019 cupnoodles
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "pointerbarrier.h"
#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>
#include <iostream>
#include <thread>
#include <vector>

std::vector<Barrier> createBarriers(Display *dpy, Window root)
{
    auto info = MonitorInfo(dpy, root);
    auto &moninf = info.moninf;
    auto &nmon = info.nmon;
    if (nmon <= 0 || moninf == nullptr) {
        throw std::runtime_error("No xranrd screens.\n");
    }
    std::vector<Barrier> barriers;

    int blockhor = 0;  // BarrierPositiveY | BarrierNegativeY;
    int blockvert = 0; // BarrierPositiveX | BarrierNegativeX;

    for (int i = 0; i < nmon; i++) {
        for (int j = i + 1; j < nmon; j++) {
            Line contact;
            if (get_monitor_contact_line(moninf[i], moninf[j], contact) == 0) {
                int dir;
                if (contact.a.x == contact.b.x) {
                    dir = blockhor;
                } else {
                    dir = blockvert;
                }
                auto b = Barrier(dpy, root, contact.a, contact.b, dir);
                barriers.push_back(std::move(b));
            }
        }
    }

    return barriers;
}

int erhandler(Display *dpy, XErrorEvent *er)
{
    char ertext[64];
    XGetErrorText(dpy, er->error_code, ertext, 64);
    std::cout << ertext << "\n";
    return 0;
}

int main(int argc, char const *argv[])
{
    // config
    Time reactivationDelay = 10;
    Time delay = 50;
    Time activationDelay = 10;
    // TODO: set from args

    // setup x
    Display *dpy = XOpenDisplay(NULL);
    int screen = DefaultScreen(dpy);
    Window root = RootWindow(dpy, screen);
    XSetErrorHandler(erhandler);

    // setup event mask for barrier hit/leave
    unsigned char m[XIMaskLen(XI_BarrierLeave)] = {0};
    XIEventMask mask;
    mask.deviceid = XIAllMasterDevices;
    mask.mask_len = XIMaskLen(XI_BarrierLeave);
    mask.mask = m;
    XISetMask(mask.mask, XI_BarrierHit);
    XISetMask(mask.mask, XI_BarrierLeave);
    XISelectEvents(dpy, DefaultRootWindow(dpy), &mask, 1);

    // we'll be creating barriers when the user starts moving a window
    // so we listen to window move events.
    XSelectInput(dpy, DefaultRootWindow(dpy),
                 StructureNotifyMask | SubstructureNotifyMask);
    XSync(dpy, False);

    Time hittime = 0;
    std::vector<Barrier> barriers;

    while (true) {
        XEvent ev;
        std::cout << barriers.size() << '\n';
        XNextEvent(dpy, &ev);
        std::cout << ev.type << '\n';
        switch (ev.type) {
        case ConfigureNotify:
            // on window resize/move
            Window c;
            Window r;
            int rx, ry, wx, wy;
            unsigned mask_return;
            std::this_thread::sleep_for(
                std::chrono::milliseconds(activationDelay));
            // XSync(dpy, true);
            XQueryPointer(dpy, root, &r, &c, &rx, &ry, &wx, &wy, &mask_return);
            if (mask_return & Button1Mask) {
                if (barriers.size() == 0) {
                    barriers = createBarriers(dpy, root);
                }
            } else {
                std::cout << "clear" << '\n';
                barriers.clear();
                // XSync(dpy, true);
            }
            break;
        case GenericEvent: {
            std::cout << "ge" << '\n';
            XGetEventData(dpy, &ev.xcookie);
            XIBarrierEvent *b = static_cast<XIBarrierEvent *>(ev.xcookie.data);
            if (b->evtype == XI_BarrierHit) {
                if (hittime == 0) {
                    hittime = b->time;
                } else if (b->time - hittime > delay) {
                    std::cout << "hit" << '\n';
                    hittime = 0;
                    // barriers.clear();
                    XIBarrierReleasePointer(dpy, 0, b->barrier, b->eventid);
                    // XSync(dpy, True);
                    std::this_thread::sleep_for(
                        std::chrono::milliseconds(reactivationDelay));
                }
            } else if (b->evtype == XI_BarrierLeave) {
                // User stops touching the barrier, reset the timer
                hittime = 0;
                // XSync(dpy, True);
            }
            break;
        }
        default:
            break;
        }
        XFreeEventData(dpy, &ev.xcookie);
    }

    return 0;
}
