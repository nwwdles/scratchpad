# xhkey ⌨

A program that binds commands to keys️.

Example config (something like sxhkd format):

```bash
# this is a parallel mapping
super+{B,C}
notify-send "You pressed: {B,C}!"

# this is a button release mapping
@super+{B,C}
notify-send "You released: {B,C}!"

# this is a sequence mapping
super+X;super+Z
notify-send "You pressed X and then Z!"

# release works when you release the last button in the sequence
@super+M;super+Z
notify-send "You pressed M and then released Z!"

# parallel + sequence
super{+shift,_}+N;super+Z
notify-send "You pressed super{+shift,}+N, then super+Z!"

# parallel + sequence + release
@super{+shift,_}+Z;super+Z
notify-send "You pressed super{+shift,}+Z, then released super+Z!"
```

## TODO

- [x] Reasonable behavior with CapsLock and NumLock modifiers
  - ~~currently bindings work only with Caps Lock and Num Lock disabled~~
  - [x] provide `capslock+`, `numlock+` and `capslock+numlock+` bindings by default if user doesn't specify any of them in a binding
    - in this case don't support switching num/caps midway through a sequence (user can still make a keybinding that specifically wants a caps/num in a part of sequence, e.g. `capslock+super+k,super+k`. in this case all parts are caps/num-sensitive)
- [x] support empty parallel mappings
- [x] support key release actions defined in config
- [x] move wacom zoom into a separate program because i had a change of mind
- [ ] see how i can make it a _daemon_ (whatever that means) like sxhkd?
  - https://stackoverflow.com/questions/17954432/creating-a-daemon-in-linux/17955149#17955149
  - http://shahmirj.com/blog/beginners-guide-to-creating-a-daemon-in-linux
- [ ] hotkey tree based on `map` instead of `vector` would be more convenient
  - we wouldn't need a separate map for the currently grabbed bindings, it would be a pointer
  - why did i base it on `vector`? 🤔
- [ ] break up `hotkeyManager` class into smaller parts?
- [ ] change config folder to `~/.config/...`
- [ ] ability to bind a command to a part of a sequence (with or w/o a timer) would be nice too
