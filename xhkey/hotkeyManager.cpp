#include "hotkeyManager.h"
#include <fstream>
#include <set>
#include <stdexcept>
#include <string>

namespace HK
{

std::stringstream readConfig(const char *filepath)
{
    std::ifstream fin(filepath);
    if (!fin.is_open()) {
        throw std::invalid_argument("No config file provided!");
    }
    std::stringstream ss;
    if (fin) {
        ss << fin.rdbuf();
    }
    fin.close();
    return ss;
}

unsigned parseMod(std::string btn)
{
    if (btn == "super" || btn == "Super" || btn == "mod4") {
        return Mod4Mask;
    } else if (btn == "shift" || btn == "Shift") {
        return ShiftMask;
    } else if (btn == "numlock" || btn == "NumLock") {
        return Mod2Mask;
    } else if (btn == "mod3") {
        return Mod3Mask;
    } else if (btn == "ctrl" || btn == "Ctrl") {
        return ControlMask;
    } else if (btn == "alt" || btn == "Alt") {
        return Mod1Mask;
    } else if (btn == "capslock" || btn == "CapsLock") {
        return LockMask;
    } else if (btn == "_") {
        return AnyModifier;
    }
    return 0;
}

// parses binding and stores result in keysym and modifiers variables
int parseBinding(std::string &binding, int &keysym, unsigned &modifiers)
{
    auto btns = splitString(binding, '+');
    for (auto &btn : btns) {
        trim(btn);
        auto mod = parseMod(btn);
        modifiers |= mod;
        // std::cout << btn.c_str() << "\n";
        if (mod == 0) {
            // if it isn't a modifier it should be a normal key
            // find its keysym
            keysym = XStringToKeysym(btn.c_str());
            // skip the rest since only one normal key
            // is allowed in a binding
            break;
        }
    }
    // if no modifiers, we mark it as AnyMod
    if (modifiers == 0)
        modifiers = AnyModifier;
    // modifiers |= Mod2Mask;
    // modifiers |= LockMask;
    return (keysym <= 0); // return 1 if keysym couldn't be parsed
}

void HotkeyManager::bindSequence(const std::string &sequence,
                                 const std::string &cmd, bool onRelease,
                                 unsigned mod = 0)
{
    auto *parentNode = &getHotkeyRoot();

    // separate bindings by layers into a vector
    // (example: super+K; super+L)
    auto layers = splitString(sequence, ';');

    for (int i = 0; i < layers.size(); ++i) {
        auto &binding = layers[i];

        // parse modifiers and key
        int keysym = 0;
        unsigned modifiers = 0;
        if (parseBinding(binding, keysym, modifiers)) {
            // keysym couldn't be parsed correctly
            // probably need to throw an error,
            break;
        }

        // auto apply numlock/capslock modifiers if needed
        modifiers |= mod;

        // if it's the last binding in a sequence, bind the command
        // otherwise bind empty mapping
        Hotkey hk;
        if (i >= layers.size() - 1) {
            std::cout << binding << "\n";
            std::cout << cmd << "\n";
            hk = Hotkey(keysym, modifiers, cmd, onRelease);
        } else {
            std::cout << binding << "; ";
            hk = Hotkey(keysym, modifiers);
        }
        // add hotkey and remember it's location
        // for the next hierarchical layer (if it's a sequence)
        parentNode = &addHotkey(*parentNode, std::move(hk));
    }
}

bool usesLock(const std::string &sequence)
{
    // TODO: make sure it doesn't conflict with numpad keys etc
    auto words = {"capslock", "CapsLock", "NumLock", "numlock"};
    for (auto &word : words) {
        if (sequence.find(word) != sequence.npos)
            return true;
    }
    return false;
}

// expands raw binding and commands into lists of bindings and commands
// and binds them to hotkeys
void HotkeyManager::bindRaw(std::string &rawBinding, std::string &rawCommand)
{
    // remove onRelease indicator ('@') from the raw binding
    auto onRelease = false;
    if (rawBinding[0] == '@') {
        onRelease = true;
        rawBinding.erase(0, 1);
        trim(rawBinding);
        // std::cout << "onrelease!\n";
    }

    // expand parallel mappings
    auto sequences = expandString(rawBinding);
    auto cmds = expandString(rawCommand);

    // make sure that all binding have commands
    if (sequences.size() != cmds.size()) {
        std::string errorstr = "the amount of hotkeys doesn't match "
                               "the amount of commands: " +
                               rawBinding;
        throw std::invalid_argument(errorstr.c_str());
    }

    // bind commands
    for (int i = 0; i < sequences.size(); ++i) {
        auto sequence = sequences[i];
        // if user doesn't use capslock/numlock, make binding work with any
        // combination of them
        bool hasLock = usesLock(sequence);
        if (!hasLock) {
            bindSequence(sequence, cmds[i], onRelease, LockMask);
            bindSequence(sequence, cmds[i], onRelease, Mod2Mask);
            bindSequence(sequence, cmds[i], onRelease, Mod2Mask | LockMask);
        }
        bindSequence(sequence, cmds[i], onRelease);
    }
}

// extracts rawbindings and rawcommands from an istream and binds them
void HotkeyManager::parseConfig(std::istream &is)
{
    while (is) {
        // first non-blank non-comment line is a binding
        std::string rawBinding;
        do {
            getline(is, rawBinding);
            trim(rawBinding);
            if (rawBinding[0] == '#') // ignore comment lines
                rawBinding = "";
        } while (rawBinding.size() == 0 && is);

        // next first non-blank non-comment line is a command
        std::string rawCommand;
        do {
            getline(is, rawCommand);
            trim(rawCommand);
            if (rawBinding[0] == '#') // ignore comment lines
                rawBinding = "";
        } while (rawCommand.size() == 0 && is);

        // we may have reached the end of the stream and have empty command etc
        // just make sure that everything looks fine
        if (rawBinding.size() == 0 || rawCommand.size() == 0)
            continue;
        // std::cout << rawBinding << "\n";
        // std::cout << rawCommand << "\n";
        bindRaw(rawBinding, rawCommand);
    }
}

void HotkeyManager::ungrabKeys()
{
    // ungrab each key and clear lookup map
    for (const auto &binding : currentBindings) {
        auto hotkey = binding.second->data;
        int keycode = XKeysymToKeycode(dpy, hotkey.keysym);
        XUngrabKey(dpy, keycode, hotkey.modifiers, grab_window);
    }
    currentBindings.clear();
};

void HotkeyManager::grabKeys(std::vector<HotkeyNode> &bindings)
{
    ungrabKeys();
    for (auto &hotkeyNode : bindings) {
        auto hotkey = hotkeyNode.data;
        Bool owner_events = False;
        int pointer_mode = GrabModeAsync;
        int keyboard_mode = GrabModeAsync;
        int keycode = XKeysymToKeycode(dpy, hotkey.keysym);
        // std::cout << "keycode:" << keycode << "\n";
        currentBindings.insert(std::make_pair(
            std::make_pair(keycode, hotkey.modifiers), &hotkeyNode));
        XGrabKey(dpy, keycode, hotkey.modifiers, grab_window, owner_events,
                 pointer_mode, keyboard_mode);
        // hotkeyNode.printSelf();
    }
};

std::vector<HotkeyNode> &
HotkeyManager::addHotkey(std::vector<HotkeyNode> &bindings, Hotkey &&hotkey)
{
    // try to merge hotkeys with the same keysym and modifier
    auto pos =
        std::find_if(bindings.begin(), bindings.end(), [hotkey](auto &node) {
            return (node.data.keysym == hotkey.keysym &&
                    node.data.modifiers == hotkey.modifiers);
        });
    if (pos < bindings.end()) {
        pos->data.mergeWith(hotkey);
        return pos->children;
    } else {
        // put new hotkey in the vector tree and return a reference to
        // the vector inside of the new node
        bindings.push_back({hotkey, {}});
        // std::cout << " keysym:" << keysym << " mod:" << modif << "\n";
        // hotkeys.printSelf();
        return ((--bindings.end())->children);
    }
};

std::vector<HotkeyNode> &
HotkeyManager::addHotkey(std::vector<HotkeyNode> &bindings, int keysym,
                         unsigned modif, std::function<void()> &&press,
                         std::function<void()> &&release)
{
    // put new hotkey in the vector tree and return a reference to the vector
    // inside of the new node
    bindings.push_back({Hotkey(keysym, modif, press, release), {}});
    // std::cout << " keysym:" << keysym << " mod:" << modif << "\n";
    // hotkeys.printSelf();
    return ((--bindings.end())->children);
};

void printEvent(XEvent &ev)
{
    std::cout << "t" << ev.xkey.time << " m:" << ev.xkey.state
              << " k:" << ev.xkey.keycode << "\n";
}
bool isModifierCode(int keycode)
{
    switch (keycode) {
    case 37:  // control_l
    case 50:  // shift_l
    case 62:  // shift_r
    case 64:  // alt_l
    case 66:  // caps_lock
    case 77:  // num_lock
    case 78:  // scroll_lock
    case 105: // control_r
    case 108: // alt_r
    case 133: // super_l
    case 134: // super_r
        return true;
        break;
    }
    return false;
}

void HotkeyManager::checkKeys()
{
    grabKeys(getHotkeyRoot());

    XEvent ev;
    auto hk = currentBindings.end();
    unsigned mod;
    KeyModPair keyModPair;

    // workaround to skip autorepeat events when we can't find
    // the next event in the queue (empty queue).
    // we store the time of the last event in this variable.
    int skiptime = 0;

    // and since we can't distinguish between a normal empty queue and
    // false empty queue we need to query the keyboard, this array holds the
    // query result
    char keys[32] = {0};

    std::set<KeyModPair> pressedBtns;

    while (true) {

        XNextEvent(dpy, &ev);
        if (XFilterEvent(&ev, None) == True) {
            std::cout << "now whats  this!\n";
            XUngrabKeyboard(dpy, CurrentTime);
            continue;
        }

        switch (ev.type) {
        case KeyPress:
            // std::cout << "prs:";
            // printEvent(ev);

            // if previous release event had the same time as this event
            // we skip it since it was (probably) autorepeating
            if (ev.xkey.time == skiptime) {
                skiptime = 0;
                break;
            }
            mod = (ev.xkey.state > 0 ? ev.xkey.state : AnyModifier);
            keyModPair = std::make_pair(
                ev.xkey.keycode,
                (ev.xkey.state > 0 ? ev.xkey.state : AnyModifier));

            // this relies on XkbSetDetectableAutoRepeat and duplicates the
            // autorepeat checking functionality
            if (pressedBtns.find(keyModPair) == pressedBtns.end()) {
                pressedBtns.insert(keyModPair);
            } else {
                break;
            }

            hk = currentBindings.find(keyModPair);
            if (hk != currentBindings.end()) {
                // (with no conditional would react to any released key)
                // std::cout << "Hot key pressed!" << std::endl;
                Hotkey &hotkey = hk->second->data;
                hotkey.pressed();

            } else {
                // resetting state
                auto code = ev.xkey.keycode;
                if (isModifierCode(code))
                    continue;
                std::cout << "You pressed some unrelated key! " << code
                          << std::endl;
                grabKeys(getHotkeyRoot());
                // auto sym = XKeycodeToKeysym(dpy, code, 0);
                // std::cout << XKeysymToString(sym) << "\n";
                XUngrabKeyboard(dpy, CurrentTime);
                layer = 0;
            }
            break;

        case KeyRelease:
            // std::cout << "rel:";
            // printEvent(ev);
            skiptime = 0;

            // check if the key is autorepeating
            if (XEventsQueued(dpy, QueuedAfterReading)) {
                XEvent nev;
                XPeekEvent(dpy, &nev);
                // std::cout << "next:";
                printEvent(nev);
                if ((nev.type == KeyPress) && nev.xkey.time == ev.xkey.time &&
                    nev.xkey.keycode == ev.xkey.keycode) {
                    /* Key wasn’t actually released, eat the next event */
                    printEvent(nev);

                    XNextEvent(dpy, &ev);
                    break;
                }
            } else {
                // std::cout << "empty\n";
                // queue is empty but the user may still be pressing the key
                // check if the key is actually pressed. and if it is, skip the
                // press event (it will have the same xkey.time as this event)
                XQueryKeymap(dpy, keys);
                if (keys[ev.xkey.keycode / 8] &
                    (0x1 << (ev.xkey.keycode % 8))) {
                    // https://community.khronos.org/t/key-is-down-for-linux/56946/5
                    skiptime = ev.xkey.time;
                    break;
                }
            }

            mod = (ev.xkey.state > 0 ? ev.xkey.state : AnyModifier);
            keyModPair = std::make_pair(ev.xkey.keycode, mod);

            // this relies on XkbSetDetectableAutoRepeat
            if (pressedBtns.find(keyModPair) != pressedBtns.end()) {
                pressedBtns.erase(keyModPair);
            } else {
                break;
            }

            hk = currentBindings.find(keyModPair);
            if (hk != currentBindings.end()) {
                // (with no conditional would react to any released key)
                // std::cout << "Hot key released!" << std::endl;
                hk->second->data.released();
                if (hk->second->children.size() > 0) {
                    // grab the next layer of hotkeys (release event won't be
                    // fired though)
                    // std::cout << "And there's more!" << std::endl;
                    grabKeys(hk->second->children);
                    XGrabKeyboard(dpy, grab_window, True, GrabModeAsync,
                                  GrabModeAsync, CurrentTime);
                    // grab the keyboard so that we know when when the user
                    // presses some unrelated key, then we reset the state
                    layer++;
                } else if (layer != 0) {
                    // reset state if no next level
                    // std::cout << "Reset to root!" << std::endl;
                    layer = 0;
                    grabKeys(getHotkeyRoot());
                    XUngrabKeyboard(dpy, CurrentTime);
                }
            }

            break;
        default:
            break;
        }
    }
};
} // namespace HK