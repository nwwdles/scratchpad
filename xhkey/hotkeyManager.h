#ifndef HOTKEYMANAGER_H_
#define HOTKEYMANAGER_H_

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <functional>
#include <iostream>
#include <map>

#include "tree.h"
#include "utils.h"

namespace HK
{

struct Hotkey;

typedef TreeNode<Hotkey> HotkeyNode;
typedef std::pair<int, unsigned> KeyModPair;

std::stringstream readConfig(const char *filepath);
unsigned parseMod(std::string btn);
void printEvent(XEvent &ev);

struct Hotkey {
    std::function<void()> onPress;
    std::function<void()> onRelease;
    int keysym;
    unsigned modifiers;
    bool runOnRelease;     // only matters when cmdString is used
    std::string cmdString; // holds a description or a command string if lambdas
                           // aren't bound

    Hotkey(int sym = 0, unsigned mods = 0,
           std::function<void()> press = nullptr,
           std::function<void()> release = nullptr)
        : onPress(press), onRelease(release), keysym(sym), modifiers(mods),
          runOnRelease(false)
    {
    }
    Hotkey(int sym, unsigned mods, const std::string &cmd, bool rel = false)
        : onRelease(nullptr), keysym(sym), modifiers(mods), cmdString(cmd),
          runOnRelease(rel)
    {
        auto fun = [cmd]() { exec(cmd.c_str()); };
        if (runOnRelease)
            onRelease = fun;
        else
            onPress = fun;
    }
    void mergeWith(Hotkey &rhs)
    {
        if (rhs.runOnRelease && rhs.onRelease)
            onRelease = std::move(rhs.onRelease);
        else if (rhs.onPress)
            onPress = std::move(rhs.onPress);
    }
    void pressed() const
    {
        if (onPress)
            onPress();
    }
    void released() const
    {
        if (onRelease)
            onRelease();
    }
    friend std::ostream &operator<<(std::ostream &os, const Hotkey &hk)
    {
        os << hk.keysym << " " << XKeysymToString(hk.keysym) << " "
           << hk.cmdString;
        return os;
    }
};

class HotkeyManager
{
  private:
    Display *dpy;
    Window grab_window;
    Tree<Hotkey> hotkeys; // holds the whole config
    std::map<KeyModPair, HotkeyNode *>
        currentBindings; // holds the current layer of bindings (we support
                         // multikey shortcuts like [ctrl+k;ctrl+z])

    int layer; // current layer of a multikey binding

    void grabKeys(std::vector<HotkeyNode> &newLayer);
    void ungrabKeys();
    void bindSequence(const std::string &sequence, const std::string &cmd,
                      bool onRelease, unsigned mod);
    void bindRaw(std::string &rawBinding, std::string &rawCommand);

  public:
    HotkeyManager(Display *display, Window window)
        : dpy(display), grab_window(window), layer(0){};
    ~HotkeyManager() { ungrabKeys(); }

    std::vector<HotkeyNode> &getHotkeyRoot() { return hotkeys.root; };
    void checkKeys();
    void parseConfig(std::istream &is);

    std::vector<HotkeyNode> &addHotkey(std::vector<HotkeyNode> &bindings,
                                       Hotkey &&hotkey);
    std::vector<HotkeyNode> &addHotkey(std::vector<HotkeyNode> &layer,
                                       int keysym, unsigned modif,
                                       std::function<void()> &&press,
                                       std::function<void()> &&release);
};

} // namespace HK
#endif // HOTKEYMANAGER_H_
