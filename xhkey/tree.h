#ifndef TREE_H_
#define TREE_H_

#include <iostream>
#include <string>
#include <vector>

template <typename T> struct TreeNode {
    T data;
    std::vector<TreeNode> children;

    std::vector<TreeNode> &operator[](int idx) { return children[idx]; };
    std::vector<TreeNode> &operator[](int idx) const { return children[idx]; };
    void printSelf() const { printNode(*this, 0); }
    void printNode(const TreeNode &n, int lvl) const
    {
        std::cout << std::string(" ", lvl) << n.data << "\n";
        for (const auto &child : n.children) {
            printNode(child, lvl + 1);
        }
    }
};

/*
 * Tree just holds a vector of TreeNodes
 * It's probably actually a forest. A tree would hold just a root node.
 */
template <typename T> struct Tree {
    std::vector<TreeNode<T>> root;

    Tree(){};
    Tree(const std::initializer_list<TreeNode<T>> &list)
    {
        for (const auto &var : list) {
            root.push_back(var);
        }
    };

    TreeNode<T> &operator[](int idx) { return root[idx]; };
    TreeNode<T> &operator[](int idx) const { return root[idx]; };

    void printSelf() const
    {
        for (int i = 0; i < root.size(); i++) {
            root[i].printSelf();
        }
    }
};

#endif // TREE_H_
