#include "utils.h"
#include <array>
#include <memory>
#include <string>

#include <iostream>

std::string exec(const char *cmd)
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

std::vector<std::string> splitString(const std::string &s, char delim)
{
    std::vector<std::string> result;
    std::stringstream ss{s};
    std::string temp;

    while (true) {
        std::getline(ss, temp, delim);
        if (!ss)
            break;
        result.push_back(temp);
    }
    ss.clear();
    auto lastchar = ss.str()[ss.str().size() - 1];
    if (lastchar == delim) {
        result.push_back("");
    }
    return result;
}

std::vector<std::string> cartesianConcat(const std::vector<std::string> &a,
                                         const std::vector<std::string> &b)
{
    const auto a_size = a.size();
    const auto b_size = b.size();
    const auto newsize = a_size * b_size;
    std::vector<std::string> out(newsize);

    for (int i = 0; i < newsize; ++i) {
        out[i] = a[i / b_size] + b[i % b_size];
    }
    return out;
}

std::vector<std::string> expandString(const std::string &s)
{
    std::vector<std::vector<std::string>> vars;
    std::vector<std::string> stringparts;

    // extract parts
    std::string part;
    auto lastpos = 0;
    while (true) {
        auto start = s.find_first_of("{", lastpos);
        if (start == std::string::npos) {
            break;
        }
        auto end = s.find_first_of("}", start);
        if (end == std::string::npos) {
            break;
        }

        auto substr = s.substr(start + 1, end - start - 1);
        trim(substr);

        auto pos = substr.find('_');
        if (pos != substr.npos)
            substr.replace(pos, 1, "");
        // std::cout << substr << "\n";
        vars.push_back(splitString(substr, ','));

        trim(part);
        part = s.substr(lastpos, start - lastpos);
        stringparts.push_back(part);

        lastpos = end + 1;
    }
    if (lastpos == 0) {
        // std::cout << s << "\n";
        return std::vector<std::string>{s};
    }
    part = s.substr(lastpos, s.size() - lastpos);
    stringparts.push_back(part);

    // merge parts and vars
    for (int i = 0; i < vars.size(); ++i) {
        for (int j = 0; j < vars[i].size(); ++j) {
            vars[i][j] = stringparts[i] + vars[i][j];
        }
    }
    // and merge the last part to the end
    auto &endvars = vars[vars.size() - 1];
    for (int i = 0; i < endvars.size(); ++i) {
        endvars[i] = endvars[i] + *(--stringparts.end());
    }

    // concatenate all neighbouring vectors in all possible variations until
    // only vector one is left
    auto len = vars.size();
    for (int i = 1; i < len; ++i) {
        vars[len - i - 1] = cartesianConcat(vars[len - i - 1], vars[len - i]);
        vars.pop_back();
    }

    // for (auto var : vars[0]) {
    //     std::cout << var << "\n";
    // }

    return vars[0];
}