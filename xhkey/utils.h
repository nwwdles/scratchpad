#ifndef UTILS_H_
#define UTILS_H_

#include <algorithm>
#include <sstream>
#include <string>
#include <vector>

std::string exec(const char *cmd);

/*
 * Expand "a{Z,X}b" into ["aZb","aXb"]
 *
 * */
std::vector<std::string> expandString(const std::string &s);

/*
 * concatenate
 * {{A,B}, {C,D,E}}
 * into
 * {AC, AD, AE, BC, BD, BE}
 */
std::vector<std::string> cartesianConcat(const std::vector<std::string> &a,
                                         const std::vector<std::string> &b);

std::vector<std::string> splitString(const std::string &s, char delim);

// Evan Teran @
// https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring/216883
static inline void ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return !std::isspace(ch); }));
}
static inline void rtrim(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return !std::isspace(ch); })
                .base(),
            s.end());
}
static inline void trim(std::string &s)
{
    ltrim(s);
    rtrim(s);
}

#endif // UTILS_H_
