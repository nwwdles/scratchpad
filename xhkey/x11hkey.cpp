#include "hotkeyManager.h"
#include "utils.h"

#include <sstream>
#include <stdexcept>

#include <X11/XKBlib.h>

/*
 * error handler ignores every error and makes my program not crash
 *
 * */
int erhandler(Display *dpy, XErrorEvent *er)
{
    char ertext[64];
    XGetErrorText(dpy, er->error_code, ertext, 64);
    std::cout << ertext << "\n";
    return 0;
}

int main(int argc, const char **argv)
{
    // read config
    const char *defaultFile = "x11hkeys.txt";
    const char *confFilename;
    if (argc > 1) {
        confFilename = argv[1];
    } else {
        confFilename = defaultFile;
    }
    std::cout << "config: " << confFilename << "\n";
    auto ss = HK::readConfig(confFilename);

    // init x11
    Display *dpy = XOpenDisplay(0);
    Window root = DefaultRootWindow(dpy);
    XSetErrorHandler(erhandler);
    XSelectInput(dpy, root, KeyPressMask | KeyReleaseMask);
    int supported = 0;
    XkbSetDetectableAutoRepeat(dpy, True, &supported);

    // init hotkey manager
    HK::HotkeyManager hm(dpy, root);
    hm.parseConfig(ss);

    // example hotkey with lambdas
    // hm.addHotkey(
    //     hm.getHotkeyRoot(), XK_less, AnyModifier,
    //     [dpy, root, &dev]() { zoomDevice(dpy, root, dev, 0.5); },
    //     [&dev]() { dev.resetZoom(); });

    // check keys until the user kills the program
    hm.checkKeys();

    XCloseDisplay(dpy);
    return 0;
}
