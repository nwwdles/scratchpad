# XMakePassthrough

A program that makes a focused window pass clicks through, always on top and transparent.
If a window is already on top, it reverts things back.

```txt
usage: xmakepassthrough alpha

positional arguments:
  alpha         [0-1] how transparent should window be (default: 0.8)

xmakepassthrough reset - reset all windows
```

## TODO:

- [ ] make arguments pretty
- [x] ignore special windows like Desktop
- [ ] only reset windows that we made passthrough instead of all windows that are marked as always on top
- [ ] raise windows on reset?
  - doesn't work on kwin, just makes it demand attention (orange icon in taskbar)

## Materials

- [wm hints](https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html#idm140130317612768)
