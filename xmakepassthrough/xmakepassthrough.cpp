#include <X11/Xatom.h>
#include <X11/Xlib.h>

#include <X11/extensions/Xfixes.h>
#include <X11/extensions/shape.h>

#include <cstring>
#include <iostream>

int getWindowDesktop(Display *dpy, Window win)
{
    int desktop = -1;
    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long items;
    unsigned long bytes;
    unsigned char *data;
    Atom desk_property = XInternAtom(dpy, "_NET_WM_DESKTOP", True);
    if (XGetWindowProperty(dpy, win, desk_property, 0L, //
                           1L,                          //
                           False,                       //
                           AnyPropertyType,             //
                           &actual_type_ret, &actual_format_ret, &items, &bytes,
                           &data) == Success) {
        if (items) {
            unsigned long *return_words =
                reinterpret_cast<unsigned long *>(data);
            desktop = return_words[0];
        }
        XFree(data);
    }
    return desktop;
}

// https://stackoverflow.com/questions/49485867/qt-is-there-a-way-to-check-if-the-window-is-marked-always-on-top-linux
bool getWindowOnTop(Display *dpy, Window win)
{
    bool above = false;

    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long items;
    unsigned long bytes;
    unsigned char *data;
    Atom property = XInternAtom(dpy, "_NET_WM_STATE", True);
    if (XGetWindowProperty(dpy, win, property, 0L, //
                           64L,                    //
                           False,                  //
                           AnyPropertyType,        //
                           &actual_type_ret, &actual_format_ret, &items, &bytes,
                           &data) == Success) {
        unsigned long *return_words = reinterpret_cast<unsigned long *>(data);
        Atom above_prop = XInternAtom(dpy, "_NET_WM_STATE_ABOVE", False);
        for (unsigned long item = 0; item < items; item++) {
            if (return_words[item] == above_prop) {
                above = true;
                break;
            }
        }
        XFree(data);
    }
    return above;
}

// https://stackoverflow.com/questions/1014822/how-to-know-which-window-has-focus-and-how-to-change-it
Window getFocusedWindow(Display *dpy)
{
    Window focused;
    int revert_to;
    XGetInputFocus(dpy, &focused, &revert_to);
    return focused;
};

void setWindowInputPassthrough(Display *dpy, Window win, bool ignore)
{
    if (ignore) {
        // make it passthrough
        XRectangle rect;
        XserverRegion region = XFixesCreateRegion(dpy, &rect, 0);
        XFixesSetWindowShapeRegion(dpy, win, ShapeInput, 0, 0, region);
        XFixesDestroyRegion(dpy, region);
    } else {
        // revert back
        XFixesSetWindowShapeRegion(dpy, win, ShapeInput, 0, 0, None);
    }
}

// https://gist.github.com/woodruffw/16af23d886e62fbb1526
/*	set_window_front
    On EWMH-compliant window managers, sets the _NET_WM_STATE of the given
    window to _NET_WM_STATE_ABOVE to make it stay on top of all others besides
    _NET_WM_STATE_FULLSCREEN. If the window manager is not EWMH-compliant,
    nothing is done.
    Arguments:
    Display *disp - a pointer to the X display
    Window wind - the window being moved to the front
*/
void setWindowOnTop(Display *dpy, Window wind, bool ontop)
{
    Atom wm_state, wm_state_above;
    XEvent event;

    if ((wm_state = XInternAtom(dpy, "_NET_WM_STATE", False)) != None) {
        if ((wm_state_above = XInternAtom(dpy, "_NET_WM_STATE_ABOVE", False)) !=
            None) {
            event.xclient.type = ClientMessage;
            event.xclient.serial = 0;
            event.xclient.send_event = True;
            event.xclient.display = dpy;
            event.xclient.window = wind;
            event.xclient.message_type = wm_state;
            event.xclient.format = 32;
            event.xclient.data.l[0] = 1 && ontop;
            event.xclient.data.l[1] = wm_state_above;

            /* unused */
            event.xclient.data.l[2] = 0;
            event.xclient.data.l[3] = 0;
            event.xclient.data.l[4] = 0;

            /* actually send the event */
            XSendEvent(dpy, DefaultRootWindow(dpy), False,
                       SubstructureRedirectMask | SubstructureNotifyMask,
                       &event);
        }
    }
}

/*
    Sets the transparency of the toplevel window.
    An alpha value of 1 draws the window opaque a value of 0 makes
    it totally translucent.
    https://bbs.archlinux.org/viewtopic.php?id=9934
    https://gist.github.com/je-so/901523
*/
int setWindowAlpha(Display *dpy, Window win, double alpha)
{
    if (alpha < 0 || alpha > 1) {
        return 1;
    }

    uint32_t cardinal_alpha = static_cast<uint32_t>(alpha * (uint32_t)-1);
    if (cardinal_alpha == (uint32_t)-1) {
        XDeleteProperty(dpy, win,
                        XInternAtom(dpy, "_NET_WM_WINDOW_OPACITY", 0));
    } else {
        XChangeProperty(dpy, win, XInternAtom(dpy, "_NET_WM_WINDOW_OPACITY", 0),
                        XA_CARDINAL, 32, PropModeReplace,
                        (uint8_t *)&cardinal_alpha, 1);
    }

    return 0;
}

// https://stackoverflow.com/questions/1201179/how-to-identify-top-level-x11-windows-using-xlib
void resetAllWindows(Display *dpy, Window root)
{
    Atom a = XInternAtom(dpy, "_NET_CLIENT_LIST", true);
    Atom actualType;
    int format;
    unsigned long numItems, bytesAfter;
    unsigned char *data = 0;
    int status =
        XGetWindowProperty(dpy, root, a, 0L, (~0L), false, AnyPropertyType,
                           &actualType, &format, &numItems, &bytesAfter, &data);

    if (status >= Success && numItems) {
        // success - we have data: Format should always be 32:
        // static_assert(format == 32);
        // cast to proper format, and iterate through values:
        unsigned long *array = reinterpret_cast<unsigned long *>(data);
        for (unsigned long k = 0; k < numItems; k++) {
            // get window Id:
            Window win = (Window)array[k];
            if (getWindowOnTop(dpy, win) && getWindowDesktop(dpy, win) >= 0) {
                auto enable = false;
                auto alpha = 1.0;
                setWindowInputPassthrough(dpy, win, enable);
                setWindowOnTop(dpy, win, enable);
                setWindowAlpha(dpy, win, alpha);
            }
        }
        XFree(data);
    }
}

int main(int argc, char *argv[])
{
    Display *dpy = XOpenDisplay(NULL);

    // if argument is 'reset',
    // reset all windows that are always on top to default state
    // TODO: only reset windows that we made always on top
    if (argc >= 2) {
        if (strcmp(argv[1], "reset") == 0) {
            Window root = DefaultRootWindow(dpy);
            resetAllWindows(dpy, root);
            XCloseDisplay(dpy);
            return 0;
        }
    }

    Window win = getFocusedWindow(dpy);
    // desktop and special windows have desktop=-1 (according to `wmctrl -l`)
    if (getWindowDesktop(dpy, win) >= 0) {
        bool enable = !getWindowOnTop(dpy, win);
        double alpha = 1;
        if (enable) {
            if (argc >= 2) {
                alpha = atof(argv[1]);
                alpha = (alpha <= 0.05 ? 0.05 : alpha);
            } else {
                alpha = 0.8;
            }
        }
        setWindowInputPassthrough(dpy, win, enable);
        setWindowOnTop(dpy, win, enable);
        setWindowAlpha(dpy, win, alpha);
    }

    XCloseDisplay(dpy);
    return 0;
}
