// The MIT License (MIT)
//
// Copyright (c) 2019 cupnoodles
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// usage: xpiccolo [-hH] [-m count]
//
// by default prints in hex format: #abcdef
//
// -H       HEX format: #ABCDEF
// -r       rgb format: 1,2,255
// -m [N]   select multiple colors, ESC to stop early
// -c       capture screen
// -i       instant capture
//
// esc - stop
// lclick - print color and stop
// rclick/ctrl+lclick - buffer color
//
// References
//
// https://manpages.ubuntu.com/manpages/bionic/man3/xcb_grab_pointer.3.html
//

#include <chrono>
#include <future>
#include <iostream>
#include <thread>
#include <vector>
#include <xcb/xcb.h>
#include <xcb/xcb_aux.h>
#include <xcb/xcb_ewmh.h>
#include <xcb/xcb_image.h>

#include <cstring>
#include <unistd.h>

enum class SnipMode { instant, capture, wait };

void print_error(xcb_generic_error_t *err)
{
    if (err) {
        std::cerr << "error:" << err->error_code << '\n';
        free(err);
    }
}

void check_cookie(const xcb_void_cookie_t &cookie, xcb_connection_t *connection,
                  const char *errMessage)
{
    xcb_generic_error_t *error = xcb_request_check(connection, cookie);
    if (error) {
        // fprintf(stderr, "ERROR: %s : %" PRIu8 "\n", errMessage,
        // error->error_code);
        std::cerr << "errMessage"
                  << " " << error->error_code << '\n';
        xcb_disconnect(connection);
        exit(-1);
    }
}

class PixmapCursor
{
    xcb_cursor_t cursor;
    xcb_connection_t *conn;

  public:
    PixmapCursor(xcb_connection_t *c, xcb_pixmap_t src, xcb_pixmap_t mask)
    {

        cursor = xcb_generate_id(c);
        auto f_r = 0;
        auto f_g = 0;
        auto f_b = 0;
        auto b_r = 0;
        auto b_g = 0;
        auto b_b = 0;
        auto x = 0;
        auto y = 0;
        auto cursor_cookie = xcb_create_cursor(c, cursor, src, mask, f_r, f_g,
                                               f_b, b_r, b_g, b_b, x, y);
    };
    ~PixmapCursor() { xcb_free_cursor(conn, cursor); };

    int set_for_window(xcb_connection_t *c, xcb_window_t w)
    {
        uint32_t mask = XCB_CW_CURSOR;
        uint32_t value_list = cursor;
        auto cookie = xcb_change_window_attributes(c, w, mask, &value_list);
        return 0;
    }
    xcb_cursor_t get_id() { return cursor; }
};

uint32_t create_cursor(xcb_connection_t *conn, uint16_t cursor_char)
{
    const auto font = xcb_generate_id(conn);
    xcb_open_font(conn, font, strlen("cursor"), "cursor");

    const auto cursor = xcb_generate_id(conn);
    xcb_create_glyph_cursor(conn, cursor, font, font, cursor_char,
                            cursor_char + 1, 0, 0, 0, 0, 0, 0);
    return cursor;
}

int grab_pointer(xcb_connection_t *conn, xcb_window_t w, uint16_t event_mask,
                 xcb_generic_error_t *err, xcb_cursor_t cursor)
{

    const auto cookie = xcb_grab_pointer(
        conn,
        false,      /* get all pointer events specified by the following mask */
        w,          /* grab the root window */
        event_mask, /* which events to let through */
        XCB_GRAB_MODE_ASYNC, /* pointer events should continue as normal */
        XCB_GRAB_MODE_ASYNC, /* keyboard mode */
        XCB_NONE, /* confine_to = in which window should the cursor stay */
        cursor,   /* we change the cursor to whatever the user wanted */
        XCB_CURRENT_TIME);

    const auto reply = xcb_grab_pointer_reply(conn, cookie, &err);
    if (!reply) {
        print_error(err);
        return EXIT_FAILURE;
    }

    const auto status = reply->status;
    // if (reply->status == XCB_GRAB_STATUS_SUCCESS)
    //     printf("successfully grabbed the pointer\n");
    free(reply);

    return status;
}

uint32_t get_one_pixel(xcb_connection_t *conn, xcb_drawable_t d, int x, int y)
{
    const auto img = xcb_image_get(conn, d, x, y, 1, 1, (uint32_t)(~0UL),
                                   XCB_IMAGE_FORMAT_XY_PIXMAP);
    const auto pix = xcb_image_get_pixel(img, 0, 0);
    xcb_image_destroy(img);
    return pix;
}

int get_pointer_xy(xcb_connection_t *conn, xcb_window_t window,
                   xcb_generic_error_t *err, int &x, int &y)
{
    const auto cookie = xcb_query_pointer(conn, window);
    const auto reply = xcb_query_pointer_reply(conn, cookie, &err);
    if (!reply) {
        print_error(err);
        return EXIT_FAILURE;
    }
    x = reply->win_x;
    y = reply->win_y;
    free(reply);
    return EXIT_SUCCESS;
}

uint32_t get_pixel_under_pointer(xcb_connection_t *conn, xcb_window_t window,
                                 xcb_generic_error_t *err)
{

    int x = 0;
    int y = 0;
    get_pointer_xy(conn, window, err, x, y);
    return get_one_pixel(conn, window, x, y);
}

uint32_t get_pixel_under_pointer_from_img(xcb_connection_t *conn,
                                          xcb_drawable_t d, xcb_image_t *img,
                                          xcb_generic_error_t *err)
{
    int x = 0;
    int y = 0;
    get_pointer_xy(conn, d, err, x, y);
    return xcb_image_get_pixel(img, x, y);
}

int print_pixel_colors(xcb_connection_t *conn,
                       const std::vector<uint32_t> pixels, xcb_window_t w,
                       xcb_visualid_t visual, xcb_generic_error_t *err)
{
    // get colormap
    const auto cmap = xcb_generate_id(conn);
    const auto cm_cookie =
        xcb_create_colormap(conn, XCB_COLORMAP_ALLOC_NONE, cmap, w, visual);
    check_cookie(cm_cookie, conn, "cannot create colormap");

    // get pixel colors
    const auto cookie =
        xcb_query_colors(conn, cmap, pixels.size(), pixels.data());
    const auto reply = xcb_query_colors_reply(conn, cookie, &err);
    if (!reply) {
        print_error(err);
        return EXIT_FAILURE;
    }
    const auto numcolors = xcb_query_colors_colors_length(reply);
    const auto colors = xcb_query_colors_colors(reply);

    // print colors
    for (int i = 0; i < numcolors; i++) {
        // auto it = xcb_query_colors_colors_iterator(reply);
        // auto &col = it.data[i];
        const auto &col = colors[i];
        std::cout << col.red / 256 << ","   //
                  << col.green / 256 << "," //
                  << col.blue / 256 << '\n';
    }
    free(reply);

    return EXIT_SUCCESS;
}

class PreviewWindow
{
    xcb_connection_t *conn;
    int last_x = 0;
    int last_y = 0;
    int width = 100;
    int height = 100;
    xcb_gcontext_t gc;
    const char *title = "xpiccolo";
    xcb_ewmh_connection_t ewmh;

  public:
    xcb_window_t window;
    PreviewWindow(xcb_connection_t *c, xcb_screen_t *screen,
                  xcb_generic_error_t *err)
        : conn(c)
    {
        window = xcb_generate_id(conn);
        uint32_t mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
        uint32_t values[2];
        values[0] = screen->black_pixel;
        values[1] = XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_BUTTON_PRESS |
                    XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_POINTER_MOTION;
        const xcb_void_cookie_t windowCookie = xcb_create_window_checked(
            conn, screen->root_depth, window, screen->root, 0, 0, 100, 100, 0,
            XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual, mask, values);

        // init ewmh
        auto atoms_cookie = xcb_ewmh_init_atoms(conn, &ewmh);
        xcb_ewmh_init_atoms_replies(&ewmh, atoms_cookie, &err);

        set_window_type(ewmh._NET_WM_WINDOW_TYPE_NOTIFICATION);
        set_window_title(title);

        // init gc
        gc = xcb_generate_id(conn);
        const auto gcmask = XCB_GC_FOREGROUND;
        uint32_t gc_values[] = {screen->black_pixel};
        const auto gc_cookie =
            xcb_create_gc(conn, gc, window, gcmask, gc_values);
        // xcb_change_gc(conn, gc, gcmask, gc_values);
    };

    ~PreviewWindow()
    {
        xcb_destroy_window(conn, window);
        xcb_free_gc(conn, gc);
    };

    void set_window_type(xcb_atom_t type)
    {
        xcb_atom_t atoms_type[] = {type};
        const auto type_ck =
            xcb_ewmh_set_wm_window_type(&ewmh, window, 1, atoms_type);
        check_cookie(type_ck, conn, "can't set type");
    }
    void set_window_title(const char *title)
    {
        auto prop_ck = xcb_change_property(conn, XCB_PROP_MODE_REPLACE, window,
                                           XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
                                           strlen(title), title);
        check_cookie(prop_ck, conn, "can't set window title");
    }
    void show()
    {
        const xcb_void_cookie_t mapCookie =
            xcb_map_window_checked(conn, window);
        check_cookie(mapCookie, conn, "can't map window");
    }

    void move_window(int x, int y)
    {
        int values[] = {x, y};
        xcb_configure_window(conn, window,
                             XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, values);
    }

    void move_window_to_mouse(xcb_window_t root, xcb_generic_error_t *err)
    {
        int x = 0;
        int y = 0;
        get_pointer_xy(conn, root, err, x, y);
        if (last_x != x || last_y != y) {
            last_x = x;
            last_y = y;
            int values[] = {x + 5, y + 5};
            move_window(x + 5, y + 5);
            xcb_flush(conn);
        }
    };

    void clear(int x, int y, int w, int h, uint32_t color)
    {
        uint32_t values[1] = {color};
        xcb_change_window_attributes(conn, window, XCB_CW_BACK_PIXEL, values);
        xcb_clear_area(conn, 0, window, x, y, w, h);
    }

    void draw_pixel_history(std::vector<uint32_t> &pixels)
    {
        for (size_t i = 0; i < pixels.size(); i++) {
            clear(70, 1 + (100 / pixels.size()) * i, 29,
                  (100 / pixels.size()) * i, pixels[pixels.size() - 1 - i]);
        }
    }
    void draw_border()
    {
        // TODO: there may be a built-in border
        xcb_rectangle_t rects[] = {{0, 0, 99, 99}};
        xcb_poly_rectangle(conn, window, gc, 1, rects);
    }
};

int main(int argc, char const *argv[])
{
    auto conn = xcb_connect(NULL, NULL);
    const auto setup = xcb_get_setup(conn);
    const auto screen = xcb_setup_roots_iterator(setup).data;
    const auto root = screen->root;
    xcb_generic_error_t *err = NULL;

    std::vector<uint32_t> pixels;

    // TODO: set from arguments:
    SnipMode mode = SnipMode::wait;
    int pixelsLeft = 5;
    if (pixelsLeft < 1) {
        return EXIT_FAILURE;
    }

    if (mode == SnipMode::instant) {
        // instantly get the pixel and exit
        // TODO: accept position from arguments or use pointer position
        int x = 0;
        int y = 0;
        get_pointer_xy(conn, root, err, x, y);
        pixels.push_back(get_one_pixel(conn, root, x, y));

    } else if (mode == SnipMode::wait) {
        auto foreground = xcb_generate_id(conn);
        auto mask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;
        uint32_t values[2] = {30006};
        values[0] = screen->black_pixel;
        values[1] = 0;
        xcb_create_gc(conn, foreground, screen->root, mask, values);
        // wait for click, then get pixel color
        xcb_pixmap_t src = xcb_generate_id(conn);
        xcb_create_pixmap(conn, 1, src, root, 128, 128);
        // xcb_change_window_attributes(conn, src, XCB_CW_BACK_PIXEL, values);
        // xcb_clear_area(conn, 0, src, 0, 0, 100, 100);
        xcb_gcontext_t fill = xcb_generate_id(conn);
        mask = XCB_GC_FOREGROUND | XCB_GC_BACKGROUND;
        values[0] = screen->white_pixel;
        values[1] = screen->white_pixel;
        xcb_create_gc(conn, fill, src, mask, values);

        PixmapCursor curs(conn, src, src);
        xcb_rectangle_t rect[] = {0, 0, 500, 500};
        xcb_poly_fill_rectangle(conn, src, fill, 1, rect);
        // curs.set_for_window(conn, root);
        const auto cursor = curs.get_id();
        // const auto cursor = create_cursor(conn, 29);
        xcb_create_cursor(conn, cursor, src, src, 1, 2000, 1, 1, 1, 1, 1, 11);
        auto status = grab_pointer(conn, root,
                                   XCB_EVENT_MASK_BUTTON_PRESS |
                                       XCB_EVENT_MASK_BUTTON_RELEASE |
                                       XCB_EVENT_MASK_POINTER_MOTION,
                                   err, cursor);
        // xcb_free_cursor(conn, cursor);
        // xcb_clear_area

        if (status) {
            print_error(err);
            return EXIT_FAILURE;
        }

        // make window
        PreviewWindow preview_window(conn, screen, err);

        auto window = preview_window.window;

        // show window
        // preview_window.show();

        // LOOP
        xcb_flush(conn);
        xcb_generic_event_t *event;
        bool shouldStop = false;

        auto get_color = [&]() {
            int x = 0;
            int y = 0;
            get_pointer_xy(conn, root, err, x, y);
            const auto pix = get_one_pixel(conn, root, x, y);
            if (pixels.size() > 0) {
                preview_window.clear(1, 1, 69, 99, pix);
            } else {
                preview_window.clear(1, 1, 99, 99, pix);
            }
        };

        auto get_color_loop = [&]() {
            while (true) {
                get_color();
                std::this_thread::sleep_for(std::chrono::milliseconds(16));
                if (shouldStop)
                    break;
                ;
            }
        };

        std::thread th(get_color_loop);
        while ((event = xcb_wait_for_event(conn))) {
            switch (event->response_type & ~0x80) {
            case XCB_BUTTON_RELEASE: {
                break;
            }
            case XCB_BUTTON_PRESS: {
                const auto press =
                    reinterpret_cast<xcb_button_press_event_t *>(event);
                // std::cout << press->state << '\n';

                switch (press->detail) {
                case XCB_BUTTON_INDEX_1: {
                    const auto pix =
                        get_one_pixel(conn, root, press->root_x, press->root_y);
                    pixels.push_back(pix);
                    if (--pixelsLeft <= 0) {
                        shouldStop = true;
                    } else {
                        // preview_window.draw_pixel_history(pixels);
                    }
                    break;
                }

                default:
                    break;
                }
                break;
            }
            case XCB_MOTION_NOTIFY: {
                const auto motion =
                    reinterpret_cast<xcb_motion_notify_event_t *>(event);
                // preview_window.move_window_to_mouse(root, err);
                break;
            }
            case XCB_EXPOSE: {
                get_color();
                // preview_window.move_window_to_mouse(root, err);
                xcb_flush(conn);
                break;
            }
            default:
                /* Unknown event type, ignore it */
                break;
            }
            free(event);
            if (shouldStop)
                break;
        }
        th.join();

        xcb_ungrab_pointer(conn, XCB_CURRENT_TIME);
    } else {
        // capture current image and wait for click to get pixel color

        // get screen size
        auto geom_cookie = xcb_get_geometry(conn, root);
        auto geom_reply = xcb_get_geometry_reply(conn, geom_cookie, &err);
        auto w = geom_reply->width;
        auto h = geom_reply->height;
        free(geom_reply);
        std::cout << w << " " << h << '\n';

        // capture img
        auto img = xcb_image_get(conn, root, 0, 0, w, h, (uint32_t)(~0UL),
                                 XCB_IMAGE_FORMAT_XY_PIXMAP);

        // get mouse position and pixel color
        const auto pix = get_pixel_under_pointer_from_img(conn, root, img, err);
        pixels.push_back(pix);

        xcb_image_destroy(img);
    }

    print_pixel_colors(conn, pixels, root, screen->root_visual, err);

    xcb_disconnect(conn);
    return EXIT_SUCCESS;
}
