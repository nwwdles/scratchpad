# X tile gesture

Click right button while dragging a window, move mouse in some direction, release both buttons.
Window should now be tiled on a side you've moved your pointer to.

Well, currently it doesn't tile you window, it just moves it in a corner, because tiling didn't work very well and it's too boring to fix it.
