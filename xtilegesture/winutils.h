#ifndef WINUTILS_H_
#define WINUTILS_H_

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xrandr.h>

#include <iostream>
#include <map>
#include <set>
#include <thread>
#include <vector>

struct Extents {
    long left;
    long right;
    long top;
    long bottom;
    Extents(long l = 0, long r = 0, long t = 0, long b = 0)
        : left(l), right(r), top(t), bottom(b){};
    Extents(long *data)
        : left(data[0]), right(data[1]), top(data[2]), bottom(data[3])
    {
    }
};

struct ExtendedHints {
    bool _NET_WM_STATE_MODAL = false;
    bool _NET_WM_STATE_STICKY = false;
    bool _NET_WM_STATE_MAXIMIZED_VERT = false;
    bool _NET_WM_STATE_MAXIMIZED_HORZ = false;
    bool _NET_WM_STATE_SHADED = false;
    bool _NET_WM_STATE_SKIP_TASKBAR = false;
    bool _NET_WM_STATE_SKIP_PAGER = false;
    bool _NET_WM_STATE_HIDDEN = false;
    bool _NET_WM_STATE_FULLSCREEN = false;
    bool _NET_WM_STATE_ABOVE = false;
    bool _NET_WM_STATE_BELOW = false;
    bool _NET_WM_STATE_DEMANDS_ATTENTION = false;
    bool _NET_WM_STATE_FOCUSED = false;
};

std::ostream &operator<<(std::ostream &lhs, const ExtendedHints &rhs)
{
    if (rhs._NET_WM_STATE_MODAL)
        lhs << "modal ";
    if (rhs._NET_WM_STATE_STICKY)
        lhs << "sticky ";
    if (rhs._NET_WM_STATE_MAXIMIZED_VERT)
        lhs << "horz ";
    if (rhs._NET_WM_STATE_MAXIMIZED_HORZ)
        lhs << "vert ";
    if (rhs._NET_WM_STATE_SHADED)
        lhs << "shaded ";
    if (rhs._NET_WM_STATE_SKIP_TASKBAR)
        lhs << "shaded ";
    if (rhs._NET_WM_STATE_SKIP_PAGER)
        lhs << "shaded ";
    if (rhs._NET_WM_STATE_HIDDEN)
        lhs << "hidden ";
    if (rhs._NET_WM_STATE_FULLSCREEN)
        lhs << "fullscreen ";
    if (rhs._NET_WM_STATE_ABOVE)
        lhs << "above ";
    if (rhs._NET_WM_STATE_BELOW)
        lhs << "below ";
    if (rhs._NET_WM_STATE_DEMANDS_ATTENTION)
        lhs << "below ";
    if (rhs._NET_WM_STATE_FOCUSED)
        lhs << "focused ";
    lhs << "\n";
    return lhs;
}

std::string getWindowName(Display *dpy, Window win)
{
    std::string name;
    XTextProperty wp;
    if (XGetWMName(dpy, win, &wp)) {
        name = reinterpret_cast<char *>(wp.value);
    }
    return name;
}

Window getWindowParent(Display *dpy, Window win)
{
    Window root, parent, *children = nullptr;
    unsigned int num_children;
    XQueryTree(dpy, win, &root, &parent, &children, &num_children);
    if (num_children) {
        XFree(children);
    }
    return parent;
}

Extents getWindowFrameExtents(Display *dpy, Window w)
{
    Extents ret;

    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long nitems;
    unsigned long bytes;
    unsigned char *data;
    Atom property = XInternAtom(dpy, "_NET_FRAME_EXTENTS", False);

    int status = XGetWindowProperty(dpy, w, property, //
                                    0L, 4L,           //
                                    False,            //
                                    AnyPropertyType,  //
                                    &actual_type_ret, &actual_format_ret,
                                    &nitems, &bytes, &data);

    if (status == Success) {
        if ((nitems == 4) && (bytes == 0)) {
            ret = Extents(reinterpret_cast<long *>(data));
        }
        XFree(data);
    }
    return ret;
}

int getWindowDesktop(Display *dpy, Window win)
{
    int desktop = -1;

    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long nitems;
    unsigned long bytes;
    unsigned char *data;
    Atom property = XInternAtom(dpy, "_NET_WM_DESKTOP", False);

    int status = XGetWindowProperty(dpy, win, property, //
                                    0L, 1L,             //
                                    False,              //
                                    AnyPropertyType,    //
                                    &actual_type_ret, &actual_format_ret,
                                    &nitems, &bytes, &data);

    if (status == Success) {
        if (nitems) {
            unsigned long *return_words =
                reinterpret_cast<unsigned long *>(data);
            desktop = return_words[0];
        }
        XFree(data);
    }
    return desktop;
}

int getCurrentDesktop(Display *dpy)
{
    int desktop = -1;

    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long nitems;
    unsigned long bytes;
    unsigned char *data;
    Atom property = XInternAtom(dpy, "_NET_CURRENT_DESKTOP", True);

    int status = XGetWindowProperty(dpy, DefaultRootWindow(dpy), property, //
                                    0L, 1L,                                //
                                    False,                                 //
                                    AnyPropertyType,                       //
                                    &actual_type_ret, &actual_format_ret,
                                    &nitems, &bytes, &data);

    if (status == Success) {
        if (nitems) {
            unsigned long *return_words =
                reinterpret_cast<unsigned long *>(data);
            desktop = return_words[0];
        }
        XFree(data);
    }

    return desktop;
}

int getNumDesktops(Display *dpy)
{
    int num = -1;

    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long nitems;
    unsigned long bytes;
    unsigned char *data;
    Atom property = XInternAtom(dpy, "_NET_NUMBER_OF_DESKTOPS", True);

    int status = XGetWindowProperty(dpy, DefaultRootWindow(dpy), property, //
                                    0L, 1L,                                //
                                    False,                                 //
                                    AnyPropertyType,                       //
                                    &actual_type_ret, &actual_format_ret,
                                    &nitems, &bytes, &data);

    if (status == Success) {
        if (nitems) {
            unsigned long *return_words =
                reinterpret_cast<unsigned long *>(data);
            num = return_words[0];
        }
        XFree(data);
    }

    return num;
}

void getWorkArea(Display *dpy)
{
    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long nitems;
    unsigned long bytes;
    unsigned char *data;
    Atom property = XInternAtom(dpy, "_NET_WORKAREA", True);
    int status = XGetWindowProperty(dpy, DefaultRootWindow(dpy), property, //
                                    0L, 64L,                               //
                                    False,                                 //
                                    AnyPropertyType,                       //
                                    &actual_type_ret, &actual_format_ret,
                                    &nitems, &bytes, &data);

    if (status == Success) {
        if (nitems) {
            // std::cout << nitems;
            unsigned long *return_words =
                reinterpret_cast<unsigned long *>(data);
            for (int i = 0; i < nitems; ++i) {
                std::cout << return_words[i] << '\n';
            }
        }
        XFree(data);
    }
}

// https://standards.freedesktop.org/wm-spec/wm-spec-latest.html#idm140200472615568
ExtendedHints getWindowExtendedHints(Display *dpy, Window win)
{
    ExtendedHints hints;

    Atom actual_type_ret;
    int actual_format_ret;
    unsigned long nitems;
    unsigned long bytes;
    unsigned char *data;
    Atom property = XInternAtom(dpy, "_NET_WM_STATE", True);

    int status = XGetWindowProperty(dpy, win, property, //
                                    0L, 64L,            //
                                    False,              //
                                    AnyPropertyType,    //
                                    &actual_type_ret, &actual_format_ret,
                                    &nitems, &bytes, &data);

    if (status == Success) {
        if (nitems) {
            unsigned long *return_words =
                reinterpret_cast<unsigned long *>(data);
            Atom modal = XInternAtom(dpy, "_NET_WM_STATE_MODAL", False);
            Atom sticky = XInternAtom(dpy, "_NET_WM_STATE_STICKY", False);
            Atom horz = XInternAtom(dpy, "_NET_WM_STATE_MAXIMIZED_VERT", False);
            Atom vert = XInternAtom(dpy, "_NET_WM_STATE_MAXIMIZED_HORZ", False);
            Atom shaded = XInternAtom(dpy, "_NET_WM_STATE_SHADED", False);
            Atom hidden = XInternAtom(dpy, "_NET_WM_STATE_HIDDEN", False);
            Atom fullscreen =
                XInternAtom(dpy, "_NET_WM_STATE_FULLSCREEN", False);
            Atom above = XInternAtom(dpy, "_NET_WM_STATE_ABOVE", False);
            Atom below = XInternAtom(dpy, "_NET_WM_STATE_BELOW", False);
            Atom focused = XInternAtom(dpy, "_NET_WM_STATE_FOCUSED", False);
            Atom demands =
                XInternAtom(dpy, "_NET_WM_STATE_DEMANDS_ATTENTION", False);
            Atom skiptaskbar =
                XInternAtom(dpy, "_NET_WM_STATE_SKIP_TASKBAR", False);
            Atom skippager =
                XInternAtom(dpy, "_NET_WM_STATE_SKIP_PAGER", False);

            for (unsigned long item = 0; item < nitems; item++) {
                auto word = return_words[item];
                if (word == modal) {
                    hints._NET_WM_STATE_MODAL = true;
                } else if (word == sticky) {
                    hints._NET_WM_STATE_STICKY = true;
                } else if (word == horz) {
                    hints._NET_WM_STATE_MAXIMIZED_VERT = true;
                } else if (word == vert) {
                    hints._NET_WM_STATE_MAXIMIZED_HORZ = true;
                } else if (word == shaded) {
                    hints._NET_WM_STATE_SHADED = true;
                } else if (word == hidden) {
                    hints._NET_WM_STATE_HIDDEN = true;
                } else if (word == fullscreen) {
                    hints._NET_WM_STATE_FULLSCREEN = true;
                } else if (word == above) {
                    hints._NET_WM_STATE_ABOVE = true;
                } else if (word == below) {
                    hints._NET_WM_STATE_BELOW = true;
                } else if (word == focused) {
                    hints._NET_WM_STATE_FOCUSED = true;
                } else if (word == skiptaskbar) {
                    hints._NET_WM_STATE_SKIP_TASKBAR = true;
                } else if (word == skippager) {
                    hints._NET_WM_STATE_SKIP_PAGER = true;
                } else if (word == demands) {
                    hints._NET_WM_STATE_DEMANDS_ATTENTION = true;
                }
            }
        }
        XFree(data);
    }
    return hints;
}

// https://tronche.com/gui/x/xlib/window/attributes/gravity.html
void getWindowGravityOffset(Display *dpy, Window win, int gravity,
                            int &offset_x_ret, int &offset_y_ret)
{
    // TODO: add all gravities
    offset_x_ret = 0;
    offset_y_ret = 0;
    if (gravity == StaticGravity || gravity == ForgetGravity) {
        Extents ext = getWindowFrameExtents(dpy, win);
        auto top = ext.top;
        offset_y_ret = top;
    }
};

void setWindowState(Display *dpy, Window wind, const char *state, bool ontop)
{
    Atom wm_state, wm_state_above;
    XEvent event;

    if ((wm_state = XInternAtom(dpy, "_NET_WM_STATE", False)) != None) {
        if ((wm_state_above = XInternAtom(dpy, state, False)) != None) {
            event.xclient.type = ClientMessage;
            event.xclient.serial = 0;
            event.xclient.send_event = True;
            event.xclient.display = dpy;
            event.xclient.window = wind;
            event.xclient.message_type = wm_state;
            event.xclient.format = 32;
            event.xclient.data.l[0] = 1 && ontop;
            event.xclient.data.l[1] = wm_state_above;

            /* unused */
            event.xclient.data.l[2] = 0;
            event.xclient.data.l[3] = 0;
            event.xclient.data.l[4] = 0;

            /* actually send the event */
            XSendEvent(dpy, DefaultRootWindow(dpy), False,
                       SubstructureRedirectMask | SubstructureNotifyMask,
                       &event);
        }
    }
}

std::vector<std::vector<Window>> getTopWindows(Display *dpy)
{
    std::vector<std::vector<Window>> out(getNumDesktops(dpy));

    Atom a = XInternAtom(dpy, "_NET_CLIENT_LIST_STACKING", true);
    Atom actualType;
    int format;
    unsigned long numItems, bytesAfter;
    unsigned char *data = 0;
    int status = XGetWindowProperty(dpy, DefaultRootWindow(dpy), a, 0L, (~0L),
                                    false, AnyPropertyType, &actualType,
                                    &format, &numItems, &bytesAfter, &data);

    if (status >= Success && numItems) {
        // success - we have data: Format should always be 32:
        // static_assert(format == 32);
        // cast to proper format, and iterate through values:
        unsigned long *array = reinterpret_cast<unsigned long *>(data);
        for (unsigned long i = 0; i < numItems; i++) {
            // get window Id:
            Window win = (Window)array[i];
            auto d = getWindowDesktop(dpy, win);
            if (d >= 0) // ignore special windows that have desktop=-1
                out[d].push_back(win);
        }
        XFree(data);
    }
    return out;
}

static int top_panel_height(Display *display, Window window)
{
    int height = 0; // maximum height
    Window w;
    Window *children;
    unsigned int n_children;

    XQueryTree(display, window, &w, &w, &children, &n_children);

    // looks for each one of the children
    int i;
    for (i = 0; i < n_children; i++) {
        // this is the property we're looking for
        Atom strut = XInternAtom(display, "_NET_WM_STRUT_PARTIAL", false);
        Atom type_return;
        int actual_type;
        unsigned long nitems, bytes;
        unsigned char *data = nullptr;

        // load window attributes (we only want to know about the
        //                         windows where y = 0)
        XWindowAttributes xwa;
        XGetWindowAttributes(display, window, &xwa);

        // load the property _NET_WM_STRUT_PARTIAL
        int s = XGetWindowProperty(display, window, strut, 0, 64L, false,
                                   XA_CARDINAL, &type_return, &actual_type,
                                   &nitems, &bytes, &data);
        if (s == Success) {
            // std::cout << "succ" << nitems << "\n";
            Atom *state = (Atom *)data;
            // state[2] contains the "dock height"
            if (nitems) {
                for (int i = 0; i < nitems; i++) {
                    // std::cout << i << " " << data[i];
                    /* code */
                }
            }

            if (xwa.y == 0 && nitems > 0 && state[2])
                if (state[2] > height)
                    height = state[2];
        }

        // recursively, traverse the tree of all children of children
        int children_max_height = top_panel_height(display, children[i]);
        if (children_max_height > height)
            height = children_max_height;
    }
    // std::cout << height << "height\n";
    return height;
}

void printWindowName(Display *dpy, Window win)
{
    std::cout << win << "\t" << getWindowName(dpy, win) << "\n";
}

void printChildrenNames(Display *dpy, Window win, int level = 0)
{
    Window root, parent, *children = nullptr;
    unsigned int num_children;
    if (!XQueryTree(dpy, win, &root, &parent, &children, &num_children)) {
        return;
    }
    if (num_children) {
        std::string s(level, ' ');
        for (int i = 0; i < num_children; i++) {
            auto child = children[i];
            std::cout << s << child << "\n";
            printWindowName(dpy, win);
            printChildrenNames(dpy, child, level + 1);
        }
        XFree(children);
    }
}

void printParentNames(Display *dpy, Window win, int level = 5)
{
    Window root, parent, *children = nullptr;
    unsigned int num_children;
    if (!XQueryTree(dpy, win, &root, &parent, &children, &num_children)) {
        return;
    }
    if (num_children) {
        XFree(children);
    }
    if (parent) {
        if (level < 0)
            level = 0;
        std::string s(level, ' ');
        std::cout << s << parent << "\n";
        printWindowName(dpy, win);
        printParentNames(dpy, parent, level - 1);
    }
}

// https://stackoverflow.com/questions/1014822/how-to-know-which-window-has-focus-and-how-to-change-it
Window getFocusedWindow(Display *dpy)
{
    Window focused;
    int revert_to;
    XGetInputFocus(dpy, &focused, &revert_to);
    return focused;
};

void setWindowActive(Display *dpy, Window w)
{
    XClientMessageEvent ev;
    // std::memset(&ev, 0, sizeof ev);
    ev.type = ClientMessage;
    ev.window = w;
    ev.message_type = XInternAtom(dpy, "_NET_ACTIVE_WINDOW", True);
    ev.format = 32;
    ev.data.l[0] = 1;
    ev.data.l[1] = CurrentTime;
    ev.data.l[2] = ev.data.l[3] = ev.data.l[4] = 0;
    XSendEvent(dpy, DefaultRootWindow(dpy), False,
               SubstructureRedirectMask | SubstructureNotifyMask,
               (XEvent *)&ev);
    XFlush(dpy);
}

// Status setWindowMinimized(Display *dpy, Window w, int screen)
// {
//     Atom prop;

//     prop = XInternAtom(dpy, "WM_CHANGE_STATE", False);
//     if (prop == None)
//         return False;
//     else {
//         XClientMessageEvent ev = {.type = ClientMessage,
//                                   .window = w,
//                                   .message_type = prop,
//                                   .format = 32,
//                                   .data.l[0] = IconicState};
//         Window root = RootWindow(dpy, screen);

//         return (XSendEvent(dpy, root, False,
//                            SubstructureRedirectMask | SubstructureNotifyMask,
//                            (XEvent *)&ev));
//     }
// }

void setWindowActive_(Display *dpy, Window wind)
{
    Atom active = XInternAtom(dpy, "_NET_ACTIVE_WINDOW", True);
    XClientMessageEvent event;
    if (active != None) {
        event.type = ClientMessage;
        event.serial = 0;
        event.send_event = True;
        event.display = dpy;
        event.window = wind;
        event.message_type = active;
        event.format = 32;
        event.data.l[0] = 1UL;
        event.data.l[1] = 0L;
        /* unused */
        event.data.l[2] = 0;
        event.data.l[3] = 0;
        event.data.l[4] = 0;
        /* actually send the event */
        XSendEvent(dpy, DefaultRootWindow(dpy), False,
                   SubstructureRedirectMask | SubstructureNotifyMask,
                   (XEvent *)&event);
    }
}

#endif // WINUTILS_H_