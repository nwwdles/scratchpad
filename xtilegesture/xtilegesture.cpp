#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xrandr.h>

#include <iostream>
#include <thread>
#include <vector>

#include "winutils.h"

int erhandler(Display *dpy, XErrorEvent *er)
{
    char ertext[64];
    XGetErrorText(dpy, er->error_code, ertext, 64);
    std::cout << ertext << "\n";
    return 0;
}

struct WindowFrame {
    Window win;
    int x;
    int y;
    int w;
    int h;
    WindowFrame(Window wwin = 0, int xx = 0, int yy = 0, int ww = 0, int hh = 0)
        : win(wwin), x(xx), y(yy), h(hh), w(ww){};
};

WindowFrame getWindowAndFrameOnPos(Display *dpy, int x, int y)
{
    auto curDesk = getCurrentDesktop(dpy);
    auto topWindows = getTopWindows(dpy)[curDesk];

    for (int i = topWindows.size() - 1; i >= 0; --i) {
        auto win = topWindows[i];

        int rx, ry;
        Window child;
        XWindowAttributes xwa;
        XTranslateCoordinates(dpy, win, DefaultRootWindow(dpy), 0, 0, &rx, &ry,
                              &child);
        XGetWindowAttributes(dpy, win, &xwa);
        auto e = getWindowFrameExtents(dpy, win);
        int wx = rx - xwa.x;
        int wy = ry - xwa.y;

        if (x >= wx - e.left && x <= wx + xwa.width + e.right &&
            y >= wy - e.top && y <= wy + xwa.height + e.bottom) {
            return WindowFrame(win, wx - e.left, wy - e.top,
                               xwa.width + e.right, xwa.height + e.bottom);
        }
    }
    return WindowFrame();
}

void onRelease(Display *dpy, int dx, int dy, int mousex, int mousey)
{
    auto w = getWindowAndFrameOnPos(dpy, mousex, mousey);
    std::cout << dx << " " << dy << "\n";
    int nmon = 0;
    XRRMonitorInfo *moninf =
        XRRGetMonitors(dpy, DefaultRootWindow(dpy), True, &nmon);
    if (nmon <= 0 || moninf == nullptr) {
        throw std::runtime_error("No xranrd screens.\n");
    }
    for (int mon = 0; mon < nmon; ++mon) {
        int mx = moninf[mon].x;
        int my = moninf[mon].y;
        int mw = moninf[mon].width;
        int mh = moninf[mon].height;
        if (mousex >= mx && mousex <= mx + mw && mousey >= my &&
            mousey <= my + mh) {
            if (dx < 0) {
                // XMoveResizeWindow(dpy, w.win, mx, my, mw / 2, mh);
                XMoveWindow(dpy, w.win, mx, my);
            } else {
                // XMoveResizeWindow(dpy, w.win, mx + mw - w.w, my, mw / 2, mh);
                XMoveWindow(dpy, w.win, mx + mw - w.w, my);
            }
            break;
        }
    }
}

int main(int argc, char const *argv[])
{

    // setup x
    Display *dpy = XOpenDisplay(NULL);
    Window root = DefaultRootWindow(dpy);
    XSetErrorHandler(erhandler);

    // listen to window move events
    XSelectInput(dpy, root, StructureNotifyMask | SubstructureNotifyMask);

    bool pressed = false;
    bool bothHeld = false;
    bool eitherHeld = false;
    int startx = 0;
    int starty = 0;
    Window movingWindow;

    while (true) {
        XEvent ev;
        XNextEvent(dpy, &ev);
        // std::cout << ev.type << "\n";
        unsigned mask_return;
        switch (ev.type) {
        case ConfigureNotify:
            Window c;
            Window r;
            int rx, ry, wx, wy;
            unsigned mask_return;
            XQueryPointer(dpy, root, &r, &c, &rx, &ry, &wx, &wy, &mask_return);

            bothHeld =
                (mask_return & Button1Mask) && (mask_return & Button3Mask);
            eitherHeld =
                (mask_return & Button1Mask) || (mask_return & Button3Mask);
            if (!pressed && bothHeld) {
                pressed = true;
                startx = rx;
                starty = ry;
            } else if (pressed && !eitherHeld) {
                pressed = false;

                onRelease(dpy, rx - startx, ry - starty, rx, ry);
            }
            break;
        default:
            break;
        }
    }

    return 0;
}
