#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xrandr.h>

#include <iostream>
#include <map>
#include <set>
#include <thread>
#include <vector>

#include "winutils.h"

enum HintFlags {
    Hidden = 1 << 0, // minimized
    HMaximized = 1 << 1,
    VMaximized = 1 << 2,
    Focused = 1 << 3,
    Fullscreen = 1 << 4,
    Maximized = HMaximized | VMaximized,
};

struct Rect {
    int x;
    int y;
    int w;
    int h;
    Rect(int xx = 0, int yy = 0, int ww = 0, int hh = 0)
        : x(xx), y(yy), w(ww), h(hh){};
};

// flags bits: 0 - allow tile , 1 - don't tile
bool shouldTile(const ExtendedHints &h, unsigned flags)
{
    return !(
        h._NET_WM_STATE_BELOW                                              //
        || h._NET_WM_STATE_ABOVE                                           //
        || (h._NET_WM_STATE_FULLSCREEN && (flags & HintFlags::Fullscreen)) //
        || (h._NET_WM_STATE_HIDDEN && (flags & HintFlags::Hidden))         //
        || h._NET_WM_STATE_SHADED                                          //
        ||
        (h._NET_WM_STATE_MAXIMIZED_HORZ && (flags & HintFlags::HMaximized)) //
        ||
        (h._NET_WM_STATE_MAXIMIZED_VERT && (flags & HintFlags::VMaximized)) //
        || h._NET_WM_STATE_STICKY                                           //
        || h._NET_WM_STATE_MODAL                                            //
    );
}

unsigned getDefaultFlags()
{
    // bit not set - allow tile, set  - don't tile
    int ret = 0;
    ret |= HintFlags::Fullscreen;
    // ret |= HintFlags::Maximized;
    ret |= HintFlags::Hidden;
    return ret;
}

bool getWindowIsTileable(Display *dpy, Window win, unsigned flags)
{
    auto hints = getWindowExtendedHints(dpy, win);
    // std::cout << hints;
    return shouldTile(hints, flags);
};

int getClosestMonitor(Display *dpy, Window win, XRRMonitorInfo *moninf,
                      int nmon)
{
    int x, y;
    Window child;
    XWindowAttributes xwa;
    XTranslateCoordinates(dpy, win, DefaultRootWindow(dpy), 0, 0, &x, &y,
                          &child);
    XGetWindowAttributes(dpy, win, &xwa);
    int centerx = x - xwa.x + xwa.width / 2;
    int centery = y - xwa.y + xwa.height / 2;
    int closest_mon = -1;
    int leastdist = __INT_MAX__;
    for (int i = 0; i < nmon; i++) {
        int mcx = moninf[i].x + moninf[i].width / 2;
        int mcy = moninf[i].y + moninf[i].height / 2;
        int dist = (mcx - centerx) * (mcx - centerx) +
                   (mcy - centery) * (mcy - centery);
        if (dist < leastdist) {
            leastdist = dist;
            closest_mon = i;
        }
    }
    return closest_mon;
}

std::vector<Window> getTileableWindows(Display *dpy,
                                       const std::vector<Window> &windows,
                                       unsigned flags)
{
    std::vector<Window> out;
    for (auto win : windows) {
        if (getWindowIsTileable(dpy, win, flags)) {
            out.push_back(win);
        }
    }
    return out;
}

std::vector<std::vector<Window>>
sortWindowsByMonitor(Display *dpy, const std::vector<Window> &windows,
                     XRRMonitorInfo *moninf, int nmon)
{
    std::vector<std::vector<Window>> out(nmon);
    for (auto win : windows) {
        int closest_mon = getClosestMonitor(dpy, win, moninf, nmon);
        out[closest_mon].push_back(win);
    }
    return out;
}

void moveWindow(Display *dpy, Window win, int x, int y)
{
    // in order to arrange windows nicely
    // we calculate window offset because of its gravity
    long sup_ret;
    XSizeHints h_ret;
    XGetWMNormalHints(dpy, win, &h_ret, &sup_ret);
    int offsetx = 0;
    int offsety = 0;
    // getWindowGravityOffset(dpy, win, h_ret.win_gravity, offsetx,
    // offsety);

    // rewrite gravity instead because it's easier
    // TODO: stop writing things
    h_ret.win_gravity = NorthWestGravity;
    XSetWMNormalHints(dpy, win, &h_ret);

    XMoveWindow(dpy, win, x - offsetx, y - offsety);
    // printWindowName(dpy, win);
}

void moveResizeWindow(Display *dpy, Window win, int x, int y, int w, int h)
{
    // in order to arrange windows nicely
    // we calculate window offset because of its gravity
    long sup_ret;
    XSizeHints h_ret;
    XGetWMNormalHints(dpy, win, &h_ret, &sup_ret);
    int offsetx = 0;
    int offsety = 0;
    // getWindowGravityOffset(dpy, win, h_ret.win_gravity, offsetx,
    // offsety);

    // rewrite gravity instead because it's easier
    // TODO: stop writing things
    h_ret.win_gravity = NorthWestGravity;
    XSetWMNormalHints(dpy, win, &h_ret);

    XMoveResizeWindow(dpy, win, x - offsetx, y - offsety, w, h - 29);
    // printWindowName(dpy, win);
}

void applyFlags(Display *dpy, Window win, unsigned flags)
{
    if (flags && HintFlags::VMaximized)
        setWindowState(dpy, win, "_NET_WM_STATE_MAXIMIZED_VERT", false);
    if (flags && HintFlags::HMaximized)
        setWindowState(dpy, win, "_NET_WM_STATE_MAXIMIZED_HORZ", false);
    // if (flags && HintFlags::Hidden)
    // setWindowState(dpy, win, "_NET_WM_STATE_HIDDEN", false);
    // unminimizeWindow(dpy, win);
}

Rect getMonitorAvailableRect(XRRMonitorInfo &mon, int wmargin, int hmargin)
{
    // TODO: also include workarea in calculation (react to taskbars)
    return Rect(mon.x + wmargin, mon.y + hmargin, //
                mon.width - 2 * wmargin, mon.height - 2 * hmargin);
}

void cascadeOnArea(Display *dpy, const std::vector<Window> &windows,
                   unsigned flags, const Rect &area, int xstep, int ystep,
                   bool ascending, bool preserveWinSize)
{
    int windowCount = windows.size();
    int winw = area.w - (windowCount - 1) * xstep;
    int winh = area.h - (windowCount - 1) * ystep;
    for (int i = 0; i < windowCount; ++i) {
        int winx;
        if (ascending)
            winx = area.x + (windowCount - 1 - i) * xstep;
        else
            winx = area.x + i * xstep;
        int winy = area.y + i * ystep;
        auto win = windows[i];
        applyFlags(dpy, win, flags);
        if (preserveWinSize)
            moveWindow(dpy, win, winx, winy);
        else
            moveResizeWindow(dpy, win, winx, winy, winw, winh);
    }
}

void tileBySideOnArea(Display *dpy, const std::vector<Window> &windows,
                      unsigned flags, const Rect &area, int xmargin)
{
    int windowCount = windows.size();
    XWindowAttributes xwa;
    XGetWindowAttributes(dpy, windows[windowCount - 1], &xwa);

    int mainw = area.w;
    if (windowCount > 1)
        mainw = xwa.width;
    int winy = area.y;
    int winh = area.h;

    moveResizeWindow(dpy, windows[windowCount - 1], area.x, winy, mainw,
                     area.h);
    if (windowCount == 1)
        return;
    int winw = (area.w - mainw) / (windowCount - 1) - xmargin;
    for (int i = 0; i < windowCount - 1; ++i) {
        int winx = xmargin + area.x + mainw + (xmargin + winw) * i;
        auto win = windows[i];
        applyFlags(dpy, win, flags);
        moveResizeWindow(dpy, win, winx, winy, winw, winh);
    }
}

void tileOnArea(Display *dpy, const std::vector<Window> &windows,
                unsigned flags, const Rect &area, int xmargin, int ymargin)
{
    int windowCount = windows.size();

    XWindowAttributes xwa;
    XGetWindowAttributes(dpy, windows[windowCount - 1], &xwa);
    int mainw = area.w;
    if (windowCount > 1)
        mainw = xwa.width;

    moveResizeWindow(dpy, windows[windowCount - 1], area.x, area.y, mainw,
                     area.h);
    if (windowCount == 1)
        return;
    int winx = area.x + mainw + xmargin;
    int winw = area.w - mainw - xmargin;
    int winh = area.h / (windowCount - 1) - (windowCount - 2) * ymargin;

    for (int i = 0; i < windowCount - 1; ++i) {
        auto win = windows[i];
        int winy = area.y + (windowCount - 2 - i) * (winh + ymargin);
        applyFlags(dpy, win, flags);
        moveResizeWindow(dpy, win, winx, winy, winw, winh);
    }
}

void cascadeWindows(Display *dpy, const std::vector<Window> &windows,
                    unsigned flags, bool preserveWindowSize)
{
    int nmon = 0;
    XRRMonitorInfo *moninf =
        XRRGetMonitors(dpy, DefaultRootWindow(dpy), True, &nmon);
    if (nmon <= 0 || moninf == nullptr) {
        throw std::runtime_error("No xranrd screens.\n");
    }

    auto windowsByMonitor = sortWindowsByMonitor(dpy, windows, moninf, nmon);

    int wmargin = 40;
    int hmargin = 40;
    int xstep = 40;
    int ystep = 40;
    for (int mon = 0; mon < nmon; ++mon) {
        auto windowsOnMonitor = windowsByMonitor[mon];
        Rect area = getMonitorAvailableRect(moninf[mon], wmargin, hmargin);
        cascadeOnArea(dpy, windowsOnMonitor, flags, area, xstep, ystep, true,
                      preserveWindowSize);
    }
    XRRFreeMonitors(moninf);
}

void tileWindows(Display *dpy, const std::vector<Window> &windows,
                 unsigned flags)
{
    int nmon = 0;
    XRRMonitorInfo *moninf =
        XRRGetMonitors(dpy, DefaultRootWindow(dpy), True, &nmon);
    if (nmon <= 0 || moninf == nullptr) {
        throw std::runtime_error("No xranrd screens.\n");
    }

    auto windowsByMonitor = sortWindowsByMonitor(dpy, windows, moninf, nmon);

    int wmargin = 40;
    int hmargin = 40;
    int xmargin = 5;
    int ymargin = 5;
    for (int mon = 0; mon < nmon; ++mon) {
        auto windowsOnMonitor = windowsByMonitor[mon];
        Rect area = getMonitorAvailableRect(moninf[mon], wmargin, hmargin);
        tileBySideOnArea(dpy, windowsOnMonitor, flags, area, xmargin);
    }
    XRRFreeMonitors(moninf);
}

int erhandler(Display *dpy, XErrorEvent *er)
{
    char ertext[64];
    XGetErrorText(dpy, er->error_code, ertext, 64);
    std::cout << ertext << "\n";
    return 0;
}

// int loop(Display *dpy, Window root)
// {
//     XSelectInput(dpy, root, SubstructureNotifyMask | StructureNotifyMask);

//     while (true) {
//         XEvent ev;
//         XNextEvent(dpy, &ev);

//         // on window resize
//         if (ev.type == ConfigureNotify) {
//             auto win = ev.xconfigure.window;
//             // std::cout << win << "\n";
//             while (XEventsQueued(dpy, QueuedAfterReading)) {
//                 XNextEvent(dpy, &ev);
//             }
//             std::this_thread::sleep_for(std::chrono::milliseconds(5));
//             if (!XEventsQueued(dpy, QueuedAfterReading)) {
//                 std::cout << "seems like y'all done\n";
//                 auto w = getTopWindows(dpy);
//                 cascadeWindows(dpy, w[getCurrentDesktop(dpy)],
//                                getDefaultFlags());
//             }
//         }
//     }
// };

int main(int argc, char const *argv[])
{

    // setup x
    Display *dpy = XOpenDisplay(NULL);
    int screen = DefaultScreen(dpy);
    Window root = RootWindow(dpy, screen);

    XSetErrorHandler(erhandler);

    auto w = getTopWindows(dpy);
    auto curw = w[getCurrentDesktop(dpy)];
    curw = getTileableWindows(dpy, curw, getDefaultFlags());
    // for (auto win : curw) {
    //     std::cout << top_panel_height(dpy, root) << '\n';
    // }
    cascadeWindows(dpy, curw, getDefaultFlags(), true);
    cascadeWindows(dpy, curw, getDefaultFlags(), true);
    // tileWindows(dpy, curw, getDefaultFlags());
    // getWorkArea(dpy);
    // loop(dpy, root);
    return 0;
}
