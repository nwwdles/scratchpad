# xwacomzoom

A program that uses `xsetwacom` to change size of the screen area that the tablet is mapped to.

**Note:** This was a part of my xhotkeys program. Tablet zoom was the whole idea behind it because i didn't know how to call a script on key release to reset zoom back. So i wanted to try making my own hotkey thing that does it. Now that i know how to bind a script to key release (using `sxhkd` or that my xhotkeys thing), a script (below) is a more sane solution.

## A shell script that does the same

```bash
#!/usr/bin/env bash
# call `script.sh zoom` where zoom > 0 to zoom
# call `script.sh` to reset zoom

# SETTINGS
# to get the name use `xsetwacom --list devices`
devicename="Wacom Bamboo One M Pen stylus"
screenx=0
screeny=0
screenw=1920
screenh=1080

if [ -z "$1" ]; then
    area=$screenw"x"$screenh+$screenx+$screeny
    echo "Resetting zoom. Area: $area"
else
    zoom=$1
    eval $(xdotool getmouselocation --shell)

    xfac=$(awk "BEGIN {print ($X-$screenx)/$screenw}")
    yfac=$(awk "BEGIN {print ($Y-$screeny)/$screenh}")

    areaw=$(awk "BEGIN {print int($screenw / $zoom)}")
    areah=$(awk "BEGIN {print int($screenh / $zoom)}")
    areax=$(awk "BEGIN {print int($X - $areaw * $xfac)}")
    areay=$(awk "BEGIN {print int($Y - $areah * $yfac)}")
    area=$areaw"x"$areah+$areax+$areay
    echo "Zooming by $zoom. Area: $area"
fi

xsetwacom --set "$devicename" MapToOutput $area
```

# Materials

- [`xsetwacom` GitHub Wiki](https://github.com/linuxwacom/xf86-input-wacom/wiki/xsetwacom)
