#include "device.h"
#include "utils.h"

#include <iostream>
#include <sstream>

Device findDevice()
{
    auto ss = std::stringstream(exec("xsetwacom --list"));
    bool foundDevice = false;
    Device d;
    while (ss) {
        std::string name;
        std::string id;
        std::string type;
        std::getline(ss, name, '\t');
        std::getline(ss, id, '\t');
        std::getline(ss, type, '\n');
        trim(type);
        trim(name);
        // std::cout << "Device: <" << name << "> <" << type << ">\n";
        if (type == "type: STYLUS") {
            d = Device(name, id, type);
            foundDevice = true;
            break;
        }
    }
    if (!foundDevice) {
        throw std::runtime_error("no device found!");
    }
    std::cout << "Found device: \n";
    d.reportInfo();
    return d;
}

void Device::resetZoom()
{
    // std::cout << "reset zoom\n";
    mapToScreen(screenRect);
}

void Device::zoomIn(double zoom, int mouse_x, int mouse_y)
{
    // std::cout << "zooming in\n";
    double xfac = double(mouse_x - screenRect.x) / screenRect.w;
    double yfac = double(mouse_y - screenRect.y) / screenRect.h;
    int w = screenRect.w / zoom;
    int h = screenRect.h / zoom;
    int x = mouse_x - w * xfac;
    int y = mouse_y - h * yfac;
    mapToScreen({x, y, w, h});
};

void Device::setScreenRect(const Rect<int> &rect)
{
    screenRect = rect;
    resetZoom();
}

Rect<int> Device::readArea() const
{
    std::string cmd = "xsetwacom --get \"" + name + "\" Area";
    auto s = exec(cmd.c_str());
    auto out = Rect<int>(s);
    return out;
};

void Device::setArea(const Rect<int> &newArea)
{
    std::string cmd =
        "xsetwacom --set \"" + name + "\" Area " + newArea.toAreaString();
    exec(cmd.c_str());
    area = readArea();
};

void Device::mapToScreen(const Rect<int> &newRect)
{
    std::string cmd = "xsetwacom --set \"" + name + "\" MapToOutput " +
                      newRect.toGeometryString();
    exec(cmd.c_str());
};

void Device::reportInfo() const
{
    std::cout << "Name: " << name << "; Area: " << area.toAreaString() << "\n";
};
