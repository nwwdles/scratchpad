#ifndef DEVICE_H_
#define DEVICE_H_

#include <sstream>
#include <string>
#include <vector>

template <typename T> struct Rect {
    T x;
    T y;
    T w;
    T h;
    Rect(T xx = 0, T yy = 0, T ww = 0, T hh = 0) : x(xx), y(yy), w(ww), h(hh){};
    Rect(std::string &s)
    {
        // we just assume that string is 'x y w h'
        auto ss = std::stringstream(s);
        ss >> x;
        ss >> y;
        ss >> w;
        ss >> h;
    }
    std::string toGeometryString() const
    {
        std::stringstream ss;
        ss << w << "x" << h << "+" << x << "+" << y;
        return ss.str();
    }
    std::string toAreaString() const
    {
        // TODO: it probably wants x+w and y+h instead of w and h
        // i don't care about it right now though
        std::stringstream ss;
        ss << x << " " << y << " " << w << " " << h;
        return ss.str();
    };
};

class Device;

Device findDevice();

class Device
{
  private:
    std::string name;
    std::string id;
    std::string type;
    Rect<int> area;
    Rect<int> screenRect;

  public:
    Device() { init(); };
    Device(std::string name, std::string id, std::string type)
        : name(name), id(id), type(type)
    {
        init();
    };
    void init()
    {
        if (name != "")
            area = readArea();
    };

    Rect<int> readArea() const;
    void setArea(const Rect<int> &newArea);
    void setScreenRect(const Rect<int> &rect);
    void mapToScreen(const Rect<int> &newRect);
    void zoomIn(double zoom, int mouse_x, int mouse_y);
    void resetZoom();

    bool isStylus() const { return (type == "type: STYLUS"); }
    std::string getName() const { return name; }

    void reportInfo() const;
};

#endif // !DEVICE_H_
