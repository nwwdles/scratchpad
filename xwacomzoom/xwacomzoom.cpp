#include "device.h"
#include "utils.h"

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <X11/XKBlib.h>

void zoomDevice(Display *dpy, Window grab_window, Device d, double zoom)
{
    int root_x = 0;
    int root_y = 0;
    int win_x_return = 0;
    int win_y_return = 0;
    unsigned mask_return = 0;
    Window root_return;
    Window child_return;
    XQueryPointer(dpy, grab_window, &root_return, &child_return, &root_x,
                  &root_y, &win_x_return, &win_y_return, &mask_return);
    d.zoomIn(zoom, root_x, root_y);
}

/*
 * error handler ignores every error and makes my program not crash
 *
 * */
int erhandler(Display *dpy, XErrorEvent *er)
{
    char ertext[64];
    XGetErrorText(dpy, er->error_code, ertext, 64);
    std::cout << ertext << "\n";
    return 0;
}

int main(int argc, const char **argv)
{
    // read config
    // const char *defaultFile = "xwacomzoom.txt";
    // const char *confFilename;
    // if (argc > 1) {
    //     confFilename = argv[1];
    // } else {
    //     confFilename = defaultFile;
    // }
    // std::cout << "config: " << confFilename << "\n";

    // init wacom
    auto dev = findDevice();
    // TODO: this should be read from file or from  xsetwacom  if possible
    dev.setScreenRect({0, 0, 1920, 1080});

    double zoom = 0;
    if (argc >= 2) {
        zoom = atoi(argv[1]);
    }

    std::cout << "zoom: " << zoom << "\n";
    // init x11
    Display *dpy = XOpenDisplay(0);
    Window root = DefaultRootWindow(dpy);
    XSetErrorHandler(erhandler);

    // zoom or reset
    if (zoom > 0) {
        zoomDevice(dpy, root, dev, zoom);
    } else {
        dev.resetZoom();
    }
    XCloseDisplay(dpy);
    return 0;
}
