#!/usr/bin/env bash
# call `script.sh zoom` where zoom > 0 to zoom
# call `script.sh` to reset zoom

# SETTINGS
# to get the name use `xsetwacom --list devices`
devicename="Wacom Bamboo One M Pen stylus"
screenx=0
screeny=0
screenw=1920
screenh=1080

if [ -z "$1" ]; then
    area=$screenw"x"$screenh+$screenx+$screeny
    echo Resetting zoom. Area: $area
else
    zoom=$1
    eval "$(xdotool getmouselocation --shell)"

    xfac=$(awk "BEGIN {print ($X-$screenx)/$screenw}")
    yfac=$(awk "BEGIN {print ($Y-$screeny)/$screenh}")

    areaw=$(awk "BEGIN {print int($screenw / $zoom)}")
    areah=$(awk "BEGIN {print int($screenh / $zoom)}")
    areax=$(awk "BEGIN {print int($X - $areaw * $xfac)}")
    areay=$(awk "BEGIN {print int($Y - $areah * $yfac)}")
    area=$areaw"x"$areah+$areax+$areay
    echo "Zooming by $zoom. Area: $area"
fi

xsetwacom --set "$devicename" MapToOutput "$area"
