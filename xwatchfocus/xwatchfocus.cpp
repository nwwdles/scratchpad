// The MIT License (MIT)
//
// Copyright (c) 2019 cupnoodles
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//
// xwatchfocus - print the focused window's id (decimal) when the focus changes

#include <iostream>
#include <xcb/xcb.h>

xcb_window_t get_focused_window(xcb_connection_t *conn,
                                xcb_generic_error_t *err)
{
    auto focus_ck = xcb_get_input_focus(conn);
    auto focus_reply = xcb_get_input_focus_reply(conn, focus_ck, &err);
    return focus_reply->focus;
}

int main(int argc, char const *argv[])
{
    // setup xcb
    auto conn = xcb_connect(NULL, NULL);
    const auto setup = xcb_get_setup(conn);
    const auto screen = xcb_setup_roots_iterator(setup).data;
    const auto root = screen->root;
    xcb_generic_error_t *err = NULL;

    const uint32_t mask = XCB_CW_EVENT_MASK;
    const uint32_t values[] = {XCB_EVENT_MASK_FOCUS_CHANGE};

    auto focused_w = get_focused_window(conn, err);
    xcb_change_window_attributes(conn, focused_w, mask, values);
    std::cout << focused_w << '\n';

    // loop
    xcb_flush(conn);
    xcb_generic_event_t *event;
    bool shouldStop = false;
    while (!shouldStop && (event = xcb_wait_for_event(conn))) {
        switch (event->response_type & ~0x80) {
        case XCB_FOCUS_IN:
        case XCB_FOCUS_OUT: { // with the right algorithm we may only need _OUT
            auto new_focused_w = get_focused_window(conn, err);
            if (new_focused_w != focused_w) {
                focused_w = new_focused_w;
                xcb_change_window_attributes(conn, focused_w, mask, values);
                std::cout << focused_w << '\n';
                xcb_flush(conn); // we may not need it really but sometimes we
                                 // don't get the event so just to be safe
            }
            break;
        }
        default:
            // Unknown event type, ignore it
            break;
        }
        free(event);
    }

    xcb_disconnect(conn);
    return EXIT_SUCCESS;
}
