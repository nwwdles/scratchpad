// The MIT License (MIT)
//
// Copyright (c) 2019 cupnoodles
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//
// xwindowwatch - monitor window resize/move events.
//

#include <X11/Xlib.h>

#include <iostream>
#include <map>
#include <set>
#include <thread>

int erhandler(Display *dpy, XErrorEvent *er)
{
    char ertext[64];
    XGetErrorText(dpy, er->error_code, ertext, 64);
    std::cout << ertext << "\n";
    return 0;
}

int main(int argc, char const *argv[])
{
    // setup x
    Display *dpy = XOpenDisplay(NULL);
    int screen = DefaultScreen(dpy);
    Window root = RootWindow(dpy, screen);
    XSetErrorHandler(erhandler);

    XSelectInput(dpy, root, SubstructureNotifyMask | StructureNotifyMask);

    std::set<Window> movedWindows;
    std::map<Window, XEvent> lastEvents;

    while (true) {
        XEvent ev;
        XNextEvent(dpy, &ev);

        // on window resize
        if (ev.type == ConfigureNotify) {
            auto win = ev.xconfigure.window;
            std::cout << win << "\n";
            movedWindows.insert(win);
            lastEvents.insert(std::make_pair(win, ev));
            while (XEventsQueued(dpy, QueuedAfterReading)) {
                XNextEvent(dpy, &ev);
                win = ev.xconfigure.window;
                std::cout << win << "\n";
                lastEvents.insert(std::make_pair(win, ev));
                movedWindows.insert(win);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            if (!XEventsQueued(dpy, QueuedAfterReading)) {
                std::cout << "seems like y'all done\n";
                lastEvents.clear();
                movedWindows.clear();
            }
        }
    }

    return 0;
}
